<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/forms/edit_slider_form.php');

$id = optional_param('id', 0, PARAM_INT);

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:manage_slider', $context);

$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->set_url('/local/ecommerce/slider/index.php', array('id' => $id));

if ($id) {
    $slider = $DB->get_record('local_ecommerce_slider', array('id' => $id), '*', MUST_EXIST);
} else {
    // Force there to be only one slider in DB.
    $slider = $DB->get_record_sql('SELECT * FROM {local_ecommerce_slider} LIMIT 1');
    if (!$slider) {
        $slider = new stdClass();
        $slider->id = 0;
    }
}

$attachmentoptions = array('subdirs' => false, 'maxfiles' => 1, 'maxbytes' => $CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>context_system::instance(), 'accepted_types' => array('.jpg', '.gif', '.png'));

if ($slider->id) {
    // Edit existing.
    $slider = file_prepare_standard_filemanager($slider, 'slider_image_1', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_1', $slider->id);
    $slider = file_prepare_standard_filemanager($slider, 'slider_image_2', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_2', $slider->id);
    $slider = file_prepare_standard_filemanager($slider, 'slider_image_3', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_3', $slider->id);
    $slider = file_prepare_standard_filemanager($slider, 'slider_image_4', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_4', $slider->id);
    $slider = file_prepare_standard_filemanager($slider, 'slider_image_5', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_5', $slider->id);
    $strheading = get_string('edit_slider', 'local_ecommerce');
    $returnMessage = get_string('slider_updated', 'local_ecommerce');
    $returnURL = new moodle_url($CFG->wwwroot . '/local/ecommerce/slider/index.php', array('id' => $slider->id));

} else {
    $slider = file_prepare_standard_filemanager($slider, 'slider_image_1', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_1', null);
    $slider = file_prepare_standard_filemanager($slider, 'slider_image_2', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_2', null);
    $slider = file_prepare_standard_filemanager($slider, 'slider_image_3', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_3', null);
    $slider = file_prepare_standard_filemanager($slider, 'slider_image_4', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_4', null);
    $slider = file_prepare_standard_filemanager($slider, 'slider_image_5', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_5', null);
    $strheading = get_string('create_slider', 'local_ecommerce');
    $returnMessage = get_string('slider_created', 'local_ecommerce');
    $returnURL = new moodle_url($CFG->wwwroot . '/local/ecommerce/slider/index.php');
}

$editform = new edit_slider_form(null, array('attachmentoptions' => $attachmentoptions, 'data' => $slider));

if ($editform->is_cancelled()) {
    redirect($returnURL);
} else if ($data = $editform->get_data()) {
    $data->id = \local_ecommerce\slider::save_slider($data);
    if ($data->id) {
        $data = file_postupdate_standard_filemanager($data, 'slider_image_1', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_1', $data->id);
        $data = file_postupdate_standard_filemanager($data, 'slider_image_2', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_2', $data->id);
        $data = file_postupdate_standard_filemanager($data, 'slider_image_3', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_3', $data->id);
        $data = file_postupdate_standard_filemanager($data, 'slider_image_4', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_4', $data->id);
        $data = file_postupdate_standard_filemanager($data, 'slider_image_5', $attachmentoptions, $context, 'local_ecommerce', 'slider_image_5', $data->id);
        $returnURL = new moodle_url($CFG->wwwroot . '/local/ecommerce/slider/index.php', array('id' => $data->id));
    }
    redirect($returnURL, $returnMessage);
}

$renderer = $PAGE->get_renderer('local_ecommerce');
$params = [
        'title' => $strheading,
        'formhtml' => $editform->export_for_template($renderer)
];
$renderable = new \local_ecommerce\output\slider_index($params);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/index.php'));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);

echo $OUTPUT->header();

echo $renderer->render($renderable);
$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();

