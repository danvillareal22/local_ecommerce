<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/tables/reviews_table.php');

$productID = optional_param('productid', 0, PARAM_INT);
$search     = optional_param('search', '', PARAM_TEXT);
$pageNo     = optional_param('page', 0, PARAM_INT);

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:manage_reviews', $context);

$title = get_string('reviews', 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/reviews/index.php', array('search' => $search, 'productid' => $productID));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new reviews_table('reviews_table', $productID, $search);
$renderer = $PAGE->get_renderer('local_ecommerce');

$params = [
        'title' => $title,
        'productid' => $productID,
        'search' => $search,
        'search_reviews_panel' => $renderer->store_print_search_reviews_panel($productID, $search),
        'tablehtml' => $table->export_for_template($renderer)
];

$urlParams = array(
    'productid' => $productID,
    'search' => $search
);

//$renderable = new \local_ecommerce\output\reviews_index($params);

echo $OUTPUT->header();

echo $renderer->print_manage_tabs('reviews');
echo $renderer->store_list_reviews($params);

//echo $OUTPUT->paging_bar($table->totalrows, $pageNo, $table->pagesize, new moodle_url('/local/ecommerce/reviews/index.php', $urlParams));

$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();
