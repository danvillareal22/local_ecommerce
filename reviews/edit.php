<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');

$id = required_param('id', PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$confirm = optional_param('confirm', 0, PARAM_BOOL);

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:manage_reviews', $context);

$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->set_url('/local/ecommerce/reviews/edit.php', array('id' => $id));

$review = \local_ecommerce\review::get_product_review($id);

$returnURL = new moodle_url($CFG->wwwroot . '/local/ecommerce/reviews/index.php');

if ($action == 'delete' && $review->id) {
    $PAGE->url->param('action', 'delete');
    if ($confirm and confirm_sesskey()) {
        \local_ecommerce\review::delete_review($review->id);
        $message = get_string('review_deleted', 'local_ecommerce');
        redirect($returnURL);
    }
    $strheading = get_string('delete_review', 'local_ecommerce');
    $PAGE->navbar->add(get_string('reviews', 'local_ecommerce'), new moodle_url('/local/ecommerce/reviews/index.php'));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $yesurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/reviews/edit.php',
            array('id' => $review->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey()));
    $message = get_string('delete_review_msg', 'local_ecommerce', format_string($review->id));
    echo $OUTPUT->confirm($message, $yesurl, $returnURL);
    echo $OUTPUT->footer();
    die;
}

$strheading = get_string('review', 'local_ecommerce');
$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/index.php'));
$PAGE->navbar->add(get_string('reviews', 'local_ecommerce'), new moodle_url('/local/ecommerce/reviews/index.php'));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);

$renderer = $PAGE->get_renderer('local_ecommerce');

echo $renderer->header();

echo $renderer->store_print_review($review);

echo $renderer->footer();

