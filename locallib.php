<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function local_ecommerce_enable($config = '', $return = false) {
    global $CFG;

    // check if plugin enabled
    if (!get_config('local_ecommerce', 'enabled')) {
        if ($return) return false;
        redirect(new moodle_url('/'), get_string('eCommerceDisabled', 'local_ecommerce'));
    }

    // check if feature enabled
    if (!empty($config) and !get_config('local_ecommerce', $config)) {
        if ($return) return false;
        redirect(new moodle_url('/local/ecommerce/dashboard/index.php'), get_string('featuredisabled', 'local_ecommerce', $config));
    }

    // check access
    if (!\local_ecommerce\settings::check_license()) {
        if ($return) return false;
        redirect(new moodle_url('/'), get_string('ecommercenotavailable', 'local_ecommerce'));
    }

    if ($return) {
        return true;
    }
}

function local_ecommerce_guestaccess() {
    return get_config('local_ecommerce', 'guestaccess');
}

function local_ecommerce_ipn_exception_handler($ex) {
    global $CFG;

    $info = get_exception_info($ex);

    $logerrmsg = "payment IPN exception handler: ".$info->message;
    if (debugging('', DEBUG_NORMAL)) {
        $logerrmsg .= ' Debug: '.$info->debuginfo."\n".format_backtrace($info->backtrace, true);
    }

    $file = $CFG->dirroot."/local/ecommerce/payments/logs/exceptions.log";
    file_put_contents($file, $logerrmsg, FILE_APPEND);

    exit(0);
}

function local_ecommerce_log_payment_error($type, $subject, $data) {
    global $CFG;

    $message = "$type payment IPN ERROR .\n\n$subject\n\n";
    foreach ($data as $key => $value) {
        $message .= "$key => $value\n";
    }

    $file = $CFG->dirroot."/local/ecommerce/payments/logs/errors.log";
    file_put_contents($file, $message, FILE_APPEND);
}