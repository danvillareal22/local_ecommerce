<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:viewreports', $context);

$title = get_string('reports', 'local_ecommerce');
$subtitle = get_string('allreports', 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/reports/index.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/dashboard/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$renderer = $PAGE->get_renderer('local_ecommerce');
$getIncome = \local_ecommerce\report::get_income();
$params = [
        'title' => $title,
        'subtitle' => $subtitle,
        'page-type' => 'reports-index',
        'reports' => \local_ecommerce\report::get_reports_list(),
        'reports-index-box-header1' => get_string('reports-index-box-header1', 'local_ecommerce'),
        'reports-index-box-header2' => get_string('reports-index-box-header2', 'local_ecommerce'),
        'income' => $getIncome,
        'income-currency-code' =>  \local_ecommerce\payment::get_currency('code'),
        'incomegraph' => \local_ecommerce\report::get_income_chart_data(),
        'reports_header' => $renderer->print_basic_header(get_string('reports', 'local_ecommerce'))
];

//$renderable = new \local_ecommerce\output\reports_index($params);

echo $OUTPUT->header();


echo $renderer->print_manage_tabs('reports');
echo $renderer->store_print_report_index($params);

$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();
