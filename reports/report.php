<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');

$id         = optional_param('id', 0, PARAM_INT);
$itemid     = optional_param('itemid', 0, PARAM_INT);
$daterange  = optional_param('daterange', '', PARAM_TEXT);
$search     = optional_param('search', '', PARAM_TEXT);
$download   = optional_param('download', '', PARAM_ALPHA);

require_login();
local_ecommerce_enable();
$report = \local_ecommerce\report::get_report($id);

require($CFG->dirroot . '/local/ecommerce/classes/output/tables/report'.$report->id.'_table.php');

$context = context_system::instance();
require_capability('local/ecommerce:viewreports', $context);

$title = get_string('report'.$id.'title', 'local_ecommerce');
$params = array('daterange'=>$daterange, 'id'=>$id, 'search'=>$search, 'itemid'=>$itemid);
$PAGE->set_url('/local/ecommerce/reports/report.php', $params);
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/dashboard/index.php'));
$PAGE->navbar->add(get_string('reports', 'local_ecommerce'), new moodle_url('/local/ecommerce/reports/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

// process datarange
$daterange = \local_ecommerce\api::get_daterange($daterange);

$params['download'] = $download;
$params['daterange'] = $daterange;
$reportabletname = 'report'.$report->id.'_table';
$table = new $reportabletname($reportabletname, $params);
$table->is_downloading($download, $title, $title);
if ($download) {
    $table->out(20, true);
}

$renderer = $PAGE->get_renderer('local_ecommerce');

$pageHeader = $title;
$title = ($itemid) ? html_writer::link(new moodle_url('/local/ecommerce/reports/report.php', array('id'=>$id)), $title) : $title;
$params = [
    'title' => html_writer::link(new moodle_url('/local/ecommerce/reports/index.php'), get_string('allreports', 'local_ecommerce')).' | '.$title,
    'page-type' => 'report-page '.$reportabletname,
    'search_panel' => $renderer->print_report_filter_panel($id, $daterange, $search, $itemid, $pageHeader),
    'tablehtml' => $table->export_for_template($renderer)
];

//$renderable = new \local_ecommerce\output\report_page($params);

echo $OUTPUT->header();

echo $renderer->print_manage_tabs('reports');
echo $renderer->store_print_reports($params);

$PAGE->requires->js_call_amd('local_ecommerce/ecommerce_salesfilter', 'init',
        array('timestart_date'=>$daterange['timestart_date'], 'timefinish_date'=>$daterange['timefinish_date']));
$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();
