<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce products
 *
 * @package    local_ecommerce
 * @copyright  2018
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');

if (!local_ecommerce_guestaccess()) {
    require_login();
}
local_ecommerce_enable();
$context = context_system::instance();

$search = optional_param('search', '', PARAM_TEXT);
$currentCategory = optional_param('category', 0, PARAM_INT);
$orderBy = optional_param('orderby', '', PARAM_TEXT);
if (empty($orderBy)) {
    $orderBy = 'featured';
}

$title = get_string('store', 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/store.php', array('search' => $search));
$PAGE->set_pagelayout('standard');
$PAGE->blocks->add_region('content');
$PAGE->set_context($context);

$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/local/ecommerce/assets/css/bootstrap-select.min.css'));
//$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$renderer = $PAGE->get_renderer('local_ecommerce');
$itemPerPage = get_config('local_ecommerce', 'default_number_products_per_page');
$page = 0;

$categories = \local_ecommerce\category::get_available_categories();
$slider = \local_ecommerce\slider::get_slider();

echo $renderer->header();
if (!isloggedin() && get_config('local_ecommerce', 'display_company_header')) {
    $logoURL = $renderer->get_logo_url(null, 80);
    $siteName = $SITE->fullname;
    echo $renderer->store_print_company_header($logoURL, $siteName);
}
echo $renderer->store_print_menu('store', isloggedin());
echo $renderer->store_print_nav($categories, $search, $currentCategory, $orderBy, $slider);
if (get_config('local_ecommerce', 'display_virtual_products')) {
    $virtualCategoryName = get_config('local_ecommerce', 'virtual_products_name');
    $virtualProducts = \local_ecommerce\store::get_store_products(0, $search, $orderBy, $itemPerPage, $page, $currentCategory);
    echo $renderer->store_print_products($itemPerPage, $virtualCategoryName, $virtualProducts, $page, 0);
}
if (get_config('local_ecommerce', 'display_physical_products')) {
    $physicalCategoryName = get_config('local_ecommerce', 'physical_products_name');
    $physicalProducts = \local_ecommerce\store::get_store_products(1, $search, $orderBy, $itemPerPage, $page, $currentCategory);
    echo $renderer->store_print_products($itemPerPage, $physicalCategoryName, $physicalProducts, $page, 1);
}

echo $renderer->store_print_checkout_footer();


if ($slider->enableSlider) {
    $PAGE->requires->js_call_amd('local_ecommerce/store', 'initSlider', array($slider->enableSlider));
} else {
    $PAGE->requires->js_call_amd('local_ecommerce/store', 'initContact');
}
$PAGE->requires->js_call_amd('local_ecommerce/ecommerce', 'purchased');
$PAGE->requires->js(new moodle_url("https://platform.linkedin.com/in.js"));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/ecommerce/amd/build/url-search-params.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/local/ecommerce/amd/build/url-search-params-sort.min.js'));

//$PAGE->requires->jquery_plugin( 'bootstrap-select', 'local_ecommerce');
//$PAGE->requires->js(new moodle_url('/local/ecommerce/assets/js/bootstrap-select.min.js'));
echo $renderer->footer();
