<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/forms/edit_coupon_form.php');

$id = optional_param('id', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$confirm = optional_param('confirm', 0, PARAM_BOOL);

require_login();
local_ecommerce_enable('enablecoupons');

$context = context_system::instance();
require_capability('local/ecommerce:editcoupons', $context);

$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->set_url('/local/ecommerce/coupons/edit.php', array('id' => $id));

if ($id) {
    $coupon = $DB->get_record('local_ecommerce_coupons', array('id' => $id), '*', MUST_EXIST);
} else {
    $coupon = new stdClass();
    $coupon->id = 0;
}

$returnurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/coupons/index.php');

if ($action == 'delete' and $coupon->id) {
    $PAGE->url->param('action', 'delete');
    if ($confirm and confirm_sesskey()) {
        \local_ecommerce\coupon::delete_coupon($coupon->id);
        redirect($returnurl);
    }
    $strheading = get_string('deletecoupons', 'local_ecommerce');
    $PAGE->navbar->add(get_string('coupons', 'local_ecommerce'), new moodle_url('/local/ecommerce/coupons/index.php'));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    $renderer = $PAGE->get_renderer('local_ecommerce');
    $params = [
        'title' => $strheading
    ];

    $renderable = new \local_ecommerce\output\coupons_delete($params);

    echo $renderer->render($renderable);

    $yesurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/coupons/edit.php',
            array('id' => $coupon->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey()));
    $message = get_string('deletecouponmsg', 'local_ecommerce');
    echo $OUTPUT->confirm($message, $yesurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}

if ($action == 'show' && $coupon->id && confirm_sesskey()) {
    if (!$coupon->status) {
        $record = (object) array('id' => $coupon->id, 'status' => 1);
        \local_ecommerce\coupon::save_coupon($record);
    }
    redirect($returnurl);

} else if ($action == 'hide' && $coupon->id && confirm_sesskey()) {

    if ($coupon->status) {
        $record = (object) array('id' => $coupon->id, 'status' => 0);
        \local_ecommerce\coupon::save_coupon($record);
    }
    redirect($returnurl);
}

if ($coupon->id) {
    $strheading = get_string('editcoupon', 'local_ecommerce');
} else {
    $strheading = get_string('createcoupon', 'local_ecommerce');
}

$editform = new edit_coupon_form(null, array('data' => $coupon));

if ($editform->is_cancelled()) {

    redirect($returnurl);

} else if ($data = $editform->get_data()) {

    $data->id = \local_ecommerce\coupon::save_coupon($data);
    redirect($returnurl);
}

$renderer = $PAGE->get_renderer('local_ecommerce');
$params = [
        'title' => $strheading,
        'formhtml' => $editform->export_for_template($renderer)
];
$renderable = new \local_ecommerce\output\coupons_edit($params);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/index.php'));
$PAGE->navbar->add(get_string('coupons', 'local_ecommerce'), new moodle_url('/local/ecommerce/coupons/index.php'));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);

echo $OUTPUT->header();

echo $renderer->render($renderable);
$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();

