<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * local_ecommerce API
 *
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("$CFG->libdir/externallib.php");

/**
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_ecommerce_external extends external_api {

    // assign products autocompletion
    public static function get_assignable_products_parameters() {
        return new external_function_parameters(
                array(
                        'query' => new external_value(PARAM_TEXT),
                        'instanceid' => new external_value(PARAM_INT),
                        'type' => new external_value(PARAM_TEXT)
                )
        );
    }

    public static function get_assignable_products($query, $instanceid, $type) {

        $params = self::validate_parameters(
                self::get_assignable_products_parameters(),
                array(
                        'query' => $query,
                        'instanceid' => $instanceid,
                        'type' => $type
                )
        );

        return array('resp' => \local_ecommerce\api::get_assignable_products($params['query'], $params['instanceid'], $params['type']));
    }

    public static function get_assignable_products_returns() {
        return new external_single_structure(
                array(
                        'resp' => new external_value(PARAM_TEXT, 'resp'),
                )
        );
    }

    // add to cart
    public static function add_to_cart_parameters() {
        return new external_function_parameters(
                array(
                        'productid' => new external_value(PARAM_INT, 'product id')
                )
        );
    }

    public static function add_to_cart($productid) {

        $params = self::validate_parameters(
                self::add_to_cart_parameters(),
                array(
                        'productid' => $productid
                )
        );

        $productid = $params['productid'];
        return \local_ecommerce\api::add_to_cart($productid);
    }

    public static function add_to_cart_returns() {
        return new external_single_structure(
                array(
                        'message' => new external_value(PARAM_TEXT, 'message'),
                        'status' => new external_value(PARAM_TEXT, 'status'),
                        'productscount' => new external_value(PARAM_INT, 'ecommerce products'),
                        'wishlistcount' => new external_value(PARAM_INT, 'ecommerce products')
                )
        );
    }

    // add to waitlist
    public static function add_to_waitlist_parameters() {
        return new external_function_parameters(
                array(
                        'productid' => new external_value(PARAM_INT, 'product id')
                )
        );
    }

    public static function add_to_waitlist($productid) {

        $params = self::validate_parameters(
                self::add_to_waitlist_parameters(),
                array(
                        'productid' => $productid
                )
        );

        $productid = $params['productid'];
        return \local_ecommerce\api::add_to_waitlist($productid);
    }

    public static function add_to_waitlist_returns() {
        return new external_single_structure(
                array(
                        'message' => new external_value(PARAM_TEXT, 'message'),
                        'productscount' => new external_value(PARAM_INT, 'ecommerce products'),
                )
        );
    }

    /**
     * Get popup ecommerce products parameters  description.
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function get_ecommerce_products_parameters() {
        return new external_function_parameters(
                array(
                        'userid' => new external_value(PARAM_INT, 'the user id', VALUE_REQUIRED),
                )
        );
    }

    /**
     * Get products function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @return external_description
     */
    public static function get_ecommerce_products($userid) {
        global $USER, $PAGE;

        $params = self::validate_parameters(
                self::get_ecommerce_products_parameters(),
                array(
                        'userid' => $userid,
                )
        );

        $context = context_system::instance();
        self::validate_context($context);

        $userid = $params['userid'];

        if (!empty($userid)) {
            if (core_user::is_real_user($userid)) {
                $user = core_user::get_user($userid, '*', MUST_EXIST);
            } else {
                throw new moodle_exception('invaliduser');
            }
        }

        if ($userid != $USER->id) {
            throw new moodle_exception('accessdenied', 'admin');
        }

        if (!isloggedin()) {
            throw new moodle_exception('invaliduser');
        }

        $renderer = $PAGE->get_renderer('local_ecommerce');

        $products = \local_ecommerce\checkout::get_products_in_cart();

        $productscontexts = [];
        $checkouturl = new \moodle_url('/local/ecommerce/index.php');
        $waitlisturl = new \moodle_url('/local/ecommerce/waitlist.php');
        $currency = \local_ecommerce\payment::get_currency();

        if ($products) {

            $coupons = \local_ecommerce\coupon::get_applied_coupons();
            $discounts = \local_ecommerce\checkout::get_products_discounts($products);
            $discountprices = \local_ecommerce\checkout::get_checkout_products_prices($products, $coupons, $discounts);
            $total = \local_ecommerce\checkout::get_cart_total($products, $discountprices);

            foreach ($products as $product) {

                $product->discountprice = \local_ecommerce\checkout::get_product_price($product, $coupons, $discounts);
                $productoutput = new \local_ecommerce\output\ecommerce_product($product);

                $productcontexts = $productoutput->export_for_template($renderer);
                $productscontexts[] = $productcontexts;
            }
        }

        return array(
                'products' => $productscontexts,
                'isproducts' => (count($productscontexts)),
                'productscount' => \local_ecommerce\api::count_ecommerce_products(),
                'subtotal' => (isset($total->subtotal)) ? $total->subtotal : 0,
                'discount' => (isset($total->discount)) ? $total->discount : 0,
                'total' => (isset($total->total)) ? $total->total : 0,
                'checkouturl' => $checkouturl->out(),
                'currency' => $currency,
                'enablewaitlist' => !empty(get_config('local_ecommerce', 'enablewaitlist')) ? 1 : 0,
                'waitlisturl' => $waitlisturl->out(),
        );

    }

    /**
     * Get announcements return description.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function get_ecommerce_products_returns() {
        return new external_single_structure(
                array(
                        'products' => new external_multiple_structure(
                                new external_single_structure(
                                        array(
                                                'id' => new external_value(PARAM_INT, 'Announcement id (this is not guaranteed to be unique within this result set)'),
                                                'name' => new external_value(PARAM_TEXT, 'The product name'),
                                                'idnumber' => new external_value(PARAM_TEXT, 'The product idnumber'),
                                                'price' => new external_value(PARAM_TEXT, 'The product price'),
                                                'discountprice' => new external_value(PARAM_TEXT, 'The product discount price'),
                                                'currency' => new external_value(PARAM_TEXT, 'The product currency'),
                                                'imageurl' => new external_value(PARAM_URL, 'URL for product image'),
                                                'viewurl' => new external_value(PARAM_URL, 'URL for product view'),
                                        ), 'product item'
                                )
                        ),
                        'checkouturl' => new external_value(PARAM_URL, 'the number of ecommerce products'),
                        'productscount' => new external_value(PARAM_INT, 'the number of ecommerce products'),
                        'subtotal' => new external_value(PARAM_TEXT, 'total price without discount'),
                        'discount' => new external_value(PARAM_TEXT, 'discount'),
                        'total' => new external_value(PARAM_TEXT, 'total price with discount'),
                        'currency' => new external_value(PARAM_TEXT, 'currency'),
                        'enablewaitlist' => new external_value(PARAM_BOOL, 'enablewaitlist'),
                        'waitlisturl' => new external_value(PARAM_URL, 'waitlisturl'),
                )
        );
    }

    /**
     * eCommerce product count.
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function get_ecommerce_products_count_parameters() {
        return new external_function_parameters(
                array(
                        'userid' => new external_value(PARAM_INT, 'the user id', VALUE_REQUIRED),
                )
        );
    }

    /**
     * Get products count function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @return external_description
     */
    public static function get_ecommerce_products_count($userid) {
        global $USER;

        $params = self::validate_parameters(
                self::get_ecommerce_products_count_parameters(),
                array('userid' => $userid)
        );

        $context = context_system::instance();
        self::validate_context($context);

        $userid = $params['userid'];

        if (!empty($userid)) {
            if (core_user::is_real_user($userid)) {
                $user = core_user::get_user($userid, '*', MUST_EXIST);
            } else {
                throw new moodle_exception('invaliduser');
            }
        }

        if ($userid != $USER->id) {
            throw new moodle_exception('accessdenied', 'admin');
        }

        if (!isloggedin()) {
            throw new moodle_exception('invaliduser');
        }

        return \local_ecommerce\api::count_ecommerce_products();
    }

    /**
     * Get ecommerce products count return description.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function get_ecommerce_products_count_returns() {
        return new external_value(PARAM_INT, 'The count of products in ecommerce');
    }

    /**
     * Delete product description.
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function delete_ecommerce_product_parameters() {
        return new external_function_parameters(
                array(
                        'productid' => new external_value(PARAM_INT, 'the product id', VALUE_REQUIRED),
                )
        );
    }

    /**
     * delete product function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @param  int      $productid       the product id
     * @return external_description
     */
    public static function delete_ecommerce_product($productid) {
        $params = self::validate_parameters(
                self::delete_ecommerce_product_parameters(),
                array('productid' => $productid)
        );
        \local_ecommerce\checkout::delete_from_cart($productid);

        return array(
                'status' => 1
        );
    }

    /**
     * delete product return description.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function delete_ecommerce_product_returns() {
        return new external_function_parameters(
                array(
                        'status' => new external_value(PARAM_BOOL, 'action status', VALUE_REQUIRED),
                )
        );
    }

    /**
     * Approve invoice.
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function approve_invoice_parameters() {
        return new external_function_parameters(
                array(
                        'id' => new external_value(PARAM_INT, 'the invoice id', VALUE_REQUIRED),
                )
        );
    }

    /**
     * delete product function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @param  int      $id       the invoice id
     * @return external_description
     */
    public static function approve_invoice($id) {
        $params = self::validate_parameters(
                self::approve_invoice_parameters(),
                array('id' => $id)
        );
        \local_ecommerce\invoices::approve_invoice($id);

        return array(
                'status' => 1
        );
    }

    /**
     * delete product return description.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function approve_invoice_returns() {
        return new external_function_parameters(
            array(
                'status' => new external_value(PARAM_BOOL, 'action status', VALUE_REQUIRED),
            )
        );
    }

    public static function get_purchases_autocomplete_parameters() {
        return new external_function_parameters(
            array(
                'query' => new external_value(PARAM_TEXT, '', false),
                'daterange' => new external_value(PARAM_TEXT, '', false)
            )
        );
    }

    public static function get_purchases_autocomplete($query, $daterange = null) {

        $params = self::validate_parameters(
            self::get_purchases_autocomplete_parameters(),
            array(
                'query' => $query,
                'daterange' => $daterange
            )
        );

        return array('resp' => \local_ecommerce\api::get_purchases_autocomplete($params['query'], $params['daterange']));
    }

    public static function get_purchases_autocomplete_returns() {
        return new external_single_structure(
            array(
                'resp' => new external_value(PARAM_TEXT, 'resp'),
            )
        );
    }

    public static function get_store_autocomplete_parameters() {
        return new external_function_parameters(
            array(
                'query' => new external_value(PARAM_TEXT, '', false),
                'category' => new external_value(PARAM_TEXT, '', false),
                'instanceid' => new external_value(PARAM_TEXT, '', false)
            )
        );
    }

    public static function get_store_autocomplete($query, $category, $instanceID = null) {

        $params = self::validate_parameters(
            self::get_store_autocomplete_parameters(),
            array(
                'query' => $query,
                'category' => $category,
                'instanceid' => $instanceID
            )
        );

        return array('resp' => \local_ecommerce\api::get_store_autocomplete($params['query'], $params['category'], $params['instanceid']));
    }

    public static function get_store_autocomplete_returns() {
        return new external_single_structure(
            array(
                'resp' => new external_value(PARAM_TEXT, 'resp'),
            )
        );
    }

    public static function add_to_wishlist_parameters() {
        return new external_function_parameters(
            array(
                'productid' => new external_value(PARAM_INT, 'product id')
            )
        );
    }

    public static function add_to_wishlist($productid) {

        $params = self::validate_parameters(
            self::add_to_wishlist_parameters(),
            array(
                'productid' => $productid
            )
        );

        $productid = $params['productid'];
        return \local_ecommerce\api::add_to_wishlist($productid);
    }

    public static function add_to_wishlist_returns() {
        return new external_single_structure(
            array(
                'message' => new external_value(PARAM_TEXT, 'message'),
                'productscount' => new external_value(PARAM_INT, 'ecommerce products'),
            )
        );
    }

    public static function get_wishlist_parameters() {
        return new external_function_parameters(
            array(
//                'productid' => new external_value(PARAM_INT, 'product id')
            )
        );
    }

    public static function get_wishlist() {
        global $USER, $PAGE;

        $params = self::validate_parameters(
            self::get_wishlist_parameters(),
            array(
//                'productid' => $productid
            )
        );

        $context = context_system::instance();
        self::validate_context($context);

        $userid = $USER->id;

        if (!empty($userid)) {
            if (!core_user::is_real_user($userid)) {
                throw new moodle_exception('invaliduser');
            }
        }

        if (!isloggedin()) {
            throw new moodle_exception('invaliduser');
        }

        $renderer = $PAGE->get_renderer('local_ecommerce');

        $wishlist = \local_ecommerce\wishlist::get_wishlist();

        $productscontexts = [];
        $checkouturl = new \moodle_url('/local/ecommerce/index.php');
        $waitlisturl = new \moodle_url('/local/ecommerce/waitlist.php');
        $currency = \local_ecommerce\payment::get_currency();

        if ($wishlist) {

            $coupons = \local_ecommerce\coupon::get_applied_coupons();
            $discounts = \local_ecommerce\checkout::get_products_discounts($wishlist);

            foreach ($wishlist as $product) {

                $product->discountprice = \local_ecommerce\checkout::get_product_price($product, $coupons, $discounts);
                $productoutput = new \local_ecommerce\output\ecommerce_wishlist_item($product);

                $productcontexts = $productoutput->export_for_template($renderer);
                $productscontexts[] = $productcontexts;
            }
        }

        return array(
            'products' => $productscontexts,
            'isproducts' => (count($productscontexts)),
            'productscount' => count($wishlist),
            'checkouturl' => $checkouturl->out(),
            'currency' => $currency,
            'enablewaitlist' => !empty(get_config('local_ecommerce', 'enablewaitlist')) ? 1 : 0,
            'waitlisturl' => $waitlisturl->out(),
        );

    }

    /**
     * Get announcements return description.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function get_wishlist_returns() {
        return new external_single_structure(
            array(
                'products' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'Announcement id (this is not guaranteed to be unique within this result set)'),
                            'name' => new external_value(PARAM_TEXT, 'The product name'),
                            'idnumber' => new external_value(PARAM_TEXT, 'The product idnumber'),
                            'price' => new external_value(PARAM_TEXT, 'The product price'),
                            'discountprice' => new external_value(PARAM_TEXT, 'The product discount price'),
                            'currency' => new external_value(PARAM_TEXT, 'The product currency'),
                            'imageurl' => new external_value(PARAM_URL, 'URL for product image'),
                            'viewurl' => new external_value(PARAM_URL, 'URL for product view'),
                            'seatsallow' => new external_value(PARAM_BOOL, 'URL for product view'),
                            'enablewaitlist' => new external_value(PARAM_BOOL, 'enablewaitlist')
                        ), 'product item'
                    )
                ),
                'checkouturl' => new external_value(PARAM_URL, 'the number of ecommerce products'),
                'productscount' => new external_value(PARAM_INT, 'the number of ecommerce products'),
                'currency' => new external_value(PARAM_TEXT, 'currency'),
                'enablewaitlist' => new external_value(PARAM_BOOL, 'enablewaitlist'),
                'waitlisturl' => new external_value(PARAM_URL, 'waitlisturl'),
            )
        );
    }

    public static function get_wishlist_products_count_parameters() {
        return new external_function_parameters(
            array(
//                'productid' => new external_value(PARAM_INT, 'product id')
            )
        );
    }

    public static function get_wishlist_products_count() {

        $params = self::validate_parameters(
            self::get_wishlist_products_count_parameters(),
            array(
//                'productid' => $productid
            )
        );

        return \local_ecommerce\api::count_wishlist_products();
    }

    public static function get_wishlist_products_count_returns() {
        return new external_value(PARAM_INT, 'The count of products in ecommerce');
    }

    public static function delete_wishlist_item_parameters() {
        return new external_function_parameters(
            array(
                'productid' => new external_value(PARAM_INT, 'product id')
            )
        );
    }

    public static function delete_wishlist_item($productid) {

        $params = self::validate_parameters(
            self::delete_wishlist_item_parameters(),
            array(
                'productid' => $productid
            )
        );

        $productid = $params['productid'];
        \local_ecommerce\wishlist::delete_wishlist_item($productid);

        return array(
            'status' => 1
        );
    }

    public static function delete_wishlist_item_returns() {
        return new external_function_parameters(
            array(
                'status' => new external_value(PARAM_BOOL, 'action status', VALUE_REQUIRED),
            )
        );
    }

    public static function rate_product_parameters() {
        return new external_function_parameters(
            array(
                'productid' => new external_value(PARAM_INT, 'product id'),
                'rating' => new external_value(PARAM_INT, 'rating'),
                'review' => new external_value(PARAM_TEXT, 'review')
            )
        );
    }

    public static function rate_product($productid, $rating, $review) {

        $params = self::validate_parameters(
            self::rate_product_parameters(),
            array(
                'productid' => $productid,
                'rating' => $rating,
                'review' => $review
            )
        );

        $productid = $params['productid'];
        $rating = $params['rating'];
        $review = $params['review'];
        return \local_ecommerce\api::rate_product($productid, $rating, $review);
    }

    public static function rate_product_returns() {
        return new external_single_structure(
            array(
                'message' => new external_value(PARAM_TEXT, 'message'),
                'newRating' => new external_value(PARAM_TEXT, 'new rating'),
                'newRatingUsers' => new external_value(PARAM_TEXT, 'number of raters')
            )
        );
    }

    public static function load_more_reviews_parameters() {
        return new external_function_parameters(
            array(
                'productid' => new external_value(PARAM_INT, 'product id'),
                'page' => new external_value(PARAM_INT, 'rating')
            )
        );
    }

    public static function load_more_reviews($productid, $page) {

        $params = self::validate_parameters(
            self::load_more_reviews_parameters(),
            array(
                'productid' => $productid,
                'page' => $page
            )
        );

        $productid = $params['productid'];
        $page = $params['page'];
        return array('reviews' => \local_ecommerce\review::load_more_reviews($productid, $page));
    }

    public static function load_more_reviews_returns() {
        return new external_single_structure(
            array(
                'reviews' => new external_value(PARAM_RAW, 'Review template')
            )
        );
    }

    public static function load_more_products_parameters() {
        return new external_function_parameters(
            array(
                'type' => new external_value(PARAM_INT, 'virtual (0), physical (1), or both (2)'),
                'page' => new external_value(PARAM_INT, 'page'),
                'limit' => new external_value(PARAM_INT, 'limit'),
                'search' => new external_value(PARAM_TEXT, 'search term', false),
                'category' => new external_value(PARAM_INT, 'category id', false),
                'orderBy' => new external_value(PARAM_TEXT, 'order by', false)
            )
        );
    }

    public static function load_more_products($type, $page, $limit, $search, $category, $orderBy) {

        $params = self::validate_parameters(
            self::load_more_products_parameters(),
            array(
                'type' => $type,
                'page' => $page,
                'limit' => $limit,
                'search' => $search,
                'category' => $category,
                'orderBy' => $orderBy
            )
        );

        $type = $params['type'];
        $page = $params['page'];
        $limit = $params['limit'];
        $search = $params['search'];
        $category = $params['category'];
        $orderBy = $params['orderBy'];
        return array('products' => \local_ecommerce\store::load_more_products($type, $page, $limit, $search, $category, $orderBy));
    }

    public static function load_more_products_returns() {
        return new external_single_structure(
            array(
                'products' => new external_value(PARAM_RAW, 'Products template')
            )
        );
    }
}