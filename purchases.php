<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require($CFG->dirroot . '/local/ecommerce/lib.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/tables/purchases_table.php');

require_login();

local_ecommerce_enable();
$context = context_system::instance();

$daterange  = optional_param('daterange', '', PARAM_TEXT);
$search     = optional_param('search', '', PARAM_TEXT);
$download   = optional_param('download', '', PARAM_ALPHA);
$pageNo     = optional_param('page', 0, PARAM_INT);

$title = get_string('purchases', 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/purchases.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$params = array('search' => $search, 'download' => $download);
$daterange = \local_ecommerce\api::get_daterange($daterange);
$params['daterange'] = $daterange;

$table = new purchases_table('purchases_table', $params);
$renderer = $PAGE->get_renderer('local_ecommerce');

$table->is_downloading($download, $title, $title);
if ($download) {
    $table->out(20, true);
}

$outputParams = [
    'title' => get_string('purchases', 'local_ecommerce'),
    'search_panel' => $renderer->store_print_purchases_filter_panel($daterange, $search),
    'tablehtml' => $table->export_for_template($renderer)
];

echo $OUTPUT->header();
echo $renderer->store_print_menu('purchases');
echo $renderer->store_print_purchases($outputParams);

echo $renderer->store_print_checkout_footer();

$urlParams = $params;
$urlParams['daterange'] = $daterange['daterange'];

//echo $OUTPUT->paging_bar($table->totalrows, $pageNo, $table->pagesize, new moodle_url('/local/ecommerce/purchases.php', $urlParams));

$PAGE->requires->js_call_amd('local_ecommerce/ecommerce_salesfilter', 'init', array(
        'timestart_date' => $daterange['timestart_date'],
        'timefinish_date'=>$daterange['timefinish_date']
    )
);
$PAGE->requires->js_call_amd('local_ecommerce/store', 'init');

echo $OUTPUT->footer();
