<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2018
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/tables/customers_table.php');

$daterange  = optional_param('daterange', '', PARAM_TEXT);
$search     = optional_param('search', '', PARAM_TEXT);
$download   = optional_param('download', '', PARAM_ALPHA);

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:viewsales', $context);

$title = get_string('customers', 'local_ecommerce');
$subtitle = get_string('customers', 'local_ecommerce');
$params = array('daterange'=>$daterange, 'search'=>$search);
$PAGE->set_url('/local/ecommerce/sales/customers.php', $params);
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/dashboard/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

// process datarange
$daterange = \local_ecommerce\api::get_daterange($daterange);

$params['download'] = $download;
$params['daterange'] = $daterange;
$table = new customers_table('customers_table', $params);
$table->is_downloading($download, $title, $title);
if ($download) {
    $table->out(20, true);
}

$renderer = $PAGE->get_renderer('local_ecommerce');
$params = [
        'title' => $subtitle,
        'search_panel' => $renderer->print_sales_filter_panel('', $daterange, $search, 'customers'),
        'salestabs' => $renderer->print_sales_tabs('customers'),
        'page-type' => 'customers-index',
        'tablehtml' => $table->export_for_template($renderer)
];

$renderable = new \local_ecommerce\output\sales_index($params);

echo $OUTPUT->header();

echo $renderer->print_manage_tabs('customers');
echo $renderer->store_list_sales($params);

$PAGE->requires->js_call_amd('local_ecommerce/ecommerce_salesfilter', 'init',
        array('timestart_date'=>$daterange['timestart_date'], 'timefinish_date'=>$daterange['timefinish_date']));
$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();
