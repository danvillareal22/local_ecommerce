<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');

$id         = optional_param('id', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_TEXT);

require_login();
local_ecommerce_enable();
confirm_sesskey();

$context = context_system::instance();
$params = array('id'=>$id, 'action'=>$action);
$PAGE->set_url('/local/ecommerce/sales/actions.php', $params);
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

if ($action == 'print') {

    if (!$checkout = $DB->get_record('local_ecommerce_checkout', array('id'=>$id))) {
        print_error(get_string('wrong-id', 'local_ecommerce'));
    }

    if (!$checkout->invoicepayment) {
        $checkout->invoicepayment = 1;
        $DB->update_record('local_ecommerce_checkout', $checkout);

        \local_ecommerce\notification::send_checkout_notification($checkout, true);
        \local_ecommerce\notification::send_invoicepayment_notification($checkout);
    }

    \local_ecommerce\invoices::print_invoice($checkout);

    exit;
} else if ($action == 'mail') {

    if (!$checkout = $DB->get_record('local_ecommerce_checkout', array('id'=>$id))) {
        print_error(get_string('wrong-id', 'local_ecommerce'));
    }

    \local_ecommerce\notification::send_checkout_notification($checkout, true);
    redirect(new \moodle_url('/local/ecommerce/sales/invoices.php'));
    exit;
}
