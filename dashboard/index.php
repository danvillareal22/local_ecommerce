<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot.'/local/ecommerce/classes/output/tables/latest_orders_table.php');

$daterange  = optional_param('daterange', '', PARAM_TEXT);

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:dashboardview', $context);

$params = array('daterange'=>$daterange);

$title = get_string('dashboard', 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/dashboard/index.php', $params);
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

// process datarange
$daterange = \local_ecommerce\api::get_daterange($daterange);
$table = new latest_orders_table('latest_orders_table', $daterange);

$renderer = $PAGE->get_renderer('local_ecommerce');
$params = new stdClass();
$params->title = $title;
$params->data = \local_ecommerce\report::get_dashboard_data($daterange);
$params->daterange = $daterange;
$params->enablediscounts = (get_config('local_ecommerce', 'enablediscounts'));
$params->enablecoupons = (get_config('local_ecommerce', 'enablecoupons'));
$params->enablewaitlist = (get_config('local_ecommerce', 'enablewaitlist'));
$params->reporttable = $table->export_for_template($renderer, 10);
$params->{'page-type'} = 'dashboard';

//$renderable = new \local_ecommerce\output\dashboard($params);

echo $OUTPUT->header();

echo $renderer->print_manage_tabs('dashboard');
echo $renderer->store_print_dashboard($params);

$PAGE->requires->js_call_amd('local_ecommerce/ecommerce_daterangefilter', 'init',
        array('timestart_date'=>$daterange['timestart_date'], 'timefinish_date'=>$daterange['timefinish_date']));
$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();
