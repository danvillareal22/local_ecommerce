<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/forms/edit_discount_form.php');

$id = optional_param('id', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$confirm = optional_param('confirm', 0, PARAM_BOOL);

require_login();
local_ecommerce_enable('enablediscounts');

$context = context_system::instance();
require_capability('local/ecommerce:editdiscount', $context);

$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->set_url('/local/ecommerce/discounts/edit.php', array('id' => $id));

if ($id) {
    $discount = $DB->get_record('local_ecommerce_discounts', array('id' => $id), '*', MUST_EXIST);
} else {
    $discount = new stdClass();
    $discount->id = 0;
}

$returnurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/discounts/index.php');

if ($action == 'delete' and $discount->id) {
    $PAGE->url->param('action', 'delete');
    if ($confirm and confirm_sesskey()) {
        \local_ecommerce\discount::delete_discount($discount->id);
        redirect($returnurl);
    }
    $strheading = get_string('deletediscount', 'local_ecommerce');
    $PAGE->navbar->add(get_string('discounts', 'local_ecommerce'), new moodle_url('/local/ecommerce/discounts/index.php'));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    $renderer = $PAGE->get_renderer('local_ecommerce');
    $params = [
        'title' => $strheading
    ];

    $renderable = new \local_ecommerce\output\discounts_delete($params);

    echo $renderer->render($renderable);

    $yesurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/discounts/edit.php',
            array('id' => $discount->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey()));
    $message = get_string('deletediscountmsg', 'local_ecommerce');
    echo $OUTPUT->confirm($message, $yesurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}

if ($action == 'show' && $discount->id && confirm_sesskey()) {
    if (!$discount->status) {
        $record = (object) array('id' => $discount->id, 'status' => 1);
        \local_ecommerce\discount::save_discount($record);
    }
    redirect($returnurl);

} else if ($action == 'hide' && $discount->id && confirm_sesskey()) {

    if ($discount->status) {
        $record = (object) array('id' => $discount->id, 'status' => 0);
        \local_ecommerce\discount::save_discount($record);
    }
    redirect($returnurl);
}

if ($discount->id) {
    $strheading = get_string('editdiscount', 'local_ecommerce');
} else {
    $strheading = get_string('creatediscount', 'local_ecommerce');
}

$editform = new edit_discount_form(null, array('data' => $discount));

if ($editform->is_cancelled()) {

    redirect($returnurl);

} else if ($data = $editform->get_data()) {

    $data->id = \local_ecommerce\discount::save_discount($data);
    redirect($returnurl);
}

$renderer = $PAGE->get_renderer('local_ecommerce');
$params = [
        'title' => $strheading,
        'formhtml' => $editform->export_for_template($renderer)
];
$renderable = new \local_ecommerce\output\discounts_edit($params);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/index.php'));
$PAGE->navbar->add(get_string('discounts', 'local_ecommerce'), new moodle_url('/local/ecommerce/discounts/index.php'));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);

echo $OUTPUT->header();

echo $renderer->render($renderable);
$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();

