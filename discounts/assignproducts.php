<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot.'/local/ecommerce/classes/output/forms/assignproducts_form.php');
require($CFG->dirroot.'/local/ecommerce/classes/output/tables/assigned_products_table.php');

$id = required_param('id', PARAM_INT);
$delete    = optional_param('delete', 0, PARAM_BOOL);
$productid  = optional_param('productid', 0, PARAM_INT);

require_login();
local_ecommerce_enable('enablediscounts');
$context = context_system::instance();
require_capability('local/ecommerce:assign', $context);

$returnurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/discounts/index.php');
$discount = $DB->get_record('local_ecommerce_discounts', array('id'=>$id));
if (!$discount->id) {
    redirect($returnurl);
}

$title = get_string("assignproducts", 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/discounts/assignproducts.php', array('id'=>$id));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/discounts/index.php'));
$PAGE->navbar->add(get_string('discounts', 'local_ecommerce'), new moodle_url('/local/ecommerce/discounts/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

if (optional_param('cancel', false, PARAM_BOOL)) {
    redirect($returnurl);
}

$editform = new assignproducts_form(null, array('data'=>$discount, 'type'=> 'discount'));

if ($delete and $discount->id and $productid) {
    \local_ecommerce\product::delete_assigned_products($discount->id, $productid, 'discount');
    redirect($PAGE->url);
}
if ($editform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $editform->get_data()) {
    \local_ecommerce\product::save_assigned_products($data, 'discount');
    redirect($PAGE->url);
}

$table = new assigned_products_table('assigned_products_table', $discount, 'discount');
$renderer = $PAGE->get_renderer('local_ecommerce');
$strheading = get_string("assignproductsfordiscount", 'local_ecommerce', format_string($discount->name));
$discountsURL = new moodle_url('/local/ecommerce/discounts/index.php');
$params = [
    'title' => $strheading,
    'page-type' => 'products-assign-products',
    'menuparent' => 'discounts',
    'formhtml' => $editform->export_for_template($renderer),
    'tablehtml' => $table->export_for_template($renderer),
    'products_header' => $renderer->print_basic_header($strheading),
    'breadcrumbs' => $renderer->store_print_admin_breadcrumbs($discountsURL, get_string('discounts', 'local_ecommerce'), get_string('assignproducts', 'local_ecommerce'))
];
$renderable = new \local_ecommerce\output\assign_products($params);

echo $OUTPUT->header();

echo $renderer->render($renderable);
$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();