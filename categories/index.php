<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/tables/categories_table.php');

$parentid   = optional_param('parentid', 0, PARAM_INT);
$view       = optional_param('view', 0, PARAM_INT);
$search     = optional_param('search', '', PARAM_TEXT);
$pageNo     = optional_param('page', 0, PARAM_INT);

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:managecategories', $context);

$filter = get_user_preferences('ecommerce_categories_filter', 3);
if ($filter != $view and $view > 0) {
    set_user_preference('ecommerce_categories_filter', $view);
    $filter = $view;
}

$title = get_string('categories', 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/categories/index.php', array('view' => $filter, 'search' => $search, 'parentid' => $parentid));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new categories_table('categories_table', $parentid, $search, $filter);
$renderer = $PAGE->get_renderer('local_ecommerce');

$params = [
    'title' => $title,
    'parentid' => $parentid,
    'search' => $search,
    'filter' => $filter,
    'search_category_panel' => $renderer->print_search_category_panel($parentid, $search, $filter),
    'tablehtml' => $table->export_for_template($renderer)
];

$urlParams = array(
    'parentid' => $parentid,
    'search' => $search,
    'view' => $view
);

//$renderable = new \local_ecommerce\output\categories_index($params);

echo $OUTPUT->header();
echo $renderer->print_manage_tabs('categories');
echo $renderer->store_print_categories($params);

//echo $OUTPUT->paging_bar($table->totalrows, $pageNo, $table->pagesize, new moodle_url('/local/ecommerce/categories/index.php', $urlParams));

$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');
//echo $renderer->render($renderable);

echo $OUTPUT->footer();
