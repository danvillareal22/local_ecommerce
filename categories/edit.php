<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/forms/edit_category_form.php');

$id = optional_param('id', 0, PARAM_INT);
$parentid = optional_param('parentid', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$confirm = optional_param('confirm', 0, PARAM_BOOL);
$move = optional_param('move', '', PARAM_TEXT);

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:editcategory', $context);

$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->set_url('/local/ecommerce/index.php', array('id' => $id, 'parentid' => $parentid));

if ($id) {
    $category = $DB->get_record('local_ecommerce_cat', array('id' => $id), '*', MUST_EXIST);
    $category->descriptionformat = 1;
} else {
    $category = new stdClass();
    $category->id = 0;
    $category->name = '';
    $category->description = '';
    $category->descriptionformat = 1;
}

$returnurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/categories/index.php', array('parentid' => $parentid));

if ($action == 'delete' and $category->id) {
    $PAGE->url->param('action', 'delete');
    if ($confirm and confirm_sesskey()) {
        \local_ecommerce\category::delete_category($category->id);
        redirect($returnurl);
    }
    $strheading = get_string('deletecategory', 'local_ecommerce');
    $PAGE->navbar->add(get_string('categories', 'local_ecommerce'), new moodle_url('/local/ecommerce/categories/index.php'));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    $renderer = $PAGE->get_renderer('local_ecommerce');
    $params = [
        'title' => $strheading
    ];

    $renderable = new \local_ecommerce\output\categories_delete($params);

    echo $renderer->render($renderable);

    $yesurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/categories/edit.php',
            array('id' => $category->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey()));
    $message = get_string('deletecategorymsg', 'local_ecommerce', format_string($category->name));
    echo $OUTPUT->confirm($message, $yesurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}

if ($action == 'show' && $category->id && confirm_sesskey()) {
    if (!$category->visible) {
        $record = (object) array('id' => $category->id, 'visible' => 1);
        \local_ecommerce\category::save_category($record);
    }
    redirect($returnurl);

} else if ($action == 'hide' && $category->id && confirm_sesskey()) {

    if ($category->visible) {
        $record = (object) array('id' => $category->id, 'visible' => 0);
        \local_ecommerce\category::save_category($record);
    }
    redirect($returnurl);

} else if (($action == 'moveup' or $action == 'movedown') && $category->id && confirm_sesskey()) {
    // move category
    \local_ecommerce\category::move_category($category, $action);
    redirect($returnurl);
}

$attachmentoptions = array('subdirs' => false, 'maxfiles' => 1, 'maxbytes' => $CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>context_system::instance(), 'accepted_types' => array('.jpg', '.gif', '.png'));
$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES,
        'maxbytes' => $SITE->maxbytes, 'context' => $context, 'descriptionformat' => 1);

if ($category->id) {
    // Edit existing.
    $category = file_prepare_standard_editor($category, 'description', $editoroptions,
            $context, 'local_ecommerce', 'catdescription', $category->id);
    $category = file_prepare_standard_filemanager($category, 'categoryimage', $attachmentoptions, $context, 'local_ecommerce', 'categoryimage', $category->id);

    $strheading = get_string('editcategory', 'local_ecommerce');
} else {
    // Add new.
    $category = file_prepare_standard_editor($category, 'description', $editoroptions,
            $context, 'local_ecommerce', 'catdescription', null);
    $category = file_prepare_standard_filemanager($category, 'categoryimage', $attachmentoptions, $context, 'local_ecommerce', 'categoryimage', null);

    $strheading = get_string('createcategory', 'local_ecommerce');
}

$editform = new edit_category_form(null, array('editoroptions' => $editoroptions, 'attachmentoptions' => $attachmentoptions, 'data' => $category));

if ($editform->is_cancelled()) {

    redirect($returnurl);

} else if ($data = $editform->get_data()) {

    $data->id = \local_ecommerce\category::save_category($data);

    if ($data->id) {
        $data = file_postupdate_standard_editor($data, 'description', $editoroptions,
                $context, 'local_ecommerce', 'catdescription', $data->id);
        $data = file_postupdate_standard_filemanager($data, 'categoryimage', $attachmentoptions, $context, 'local_ecommerce', 'categoryimage', $data->id);
        \local_ecommerce\category::save_category($data);
    }

    redirect($returnurl);
}

$renderer = $PAGE->get_renderer('local_ecommerce');
$params = [
        'title' => $strheading,
        'formhtml' => $editform->export_for_template($renderer)
];
$renderable = new \local_ecommerce\output\categories_edit($params);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/index.php'));
$PAGE->navbar->add(get_string('categories', 'local_ecommerce'), new moodle_url('/local/ecommerce/categories/index.php', array('parentid' => $parentid)));
$PAGE->navbar->add($strheading);
$PAGE->set_title(get_string('pluginname', 'local_ecommerce'));
$PAGE->set_heading($strheading);

echo $OUTPUT->header();

echo $renderer->render($renderable);
$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();

