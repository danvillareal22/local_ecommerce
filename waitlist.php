<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2018
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/tables/user_waitlist_table.php');

$id         = optional_param('id', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_TEXT);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);

require_login();
local_ecommerce_enable('enablewaitlist');

$context = context_system::instance();
require_capability('local/ecommerce:viewwaitlist', $context);
$PAGE->set_context($context);
$PAGE->set_url('/local/ecommerce/waitlist.php');

if ($action == 'delete' and $id) {
    $returnurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/waitlist.php');
    $item = $DB->get_record('local_ecommerce_waitlist', array('id'=>$id));
    $product = $DB->get_record('local_ecommerce_products', array('id'=>$item->productid));

    $PAGE->url->param('action', 'delete');
    if ($confirm and confirm_sesskey()) {
        \local_ecommerce\waitlist::delete_item($item);
        redirect($returnurl, get_string('removedfromlist', 'local_ecommerce'));
    }
    $strheading = get_string('removefromlist', 'local_ecommerce');
    $PAGE->navbar->add(get_string('waitlist', 'local_ecommerce'), new moodle_url('/local/ecommerce/products/waitlist.php'));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $yesurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/waitlist.php',
            array('id' => $item->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey()));
    $message = get_string('deletefromlisttmsg', 'local_ecommerce', format_string($product->name));
    echo $OUTPUT->confirm($message, $yesurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}

$title = get_string('ecommercewaitlist', 'local_ecommerce');
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->navbar->add(get_string('store', 'local_ecommerce'), new moodle_url('/local/ecommerce/store.php'));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new user_waitlist_table('user_waitlist_table');
$renderer = $PAGE->get_renderer('local_ecommerce');

$params = array(
    'title' => $title,
    'tablehtml' => $table->export_for_template($renderer),
    'waitlist_header' => $renderer->print_basic_header(get_string('waitlist', 'local_ecommerce'))
);

//$renderable = new \local_ecommerce\output\ecommerce_waitlist($params);

echo $OUTPUT->header();

echo $renderer->store_print_menu('waitlist');
echo $renderer->store_print_waitlist($params);
echo $renderer->store_print_checkout_footer();

$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');
$PAGE->requires->js_call_amd('local_ecommerce/store', 'init');

echo $OUTPUT->footer();
