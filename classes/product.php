<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;
use MoodleQuickForm_autocomplete;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class product {

    public static function save_product($data = null) {
        global $DB, $USER;

        if (isset($data->enableseats) and $data->enableseats == 0) {
            $data->seats = 0;
        }

        if ($data->id) {
            $product = $DB->get_record('local_ecommerce_products', array('id'=>$data->id));

            $data->timemodified = time();
            $DB->update_record('local_ecommerce_products', $data);

            $event_data = array(
                    'objectid' => $data->id,
                    'userid' => $USER->id,
                    'relateduserid' => $USER->id,
                    'context' => context_system::instance(),
            );

            waitlist::process_product_waitlist($product, $data);

            $event = event\local_ecommerce_product_updated::create($event_data);
            $event->trigger();

        } else {
            $data->timemodified = time();
            $data->id = $DB->insert_record('local_ecommerce_products', $data);

            $event_data = array(
                    'objectid' => $data->id,
                    'userid' => $USER->id,
                    'relateduserid' => $USER->id,
                    'context' => context_system::instance(),
            );

            $event = event\local_ecommerce_product_created::create($event_data);
            $event->trigger();
        }
        $DB->delete_records('local_ecommerce_lkd_prods', array('productid' => $data->id));
        if (isset($_POST['linked_products']) && count($_POST['linked_products']) > 0) {
            foreach ($_POST['linked_products'] as $linked_productid) {
                $linkedProduct = new stdClass();
                $linkedProduct->productid = $data->id;
                $linkedProduct->linked_productid = $linked_productid;
                $linkedProduct->timecreated = time();
                $DB->insert_record('local_ecommerce_lkd_prods', $linkedProduct);
            }
        }

        return $data->id;
    }

    public static function delete_linked_products($product = null) {
        global $DB;

        $linkedProducts = $DB->get_records_sql(
            'SELECT DISTINCT(id)
            FROM {local_ecommerce_lkd_prods}
            WHERE productid = :pid OR linked_productid = :lpid',
            array('pid' => $product->id, 'lpid' => $product->id)
        );

        if (!empty($linkedProducts) && count($linkedProducts) > 0) {
            foreach ($linkedProducts as $linkedProductID) {
                $DB->delete_record('local_ecommerce_lkd_prods', array('id' => $linkedProductID));
            }
        }
    }

    public static function delete_product($id = 0) {
        global $DB, $USER, $SITE;

        if (has_capability('local/ecommerce:editproduct', context_system::instance())) {

            $product = $DB->get_record('local_ecommerce_products', array('id' => $id));

            // delete tags
            core_tag_tag::remove_all_item_tags('local_ecommerce', 'local_ecommerce_products', $product->id);

            // delete product relations
            self::delete_product_relations($product);
            self::delete_linked_products($product);

            // delete product
            $DB->delete_records('local_ecommerce_products', array('id' => $product->id));

            $event_data = array(
                    'objectid' => $product->id,
                    'userid' => $USER->id,
                    'relateduserid' => $USER->id,
                    'context' => context_system::instance(),
                    'other' => array('name' => $product->name, 'idnumber' => $product->idnumber)
            );

            $event = event\local_ecommerce_product_deleted::create($event_data);
            $event->trigger();
        }
    }

    public static function delete_product_relations($product = null) {
        global $DB, $SITE;

        $relations = $DB->get_records('local_ecommerce_relations', array('productid' => $product->id));

        if (count($relations)) {
            foreach ($relations as $relation) {
                if ($relation->type == 'course') {
                    self::delete_product_course($product->id, $relation->instanceid);
                }
            }
        }

        $DB->delete_records('local_ecommerce', array('productid' => $product->id));
    }

    public static function delete_product_course($productid, $courseid) {
        global $DB, $SITE, $USER;

        $product = $DB->get_record('local_ecommerce_products', array('id' => $productid));
        $DB->delete_records('local_ecommerce_relations',
                array('productid' => $productid, 'instanceid' => $courseid, 'type' => 'course'));

        $event_data = array(
                'objectid' => $product->id,
                'userid' => $USER->id,
                'relateduserid' => $USER->id,
                'context' => context_system::instance(),
                'other' => array('name' => $product->name, 'instanceid' => $courseid, 'type' => 'course')
        );

        $event = event\local_ecommerce_relation_deleted::create($event_data);
        $event->trigger();
    }

    public static function save_product_courses($data) {
        global $DB, $CFG, $USER, $SITE;

        $product = $DB->get_record('local_ecommerce_products', array('id' => $data->id));
        if ($product->id) {
            if (empty($data->courses)) {
                return false;
            }
            foreach ($data->courses as $course) {
                if ($course <= 1) {
                    continue;
                }
                $course = $DB->get_record('course', array('id' => $course));
                if (!$course) {
                    continue;
                }

                $d                  = new stdClass();
                $d->productid       = $data->id;
                $d->instanceid      = $course->id;
                $d->type            = 'course';
                $d->timemodified    = time();
                $id = $DB->insert_record('local_ecommerce_relations', $d);

                $event_data = array(
                        'objectid' => $product->id,
                        'userid' => $USER->id,
                        'relateduserid' => $USER->id,
                        'context' => context_system::instance(),
                        'other' => array('name' => $product->name, 'instanceid' => $course->id, 'type' => 'course')
                );

                $event = event\local_ecommerce_relation_created::create($event_data);
                $event->trigger();
            }
        }
    }

    public static function get_assignable_courses($product = null, $categoryid = 0) {
        global $DB;
        $options = array();

        $params = array('productid' => $product->id, 'type' => 'course');
        $where = "category > 0 AND visible = 1";
        if ($categoryid) {
            $where .= " AND category = :category";
            $params['category'] = $categoryid;
        }

        $courses = $DB->get_records_sql("
            SELECT id, fullname 
              FROM {course} 
              WHERE $where AND id NOT IN (
                          SELECT DISTINCT c.id FROM {course} c, 
                          {local_ecommerce_relations} pr WHERE c.id = pr.instanceid 
                          AND pr.instanceid > 0 AND pr.type = :type AND pr.productid = :productid) 
                ORDER BY fullname ASC", $params);

        if (count($courses)) {
            foreach ($courses as $course) {
                $options[$course->id] = $course->fullname;
            }
        }

        return $options;
    }

    public static function get_assigned_courses($product = null, $ids = false) {
        global $DB;
        $options = array();

        $params = array('productid' => $product->id, 'type' => 'course');
        $where = "category > 0 AND visible = 1";

        $courses = $DB->get_records_sql("
            SELECT id, fullname 
              FROM {course} 
              WHERE $where AND id IN (
                          SELECT DISTINCT c.id FROM {course} c, 
                          {local_ecommerce_relations} pr WHERE c.id = pr.instanceid 
                          AND pr.instanceid > 0 AND pr.type = :type AND pr.productid = :productid) 
                ORDER BY fullname ASC", $params);

        if (count($courses)) {
            foreach ($courses as $course) {
                $options[$course->id] = $course->fullname;
            }
        }

        return ($ids) ? array_keys($options) : $options;
    }

    public static function delete_assigned_products($intanceid, $productid, $type = 'coupon') {
        global $DB, $SITE, $USER;

        $DB->delete_records('local_ecommerce_relations',
                array('productid' => $productid, 'instanceid' => $intanceid, 'type' => $type));
    }

    public static function save_assigned_products($data, $type = 'coupon') {
        global $DB, $CFG, $USER, $SITE;

        if ($data->id) {
            if (empty($data->products)) {
                return false;
            }
            foreach ($data->products as $product) {
                $product = $DB->get_record('local_ecommerce_products', array('id' => $product));
                if (!$product) {
                    continue;
                }

                $d                  = new stdClass();
                $d->productid       = $product->id;
                $d->instanceid      = $data->id;
                $d->type            = $type;
                $d->timemodified    = time();
                $id = $DB->insert_record('local_ecommerce_relations', $d);
            }
        }
    }

    public static function get_product_image($product) {
        global $CFG, $OUTPUT;

        $systemcontext = context_system::instance();

        $image_url = '';
        $fs = get_file_storage();
        $imgfiles = $fs->get_area_files($systemcontext->id, 'local_ecommerce', 'productimage', $product->id);
        foreach ($imgfiles as $file) {
            $filename = $file->get_filename();
            $filetype = $file->get_mimetype();
            if ($filename == '.' or !$filetype) {
                continue;
            }
            $url = moodle_url::make_pluginfile_url($systemcontext->id, 'local_ecommerce', 'productimage', $product->id, '/',
                    $filename);
            $image_url = $url->out();
        }

        if ($image_url == "") {
            $image_url = $OUTPUT->image_url('productimg', 'local_ecommerce')->out();
        }

        return $image_url;
    }

    public static function get_product_courses($product) {
        global $DB, $USER, $CFG;

        $sql = "SELECT c.* 
              FROM {course} c, {local_ecommerce_relations} pr
             WHERE c.visible = 1 AND c.id = pr.instanceid AND pr.type = 'course' AND pr.productid = :productid  
                    ORDER BY c.sortorder ASC";

        return $DB->get_records_sql($sql, array('productid'=>$product->id));
    }

    public static function get_course_images($courseItem)
    {
        global $CFG;
        foreach ($courseItem->get_course_overviewfiles() as $file) {
            $isimage = $file->is_valid_image();
            $url = file_encode_url("$CFG->wwwroot/pluginfile.php",
                '/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
                $file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
            if ($isimage) {
                return $url;
            }
        }
        return null;
    }

    public static function get_available_products($limit = 12, $page = 0, $categoryid = 0) {
        global $DB, $USER, $CFG;

        $products = array();

        $basefields = array('id', 'categoryid', 'sortorder', 'name', 'price', 'idnumber', 'seats', 'visible');
        $fields = 'p.' . join(',p.', $basefields);

        $params = array('userid1'=>((isset($USER->id)) ? $USER->id : 0), 'userid2'=>((isset($USER->id)) ? $USER->id : 0));
        $orderby = "ORDER BY p.sortorder";

        $wheres = array("p.visible > 0");

        // courses filter
        if ($categoryid) {
            $wheres[] = "p.categoryid = :category";
            $params['category'] = $categoryid;
        }

        $wheres = implode(" AND ", $wheres);

        //note: we can not use DISTINCT + text fields due to Oracle and MS limitations, that is why we have the subselect there
        $sql = "SELECT $fields, pc.name as categoryname, crt.id as incart, s.sold, wl.wlsize, w.id as inwaitlist 
                  FROM {local_ecommerce_products} p
                  LEFT JOIN {local_ecommerce_cat} pc ON pc.id = p.categoryid
                  LEFT JOIN {local_ecommerce} crt ON crt.productid = p.id AND crt.userid = :userid1
                  LEFT JOIN (SELECT instanceid, COUNT(id) as sold FROM {local_ecommerce_logs} WHERE type='product' GROUP BY instanceid) s ON s.instanceid = p.id 
                  LEFT JOIN (SELECT productid, COUNT(id) as wlsize FROM {local_ecommerce_waitlist} GROUP BY productid) wl ON wl.productid = p.id 
                  LEFT JOIN {local_ecommerce_waitlist} w ON w.productid = p.id AND w.userid = :userid2
                 WHERE $wheres 
                  $orderby";

        $start = $limit * $page;
        $dbproducts = $DB->get_records_sql($sql, $params, $start, $limit);

        if (count($dbproducts)) {
            foreach ($dbproducts as $product) {
                $product->price = format_float($product->price, 2, false);
                $product->image = self::get_product_image($product);
                $product->description = self::get_product_info($product);
                $product->currency = payment::get_currency();
                $product->incart = $product->incart;

                $products_in_logs = (get_config('local_ecommerce', 'enablewaitlist')) ? (int)$product->sold + (int)$product->wlsize : $product->sold;
                $product->seatsallow = (
                        ($product->seats == 0 or
                                ($product->seats and $product->seats > $products_in_logs)
                        ) and !$product->inwaitlist);

                $producturl = new moodle_url('/local/ecommerce/view.php', array('id'=>$product->id));
                $product->producturl = $producturl->out();
                $products[] = $product;
            }
        }

        $sql = "SELECT COUNT(p.id) as products 
                  FROM {local_ecommerce_products} p
                  LEFT JOIN {local_ecommerce_cat} pc ON pc.id = p.categoryid
                  LEFT JOIN {local_ecommerce} crt ON crt.productid = p.id AND crt.userid = :userid1
                  LEFT JOIN (SELECT instanceid, COUNT(id) as sold FROM {local_ecommerce_logs} WHERE type='product' GROUP BY instanceid) s ON s.instanceid = p.id 
                  LEFT JOIN (SELECT productid, COUNT(id) as wlsize FROM {local_ecommerce_waitlist} GROUP BY productid) wl ON wl.productid = p.id 
                  LEFT JOIN {local_ecommerce_waitlist} w ON w.productid = p.id AND w.userid = :userid2
                 WHERE $wheres 
                  $orderby";
        $products_count = $DB->get_record_sql($sql, $params);
        $total = ($products_count->products) ? $products_count->products : 0;

        return array('total' => $total, 'products' => $products);
    }

    public static function get_product_info($product) {
        global $CFG, $USER, $DB;
        require_once($CFG->dirroot.'/local/ecommerce/lib.php');

        $output = '';

        if (isset($product->description) and !empty($product->description)) {
            // product description
            if (!empty($product->description)) {
                $output = local_ecommerce_get_formatted_text($product->description, context_system::instance(), $product->id, 'description');
            }
        }

        return $output;
    }

    public static function get_active_products()
    {
        global $DB;
        return $DB->get_records('local_ecommerce_products', array('visible' => 1));
    }
}
