<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class invoices {

    public static function print_invoice($checkout) {
        global $CFG, $DB;

        $products = array();
        if (!empty($checkout->items)) {
            $products = $DB->get_records_sql("
                        SELECT p.*, l.price, l.discountprice, l.discount
                          FROM {local_ecommerce_logs} l 
                     LEFT JOIN {local_ecommerce_products} p ON p.id = l.instanceid  
                         WHERE l.checkoutid = :checkoutid AND l.type = 'product' 
                      ORDER BY p.name", array('checkoutid'=>$checkout->id));
        }

        $params = new stdClass;
        $params->products = $products;
        $params->currency = get_config('local_ecommerce', 'currency').' ';

        $userto = $DB->get_record('user', array('id'=>$checkout->userid));
        $params->username = fullname($userto);
        $params->amount = $checkout->amount;
        $params->subtotal = $checkout->subtotal;
        $params->discount = $checkout->discount;
        $params->salestax = $checkout->sales_tax;

        $filename = clean_filename("invoice".$checkout->id.".pdf");

        self::generate_invoice($checkout, $params, $filename, true);
    }


    public static function generate_invoice($checkout, $params, $filename, $download = false) {
        global $CFG, $DB, $OUTPUT, $PAGE;
        require_once("$CFG->libdir/pdflib.php");

        make_cache_directory('tcpdf');

        // No debugging here, sorry.
        $PAGE->set_context(context_system::instance());
        $CFG->debugdisplay = 0;
        @ini_set('display_errors', '0');
        @ini_set('log_errors', '1');

        $pdf = new \PDF('P', 'mm', 'A4', true, 'UTF-8', false);

        $pdf->SetTitle($filename);
        $pdf->SetProtection(array('modify'));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(false, 0);
        $pdf->AddPage();
        $pdf->SetTextColor(0, 0, 0);

        $site = get_site();
        $params->sitename = $site->fullname;
        $params->receiptdate = userdate($checkout->timeupdated, get_string('strftimedatefullshort', 'core_langconfig'));

        // company info
        $company_info = get_config('local_ecommerce', 'company_info');
        if ($company_info) {
            self::receipt_print_text($pdf, 15, 15, 'L', 'Helvetica', '', 10,
                    $company_info, 100);
        }

        // company logo
        $logo_image = $OUTPUT->image_url('invoice/logo', 'local_ecommerce');
        if ($company_logo = get_config('local_ecommerce', 'company_logo')) {
            $logo_image = moodle_url::make_pluginfile_url(context_system::instance()->id, 'local_ecommerce', 'company_logo', '0', '', $company_logo);
        }
        self::receipt_print_image($pdf, $logo_image->out(), 160, 15, 35, 35);

        self::receipt_print_text($pdf, 15, 55, 'L', 'Helvetica', '', 20, strtoupper(get_string('invoice', 'local_ecommerce')));

        // Bill To
        self::receipt_print_text($pdf, 15, 70, 'L', 'Helvetica', 'B', 11, strtoupper(get_string('billto', 'local_ecommerce')), 250);
        self::receipt_print_text($pdf, 15, 77, 'L', 'Helvetica', '', 11, $params->username, 30);

        // invoice data
        self::receipt_print_text($pdf, 65, 70, 'R', 'Helvetica', 'B', 11, get_string('invoicenumber', 'local_ecommerce'), 100);
        self::receipt_print_text($pdf, 164, 70, 'L', 'Helvetica', '', 11, $checkout->id, 30);

        self::receipt_print_text($pdf, 65, 77, 'R', 'Helvetica', 'B', 11, get_string('invoicedate', 'local_ecommerce'), 100);
        self::receipt_print_text($pdf, 164, 77, 'L', 'Helvetica', '', 11, $params->receiptdate, 30);

        if ($invoice_duration = get_config('local_ecommerce', 'invoice_duration')) {
            $params->duedate = userdate($checkout->timeupdated+$invoice_duration, get_string('strftimedatefullshort', 'core_langconfig'));
            self::receipt_print_text($pdf, 65, 84, 'R', 'Helvetica', 'B', 11, get_string('invoiceduedate', 'local_ecommerce'), 100);
            self::receipt_print_text($pdf, 164, 84, 'L', 'Helvetica', '', 11, $params->duedate, 30);
        }

        // divider
        self::receipt_print_text($pdf, 15, 95, 'L', 'Helvetica', '', 11, '<hr>', 182);

        // order
        /*self::receipt_print_text($pdf, 15, 105, 'L', 'Helvetica', 'B', 11, get_string('purchaseorder', 'local_ecommerce'), 100);
        self::receipt_print_text($pdf, 15, 112, 'L', 'Helvetica', '', 11, $checkout->id, 100);*/

        $table = self::generate_products_table($checkout, $params);
        self::receipt_print_text($pdf, 15, 100, 'L', 'Helvetica', '', 10, $table, 182);

        if ($download) {
            $pdf->Output($filename, 'I');
            exit;
        }

        return $pdf->Output($filename, 'S');
    }


    /**
     * Sends text to output given the following params.
     *
     * @param stdClass $pdf
     * @param int $x horizontal position
     * @param int $y vertical position
     * @param char $align L=left, C=center, R=right
     * @param string $font any available font in font directory
     * @param char $style ''=normal, B=bold, I=italic, U=underline
     * @param int $size font size in points
     * @param string $text the text to print
     * @param int $width horizontal dimension of text block
     */
    public static function receipt_print_text($pdf, $x, $y, $align, $font='freeserif', $style, $size = 10, $text, $width = 0) {
        $pdf->setFont($font, $style, $size);
        $pdf->SetXY($x, $y);
        $pdf->writeHTMLCell($width, 0, '', '', $text, 0, 0, 0, true, $align);
    }


    /**
     * Prints border images from the borders folder in PNG or JPG formats.
     *
     * @param stdClass $pdf
     * @param string $filename
     * @param int $x x position
     * @param int $y y position
     * @param int $w the width
     * @param int $h the height
     */
    public static function receipt_print_image($pdf, $filepath, $x, $y, $w, $h) {
        global $CFG, $OUTPUT;

        $pdf->Image($filepath, $x, $y, $w, $h);
    }

    public static function generate_products_table($checkout, $params) {

        $productslist = '';
        if (count($params->products)) {
            foreach ($params->products as $product) {
                $productslist .= '<tr><td width="50%"><strong>'.$product->name.'</strong>';
                if ($product->enrolperiod > 0) {
                    $productslist .= '<br>'.get_string('period', 'local_ecommerce').': '.userdate($checkout->timeupdated, get_string('strftimedatefullshort', 'core_langconfig')).' - '.userdate($checkout->timeupdated+$product->enrolperiod, get_string('strftimedatefullshort', 'core_langconfig'));
                }
                $productslist .= '</td><td width="15%" align="left">'.$params->currency.' '.format_float($product->price, 2, false).'</td>';
                $productslist .= '<td width="15%" align="left">'.(($product->discount) ? $params->currency.' '.format_float($product->discount, 2, false) : '').'</td>';
                $productslist .= '<td width="20%" align="right">';
                $productslist .= ($product->discountprice) ? $params->currency.' '.format_float($product->discountprice, 2, false) : $params->currency.' '.format_float($product->price, 2, false);
                $productslist .= '</td></tr>';
            }
            $params->productslist = $productslist;
        }

        $salesTaxHTML = '';
        if (get_config('local_ecommerce', 'enable_sales_tax')) {
            $salesTaxHTML =
            '<tr>
                <td width="70%" align="right">'.get_config('local_ecommerce', 'sales_tax_name') . ' @ ' . get_config('local_ecommerce', 'sales_tax_percentage') . '%</td>
                <td width="30%" align="right">'.$params->currency.$params->salestax.'</td>
            </tr>';
        }

        return '<table cellpadding="5">
                <thead>
                    <tr style="background-color: #cccccc;">
                        <th width="50%">'.strtoupper(get_string('product', 'local_ecommerce')).'</th>
                        <th width="15%" align="left">'.strtoupper(get_string('price', 'local_ecommerce')).'</th>
                        <th width="15%" align="left">'.strtoupper(get_string('discount', 'local_ecommerce')).'</th>
                        <th width="20%" align="right">'.strtoupper(get_string('amount', 'local_ecommerce')).'</th>
                    </tr>
                </thead>
                <tbody>
                    '.$productslist.'
                </tbody>
                </table>
                <br><hr>
                <table cellpadding="3" width="100%">
                <tbody>
                </tbody>
                    <tr>
                        <td width="70%" align="right">'.strtoupper(get_string('subtotal', 'local_ecommerce')).'</td>
                        <td width="30%" align="right">'.$params->currency.$params->subtotal.'</td>
                    </tr>
                    <tr>
                        <td width="70%" align="right">'.strtoupper(get_string('discount', 'local_ecommerce')).'</td>
                        <td width="30%" align="right">'.$params->currency.$params->discount.'</td>
                    </tr>' . $salesTaxHTML . '
                    <tr>
                        <td width="70%" align="right"><strong>'.strtoupper(get_string('balancedue', 'local_ecommerce')).'</strong></td>
                        <td width="30%" align="right"><strong>'.$params->currency.$params->amount.'</strong></td>
                    </tr>
                </tbody>
                </table>';
    }

    public static function approve_invoice($id) {
        global $CFG, $DB;

        require_once($CFG->dirroot."/enrol/ecommerce/locallib.php");

        $timestamp = time();

        $checkout = $DB->get_record("local_ecommerce_checkout", array("id"=>$id, "payment_status"=>\local_ecommerce\payment::$STATUS_PENDING));

        $checkout->txn_id = $timestamp;
        $checkout->payment_status = \local_ecommerce\payment::$STATUS_COMPLETED;
        $checkout->pending_reason = '';
        $checkout->reason_code = '';
        $checkout->timeupdated = $timestamp;
        $checkout->paymentid = 0;

        // ALL CLEAR !
        $DB->update_record("local_ecommerce_checkout", $checkout);

        // Enrol user
        \local_ecommerce\checkout::enrol_user($checkout);

        // send checkout notification
        \local_ecommerce\notification::send_checkout_notification($checkout);
    }

}
