<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class coupon {

    public static $SHOPPINGCART_DISCOUNT_ANY_SELECTED = 1;
    public static $SHOPPINGCART_DISCOUNT_ALL = 2;

    public static function save_coupon($data = null) {
        global $DB;

        if (empty($data->code)) {
            $data->code = self::generate_coupon_code();
        }

        if ($data->id) {
            $data->timemodified = time();
            $DB->update_record('local_ecommerce_coupons', $data);

            return $data->id;

        } else {
            $data->timemodified = time();
            $data->timecreated = time();

            return $DB->insert_record('local_ecommerce_coupons', $data);
        }
    }

    public static function generate_coupon_code() {
        global $DB;

        $code = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < 16; $i++) {
            if (in_array($i, array(4,8,12))) {
                $code .= '-';
            }
            $code .= $codeAlphabet[random_int(0, $max-1)];
        }

        if ($DB->get_record('local_ecommerce_coupons', array('code' => $code))) {
            $code = generate_coupon_code();
        }

        return $code;
    }

    public static function delete_coupon($id = 0) {
        global $DB, $USER, $SITE;

        if (has_capability('local/ecommerce:editcoupons', \context_system::instance())) {

            $coupon = $DB->get_record('local_ecommerce_coupons', array('id' => $id));

            // delete coupon relations
            self::delete_coupon_relations($coupon);

            // delete coupon
            $DB->delete_records('local_ecommerce_coupons', array('id' => $coupon->id));
        }
    }

    public static function delete_coupon_relations($coupon = null) {
        global $DB, $SITE;

        $DB->delete_records('local_ecommerce_relations', array('instanceid' => $coupon->id, 'type' => 'coupon'));
    }

    public static function coupon_apply($code = '') {
        global $CFG, $USER, $DB;

        $coupon = $DB->get_record('local_ecommerce_coupons', array('code' => $code, 'status' => 1));

        if (!$coupon) {
            return get_string('invalidcoupon', 'local_ecommerce');
        }

        if ($DB->get_record('local_ecommerce_logs', array('instanceid' => $coupon->id, 'type' => 'coupon', 'userid' => $USER->id, 'status' => payment::$STATUS_PENDING))) {
            return get_string('couponalreadyapplied', 'local_ecommerce');
        }

        // check coupon period
        $now = time();
        if ($coupon->starttime > 0 and $coupon->starttime > $now) {
            return get_string('couponinactive', 'local_ecommerce');
        } elseif ($coupon->endtime > 0 and $coupon->endtime < $now) {
            return get_string('couponisover', 'local_ecommerce');
        }

        // check all used coupons
        if ((int)$coupon->usedcount > 0 and (int)$coupon->usedcount <= $DB->count_records('local_ecommerce_logs', array('instanceid' => $coupon->id, 'type' => 'coupon'))) {
            return get_string('couponmaxnumber', 'local_ecommerce');
        }

        if ((int)$coupon->usedperuser > 0 and (int)$coupon->usedperuser <= $DB->count_records('local_ecommerce_logs', array('instanceid' => $coupon->id, 'type' => 'coupon', 'userid' => $USER->id))) {
            return get_string('couponusermaxnumber', 'local_ecommerce');
        }

        // insert coupon
        $record = new stdClass();
        $record->userid = $USER->id;
        $record->instanceid = $coupon->id;
        $record->type = 'coupon';
        $record->status = payment::$STATUS_PENDING;
        $record->timecreated = time();
        $record->timemodified = time();

        $DB->insert_record('local_ecommerce_logs', $record);
        return false;
    }

    public static function get_product_applied_coupons($product, $coupons) {
        global $DB;

        if (!get_config('local_ecommerce', 'enablecoupons')) {
            return array();
        }

        $sql_in = array();
        foreach ($coupons as $coupon) {
            $sql_in[] = $coupon->id;
        }
        $idArray = implode(',', $sql_in);

        if ($coupon->type == self::$SHOPPINGCART_DISCOUNT_ALL) {
            return $DB->get_records_sql('SELECT * FROM {local_ecommerce_coupons}
                                              WHERE status = 1
                                              AND id IN(:idarray)',
                                                array('idarray' => $idArray));
        }

        return $DB->get_records_sql("SELECT c.* FROM {local_ecommerce_relations} r JOIN {local_ecommerce_coupons} c ON c.id = r.instanceid WHERE r.type = :type AND r.productid = :productid AND c.status = 1 AND r.instanceid IN (".implode(',', $sql_in).")", array('type'=>'coupon', 'productid'=>$product->id));
    }

    public static function get_applied_coupons($userid = 0) {
        global $CFG, $USER, $DB;

        if (!get_config('local_ecommerce', 'enablecoupons')) {
            return array();
        }

        $userid = ($userid) ? $userid : $USER->id;

        return $DB->get_records_sql("
          SELECT c.*, l.id as logid 
            FROM {local_ecommerce_logs} l
            JOIN {local_ecommerce_coupons} c ON l.instanceid = c.id 
           WHERE l.type = :type AND l.status = :status AND l.userid = :userid",
                array('type' => 'coupon', 'userid' => $USER->id, 'status' => payment::$STATUS_PENDING)
        );
    }

    public static function coupon_remove($id) {
        global $CFG, $USER, $DB;

        return $DB->delete_records("local_ecommerce_logs", array('id' => $id));
    }
}
