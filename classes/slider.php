<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;
use MoodleQuickForm_autocomplete;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class slider {

    public static function get_slider()
    {
        global $DB;

        $returnObject = new stdClass();
        $slider = $DB->get_record_sql('SELECT * FROM {local_ecommerce_slider} LIMIT 1');
        if (!$slider) {
            $returnObject->enableSlider = false;
            return $returnObject;
        }
        $returnObject->enableSlider = $slider->enable_slider;
        if ($returnObject->enableSlider) {
            $displaySlider = $slider->display_slider;
            if ($displaySlider == 'internal' && !isloggedin() || $displaySlider == 'external' && isloggedin()) {
                $returnObject->enableSlider = false;
                return $returnObject;
            }
            $slide1Image = self::get_slider_image_url(1, $slider->id);
            if (!empty($slide1Image)) {
                $returnObject->slide1 = new stdClass();
                $returnObject->slide1->caption = $slider->slider_caption_1;
                $returnObject->slide1->url = $slider->slider_url_1;
                $returnObject->slide1->image = $slide1Image;
            }
            $slide2Image = self::get_slider_image_url(2, $slider->id);
            if (!empty($slide2Image)) {
                $returnObject->slide2 = new stdClass();
                $returnObject->slide2->caption = $slider->slider_caption_2;
                $returnObject->slide2->url = $slider->slider_url_2;
                $returnObject->slide2->image = $slide2Image;
            }
            $slide3Image = self::get_slider_image_url(3, $slider->id);
            if (!empty($slide3Image)) {
                $returnObject->slide3 = new stdClass();
                $returnObject->slide3->caption = $slider->slider_caption_3;
                $returnObject->slide3->url = $slider->slider_url_3;
                $returnObject->slide3->image = $slide3Image;
            }
            $slide4Image = self::get_slider_image_url(4, $slider->id);
            if (!empty($slide4Image)) {
                $returnObject->slide4 = new stdClass();
                $returnObject->slide4->caption = $slider->slider_caption_4;
                $returnObject->slide4->url = $slider->slider_url_4;
                $returnObject->slide4->image = $slide4Image;
            }
            $slide5Image = self::get_slider_image_url(5, $slider->id);
            if (!empty($slide5Image)) {
                $returnObject->slide5 = new stdClass();
                $returnObject->slide5->caption = $slider->slider_caption_5;
                $returnObject->slide5->url = $slider->slider_url_5;
                $returnObject->slide5->image = $slide5Image;
            }
        }
        return $returnObject;
    }

    private static function get_slider_image_url($id, $sliderID)
    {
        $context = context_system::instance();
        $fs = get_file_storage();
        $imgfiles = $fs->get_area_files($context->id, 'local_ecommerce', 'slider_image_' . $id, $sliderID);
        foreach ($imgfiles as $file) {
            $filename = $file->get_filename();
            $filetype = $file->get_mimetype();
            if ($filename == '.' or !$filetype) {
                continue;
            }
            $url = moodle_url::make_pluginfile_url($context->id, 'local_ecommerce', 'slider_image_' . $id, $sliderID, '/', $filename);
            $image_url = $url->out();
            return $image_url;
        }
        return false;
    }

    public static function save_slider($data) {
        global $DB;

        if ($data->id) {
            $data->timeupdated = time();
            $DB->update_record('local_ecommerce_slider', $data);
            return $data->id;
        } else {
            $data->timecreated = time();
            return $DB->insert_record('local_ecommerce_slider', $data);
        }
    }
}
