<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class checkout {

    public static function add_to_cart_from_login($productid) {
        global $DB, $USER, $CFG;

        $enablewaitlist = get_config('local_ecommerce', 'enablewaitlist');

        $checkoutURL = new moodle_url($CFG->wwwroot . '/local/ecommerce/index.php');

        if ($DB->get_record('local_ecommerce', array('userid'=>$USER->id, 'productid'=>$productid))) {
            redirect($checkoutURL);
        } else if ($DB->get_record('local_ecommerce_logs', array('userid'=>$USER->id, 'instanceid'=>$productid, 'type' =>'product', 'status' =>'completed'))) {
            // Do nothing
        } else if ($enablewaitlist and $DB->get_record('local_ecommerce_waitlist', array('userid' => $USER->id, 'productid' => $productid))) {
            // Do nothing
        } else if ($product = $DB->get_record('local_ecommerce_products', array('visible'=>1, 'id'=>$productid))) {

            $sold = $DB->count_records('local_ecommerce_logs', array('instanceid'=>$product->id, 'type'=>'product'));
            $seatsallow = ($product->seats == 0 or
                ($product->seats and $product->seats > $sold + waitlist::get_product_count_waitlist($product))
            );

            if (!$seatsallow and $enablewaitlist) {
                // Do nothing
            } elseif (!$seatsallow and !$enablewaitlist) {
                // Do nothing
            } else {

                $record = new stdClass();
                $record->userid = $USER->id;
                $record->productid = $product->id;
                $record->timecreated = time();

                if ($id = $DB->insert_record('local_ecommerce', $record)) {

                    // create/update checkout
                    if ($products = checkout::get_products_in_cart()) {
                        $coupons = coupon::get_applied_coupons();
                        $discounts = checkout::get_products_discounts($products);
                        $discountprices =
                            checkout::get_checkout_products_prices($products, $coupons, $discounts);
                        $total = checkout::get_cart_total($products, $discountprices);
                    }
                    checkout::get_payment_details($products, $total);
                    wishlist::delete_wishlist_item($productid);
                    redirect($checkoutURL);
                }
            }
        }
    }

    public static function get_products_in_cart() {
        global $CFG, $USER, $DB;

        $sql = "SELECT p.* 
              FROM {local_ecommerce_products} p, {local_ecommerce} s
             WHERE p.visible = 1 AND p.id = s.productid AND s.userid = :userid  
                    ORDER BY p.sortorder ASC";

        return $DB->get_records_sql($sql, array('userid'=>$USER->id));
    }

    public static function get_products_discounts($products) {
        global $DB;

        if (!get_config('local_ecommerce', 'enablediscounts')) {
            return array();
        }
        $now = time();
        $availablediscounts = $DB->get_records_sql(
        "SELECT DISTINCT d.* 
            FROM {local_ecommerce_discounts} d
            WHERE d.starttime < :now1
            AND (d.endtime = 0 OR d.endtime > :now2)
            AND d.status = 1",
            array('now1' => $now, 'now2' => $now)
        );
        $applied_discounts = $used_discounts = array();
        if (count($availablediscounts)) {
            $products_arr = array();
            foreach ($products as $product) {
                $products_arr[$product->id] = $product;
            }
            foreach ($availablediscounts as $discount) {
                if ($discount->type == 2) {
                    $discount_products = product::get_active_products();
                } else {
                    $discount_products = discount::get_discount_products($discount);
                }

                $canuse = true;
                if ($discount_products) {
                    // check if discount can be applied
                    if ($discount->type == discount::$SHOPPINGCART_DISCOUNT_ANY_SELECTED) {

                        // check if any from assigned products added to the shopping cart
                        $canuse = false;
                        foreach ($discount_products as $product) {
                            if (isset($products_arr[$product->id])) {
                                $canuse = true;
                                break;
                            }
                        }
                    } elseif ($discount->type == discount::$SHOPPINGCART_DISCOUNT_CONDITION) {

                        // check condition
                        $assignedproducts = 0; $canuse = false;
                        foreach ($discount_products as $product) {
                            if (isset($products_arr[$product->id])) {
                                $assignedproducts++;
                            }
                        }

                        if ($assignedproducts >= $discount->minnumber and ($discount->maxnumber == 0 or $assignedproducts <= $discount->maxnumber)) {
                            $canuse = true;
                        }
                    } elseif ($discount->type == discount::$SHOPPINGCART_DISCOUNT_ALL_SELECTED) {

                        // check if all assigned products added to the shopping cart
                        foreach ($discount_products as $product) {
                            if (!isset($products_arr[$product->id])) {
                                $canuse = false;
                                break;
                            }
                        }
                    } else {
                        $canuse = true;
                    }

                    if ($canuse) {
                        $discount->discount = (float)$discount->discount;
                        $discount->products = $discount->prices = array();

                        foreach ($discount_products as $product) {
                            $product->price = (float)$product->price;
                            $price = $product->price - (($discount->discount / 100) * $product->price);
                            $discountval = ($discount->discount / 100) * $product->price;

                            $applied_discounts[$product->id][] = $discount->discount;

                            $product->discountprice = $price;
                            $discount->products[$product->id] = $product;
                        }

                        $used_discounts[] = $discount;
                    }
                }
            }
        }

        return array('used_discounts' => $used_discounts, 'applied_discounts' => $applied_discounts);
    }

    public static function get_checkout_products_prices($products, $coupons, $discounts) {
        $discountprices = array();
        if (count($products)) {
            foreach ($products as $product) {
                $product->discountprice = null;
                if (!empty($product->price)) {
                    // calculate price with coupons and discounts
                    $product->discountprice = self::get_product_price($product, $coupons, $discounts);
                }
                $discountprices[$product->id] = $product->discountprice;
            }
        }

        return $discountprices;
    }

    public static function get_product_price($product, $coupons, $discounts) {
        $discount = 0;
        $price = $discountval = null;
        if (count($coupons)) {
            $productcoupons = coupon::get_product_applied_coupons($product, $coupons);
            if ($productcoupons) {
                foreach ($productcoupons as $coupons) {
                    $discount += $coupons->discount;
                }
            }
        }

        // calculate discounts
        if (isset($discounts['applied_discounts'][$product->id])) {
            foreach ($discounts['applied_discounts'][$product->id] as $pdiscount) {
                $discount += $pdiscount;
            }
        }

        $discount = ($discount > 100) ? 100 : $discount;
        if ($discount > 0) {
            $discount = (float)$discount;
            $product->price = (float)$product->price;
            $price = $product->price - (($discount / 100) * $product->price);
            $discountval = ($discount / 100) * $product->price;
        }

        return array('price'=>$price, 'discount' => $discountval);
    }

    public static function get_cart_total($products, $discountprices) {
        $data = new stdClass();

        $data->subtotal = 0;
        $data->total = 0;
        $data->discount = 0;

        foreach ($products as $product) {
            $data->subtotal += (float)$product->price;

            // calculate coupons
            if (isset($discountprices[$product->id]['discount']) and $discountprices[$product->id]['discount'] !== null) {
                $data->discount += $discountprices[$product->id]['discount'];
            }
        }

        if ($data->discount) {
            $data->total = $data->subtotal - $data->discount;
        } else {
            $data->total = $data->subtotal;
        }

        if (get_config('local_ecommerce', 'enable_sales_tax')) {
            $data->salesTaxPercentage = get_config('local_ecommerce', 'sales_tax_percentage');
            $salesTax = 1 + ($data->salesTaxPercentage / 100);
            $data->preTaxTotal = $data->total;
            $data->salesTax = $data->total * $salesTax - $data->total;
            $data->total *= $salesTax;
            $data->salesTax = format_float($data->salesTax, 2, false);
            $data->preTaxTotal = format_float($data->preTaxTotal, 2, false);
        }

        $data->subtotal = format_float($data->subtotal, 2, false);
        $data->total    = format_float($data->total, 2, false);
        $data->discount = format_float($data->discount, 2, false);

        return $data;
    }

    public static function get_payment_details($products, $total = array(), $discountprices = array()) {
        global $CFG, $USER, $DB;

        $existing = $DB->get_record('local_ecommerce_checkout', array('userid'=>$USER->id, 'payment_status'=>payment::$STATUS_PENDING));

        $items = $names = array();

        foreach ($products as $product) {
            $items[] = $product->id;
            $names[] = $product->name;
        }

        $checkout = new stdClass();
        $checkout->item_name = implode(', ', $names);
        $checkout->userid = $USER->id;
        $checkout->items = implode(',', $items);
        $checkout->payment_status = payment::$STATUS_PENDING;
        $checkout->amount = (isset($total->total)) ? $total->total : '';
        $checkout->subtotal = (isset($total->subtotal)) ? $total->subtotal : '';
        $checkout->discount = (isset($total->discount)) ? $total->discount : '';
        $checkout->sales_tax = (isset($total->salesTax)) ? $total->salesTax : '';
        $checkout->timeupdated = time();
        $checkout->reminder_email = 0;

        if ($existing) {
            $checkout->id = $existing->id;
            $DB->update_record('local_ecommerce_checkout', $checkout);
        } else {
            $checkout->id = $DB->insert_record('local_ecommerce_checkout', $checkout);
        }

        self::set_payment_logs($products, $checkout, $discountprices);

        return $checkout;
    }

    public static function set_payment_logs($products, $checkout, $discountprices) {
        global $CFG, $USER, $DB;

        $DB->delete_records('local_ecommerce_logs', array('userid'=>$checkout->userid, 'type'=>'product', 'status'=>payment::$STATUS_PENDING, 'checkoutid'=>$checkout->id));

        if (count($products)) {
            foreach ($products as $product) {
                $record = new stdClass();
                $record->userid = $checkout->userid;
                $record->instanceid = $product->id;
                $record->type = 'product';
                $record->status = payment::$STATUS_PENDING;
                $record->timecreated = time();
                $record->timemodified = time();
                $record->checkoutid = $checkout->id;
                $record->price = (float) $product->price;
                $record->discountprice =
                        (isset($discountprices[$product->id]['price']) and $discountprices[$product->id]['price'] !== null) ?
                                $discountprices[$product->id]['price'] : '';
                $record->discount =
                        (isset($discountprices[$product->id]['discount']) and $discountprices[$product->id]['discount'] !== null) ?
                                $discountprices[$product->id]['discount'] : '';
                $record->sales_tax = $checkout->sales_tax;

                $DB->insert_record('local_ecommerce_logs', $record);
            }
        }

        // update coupons logs
        $coupons_logs = $DB->get_records('local_ecommerce_logs', array('type'=>'coupon', 'checkoutid'=>0, 'userid'=>$checkout->userid));
        if (count($coupons_logs)) {
            foreach ($coupons_logs as $log) {
                $log->checkoutid = $checkout->id;
                $log->timemodified = time();
                $DB->update_record('local_ecommerce_logs', $log);
            }
        }

        // update discounts logs
        $discounts_logs = $DB->get_records('local_ecommerce_logs', array('type'=>'discount', 'checkoutid'=>0, 'userid'=>$checkout->userid));
        if (count($discounts_logs)) {
            foreach ($discounts_logs as $log) {
                $log->checkoutid = $checkout->id;
                $log->timemodified = time();
                $DB->update_record('local_ecommerce_logs', $log);
            }
        }
    }

    public static function delete_from_cart($id, $userid = 0) {
        global $CFG, $USER, $DB;

        $userid = ($userid) ? $userid : $USER->id;
        $DB->delete_records('local_ecommerce', array('userid'=>$userid, 'productid'=>$id));
        $DB->delete_records('local_ecommerce_logs', array('userid'=>$userid, 'type'=>'product', 'status'=>payment::$STATUS_PENDING, 'instanceid'=>$id));
    }

    public static function enrol_user($checkout) {
        global $CFG, $USER, $DB;

        require_once($CFG->dirroot.'/enrol/ecommerce/locallib.php');

        if (!empty($checkout->items)) {
            $products = $DB->get_records_sql("SELECT * FROM {local_ecommerce_products} WHERE id IN (".$checkout->items.")");

            // insert logs
            $coupons = coupon::get_applied_coupons($checkout->userid);
            $discounts = self::get_products_discounts($products);
            $discountprices = self::get_checkout_products_prices($products, $coupons, $discounts);

            if (count($products)) {

                // insert discounts logs
                if (isset($discounts['used_discounts']) and count($discounts['used_discounts'])) {
                    foreach ($discounts['used_discounts'] as $discount) {
                        $record = new stdClass();
                        $record->userid = $checkout->userid;
                        $record->instanceid = $discount->id;
                        $record->type = 'discount';
                        $record->status = payment::$STATUS_COMPLETED;
                        $record->timecreated = time();
                        $record->timemodified = time();
                        $record->checkoutid = $checkout->id;
                        $record->discount = $discount->discount;

                        $items = $details = array();
                        $price = $discountprice = 0;
                        foreach ($discount->products as $product) {
                            $items[] = $product->id;
                            $details[] = $product->name;
                            $price += $product->price;
                            $discountprice += $product->discountprice;
                        }

                        $record->items = implode(',', $items);
                        $record->details = implode(', ', $details);
                        $record->price = $price;
                        $record->discountprice = $discountprice;

                        $DB->insert_record('local_ecommerce_logs', $record);
                    }
                }

                foreach ($products as $product) {

                    // enroll user into courses
                    enrol_ecommerce_enrol_user($product, $checkout);

                    // insert product logs
                    $record = new stdClass();
                    $record->userid = $checkout->userid;
                    $record->instanceid = $product->id;
                    $record->type = 'product';
                    $record->status = payment::$STATUS_COMPLETED;
                    $record->timecreated = time();
                    $record->timemodified = time();
                    $record->checkoutid = $checkout->id;
                    $record->price = (float)$product->price;
                    $record->discountprice = (isset($discountprices[$product->id]['price']) and $discountprices[$product->id]['price'] !== null) ?  $discountprices[$product->id]['price'] : '';
                    $record->discount = (isset($discountprices[$product->id]['discount']) and $discountprices[$product->id]['discount'] !== null) ?  $discountprices[$product->id]['discount'] : '';

                    if ($exist = $DB->get_record('local_ecommerce_logs', array('type'=>$record->type, 'instanceid'=>$record->instanceid, 'userid'=>$record->userid, 'checkoutid'=>$record->checkoutid, 'status'=>payment::$STATUS_PENDING))) {
                        $record->id = $exist->id;
                        $DB->update_record('local_ecommerce_logs', $record);
                    } else {
                        $DB->insert_record('local_ecommerce_logs', $record);
                    }

                    // delete all from cart
                    self::delete_from_cart($product->id, $checkout->userid);

                    // process waitlist logs
                    waitlist::process_checkout($product, $checkout);
                }

                // update coupons logs
                self::update_pending_logs($checkout, $products, $coupons);
            }
        }
    }

    public static function processReminderEmails() {
        global $DB;
        $checkoutDuration = get_config('local_ecommerce', 'awaiting_checkout_duration');
        $now = time();

        $reminderQueue = $DB->get_records_sql('
              SELECT chk.id, chk.item_name, chk.userid, chk.items, u.email
                FROM {local_ecommerce_checkout} chk
                JOIN {user} u ON u.id = chk.userid
               WHERE chk.timeupdated + :checkoutduration < :now AND chk.payment_status = "pending" AND chk.reminder_email = 0',
            array('checkoutduration' => $checkoutDuration, 'now' => $now)
        );

        if (is_array($reminderQueue) && count($reminderQueue) > 0) {
            foreach ($reminderQueue as $queueItem) {
                self::sendCheckoutReminder($queueItem);
            }
        }

        if (!get_config('local_ecommerce', 'enable_awaiting_checkout_emails2')) {
            return;
        }

        $checkoutDuration2 = get_config('local_ecommerce', 'awaiting_checkout_duration2');

        $reminderQueue2 = $DB->get_records_sql('
              SELECT chk.id, chk.item_name, chk.userid, chk.items, u.email
                FROM {local_ecommerce_checkout} chk
                JOIN {user} u ON u.id = chk.userid
               WHERE chk.timeupdated + :checkoutduration < :now AND chk.payment_status = "pending" AND chk.reminder_email2 = 0',
            array('checkoutduration' => $checkoutDuration2, 'now' => $now)
        );

        if (is_array($reminderQueue2) && count($reminderQueue2) > 0) {
            foreach ($reminderQueue2 as $queueItem) {
                self::sendCheckoutReminder($queueItem, 2);
            }
        }

        if (!get_config('local_ecommerce', 'enable_awaiting_checkout_emails3')) {
            return;
        }

        $checkoutDuration3 = get_config('local_ecommerce', 'awaiting_checkout_duration3');

        $reminderQueue3 = $DB->get_records_sql('
              SELECT chk.id, chk.item_name, chk.userid, chk.items, u.email
                FROM {local_ecommerce_checkout} chk
                JOIN {user} u ON u.id = chk.userid
               WHERE chk.timeupdated + :checkoutduration < :now AND chk.payment_status = "pending" AND chk.reminder_email3 = 0',
            array('checkoutduration' => $checkoutDuration3, 'now' => $now)
        );

        if (is_array($reminderQueue3) && count($reminderQueue3) > 0) {
            foreach ($reminderQueue3 as $queueItem) {
                self::sendCheckoutReminder($queueItem, 3);
            }
        }
    }

    private static function sendCheckoutReminder($queueItem, $emailNo = null) {
        global $DB;

        $userTo = $DB->get_record('user', array('id'=>$queueItem->userid));
        $userFrom = \core_user::get_support_user();
        $userFrom->maildisplay = true;
        $checkoutURL = new \moodle_url('/local/ecommerce');

        $a = new \stdClass();
        $a->productslist = $queueItem->item_name;
        $a->username = fullname($userTo);
        $a->checkouturl = $checkoutURL->out();

        $reminderMessage = get_config('local_ecommerce', 'cart_awaiting_checkout_message' . $emailNo);

        if (!empty($reminderMessage)) {
            foreach ($a as $name => $value) {
                $reminderMessage = str_replace("[[$name]]", $value, $reminderMessage);
            }
            $message = $reminderMessage;
        } else {
            return false;
        }

        $plaintext = format_text_email($message, FORMAT_HTML);

        // Subject
        $subject = get_config('local_ecommerce', 'cart_awaiting_checkout_subject' . $emailNo);

        $eventdata = new \core\message\message();
        $eventdata->userfrom         = $userFrom;
        $eventdata->userto           = $userTo;
        $eventdata->subject          = $subject;
        $eventdata->fullmessage      = $plaintext;
        $eventdata->fullmessageformat = FORMAT_HTML;
        $eventdata->fullmessagehtml  = $message;
        $eventdata->smallmessage     = '';
        $eventdata->notification     = 1;
        $eventdata->courseid = SITEID;

        // Required for messaging framework
        $eventdata->component = 'local_ecommerce';
        $eventdata->name = 'checkout_notify';

        message_send($eventdata);

        $checkoutItem = $DB->get_record('local_ecommerce_checkout', array('id' => $queueItem->id));
        $reminderEmailField = 'reminder_email' . $emailNo;
        $checkoutItem->$reminderEmailField = 1;
        var_dump($checkoutItem);
        die();
        $DB->update_record('local_ecommerce_checkout', $checkoutItem);

        return true;
    }

    public static function update_pending_logs($checkout = null, $products = null, $coupons = null) {
        global $CFG, $USER, $DB;

        $userid = (isset($checkout->userid)) ? $checkout->userid : $USER->id;

        // update coupons details
        if ($coupons and $products) {
            foreach ($coupons as $coupon) {
                $coupon_products = $DB->get_records_menu('local_ecommerce_relations', array('instanceid'=>$coupon->id, 'type'=>'coupon'), 'productid', 'productid,instanceid');
                if (!$coupon_products) continue;

                $items = $details = array();
                $price = $discountprice = 0;
                foreach ($products as $product) {
                    if (!isset($coupon_products[$product->id])) continue;

                    $items[] = $product->id;
                    $details[] = $product->name;
                    $price += $product->price;
                }

                $log = $DB->get_record('local_ecommerce_logs', array('id'=>$coupon->logid));
                if ($log) {
                    $log->status = payment::$STATUS_COMPLETED;
                    $log->timemodified = time();
                    $log->checkoutid = $checkout->id;
                    $log->items = implode(',', $items);
                    $log->details = implode(', ', $details);
                    $log->price = $price;
                    $log->discountprice = $price - (($coupon->discount / 100) * $price);
                    $log->discount = $coupon->discount;

                    $DB->update_record('local_ecommerce_logs', $log);
                }
            }
        }

        $logs = $DB->get_records('local_ecommerce_logs', array('userid'=>$userid, 'status'=>payment::$STATUS_PENDING));

        if (count($logs)) {
            foreach ($logs as $log) {
                $log->status = payment::$STATUS_COMPLETED;
                $log->timemodified = time();
                $log->checkoutid = $checkout->id;

                $DB->update_record('local_ecommerce_logs', $log);
            }
        }
    }
}
