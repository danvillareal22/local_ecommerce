<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class discount {

    public static $SHOPPINGCART_DISCOUNT_ALL_SELECTED = 0;
    public static $SHOPPINGCART_DISCOUNT_ANY_SELECTED   = 1;
    public static $SHOPPINGCART_DISCOUNT_ALL = 2;
    public static $SHOPPINGCART_DISCOUNT_CONDITION  = 3;

    public static function save_discount($data = null) {
        global $DB;

        if ($data->id) {
            $data->timemodified = time();
            $DB->update_record('local_ecommerce_discounts', $data);

            return $data->id;

        } else {
            $data->timemodified = time();
            $data->timecreated = time();

            return $DB->insert_record('local_ecommerce_discounts', $data);
        }
    }

    public static function delete_discount($id = 0) {
        global $DB, $USER, $SITE;

        if (has_capability('local/ecommerce:editdiscount', context_system::instance())) {

            $discount = $DB->get_record('local_ecommerce_discounts', array('id' => $id));

            // delete discount relations
            self::delete_discount_relations($discount);

            // delete discount
            $DB->delete_records('local_ecommerce_discounts', array('id' => $discount->id));
        }
    }

    public static function delete_discount_relations($discount = null) {
        global $DB, $SITE;

        $DB->delete_records('local_ecommerce_relations', array('instanceid' => $discount->id, 'type' => 'discount'));
    }

    public static function get_discount_products($discount) {
        global $CFG, $USER, $DB;

        return $DB->get_records_sql("SELECT p.* FROM {local_ecommerce_relations} r JOIN {local_ecommerce_products} p ON p.id = r.productid WHERE r.type = :type AND r.instanceid = :instanceid AND p.visible = 1", array('type'=>'discount', 'instanceid'=>$discount->id));
    }

}
