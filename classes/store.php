<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;
use MoodleQuickForm_autocomplete;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class store {

    public static function get_linked_products($productID) {
        global $DB;
        $returnArray = array();
        $linkedProducts = $DB->get_records_sql('SELECT l.linked_productid AS id, p.`name`
                                                      FROM {local_ecommerce_lkd_prods} l
                                                      LEFT JOIN {local_ecommerce_products} p ON p.id = l.linked_productid
                                                      WHERE l.productid = :productid', array('productid' => $productID));
        if (is_array($linkedProducts) && count ($linkedProducts) > 0) {
            foreach ($linkedProducts as $product) {
                $returnArray[$product->id] = $product->name;
            }
        }
        return $returnArray;
    }

    public static function get_store_linked_products($productID) {
        global $DB, $USER;
        $productArray = array();

        $userID = (isset($USER->id)) ? $USER->id : 0;
        $params = array('productid' => $productID, 'userid' => $userID, 'userid2' => $userID, 'userid3'=> $userID, 'userid4' => $userID, 'userid5' => $userID);
        $wheres = array("p.visible > 0", 'l.productid = :productid');
        $wheres = implode(" AND ", $wheres);
        $linkedProducts = $DB->get_records_sql('
            SELECT p.id, p.`name`, p.price, p.description, p.seats, p.associated_accreditation, p.accreditation_points, crt.id AS incart, s.sold, wl.wlsize, w.id AS inwaitlist, wish.id AS inwishlist, pch.purchased, r.rating AS userrating, r.review, r.rating AS alreadyrated, tr.total
            FROM {local_ecommerce_lkd_prods} l
            LEFT JOIN {local_ecommerce_products} p ON p.id = l.linked_productid
            LEFT JOIN {local_ecommerce} crt ON crt.productid = p.id AND crt.userid = :userid
            LEFT JOIN (SELECT instanceid, COUNT(id) as sold FROM {local_ecommerce_logs} WHERE `type` = "product" AND `status` = "completed" GROUP BY instanceid) s ON s.instanceid = p.id
            LEFT JOIN (SELECT productid, COUNT(id) as wlsize FROM {local_ecommerce_waitlist} GROUP BY productid) wl ON wl.productid = p.id 
            LEFT JOIN {local_ecommerce_waitlist} w ON w.productid = p.id AND w.userid = :userid2
            LEFT JOIN {local_ecommerce_wishlist} wish ON wish.productid = p.id AND wish.userid = :userid3
            LEFT JOIN (SELECT 1 AS purchased, instanceid FROM {local_ecommerce_logs} WHERE type="product" AND `status` = "completed" AND userid = :userid4 GROUP BY instanceid) pch ON pch.instanceid = p.id
            LEFT JOIN {local_ecommerce_ratings} r ON r.productid = p.id AND r.userid = :userid5
            LEFT JOIN (SELECT productid, ROUND((SUM(rating) / COUNT(id)), 0) as total FROM {local_ecommerce_ratings} GROUP BY productid) tr ON tr.productid = p.id
            WHERE ' . $wheres, $params
        );

        if (is_array($linkedProducts) && count($linkedProducts) > 0) {
            foreach ($linkedProducts as $product) {
                $product->price = format_float($product->price, 2, false);
                $product->image = product::get_product_image($product);
                $product->description = product::get_product_info($product);
                $product->currency = payment::get_currency();
                $product->userrating = !empty($product->userrating) ? $product->userrating : 1;
                $product->displaysold = get_config('local_ecommerce', 'display_sales');
                $ratingObject = review::get_rating($product->id);
                if (!empty($ratingObject->total) && !empty($ratingObject->count)) {
                    $product->rating = round($ratingObject->total / $ratingObject->count * 20, 0);
                    $product->ratingusers = $ratingObject->count;
                }
                $reviews = review::get_reviews($product->id);
                $product->reviews = $reviews['reviews'];
                $products_in_logs = (get_config('local_ecommerce', 'enablewaitlist')) ? (int)$product->sold + (int)$product->wlsize : $product->sold;
                $product->seatsallow = (
                    ($product->seats == 0 or
                        ($product->seats and $product->seats > $products_in_logs)
                    ) and !$product->inwaitlist);

                $producturl = new moodle_url('/local/ecommerce/view.php', array('id'=>$product->id));
                $product->producturl = $producturl->out();
                $productArray[] = $product;
            }
        }

        return array('total' => count($linkedProducts), 'products' => $productArray);
    }

    public static function get_store_products($physical, $search, $orderBy, $limit = null, $page = 0, $categoryid = 0) {
        global $DB, $USER, $CFG;

        $products = array();

        $basefields = array('id', 'categoryid', 'sortorder', 'name', 'price', 'description', 'seats', 'visible', 'featured', 'associated_accreditation', 'accreditation_points');
        $fields = 'p.' . join(',p.', $basefields);
        $asc = 'DESC';
        switch ($orderBy) {
            case 'recent':
                $orderBy = 'p.timemodified';
                break;
            case 'popular':
                $orderBy = 's.sold';
                break;
            case 'rated':
                $orderBy = 'tr.total';
                break;
            case 'price-lowest':
                $orderBy = 'p.price + 0';
                $asc = 'ASC';
                break;
            case 'price-highest':
                $orderBy = 'p.price + 0';
                break;
            default:
                $orderBy = 'p.featured';
                break;
        }
        $orderby = 'ORDER BY ' . $orderBy . ' ' . $asc;

        $userID = (isset($USER->id)) ? $USER->id : 0;
        $params = array('userid1' => $userID, 'userid2' => $userID, 'userid3' => $userID, 'userid4' => $userID, 'userid5' => $userID);

        $wheres = $countWheres = array("p.visible > 0");
        $countParams = array();

        if ($physical !== 'all') {
            $wheres[] = "p.physical = :physical";
            $countWheres[] = "p.physical = :physical";
            $params['physical']  = $physical;
            $countParams['physical']  = $physical;
        }
        // courses filter
        if ($categoryid) {
            $wheres[] = "p.categoryid = :category";
            $countWheres[] = "p.categoryid = :category";
            $params['category'] = $categoryid;
            $countParams['category'] = $categoryid;
        }

        if (!empty($search)) {
            $wheres[] .= "(" . $DB->sql_like('p.name', ':searchname', false, false, false). "  
                         OR " . $DB->sql_like('p.idnumber', ':searchidnumber', false, false, false).")";
            $countWheres[] .= "(" . $DB->sql_like('p.name', ':searchname', false, false, false). "  
                         OR " . $DB->sql_like('p.idnumber', ':searchidnumber', false, false, false).")";
            $params['searchname'] = '%' . $search . '%';
            $params['searchidnumber'] = '%' . $search . '%';
            $countParams['searchname'] = '%' . $search . '%';
            $countParams['searchidnumber'] = '%' . $search . '%';
        }

        $wheres = implode(" AND ", $wheres);
        //note: we can not use DISTINCT + text fields due to Oracle and MS limitations, that is why we have the subselect there
        $sql = "SELECT $fields, pc.name as categoryname, crt.id as incart, s.sold, wl.wlsize, w.id as inwaitlist, wish.id AS inwishlist, pch.purchased, r.rating AS userrating, r.review, r.rating AS alreadyrated, tr.total
              FROM {local_ecommerce_products} p
              LEFT JOIN {local_ecommerce_cat} pc ON pc.id = p.categoryid
              LEFT JOIN {local_ecommerce} crt ON crt.productid = p.id AND crt.userid = :userid1
              LEFT JOIN (SELECT instanceid, COUNT(id) as sold FROM {local_ecommerce_logs} WHERE type='product' AND `status` = 'completed' GROUP BY instanceid) s ON s.instanceid = p.id 
              LEFT JOIN (SELECT productid, COUNT(id) as wlsize FROM {local_ecommerce_waitlist} GROUP BY productid) wl ON wl.productid = p.id 
              LEFT JOIN {local_ecommerce_waitlist} w ON w.productid = p.id AND w.userid = :userid2
              LEFT JOIN {local_ecommerce_wishlist} wish ON wish.productid = p.id AND wish.userid = :userid3
              LEFT JOIN (SELECT 1 AS purchased, instanceid FROM {local_ecommerce_logs} WHERE type='product' AND `status` = 'completed' AND userid = :userid4 GROUP BY instanceid) pch ON pch.instanceid = p.id
              LEFT JOIN {local_ecommerce_ratings} r ON r.productid = p.id AND r.userid = :userid5
              LEFT JOIN (SELECT productid, ROUND((SUM(rating) / COUNT(id)), 1) as total FROM {local_ecommerce_ratings} GROUP BY productid) tr ON tr.productid = p.id 
                 WHERE $wheres 
                  $orderby";

        $start = $limit * $page;
        $dbproducts = $DB->get_records_sql($sql, $params, $start, $limit);
        if (count($dbproducts)) {
            $discounts = checkout::get_products_discounts($dbproducts);
            foreach ($dbproducts as $product) {
                $product->price = format_float($product->price, 2, false);
                $discountPrice = checkout::get_product_price($product, null, $discounts);
                $product->discountprice = $discountPrice['price'];
                $product->image = product::get_product_image($product);
                $product->description = product::get_product_info($product);
                $product->currency = payment::get_currency();
                $product->displaysold = get_config('local_ecommerce', 'display_sales');
                $product->enable_reviews = get_config('local_ecommerce', 'enable_reviews');
                $product->enable_review_changes = get_config('local_ecommerce', 'enable_review_changes');
                $product->userrating = !empty($product->userrating) ? $product->userrating : 1;
                $ratingObject = review::get_rating($product->id);
                if (!empty($ratingObject->total) && !empty($ratingObject->count)) {
                    $product->ratingnumber = round($ratingObject->total / $ratingObject->count, 1);
                    $product->rating = round($ratingObject->total / $ratingObject->count * 20, 0);
                    $product->ratingusers = $ratingObject->count;
                    $product->ratingplural = '';
                    if ($ratingObject->count > 1) {
                        $product->ratingplural = 's';
                    }
                }
                $reviews = review::get_reviews($product->id);
                $product->reviews = $reviews['reviews'];
                $products_in_logs = (get_config('local_ecommerce', 'enablewaitlist')) ? (int)$product->sold + (int)$product->wlsize : $product->sold;
                $product->seatsallow = (
                    ($product->seats == 0 or
                        ($product->seats and $product->seats > $products_in_logs)
                    ) and !$product->inwaitlist);

                $producturl = new moodle_url('/local/ecommerce/view.php', array('id'=>$product->id));
                $product->producturl = $producturl->out();
                $carturl = new moodle_url('/local/ecommerce');
                $product->carturl = $carturl->out();
                $product->socialmediasharelink = $CFG->wwwroot . '/local/ecommerce/view.php?id=' . $product->id;
                $products[] = $product;
            }
            $total = $DB->get_field_sql('
                SELECT COUNT(p.id) as total
                FROM {local_ecommerce_products} p
                WHERE ' . $wheres, $countParams);
        } else {
            $total = 0;
        }
        return array('total' => $total, 'products' => $products);
    }

    public static function load_more_products($type, $page, $limit, $search, $category, $orderBy) {
        global $PAGE;
        $context = context_system::instance();
        $PAGE->set_context($context);
        $renderer = $PAGE->get_renderer('local_ecommerce');
        if ($type == 0) {
            $categoryName = get_config('local_ecommerce', 'virtual_products_name');
        } else if ($type == 1) {
            $categoryName = get_config('local_ecommerce', 'physical_products_name');
        } else if ($type == 2) {
            $type = 'all';
            $categoryName = get_string('linked_products_teaser', 'local_ecommerce');
        }
        $products = self::get_store_products($type, $search, $orderBy, $limit, $page, $category);
        return $renderer->store_print_products($limit, $categoryName, $products, $page, $type);
    }
}
