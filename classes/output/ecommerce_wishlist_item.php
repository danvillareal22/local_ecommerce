<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Class containing dashboard data for ecommerce plugin
 *
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_ecommerce\output;
defined('MOODLE_INTERNAL') || die();

use renderable;
use renderer_base;
use templatable;

/**
 * Class containing products list for ecommerce plugin
 *
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class ecommerce_wishlist_item implements renderable, templatable {

    var $product = null;

    public function __construct($product) {
        $this->product = $product;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param \renderer_base $output
     * @return stdClass
     */
    public function export_for_template(renderer_base $output) {
        global $USER, $DB, $PAGE;

        $currency = \local_ecommerce\payment::get_currency();
        $viewurl = new \moodle_url('/local/ecommerce/view.php', array('id' => $this->product->id));

        return [
            'id' => $this->product->id,
            'name' => $this->product->name,
            'idnumber' => $this->product->idnumber,
            'price' => format_float($this->product->price, 2, false),
            'discountprice' => (isset($this->product->discountprice['price']) and $this->product->discountprice['price'] !== null and format_float($this->product->price, 2, false) != format_float($this->product->discountprice['price'], 2, false)) ? format_float($this->product->discountprice['price'], 2, false) : false,
            'imageurl' => \local_ecommerce\product::get_product_image($this->product),
            'viewurl' => $viewurl->out(),
            'currency' => $currency,
            'seatsallow' => $this->product->seatsallow,
            'enablewaitlist' => get_config('local_ecommerce', 'enablewaitlist')
        ];
    }
}
