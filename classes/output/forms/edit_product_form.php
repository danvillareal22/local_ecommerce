<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->libdir . '/form/autocomplete.php');

class edit_product_form extends moodleform {

    /**
     * Define the catalogue edit form
     */
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $editoroptions = $this->_customdata['editoroptions'];
        $attachmentoptions = $this->_customdata['attachmentoptions'];
        $data = $this->_customdata['data'];

        $enrol_config = get_config('enrol_ecommerce');

        $required = get_string('required');

        $mform->addElement('text', 'name', get_string('name', 'local_ecommerce'));
        $mform->addRule('name', $required, 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);

        $mform->addElement('text', 'idnumber', get_string('productcode', 'local_ecommerce'));
        $mform->setType('idnumber', PARAM_TEXT);

        $options = array(''=>get_string('selectcategory', 'local_ecommerce')) + \local_ecommerce\category::get_options($data->id, true);
        $mform->addElement('select', 'categoryid', get_string('parentcategory', 'local_ecommerce'), $options);
        $mform->addRule('categoryid', $required, 'required', null);
        $mform->setType('categoryid', PARAM_INT);

        $mform->addElement('text', 'price', get_string('price', 'local_ecommerce'));
        $mform->addRule('price', $required, 'required', null, 'client');
        $mform->addRule('price', null, 'numeric', null, 'client');
        $mform->setType('price', PARAM_FLOAT);

        /* seats options */
        $mform->addElement('advcheckbox', 'enableseats', get_string('enableseats', 'local_ecommerce'), '', array('group' => 1), array(0, 1));

        $mform->addElement('text', 'seats', get_string('seats', 'local_ecommerce'));
        $mform->setType('seats', PARAM_INT);
        $mform->setDefault('seats', 0);
        $mform->addRule('seats', null, 'numeric', null, 'client');
        $mform->disabledIf('seats', 'enableseats');

        $options = array('1'=>get_string('active', 'local_ecommerce'), '0'=>get_string('inactive', 'local_ecommerce'));
        $mform->addElement('select', 'visible', get_string('status', 'local_ecommerce'), $options);
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('visible', 1);

        $options = array('0' => get_string('no'), '1' => get_string('yes'));
        $mform->addElement('select', 'showitems', get_string('showproductitems', 'local_ecommerce'), $options);
        $mform->setType('showitems', PARAM_INT);
        $mform->setDefault('showitems', 1);

        /*$mform->addElement('advcheckbox', 'physical', get_string('physical_product', 'local_ecommerce'), '', array('group' => 1), array(0, 1));*/
        $mform->addElement('advcheckbox', 'featured', get_string('featured_item', 'local_ecommerce'), '', array('group' => 1), array(0, 1));

        $linkedProductOptions = array();

        $options = array(
            'multiple' => true,
            'ajax' => 'local_ecommerce/linkproducts',
            'noselectionstring' => get_string("selectproducts", 'local_ecommerce')
        );
        $mform->addElement('autocomplete', 'linked_products', get_string("linked_products", 'local_ecommerce'), $linkedProductOptions, $options);

        $mform->addElement('text', 'youtube', get_string('youtube_link', 'local_ecommerce'));
        $mform->setType('youtube', PARAM_TEXT);

        $mform->addElement('text', 'associated_accreditation', get_string('associated_accreditation', 'local_ecommerce'));
        $mform->setType('associated_accreditation', PARAM_TEXT);

        $mform->addElement('text', 'accreditation_points', get_string('accreditation_points', 'local_ecommerce'));
        $mform->setType('accreditation_points', PARAM_TEXT);

        /* product descriptiom */
        $mform->addElement('header', 'descriptionhdr', get_string("descriptionhdr", 'local_ecommerce'), array('class'=>'collapsed'));

        $mform->addElement('filemanager', 'productimage_filemanager', get_string('image', 'local_ecommerce'), null, $attachmentoptions);

        $editoroptions = array('maxfiles' => 1, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>context_system::instance(), 'subdirs'=>0);
        $mform->addElement('editor', 'description_editor', get_string('description', 'local_ecommerce'), null, $editoroptions);
        $mform->setType('description_editor', PARAM_RAW);

        if (core_tag_tag::is_enabled('local_ecommerce', 'local_ecommerce_products')) {
            $mform->addElement('tags', 'tags', get_string('tags'), array('itemtype' => 'local_ecommerce_products', 'component' => 'local_ecommerce'));
        }

        /* enrollments options */
        $mform->addElement('header', 'enrollmenthdr', get_string("enrollmenthdr", 'local_ecommerce'), array('class'=>'collapsed'));

        $options = array('optional' => true, 'defaultunit' => 86400);
        $mform->addElement('duration', 'enrolperiod', get_string('defaultperiod', 'local_ecommerce'), $options);
        if (isset($enrol_config->enrolperiod)) {
            $mform->setDefault('enrolperiod', $enrol_config->enrolperiod);
        }
        $mform->addHelpButton('enrolperiod', 'defaultperiod', 'enrol_manual');

        $options = array(
                0 => get_string('no'),
                1 => get_string('expirynotifyenroller', 'core_enrol'),
                2 => get_string('expirynotifyall', 'core_enrol')
        );
        $mform->addElement('select', 'enrolexpirynotify', get_string('expirynotify', 'local_ecommerce'), $options);
        if (isset($enrol_config->expirynotify)) {
            $mform->setDefault('enrolexpirynotify', $enrol_config->expirynotify);
        }

        $options = array('optional' => false, 'defaultunit' => 86400);
        $mform->addElement('duration', 'enrolexpirythreshold', get_string('expirythreshold', 'local_ecommerce'), $options);
        $mform->addHelpButton('enrolexpirythreshold', 'expirythreshold', 'core_enrol');
        if (isset($enrol_config->expirythreshold)) {
            $mform->setDefault('enrolexpirythreshold', $enrol_config->expirythreshold);
        }
        $mform->disabledIf('enrolexpirythreshold', 'enrolexpirynotify', 'eq', 0);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $this->add_action_buttons();

        $this->set_data($data);
    }

    public function validation($data, $files) {
        global $DB;
        $errors = parent::validation($data, $files);
        if (!empty($data['idnumber'])) {
            if ($existing = $DB->get_record('local_ecommerce_products', array('idnumber' => $data['idnumber']))) {
                if (!$data['id'] || $existing->id != $data['id']) {
                    $errors['idnumber'] = get_string('productidnumbertaken', 'error');
                }
            }
        }
        return $errors;
    }


    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output Used to do a final render of any components that need to be rendered for export.
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        ob_start();
        $this->display();
        $formhtml = ob_get_contents();
        ob_end_clean();

        return $formhtml;
    }
}
