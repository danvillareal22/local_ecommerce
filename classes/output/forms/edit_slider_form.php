<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->libdir . '/form/autocomplete.php');

class edit_slider_form extends moodleform {

    /**
     * Define the catalogue edit form
     */
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $attachmentoptions = $data = $this->_customdata['attachmentoptions'];
        $data = $this->_customdata['data'];
        $required = get_string('required');

        $mform->addElement('advcheckbox', 'enable_slider', get_string('enable_slider', 'local_ecommerce'), '', array('group' => 1), array(0, 1));

        $sliderOptions = array(
            'both' => get_string('internal_and_external', 'local_ecommerce'),
            'internal' => get_string('internal_only', 'local_ecommerce'),
            'external' => get_string('external_only', 'local_ecommerce')
        );
        $mform->addElement('select', 'display_slider', get_string('display_slider', 'local_ecommerce'), $sliderOptions);
        $mform->addRule('display_slider', $required, 'required', null);
        $mform->setType('display_slider', PARAM_TEXT);

        $mform->addElement('filemanager', 'slider_image_1_filemanager', get_string('slider_image_1', 'local_ecommerce'), null, $attachmentoptions);
        $mform->addHelpButton('slider_image_1_filemanager', 'slider_image', 'local_ecommerce');

        $mform->addElement('text', 'slider_caption_1', get_string('slider_caption_1', 'local_ecommerce'));
        $mform->setType('slider_caption_1', PARAM_TEXT);

        $mform->addElement('text', 'slider_url_1', get_string('slider_url_1', 'local_ecommerce'));
        $mform->setType('slider_url_1', PARAM_TEXT);

        $mform->addElement('filemanager', 'slider_image_2_filemanager', get_string('slider_image_2', 'local_ecommerce'), null, $attachmentoptions);
        $mform->addHelpButton('slider_image_2_filemanager', 'slider_image', 'local_ecommerce');

        $mform->addElement('text', 'slider_caption_2', get_string('slider_caption_2', 'local_ecommerce'));
        $mform->setType('slider_caption_2', PARAM_TEXT);

        $mform->addElement('text', 'slider_url_2', get_string('slider_url_2', 'local_ecommerce'));
        $mform->setType('slider_url_2', PARAM_TEXT);

        $mform->addElement('filemanager', 'slider_image_3_filemanager', get_string('slider_image_3', 'local_ecommerce'), null, $attachmentoptions);
        $mform->addHelpButton('slider_image_3_filemanager', 'slider_image', 'local_ecommerce');

        $mform->addElement('text', 'slider_caption_3', get_string('slider_caption_3', 'local_ecommerce'));
        $mform->setType('slider_caption_3', PARAM_TEXT);

        $mform->addElement('text', 'slider_url_3', get_string('slider_url_3', 'local_ecommerce'));
        $mform->setType('slider_url_3', PARAM_TEXT);

        $mform->addElement('filemanager', 'slider_image_4_filemanager', get_string('slider_image_4', 'local_ecommerce'), null, $attachmentoptions);
        $mform->addHelpButton('slider_image_4_filemanager', 'slider_image', 'local_ecommerce');

        $mform->addElement('text', 'slider_caption_4', get_string('slider_caption_4', 'local_ecommerce'));
        $mform->setType('slider_caption_4', PARAM_TEXT);

        $mform->addElement('text', 'slider_url_4', get_string('slider_url_4', 'local_ecommerce'));
        $mform->setType('slider_url_4', PARAM_TEXT);

        $mform->addElement('filemanager', 'slider_image_5_filemanager', get_string('slider_image_5', 'local_ecommerce'), null, $attachmentoptions);
        $mform->addHelpButton('slider_image_5_filemanager', 'slider_image', 'local_ecommerce');

        $mform->addElement('text', 'slider_caption_5', get_string('slider_caption_5', 'local_ecommerce'));
        $mform->setType('slider_caption_5', PARAM_TEXT);

        $mform->addElement('text', 'slider_url_5', get_string('slider_url_5', 'local_ecommerce'));
        $mform->setType('slider_url_5', PARAM_TEXT);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $this->add_action_buttons();

        $this->set_data($data);
    }

    public function validation($data, $files) {
        global $DB;
        $errors = parent::validation($data, $files);
        if (!empty($data['idnumber'])) {
            if ($existing = $DB->get_record('local_ecommerce_products', array('idnumber' => $data['idnumber']))) {
                if (!$data['id'] || $existing->id != $data['id']) {
                    $errors['idnumber'] = get_string('productidnumbertaken', 'error');
                }
            }
        }
        return $errors;
    }


    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output Used to do a final render of any components that need to be rendered for export.
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        ob_start();
        $this->display();
        $formhtml = ob_get_contents();
        ob_end_clean();

        return $formhtml;
    }
}
