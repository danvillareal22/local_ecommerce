<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class edit_discount_form extends moodleform {

    /**
     * Define the catalogue edit form
     */
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $data = $this->_customdata['data'];

        $required = get_string('required');

        $mform->addElement('text', 'name', get_string('name', 'local_ecommerce'));
        $mform->addRule('name', $required, 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);

        $mform->addElement('date_time_selector', 'starttime', get_string('starttime', 'local_ecommerce'), array('optional' => true));
        $mform->setDefault('starttime', 0);

        $mform->addElement('date_time_selector', 'endtime', get_string('endtime', 'local_ecommerce'), array('optional' => true));
        $mform->setDefault('endtime', 0);

        //$mform->addElement('duration', 'expiration', get_string('expiration', 'local_ecommerce'), array('optional' => true, 'defaultunit' => 86400));

        $mform->addElement('text', 'discount', get_string('discountinpercentage', 'local_ecommerce'));
        $mform->addRule('discount', $required, 'required', null, 'client');
        $mform->addRule('discount', null, 'numeric', null, 'client');
        $mform->setType('discount', PARAM_FLOAT);

        $options = array('1'=>get_string('active', 'local_ecommerce'), '0'=>get_string('inactive', 'local_ecommerce'));
        $mform->addElement('select', 'status', get_string('status', 'local_ecommerce'), $options);
        $mform->setType('status', PARAM_INT);
        $mform->setDefault('status', 1);

        $options = array(
                \local_ecommerce\discount::$SHOPPINGCART_DISCOUNT_ALL => get_string('allproducts', 'local_ecommerce'),
                \local_ecommerce\discount::$SHOPPINGCART_DISCOUNT_ALL_SELECTED => get_string('allselectedproducts', 'local_ecommerce'),
                \local_ecommerce\discount::$SHOPPINGCART_DISCOUNT_ANY_SELECTED => get_string('anyproduct', 'local_ecommerce'),
                \local_ecommerce\discount::$SHOPPINGCART_DISCOUNT_CONDITION => get_string('condition', 'local_ecommerce')
        );

        $mform->addElement('select', 'type', get_string('discount_type', 'local_ecommerce'), $options);
        $mform->setType('type', PARAM_INT);
        $mform->setDefault('type', \local_ecommerce\discount::$SHOPPINGCART_DISCOUNT_ALL);
        $mform->addHelpButton('type', 'discount_type', 'local_ecommerce');

        $mform->addElement('text', 'minnumber', get_string('minnumber', 'local_ecommerce'));
        $mform->setType('minnumber', PARAM_INT);
        $mform->addRule('minnumber', null, 'numeric', null, 'client');
        $mform->disabledIf('minnumber', 'type', 'neq', \local_ecommerce\discount::$SHOPPINGCART_DISCOUNT_CONDITION);

        $mform->addElement('text', 'maxnumber', get_string('maxnumber', 'local_ecommerce'));
        $mform->setType('maxnumber', PARAM_INT);
        $mform->addRule('maxnumber', null, 'numeric', null, 'client');
        $mform->disabledIf('maxnumber', 'type', 'neq', \local_ecommerce\discount::$SHOPPINGCART_DISCOUNT_CONDITION);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $this->add_action_buttons();

        if (isset($data->minnumber)) {
            $data->minnumber = ($data->minnumber) ? $data->minnumber : '';
        }

        if (isset($data->maxnumber)) {
            $data->maxnumber = ($data->maxnumber) ? $data->maxnumber : '';
        }

        $this->set_data($data);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }


    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output Used to do a final render of any components that need to be rendered for export.
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        ob_start();
        $this->display();
        $formhtml = ob_get_contents();
        ob_end_clean();

        return $formhtml;
    }
}

