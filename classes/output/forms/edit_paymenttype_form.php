<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class edit_paymenttype_form extends moodleform {

    /**
     * Define the catalogue edit form
     */
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $data = $this->_customdata['data'];

        $required = get_string('required');


        if ($data->type) {
            $path = $CFG->dirroot.'/local/ecommerce/payments/'.$data->type;

            $mform->addElement('hidden', 'type');
            $mform->setType('type', PARAM_TEXT);

            if (file_exists($path . '/locallib.php')) {
                require_once($path . '/locallib.php');

                $pluginclass = 'local_ecommerce_'.$data->type.'_payment';
                $plugin = new $pluginclass();

                $plugin->get_form_elements($data, $mform);
            }

            $this->add_action_buttons();
        } else {

            // select type firstly for new type
            $options = array(''=>get_string('selecttype', 'local_ecommerce')) + \local_ecommerce\payment::get_types();
            $mform->addElement('select', 'type', get_string('paymenttype', 'local_ecommerce'), $options);
            $mform->addRule('type', $required, 'required', null);
            $mform->setType('type', PARAM_TEXT);

            $this->add_action_buttons(true, get_string('continue'));
        }

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $this->set_data($data);
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output Used to do a final render of any components that need to be rendered for export.
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        ob_start();
        $this->display();
        $formhtml = ob_get_contents();
        ob_end_clean();

        return $formhtml;
    }
}

