<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->libdir . '/form/autocomplete.php');

class contact_form extends moodleform {

    /**
     * Define the catalogue edit form
     */
    public function definition() {

        $mform = $this->_form;
        $data = $this->_customdata['data'];
        $required = get_string('required');

        $mform->addElement('text', 'name', get_string('name'));
        $mform->addRule('name', $required, 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);

        $mform->addElement('text', 'email', get_string('email', 'local_ecommerce'));
        $mform->addRule('email', $required, 'required', null, 'client');
        $mform->addRule('email', get_string('err_email', 'form'), 'email', null, 'client');
        $mform->setType('email', PARAM_TEXT);

        $mform->addElement('text', 'subject', get_string('subject', 'local_ecommerce'));
        $mform->addRule('subject', $required, 'required', null, 'client');
        $mform->setType('subject', PARAM_TEXT);

        $mform->addElement('textarea', 'message', get_string('message', 'local_ecommerce'), 'wrap="virtual" rows="10" cols="50"');
        $mform->addRule('message', $required, 'required', null, 'client');
        $mform->setType('message', PARAM_TEXT);

        $mform->addElement('submit', 'save', get_string('send_message', 'local_ecommerce'));
        $mform->addElement('cancel', 'cancel', get_string('cancel'));

        $this->set_data($data);
    }

    public function validation($data, $files) {
        global $DB;
        $errors = parent::validation($data, $files);
        return $errors;
    }


    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output Used to do a final render of any components that need to be rendered for export.
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        ob_start();
        $this->display();
        $formhtml = ob_get_contents();
        ob_end_clean();

        return $formhtml;
    }
}
