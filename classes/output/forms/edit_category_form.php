<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class edit_category_form extends moodleform implements renderable, templatable {

    /**
     * Define the catalogue edit form
     */
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $editoroptions = $this->_customdata['editoroptions'];
        $attachmentoptions = $this->_customdata['attachmentoptions'];
        $data = $this->_customdata['data'];

        $required = get_string('required');

        $mform->addElement('text', 'name', get_string('name', 'local_ecommerce'));
        $mform->addRule('name', $required, 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);

        $mform->addElement('text', 'idnumber', get_string('categoryid', 'local_ecommerce'));
        $mform->setType('idnumber', PARAM_TEXT);

        //$options = array(0 => get_string('topcategory', 'local_ecommerce')) + local_ecommerce_categories_options($data->id);
        //$mform->addElement('select', 'parentid', get_string('parentcategory', 'local_ecommerce'), $options);
        $mform->addElement('hidden', 'parentid', 0);
        $mform->setDefault('parentid', 0);
        //$mform->addRule('parentid', $required, 'required', null);
        $mform->setType('parentid', PARAM_INT);

        $options = array('1'=>get_string('active', 'local_ecommerce'), '0'=>get_string('inactive', 'local_ecommerce'));
        $mform->addElement('select', 'visible', get_string('status', 'local_ecommerce'), $options);
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('visible', 1);

        $mform->addElement('filemanager', 'categoryimage_filemanager', get_string('image', 'local_ecommerce'), null, $attachmentoptions);

        $mform->addElement('editor', 'description_editor', get_string('description', 'local_ecommerce'), null, $editoroptions);
        $mform->setType('description_editor', PARAM_RAW);


        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $this->add_action_buttons();
        $this->set_data($data);
    }

    public function validation($data, $files) {
        global $DB;
        $errors = parent::validation($data, $files);
        if (!empty($data['idnumber'])) {
            if ($existing = $DB->get_record('local_ecommerce_cat', array('idnumber' => $data['idnumber']))) {
                if (!$data['id'] || $existing->id != $data['id']) {
                    $errors['idnumber'] = get_string('categoryidnumbertaken', 'error');
                }
            }
        }
        return $errors;
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output Used to do a final render of any components that need to be rendered for export.
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        ob_start();
        $this->display();
        $formhtml = ob_get_contents();
        ob_end_clean();

        return $formhtml;
    }
}

