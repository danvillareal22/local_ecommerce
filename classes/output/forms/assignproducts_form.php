<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class assignproducts_form extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        global $DB;

        $mform = $this->_form;
        $data = $this->_customdata['data'];
        $type = $this->_customdata['type'];

        $options = array(
            'multiple' => true,
            'ajax' => 'local_ecommerce/assignproducts',
            'noselectionstring' => ' ',
            'placeholder' => get_string("enter_product_name", 'local_ecommerce')
        );
        $mform->addElement('autocomplete', 'products', strtoupper(get_string('searchplaceholder', 'local_ecommerce')), array(), $options);
        
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'itemtype');
        $mform->setType('itemtype', PARAM_TEXT);
        $mform->setDefault('itemtype', $type);

        $this->add_action_buttons();
        $this->set_data($data);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }


    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output Used to do a final render of any components that need to be rendered for export.
     * @return array
     */
    public function export_for_template(renderer_base $output) {
        ob_start();
        $this->display();
        $formhtml = ob_get_contents();
        ob_end_clean();

        return $formhtml;
    }
}

