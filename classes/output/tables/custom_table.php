<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class custom_table extends table_sql {


    function start_html() {
        global $OUTPUT;

        // Render button to allow user to reset table preferences.
        echo $this->render_reset_button();

        // Do we need to print initial bars?
        $this->print_initials_bar();

        /*if (in_array(TABLE_P_TOP, $this->showdownloadbuttonsat)) {
            echo $this->download_buttons();
        }*/

        $this->wrap_html_start();
        // Start of main data table

        echo html_writer::start_tag('div', array('class' => 'no-overflow'));
        echo html_writer::start_tag('table', $this->attributes);

    }

    /** SEBALE
     * This function is not part of the public api.
     */
    function download_buttons() {
        if ($this->is_downloadable() && !$this->is_downloading()) {
            $formats = core_plugin_manager::instance()->get_plugins_of_type('dataformat');

            $html = html_writer::start_tag('div', array('class'=>'report-export-panel'));
            foreach ($formats as $format){
                if ($format->name == 'csv') {
                    $url = new moodle_url($this->baseurl, $this->baseurl->params() + array('download'=>$format->name, 'sesskey'=>sesskey()));
                    $html .= html_writer::link($url, html_writer::tag('i', '', array('class'=>'eicon-'.$format->name)), array('title'=>get_string('dataformat', $format->component)));
                }
            }

            $html .= html_writer::end_tag('div');

            return $html;
        } else {
            return '';
        }
    }

    /**
     * Export this data so it can be used as the context for a mustache template.
     *
     * @param renderer_base $output Used to do a final render of any components that need to be rendered for export.
     * @return array
     */
    public function export_for_template(renderer_base $output, $limit = 20) {

        ob_start();
        $this->out($limit, true);
        $tablehtml = ob_get_contents();
        ob_end_clean();

        return $tablehtml;
    }

}

