<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class payments_table extends custom_table {

    public $search = '';
    public $types = array();

    function __construct($uniqueid, $search, $filter) {
        global $CFG, $USER, $PAGE, $DB;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        $this->search    = $search;
        $this->types     = \local_ecommerce\payment::get_types();

        $columns = array('name', 'type', 'timecreated', 'status', 'actions');
        $headers = array(
            get_string('name', 'local_ecommerce'),
            get_string('type', 'local_ecommerce'),
            get_string('created', 'local_ecommerce'),
            get_string('status', 'local_ecommerce'),
            get_string('actions', 'local_ecommerce')
        );

        $this->no_sorting('actions');
        $this->is_collapsible = false;

        $this->define_columns($columns);
        $this->define_headers($headers);

        $fields = "p.id, p.name, p.type, p.timecreated, p.status";
        $from = "{local_ecommerce_payments} p";

        $where = 'p.id > 0';
        $params = array();

        // search
        if (!empty($search)) {
            $where .= " AND " . $DB->sql_like('p.type', ':searchcode', false, false, false);
            $params = array('searchcode' => '%' . $search . '%');
        }

        // filter visibility
        if ($filter == 2) {
            $where .= ' AND p.status = :status';
            $params['status'] = 1;
        } else if ($filter == 1) {
            $where .= ' AND p.status = :status';
            $params['status'] = 0;
        }

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($PAGE->url);
    }

    function col_type($values) {
        return (!empty($values->type) and isset($this->types[$values->type])) ? $this->types[$values->type] : '';
    }

    function col_timecreated($values) {
        return (!empty($values->timecreated)) ? userdate($values->timecreated, get_string('strftimedatefullshort', 'langconfig')) : '-';
    }

    function col_status($values)
    {
        $status = get_string('active', 'local_ecommerce');
        if (!$values->status) {
            $status = get_string('inactive', 'local_ecommerce');
        }
        return '<div class = "store-table-' . strtolower($status) . '"><i class = "fa fa-circle"></i>&nbsp;' . $status . '</div>';
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $PAGE;

        $systemcontext   = context_system::instance();
        if (!has_capability('local/ecommerce:managepaymenttypes', $systemcontext)){
            return '';
        }

        $urlparams = array('id' => $values->id, 'sesskey' => sesskey());
        $editURL = new moodle_url('/local/ecommerce/payments/edit.php', $urlparams);
        $deleteURL = new moodle_url('/local/ecommerce/payments/edit.php', $urlparams + array('action' => 'delete'));
        if ($values->status) {
            $toggleURL = new moodle_url('/local/ecommerce/payments/edit.php', $urlparams + array('action' => 'hide'));
            $toggleName = get_string('hide');
            $toggleIcon = 'fa fa-eye';
        } else {
            $toggleURL = new moodle_url('/local/ecommerce/payments/edit.php', $urlparams + array('action' => 'show'));
            $toggleName = get_string('show');
            $toggleIcon = 'fa fa-eye-slash';
        }

        $renderer = $PAGE->get_renderer('local_ecommerce');
        $params = array(
            'id' => $values->id,
            'buttons' => array(
                array(
                    'name' => get_string('edit'),
                    'icon' => 'fa fa-edit',
                    'url' => $editURL
                ),
                array(
                    'name' => get_string('delete'),
                    'icon' => 'fa fa-trash',
                    'url' => $deleteURL
                ),
                array(
                    'name' => $toggleName,
                    'icon' => $toggleIcon,
                    'url' => $toggleURL
                )
            )
        );

        return $renderer->store_print_action_icons($params);
    }
}

