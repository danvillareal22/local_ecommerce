<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class report3_table extends custom_table {

    public $currency = '';

    function __construct($uniqueid, $params) {
        global $CFG, $PAGE, $DB;

        parent::__construct($uniqueid);
        $this->currency = ($params['download']) ? \local_ecommerce\payment::get_currency('code').' ' : \local_ecommerce\payment::get_currency('symbol');

        $columns = array('timemodified', 'product', 'productcode', 'category', 'customer', 'price', 'amount', 'balance', 'paymenttype');
        $headers = array(
                get_string('date', 'local_ecommerce'),
                get_string('product', 'local_ecommerce'),
                get_string('productcode', 'local_ecommerce'),
                get_string('category', 'local_ecommerce'),
                get_string('customer', 'local_ecommerce'),
                get_string('salesprice', 'local_ecommerce'),
                get_string('amount', 'local_ecommerce'),
                get_string('balance', 'local_ecommerce'),
                get_string('paymenttype', 'local_ecommerce')
        );

        $this->define_headers($headers);
        $this->define_columns($columns);

        $this->sortable(true, 'product', SORT_DESC);
        $this->is_collapsible = false;
        $this->downloadable = true;

        $sql_params = array('type'=>'product');

        $userfields = get_all_user_name_fields(true, 'u');

        $fields = "l.id, l.userid, p.name as product, p.idnumber as productcode, c.name as category, CONCAT(u.firstname, ' ', u.lastname) as customer, l.price, l.discountprice as amount, l.discountprice as amount, l.status as balance, l.timemodified, pm.name as paymenttype, ch.paymentid, $userfields";
        $from = "{local_ecommerce_logs} l
                    LEFT JOIN {local_ecommerce_products} p ON p.id = l.instanceid
                    LEFT JOIN {local_ecommerce_checkout} ch ON ch.id = l.checkoutid 
                    LEFT JOIN {local_ecommerce_payments} pm ON pm.id = ch.paymentid 
                    LEFT JOIN {local_ecommerce_cat} c ON c.id = p.categoryid 
                    LEFT JOIN {user} u ON u.id = l.userid";
        $where = "p.id > 0 AND l.type = :type ";

        // search
        if (!empty($params['search'])) {
            $search = $params['search'];
            $where .= " AND (" . $DB->sql_like('concat_ws(\' \', u.firstname, u.lastname)', ':searchfullname1', false, false, false)."
                         OR " . $DB->sql_like('concat_ws(\' \', u.lastname, u.firstname)', ':searchfullname2', false, false, false)."
                         OR " . $DB->sql_like('p.name', ':searchproduct', false, false, false)."
                         OR " . $DB->sql_like('p.idnumber', ':searchidnumber', false, false, false)."
                         OR " . $DB->sql_like('pm.name', ':paymenttype', false, false, false)."
                         OR " . $DB->sql_like('c.name', ':searchcategory', false, false, false).")";
            $sql_params['searchfullname1'] = '%' . $search . '%';
            $sql_params['searchfullname2'] = '%' . $search . '%';
            $sql_params['searchproduct'] = '%' . $search . '%';
            $sql_params['searchidnumber'] = '%' . $search . '%';
            $sql_params['searchcategory'] = '%' . $search . '%';
            $sql_params['paymenttype'] = '%' . $search . '%';
        }

        // date range
        if (!empty($params['daterange']['timestart']) and !empty($params['daterange']['timefinish'])) {
            $where .= " AND l.timemodified BETWEEN :timestart AND :timefinish";
            $sql_params['timestart'] = $params['daterange']['timestart'];
            $sql_params['timefinish'] = $params['daterange']['timefinish'];
        }

        $this->set_sql($fields, $from, $where, $sql_params);
        $this->define_baseurl($PAGE->url);
    }

    function col_customer($values) {
        $user = $values;
        $user->id = $values->userid;

        return fullname($user);
    }

    function col_timemodified($values) {
        return ($values->timemodified) ? userdate($values->timemodified, get_string('strftimedate', 'langconfig')) : '-';
    }

    function col_price($values) {
        return ($values->price) ? $this->currency.format_float($values->price, 2, true) : '-';
    }

    function col_amount($values) {
        return ($values->amount) ? $this->currency.format_float($values->amount, 2, true) : '-';
    }

    function col_balance($values) {
        return ($values->balance != \local_ecommerce\payment::$STATUS_PENDING and $values->amount) ? $this->currency . format_float($values->amount, 2, true) : $this->currency . '0';
    }

    function col_paymenttype($values) {
        if ($values->paymenttype) {
            return $values->paymenttype;
        }else if ($values->balance == \local_ecommerce\payment::$STATUS_COMPLETED) {
            return (isset($values->paymentid) && $values->paymentid AND $values->paymenttype) ? $values->paymenttype : get_string('manual', 'local_ecommerce');
        } else {
            return '-';
        }
    }
}