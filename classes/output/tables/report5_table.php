<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class report5_table extends custom_table {

    public $currency = '';

    function __construct($uniqueid, $params) {
        global $CFG, $PAGE, $DB;

        parent::__construct($uniqueid);
        $this->currency = ($params['download']) ? \local_ecommerce\payment::get_currency('code').' ' : \local_ecommerce\payment::get_currency('symbol');

        $columns = array('discountname', 'discount', 'starttime', 'endtime', 'customer', 'products', 'datediscountused', 'totalsaved');
        $headers = array(
                get_string('discount', 'local_ecommerce'),
                get_string('discountwithpercent', 'local_ecommerce'),
                get_string('starttime', 'local_ecommerce'),
                get_string('endtime', 'local_ecommerce'),
                get_string('customer', 'local_ecommerce'),
                get_string('productsusedon', 'local_ecommerce'),
                get_string('datediscountused', 'local_ecommerce'),
                get_string('totalsaved', 'local_ecommerce')
        );

        $this->define_headers($headers);
        $this->define_columns($columns);

        $this->sortable(true, 'datediscountused', SORT_DESC);
        $this->is_collapsible = false;
        $this->downloadable = true;

        $sql_params = array('type'=>'discount');

        $userfields = get_all_user_name_fields(true, 'u');

        $fields = "l.id, l.userid, d.name as discountname, d.discount, d.starttime, d.endtime, CONCAT(u.firstname, ' ', u.lastname) as customer, l.details as products, l.timemodified as datediscountused, (l.price - l.discountprice) as totalsaved, $userfields";
        $from = "{local_ecommerce_logs} l
                    LEFT JOIN {local_ecommerce_discounts} d ON d.id = l.instanceid
                    LEFT JOIN {local_ecommerce_checkout} ch ON ch.id = l.checkoutid 
                    LEFT JOIN {user} u ON u.id = l.userid";
        $where = "d.id > 0 AND l.type = :type ";

        // search
        if (!empty($params['search'])) {
            $search = $params['search'];
            $where .= " AND (" . $DB->sql_like('concat_ws(\' \', u.firstname, u.lastname)', ':searchfullname1', false, false, false)."
                         OR " . $DB->sql_like('concat_ws(\' \', u.lastname, u.firstname)', ':searchfullname2', false, false, false)."
                         OR " . $DB->sql_like('d.name', ':searchdiscount', false, false, false)."
                         OR " . $DB->sql_like('l.details', ':searchitemname', false, false, false).")";
            $sql_params['searchfullname1'] = '%' . $search . '%';
            $sql_params['searchfullname2'] = '%' . $search . '%';
            $sql_params['searchdiscount'] = '%' . $search . '%';
            $sql_params['searchitemname'] = '%' . $search . '%';
        }

        // date range
        if (!empty($params['daterange']['timestart']) and !empty($params['daterange']['timefinish'])) {
            $where .= " AND l.timemodified BETWEEN :timestart AND :timefinish";
            $sql_params['timestart'] = $params['daterange']['timestart'];
            $sql_params['timefinish'] = $params['daterange']['timefinish'];
        }

        $this->set_sql($fields, $from, $where, $sql_params);
        $this->define_baseurl($PAGE->url);
    }

    function col_customer($values) {
        $user = $values;
        $user->id = $values->userid;

        return fullname($user);
    }

    function col_starttime($values) {
        return ($values->starttime) ? userdate($values->starttime, get_string('strftimerecentfull', 'langconfig')) : '-';
    }

    function col_endtime($values) {
        return ($values->endtime) ? userdate($values->endtime, get_string('strftimerecentfull', 'langconfig')) : '-';
    }

    function col_datediscountused($values) {
        return ($values->datediscountused) ? userdate($values->datediscountused, get_string('strftimerecentfull', 'langconfig')) : '-';
    }

    function col_discount($values) {
        return ($values->discount) ? format_float($values->discount, 2, true) : '-';
    }

    function col_totalsaved($values) {
        return ($values->totalsaved) ? format_float($values->totalsaved, 2, true) : '-';
    }

}