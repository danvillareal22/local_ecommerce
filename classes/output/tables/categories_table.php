<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class categories_table extends custom_table {

    public $search = '';
    public $parentid = '';

    function __construct($uniqueid, $parentid, $search, $filter) {
        global $CFG, $USER, $PAGE, $DB;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        $this->search   = $search;
        $this->parentid = $parentid;

        $columns = array('sortorder', 'name', 'idnumber', 'products', 'status', 'actions');
        $headers = array(
            get_string('sort_order', 'local_ecommerce'),
            get_string('name', 'local_ecommerce'),
            get_string('categoryid', 'local_ecommerce'),
            get_string('products', 'local_ecommerce'),
            get_string('status', 'local_ecommerce'),
            get_string('actions', 'local_ecommerce')
        );

        $this->sortable(true, 'sortorder');
        $this->no_sorting('name');
        $this->no_sorting('idnumber');
        $this->no_sorting('products');
        $this->no_sorting('actions');
        $this->is_collapsible = false;

        $this->define_columns($columns);
        $this->define_headers($headers);

        $fields = "c.sortorder, c.name, c.idnumber, c.visible AS status, c.id, c.visible, cp.products";
        $from = "{local_ecommerce_cat} c
                    LEFT JOIN (
                        SELECT COUNT(id) as products, categoryid 
                          FROM {local_ecommerce_products} 
                      GROUP BY categoryid
                      ) cp ON cp.categoryid = c.id ";

        $where = 'c.id > 0 AND c.parentid = :parentid';
        $params = array('parentid' => $parentid);

        // search
        if (!empty($search)) {
            $where .= " AND " . $DB->sql_like('c.name', ':searchname', false, false, false). "  
                         OR " . $DB->sql_like('c.idnumber', ':searchidnumber', false, false, false);
            $params += array('searchname' => '%' . $search . '%', 'searchidnumber' => '%' . $search . '%');
        }

        // filter visibility
        if ($filter == 2) {
            $where .= ' AND c.visible = :visible';
            $params['visible'] = 1;
        } else if ($filter == 1) {
            $where .= ' AND c.visible = :visible';
            $params['visible'] = 0;
        }

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($PAGE->url);
    }

    /*function col_name($values) {
        return html_writer::link(new moodle_url('/local/ecommerce/categories/index.php', array('parentid' => $values->id)), $values->name);
    }*/

    function col_products($values) {
        return ($values->products) ? $values->products : '-';
    }

    function col_status($values)
    {
        $status = get_string('active', 'local_ecommerce');
        if (!$values->status) {
            $status = get_string('inactive', 'local_ecommerce');
        }
        return '<div class = "store-table-' . strtolower($status) . '"><i class = "fa fa-circle"></i>&nbsp;' . $status . '</div>';
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $PAGE;

        $systemcontext   = context_system::instance();
        if (!has_capability('local/ecommerce:editcategory', $systemcontext)){
            return '';
        }

        $urlparams = array('id' => $values->id, 'sesskey' => sesskey());
        $editURL = new moodle_url('/local/ecommerce/categories/edit.php', $urlparams);
        $deleteURL = new moodle_url('/local/ecommerce/categories/edit.php', $urlparams + array('action' => 'delete'));
        if ($values->visible) {
            $toggleURL = new moodle_url('/local/ecommerce/categories/edit.php', $urlparams + array('action' => 'hide'));
            $toggleName = get_string('hide');
            $toggleIcon = 'fa fa-eye';
        } else {
            $toggleURL = new moodle_url('/local/ecommerce/categories/edit.php', $urlparams + array('action' => 'show'));
            $toggleName = get_string('show');
            $toggleIcon = 'fa fa-eye-slash';
        }

        $renderer = $PAGE->get_renderer('local_ecommerce');
        $params = array(
            'id' => $values->id,
            'buttons' => array(
                array(
                    'name' => get_string('edit'),
                    'icon' => 'fa fa-edit',
                    'url' => $editURL
                ),
                array(
                    'name' => get_string('delete'),
                    'icon' => 'fa fa-trash',
                    'url' => $deleteURL
                ),
                array(
                    'name' => $toggleName,
                    'icon' => $toggleIcon,
                    'url' => $toggleURL
                )
            )
        );

        if (empty($this->search)) {
            $addMoveIcon = false;
            if ($this->currentrow > 0) {
                $toggleURL = new moodle_url('/local/ecommerce/categories/edit.php', $urlparams + array('action' => 'moveup'));
                $toggleName = get_string('moveup');
                $toggleIcon = 'fa fa-angle-double-up';
                $addMoveIcon = true;
            }

            if ($this->totalrows != $this->currentrow+1) {
                $toggleURL = new moodle_url('/local/ecommerce/categories/edit.php', $urlparams + array('action' => 'movedown'));
                $toggleName = get_string('movedown');
                $toggleIcon = 'fa fa-angle-double-down';
                $addMoveIcon = true;
            }
            if ($addMoveIcon) {
                $params['buttons'][] = array(
                    'name' => $toggleName,
                    'icon' => $toggleIcon,
                    'url' => $toggleURL
                );
            }
        }

        return $renderer->store_print_action_icons($params);
    }
}

