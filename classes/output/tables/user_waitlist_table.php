<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class user_waitlist_table extends custom_table {

    public $search = '';
    public $categoryid = '';
    public $currency = '';

    function __construct($uniqueid) {
        global $CFG, $USER, $PAGE, $DB;

        parent::__construct($uniqueid);
        $this->currency = \local_ecommerce\payment::get_currency('symbol');

        $systemcontext   = context_system::instance();

        $columns = array('image', 'product', 'price', 'timemodified', 'seatnumber', 'actions');
        $headers = array(
            '&nbsp;',
            get_string('product', 'local_ecommerce'),
            get_string('price', 'local_ecommerce'),
            get_string('created', 'local_ecommerce'),
            get_string('seatnumber', 'local_ecommerce'),
            get_string('actions', 'local_ecommerce')
        );

        $this->sortable(true, 'timemodified', SORT_DESC);
        $this->no_sorting('actions');
        $this->no_sorting('seatnumber');
        $this->is_collapsible = false;

        $this->define_columns($columns);
        $this->define_headers($headers);

        $fields = "w.*, w.timemodified as seatnumber, p.name as product, p.price";
        $from = "{local_ecommerce_waitlist} w
                    LEFT JOIN {local_ecommerce_products} p ON p.id = w.productid";

        $where = 'p.id > 0 AND w.userid = :userid';
        $params = array('userid' => $USER->id);

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($PAGE->url);
    }

    function col_image($values) {
        $imageURL = \local_ecommerce\product::get_product_image($values);
        return '<img src = "' . $imageURL . '" alt = "' . $values->product . '" class = "ecommerce-store-table-thumbnail" />';
    }

    function col_product($values) {
        return html_writer::link(new moodle_url('/local/ecommerce/view.php', array('id'=>$values->productid)), $values->product);
    }

    function col_price($values) {
        return ($values->price) ? $this->currency.$values->price : $this->currency.'0';
    }

    function col_timemodified($values) {
        return ($values->timemodified) ? userdate($values->timemodified, get_string('strftimedate', 'langconfig')) : '-';
    }

    function col_seatnumber($values) {
        global $DB;

        return $DB->count_records_sql("SELECT COUNT(id) FROM {local_ecommerce_waitlist} WHERE productid = :productid AND timemodified <= :timemodified ORDER BY timemodified", ['productid'=>$values->productid, 'sent' => 0, 'timemodified' => $values->timemodified]);
    }

    function col_actions($values) {
        global $PAGE;

        $urlparams = array('id' => $values->id, 'sesskey' => sesskey());

        $aurl = new moodle_url('/local/ecommerce/waitlist.php', $urlparams + array('action' => 'delete'));
        $renderer = $PAGE->get_renderer('local_ecommerce');
        $params = array(
            'id' => $values->id,
            'buttons' => array(
                array(
                    'name' => get_string('delete'),
                    'icon' => 'fa fa-trash',
                    'url' => $aurl
                )
            )
        );

        return $renderer->store_print_action_icons($params);
    }
}

