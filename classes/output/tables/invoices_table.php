<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class invoices_table extends custom_table {

    public $currency = '';
    public $invoicepayment = false;

    function __construct($uniqueid, $params) {
        global $CFG, $PAGE, $DB;

        parent::__construct($uniqueid);
        $this->currency = ($params['download']) ? \local_ecommerce\payment::get_currency('code').' ' : \local_ecommerce\payment::get_currency('symbol');
        $this->invoicepayment = get_config('local_ecommerce', 'invoicepayment');

        $columns = array('orderid', 'customer', 'timeupdated', 'balance', 'amount_paid', 'status', 'actions');
        $headers = array(
                get_string('invoicenumber', 'local_ecommerce'),
                get_string('customer', 'local_ecommerce'),
                get_string('dateissued', 'local_ecommerce'),
                get_string('balance', 'local_ecommerce'),
                get_string('amount_paid', 'local_ecommerce'),
                get_string('status', 'local_ecommerce'),
                get_string('actions', 'local_ecommerce')
        );

        $this->define_headers($headers);
        $this->define_columns($columns);

        $this->sortable(true, 'timeupdated', SORT_DESC);
        $this->no_sorting('actions');
        $this->is_collapsible = false;
        $this->downloadable = true;

        $sql_params = array();

        $userfields = get_all_user_name_fields(true, 'u');

        $fields = "ch.id as orderid, ch.timeupdated, ch.amount as amount_paid, ch.amount as balance, ch.payment_status as status, ch.paymentid, ch.invoicepayment, u.email, ch.userid, CONCAT(u.firstname, ' ', u.lastname) as customer, $userfields";
        $from = "{local_ecommerce_checkout} ch JOIN {user} u ON u.id = ch.userid";
        $where = "u.deleted = 0 AND u.suspended = 0 AND u.confirmed = 1 AND ch.invoicepayment = 1";

        // search
        if (!empty($params['search'])) {
            $search = $params['search'];
            $where .= " AND (" . $DB->sql_like('ch.id', ':searchorderid', false, false, false). "  
                         OR " . $DB->sql_like('concat_ws(\' \', u.firstname, u.lastname)', ':searchfullname1', false, false, false)."
                         OR " . $DB->sql_like('concat_ws(\' \', u.firstname, u.lastname)', ':searchfullname2', false, false, false)."
                         OR " . $DB->sql_like('u.email', ':searchemail', false, false, false)."
                         OR " . $DB->sql_like('ch.item_name', ':searchitemname', false, false, false).")";
            $sql_params = array('searchorderid' => '%' . $search . '%', 'searchfullname1' => '%' . $search . '%', 'searchfullname2' => '%' . $search . '%', 'searchemail' => '%' . $search . '%', 'searchitemname' => '%' . $search . '%');
        }
        // status filter
        if (!empty($params['status'])) {
            $where .= " AND (" . $DB->sql_like('ch.payment_status', ':status', false, false, false). ")";
            $sql_params['status'] = '%' . $params['status'] . '%';
        }

        // date range
        if (!empty($params['daterange']['timestart']) and !empty($params['daterange']['timefinish'])) {
            $where .= " AND ch.timeupdated BETWEEN :timestart AND :timefinish";
            $sql_params['timestart'] = $params['daterange']['timestart'];
            $sql_params['timefinish'] = $params['daterange']['timefinish'];
        }

        $this->set_sql($fields, $from, $where, $sql_params);
        $this->define_baseurl($PAGE->url);
    }

    function col_customer($values) {
        $user = $values;
        $user->id = $values->userid;

        return fullname($user);
    }

    function col_timeupdated($values) {
        return ($values->timeupdated) ? userdate($values->timeupdated, get_string('strftimedate', 'langconfig')) : '-';
    }

    function col_balance($values) {
        return ($values->status == \local_ecommerce\payment::$STATUS_COMPLETED) ? $this->currency.'0' : $this->currency.$values->balance;
    }

    function col_amount_paid($values) {
        return ($values->status == \local_ecommerce\payment::$STATUS_COMPLETED) ? $this->currency.$values->balance : $this->currency.'0';
    }

    function col_status($values) {
        return ($values->status) ? get_string('status_'.$values->status, 'local_ecommerce') : '';
    }

    function col_paymenttype($values) {
        if ($values->status == \local_ecommerce\payment::$STATUS_COMPLETED) {
            return ($values->paymentid AND $values->paymenttype) ? $values->paymenttype : get_string('invoice', 'local_ecommerce');
        } else if ($values->status == \local_ecommerce\payment::$STATUS_PENDING and $values->invoicepayment) {
            return get_string('invoice', 'local_ecommerce');
        } else {
            return '-';
        }
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $USER;

        $buttons = array();

        $urlparams = array('id' => $values->orderid, 'sesskey' => sesskey());

        $aurl = new moodle_url('/local/ecommerce/sales/actions.php', $urlparams + array('action' => 'print'));
        $buttons[] = $OUTPUT->action_icon($aurl, new pix_icon('t/download', get_string('print', 'local_ecommerce'), 'core', array('class' => 'iconsmall')), null, array('target'=>'_blank'));

        $aurl = new moodle_url('/local/ecommerce/sales/actions.php', $urlparams + array('action' => 'mail'));
        $buttons[] = $OUTPUT->action_icon($aurl, new pix_icon('t/email', get_string('mail', 'local_ecommerce'), 'core', array('class' => 'iconsmall')), null);

        if ($this->invoicepayment and $values->status == \local_ecommerce\payment::$STATUS_PENDING and has_capability('local/ecommerce:approveinvoices', context_system::instance()) and $values->invoicepayment) {
            $buttons[] = $OUTPUT->action_icon('javascript:void(0);', new pix_icon('t/approve', get_string('approvepayment', 'local_ecommerce'), 'core', array('class' => 'iconsmall')), null, array('class'=>'action-icon invoice-approve', 'data-type-id'=>$values->orderid));
        }

        return implode(' ', $buttons);
    }

}