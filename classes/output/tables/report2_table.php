<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class report2_table extends custom_table {

    public $currency = '';

    function __construct($uniqueid, $params) {
        global $CFG, $PAGE, $DB;

        parent::__construct($uniqueid.$params['itemid']);
        $this->currency = ($params['download']) ? \local_ecommerce\payment::get_currency('code').' ' : \local_ecommerce\payment::get_currency('symbol');

        if ($params['itemid'] > 0) {

            $columns = array('orderid', 'customer', 'email', 'products', 'timeupdated', 'amount', 'subtotal', 'discount', 'status', 'invoice');
            $headers = array(
                    get_string('orderid', 'local_ecommerce'),
                    get_string('customer', 'local_ecommerce'),
                    get_string('email', 'local_ecommerce'),
                    get_string('products', 'local_ecommerce'),
                    get_string('paidon', 'local_ecommerce'),
                    get_string('amountpaid', 'local_ecommerce'),
                    get_string('total', 'local_ecommerce'),
                    get_string('discount', 'local_ecommerce'),
                    get_string('status', 'local_ecommerce'),
                    get_string('invoice', 'local_ecommerce')
            );

            $this->define_headers($headers);
            $this->define_columns($columns);

            $this->sortable(true, 'orderid', SORT_DESC);
            $this->is_collapsible = false;
            $this->downloadable = true;

            $sql_params = array('itemid' => $params['itemid']);

            $userfields = get_all_user_name_fields(true, 'u');

            $fields = "ch.id as orderid, ch.item_name as products, ch.timeupdated, ch.amount, ch.subtotal, ch.discount, ch.payment_status as status, u.email, ch.userid, CONCAT(u.firstname, ' ', u.lastname) as customer, ch.id as invoice, $userfields";
            $from = "{local_ecommerce_checkout} ch JOIN {user} u ON u.id = ch.userid";
            $where = "u.deleted = 0 AND u.suspended = 0 AND u.confirmed = 1 AND u.id = :itemid";

            // search
            if (!empty($params['search'])) {
                $search = $params['search'];
                $where .= " AND (" . $DB->sql_like('ch.id', ':searchorderid', false, false, false). "  
                         OR " . $DB->sql_like('concat_ws(\' \', u.firstname, u.lastname)', ':searchfullname1', false, false, false)."
                         OR " . $DB->sql_like('concat_ws(\' \', u.lastname, u.firstname)', ':searchfullname2', false, false, false)."
                         OR " . $DB->sql_like('u.email', ':searchemail', false, false, false)."
                         OR " . $DB->sql_like('ch.item_name', ':searchitemname', false, false, false).")";
                $sql_params['searchorderid'] = '%' . $search . '%';
                $sql_params['searchfullname1'] = '%' . $search . '%';
                $sql_params['searchfullname2'] = '%' . $search . '%';
                $sql_params['searchemail'] = '%' . $search . '%';
                $sql_params['searchitemname'] = '%' . $search . '%';
            }
            // status filter
            if (!empty($params['status'])) {
                $where .= " AND (" . $DB->sql_like('ch.payment_status', ':status', false, false, false). ")";
                $sql_params['status'] = '%' . $params['status'] . '%';
            }

            // date range
            if (!empty($params['daterange']['timestart']) and !empty($params['daterange']['timefinish'])) {
                $where .= " AND ch.timeupdated BETWEEN :timestart AND :timefinish";
                $sql_params['timestart'] = $params['daterange']['timestart'];
                $sql_params['timefinish'] = $params['daterange']['timefinish'];
            }

        } else {
            $columns = array('customer', 'email', 'income', 'amount', 'actions');
            $headers = array(
                    get_string('customer', 'local_ecommerce'),
                    get_string('email', 'local_ecommerce'),
                    get_string('income', 'local_ecommerce'),
                    get_string('amountpaid', 'local_ecommerce'),
                    get_string('actions', 'local_ecommerce')
            );

            $this->define_headers($headers);
            $this->define_columns($columns);

            $this->sortable(true, 'customer', SORT_DESC);
            $this->no_sorting('actions');
            $this->is_collapsible = false;
            $this->downloadable = true;

            $userfields = get_all_user_name_fields(true, 'u');
            $sql_params = array('pendingstatus' => \local_ecommerce\payment::$STATUS_PENDING,
                    'completedstatus' => \local_ecommerce\payment::$STATUS_COMPLETED);

            // date range
            $filter1 = $filter2 = '';
            if (!empty($params['daterange']['timestart']) and !empty($params['daterange']['timefinish'])) {
                $filter1 = " AND timeupdated BETWEEN :timestart1 AND :timefinish1";
                $sql_params['timestart1'] = $params['daterange']['timestart'];
                $sql_params['timefinish1'] = $params['daterange']['timefinish'];

                $filter2 = " AND timeupdated BETWEEN :timestart2 AND :timefinish2";
                $sql_params['timestart2'] = $params['daterange']['timestart'];
                $sql_params['timefinish2'] = $params['daterange']['timefinish'];
            }

            $fields =
                    "u.id, u.email, CONCAT(u.firstname, ' ', u.lastname) as customer, $userfields, b.income, t.amount, u.id as actions";
            $from = "{user} u 
                    LEFT JOIN (SELECT SUM(amount) as income, userid FROM {local_ecommerce_checkout} WHERE payment_status = :pendingstatus  $filter1 GROUP BY userid) b ON b.userid = u.id 
                    LEFT JOIN (SELECT SUM(amount) as amount, userid FROM {local_ecommerce_checkout} WHERE payment_status = :completedstatus $filter2 GROUP BY userid) t ON t.userid = u.id";
            $where = "u.deleted = 0 AND u.suspended = 0 AND u.confirmed = 1 AND (b.income IS NOT NULL OR t.amount IS NOT NULL)";

            // search
            if (!empty($params['search'])) {
                $search = $params['search'];
                $where .= " AND (" .
                        $DB->sql_like('concat_ws(\' \', u.firstname, u.lastname)', ':searchfullname1', false, false, false) . "
                         OR " .
                        $DB->sql_like('concat_ws(\' \', u.firstname, u.lastname)', ':searchfullname2', false, false, false) . "
                         OR " . $DB->sql_like('u.email', ':searchemail', false, false, false) . ")";
                $sql_params['searchfullname1'] = '%' . $search . '%';
                $sql_params['searchfullname2'] = '%' . $search . '%';
                $sql_params['searchemail'] = '%' . $search . '%';
            }
        }


        $this->set_sql($fields, $from, $where, $sql_params);
        $this->define_baseurl($PAGE->url);
    }

    function col_customer($values) {
        return fullname($values);
    }

    function col_income($values) {
        return ($values->income !== null) ? $this->currency.$values->income : $this->currency.'0';
    }

    function col_amount($values) {
        return ($values->amount !== null) ? $this->currency.$values->amount : $this->currency.'0';
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $USER;

        $buttons = array();
        $urlparams = array('id' => 2, 'itemid' => $values->id);

        $aurl = new moodle_url('/local/ecommerce/reports/report.php', $urlparams);
        $buttons[] = $OUTPUT->action_icon($aurl, new pix_icon('t/viewdetails', get_string('details', 'local_ecommerce'), 'core', array('class' => 'iconsmall')), null);

        return implode(' ', $buttons);
    }

    function col_timeupdated($values) {
        return ($values->timeupdated) ? userdate($values->timeupdated, get_string('strftimedate', 'langconfig')) : '-';
    }

    function col_subtotal($values) {
        return ($values->subtotal) ? $this->currency.$values->subtotal : '-';
    }

    function col_status($values) {
        return ($values->status) ? get_string('status_'.$values->status, 'local_ecommerce') : '';
    }

    function col_discount($values) {
        return ($values->discount) ? $this->currency.$values->discount : '-';
    }

    function col_invoice($values) {
        return ($values->invoice) ? $values->invoice : '-';
    }
}