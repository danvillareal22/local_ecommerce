<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class customers_table extends custom_table {

    public $currency = '';

    function __construct($uniqueid, $params) {
        global $CFG, $PAGE, $DB;

        parent::__construct($uniqueid);
        $this->currency = ($params['download']) ? \local_ecommerce\payment::get_currency('code').' ' : \local_ecommerce\payment::get_currency('symbol');

        $columns = array('customer', 'email', 'balance', 'totalpaid');
        $headers = array(
                get_string('customer', 'local_ecommerce'),
                get_string('email', 'local_ecommerce'),
                get_string('openbalance', 'local_ecommerce'),
                get_string('totalpaid', 'local_ecommerce')
        );

        $this->define_headers($headers);
        $this->define_columns($columns);

        $this->sortable(true, 'customer', SORT_DESC);
        $this->is_collapsible = false;
        $this->downloadable = true;

        $sql_params = array('pendingstatus' => \local_ecommerce\payment::$STATUS_PENDING, 'completedstatus' => \local_ecommerce\payment::$STATUS_COMPLETED);

        $userfields = get_all_user_name_fields(true, 'u');

        // date range
        $filter1 = $filter2 = '';
        if (!empty($params['daterange']['timestart']) and !empty($params['daterange']['timefinish'])) {
            $filter1 = " AND timeupdated BETWEEN :timestart1 AND :timefinish1";
            $sql_params['timestart1'] = $params['daterange']['timestart'];
            $sql_params['timefinish1'] = $params['daterange']['timefinish'];

            $filter2 = " AND timeupdated BETWEEN :timestart2 AND :timefinish2";
            $sql_params['timestart2'] = $params['daterange']['timestart'];
            $sql_params['timefinish2'] = $params['daterange']['timefinish'];
        }

        $fields = "u.id, u.email, CONCAT(u.firstname, ' ', u.lastname) as customer, $userfields, b.balance, t.totalpaid";
        $from = "{user} u 
                    LEFT JOIN (SELECT SUM(amount) as balance, userid FROM {local_ecommerce_checkout} WHERE payment_status = :pendingstatus  $filter1 GROUP BY userid) b ON b.userid = u.id 
                    LEFT JOIN (SELECT SUM(amount) as totalpaid, userid FROM {local_ecommerce_checkout} WHERE payment_status = :completedstatus $filter2 GROUP BY userid) t ON t.userid = u.id";
        $where = "u.deleted = 0 AND u.suspended = 0 AND u.confirmed = 1 AND (b.balance IS NOT NULL OR t.totalpaid IS NOT NULL)";

        // search
        if (!empty($params['search'])) {
            $search = $params['search'];
            $where .= " AND (" . $DB->sql_like('concat_ws(\' \', u.firstname, u.lastname)', ':searchfullname1', false, false, false)."
                         OR " . $DB->sql_like('concat_ws(\' \', u.firstname, u.lastname)', ':searchfullname2', false, false, false)."
                         OR " . $DB->sql_like('u.email', ':searchemail', false, false, false).")";
            $sql_params['searchfullname1'] = '%' . $search . '%';
            $sql_params['searchfullname2'] = '%' . $search . '%';
            $sql_params['searchemail'] = '%' . $search . '%';
        }

        $this->set_sql($fields, $from, $where, $sql_params);
        $this->define_baseurl($PAGE->url);
    }

    function col_customer($values) {
        return fullname($values);
    }

    function col_balance($values) {
        return ($values->balance !== null) ? $this->currency.$values->balance : $this->currency.'0';
    }

    function col_totalpaid($values) {
        return ($values->totalpaid !== null) ? $this->currency.$values->totalpaid : $this->currency.'0';
    }
}