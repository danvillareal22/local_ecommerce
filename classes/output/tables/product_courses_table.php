<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class product_courses_table extends custom_table {
    var $index = 1;
    function __construct($uniqueid, $product, $categoryid = 0) {
        global $CFG, $PAGE;

        parent::__construct($uniqueid);

        $columns = array('course', 'timemodified', 'actions');
        $headers = array(get_string('course'), get_string('assigned', 'local_ecommerce'), get_string('actions', 'local_ecommerce'));

        $this->define_headers($headers);
        $this->define_columns($columns);

        $this->sortable(false);
        $this->is_collapsible = false;

        $params = array('productid' => $product->id);
        $where = '';

        $fields = "DISTINCT c.id, c.fullname as course, pr.productid, pr.timemodified, '' as actions";
        $from = "{course} c, {local_ecommerce_relations} pr";
        $where = "c.category > 0 AND c.visible = 1 AND c.id = pr.instanceid AND pr.type = 'course' AND pr.productid = :productid $where 
                    ORDER BY pr.timemodified ASC";

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($PAGE->url);
    }

    function col_timemodified($values) {
        return ($values->timemodified) ? userdate($values->timemodified, get_string('strftimedate', 'langconfig')) : '-';
    }

    function col_actions($values) {
        global $CFG, $OUTPUT;

        $aurl = new moodle_url('/local/ecommerce/products/assigncourses.php', array('delete'=>'1', 'courseid'=>$values->id, 'id'=>$values->productid));

        return $OUTPUT->action_icon($aurl, new pix_icon('t/delete', get_string('delete'), 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('".get_string('deleterecordconfirmation', 'local_ecommerce')."')) return false;"));
    }
}