<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class assigned_products_table extends custom_table {

    protected $type = '';

    function __construct($uniqueid, $instance, $type) {
        global $CFG, $PAGE;

        parent::__construct($uniqueid);
        $this->type = $type;

        $columns = array('product', 'timemodified', 'actions');
        $headers = array(get_string('product', 'local_ecommerce'), get_string('assigned', 'local_ecommerce'), get_string('actions', 'local_ecommerce'));

        $this->define_headers($headers);
        $this->define_columns($columns);

        $this->sortable(false);
        $this->is_collapsible = false;

        $params = array('instanceid' => $instance->id, 'type' => $type);
        $where = '';

        $fields = "DISTINCT p.id, p.name as product, pr.instanceid, pr.timemodified, '' as actions";
        $from = "{local_ecommerce_products} p, {local_ecommerce_relations} pr";
        $where = "p.id > 0 AND p.visible = 1 AND p.id = pr.productid AND pr.type = :type AND pr.instanceid = :instanceid $where 
                    ORDER BY pr.timemodified ASC";

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($PAGE->url);
    }

    function col_timemodified($values) {
        return ($values->timemodified) ? userdate($values->timemodified, get_string('strftimedate', 'langconfig')) : '-';
    }

    function col_actions($values) {
        global $CFG, $OUTPUT;

        $aurl = new moodle_url('/local/ecommerce/'.$this->type.'s/assignproducts.php', array('delete'=>'1', 'productid'=>$values->id, 'id'=>$values->instanceid));

        return $OUTPUT->action_icon($aurl, new pix_icon('t/delete', get_string('delete'), 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('".get_string('deleterecordconfirmation', 'local_ecommerce')."')) return false;"));
    }
}