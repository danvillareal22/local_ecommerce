<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class waitlist_table extends custom_table {

    public $search = '';

    function __construct($uniqueid, $search = '') {
        global $CFG, $USER, $PAGE, $DB;

        parent::__construct($uniqueid);
        $this->currency = \local_ecommerce\payment::get_currency('symbol');

        $systemcontext   = context_system::instance();

        $columns = array('customer', 'product', 'seatkey', 'timemodified', 'seatnumber', 'actions');
        $headers = array(
            get_string('customer', 'local_ecommerce'),
            get_string('product', 'local_ecommerce'),
            get_string('key', 'local_ecommerce'),
            get_string('created', 'local_ecommerce'),
            get_string('seatnumber', 'local_ecommerce'),
            get_string('actions', 'local_ecommerce')
        );

        $this->sortable(true, 'timemodified');
        $this->no_sorting('seatnumber');
        $this->no_sorting('actions');
        $this->is_collapsible = false;

        $this->define_columns($columns);
        $this->define_headers($headers);

        $userfields = get_all_user_name_fields(true, 'u');

        $fields = "w.*, w.id as wid, w.timemodified as seatnumber, p.name as product, CONCAT(u.firstname, ' ', u.lastname) as customer, $userfields";
        $from = "{local_ecommerce_waitlist} w
                    LEFT JOIN {local_ecommerce_products} p ON p.id = w.productid
                    LEFT JOIN {user} u ON u.id = w.userid";

        $where = 'p.id > 0 AND u.deleted = 0 AND u.suspended = 0 AND u.confirmed = 1';
        $params = array();

        // search
        if (!empty($search)) {
            $where .= " AND (" . $DB->sql_like('concat_ws(\' \', u.firstname, u.lastname)', ':searchfullname1', false, false, false)."
                         OR " . $DB->sql_like('concat_ws(\' \', u.lastname, u.firstname)', ':searchfullname2', false, false, false)."
                         OR " . $DB->sql_like('u.email', ':searchemail', false, false, false)."
                         OR " . $DB->sql_like('p.name', ':searchitemname', false, false, false).")";
            $params = array('searchfullname1' => '%' . $search . '%', 'searchfullname2' => '%' . $search . '%', 'searchemail' => '%' . $search . '%', 'searchitemname' => '%' . $search . '%');
        }

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($PAGE->url);
    }

    function col_customer($values) {
        $user = $values;
        $user->id = $values->userid;

        return fullname($user);
    }

    function col_product($values) {
        return html_writer::link(new moodle_url('/local/ecommerce/view.php', array('id'=>$values->productid)), $values->product);
    }

    function col_timemodified($values) {
        return ($values->timemodified) ? userdate($values->timemodified, get_string('strftimedate', 'langconfig')) : '-';
    }

    function col_seatnumber($values) {
        global $DB;

        return $DB->count_records_sql("SELECT COUNT(id) FROM {local_ecommerce_waitlist} WHERE productid = :productid AND timemodified <= :timemodified ORDER BY timemodified", ['productid'=>$values->productid, 'sent' => 0, 'timemodified' => $values->timemodified]);
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $PAGE;

        if (!has_capability('local/ecommerce:editwaitlist', context_system::instance())) {
            return '';
        }

        $urlparams = array('id' => $values->wid, 'sesskey' => sesskey());
        $deleteURL = new moodle_url('/local/ecommerce/waitlist/index.php', $urlparams + array('action' => 'delete'));
        $mailURL = new moodle_url('/local/ecommerce/waitlist/index.php', $urlparams + array('action' => 'mail'));

        $renderer = $PAGE->get_renderer('local_ecommerce');
        $params = array(
            'id' => $values->id,
            'buttons' => array(
                array(
                    'name' => get_string('delete'),
                    'icon' => 'fa fa-trash',
                    'url' => $deleteURL
                ),
                array(
                    'name' => get_string('mail', 'local_ecommerce'),
                    'icon' => 'fa fa-envelope',
                    'url' => $mailURL
                )
            )
        );

        return $renderer->store_print_action_icons($params);
    }
}

