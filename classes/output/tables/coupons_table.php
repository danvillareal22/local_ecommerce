<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class coupons_table extends custom_table {

    public $search = '';

    function __construct($uniqueid, $search, $filter) {
        global $CFG, $USER, $PAGE, $DB;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        $this->search   = $search;

        $columns = array('code', 'starttime', 'endtime', 'discount', 'type', 'used', 'timecreated', 'status', 'actions');
        $headers = array(
            get_string('code', 'local_ecommerce'),
            get_string('starttime', 'local_ecommerce'),
            get_string('endtime', 'local_ecommerce'),
            get_string('discount', 'local_ecommerce'),
            get_string('coupon_type', 'local_ecommerce'),
            get_string('used', 'local_ecommerce'),
            get_string('created', 'local_ecommerce'),
            get_string('status', 'local_ecommerce'),
            get_string('actions', 'local_ecommerce')
        );

        $this->no_sorting('actions');
        $this->is_collapsible = false;

        $this->define_columns($columns);
        $this->define_headers($headers);

        $fields = "c.id, c.code, c.starttime, c.endtime, c.discount, c.type, c.timecreated, c.status, l.used";
        $from = "{local_ecommerce_coupons} c
                    LEFT JOIN (SELECT count(id) as used, instanceid FROM {local_ecommerce_logs} WHERE type = :type AND status = :status GROUP BY instanceid) l ON l.instanceid = c.id ";

        $where = 'c.id > 0';
        $params = array('type'=>'coupon', 'status'=>\local_ecommerce\payment::$STATUS_COMPLETED);

        // search
        if (!empty($search)) {
            $where .= " AND " . $DB->sql_like('c.code', ':searchcode', false, false, false);
            $params['searchcode'] = '%' . $search . '%';
        }

        // filter visibility
        if ($filter == 2) {
            $where .= ' AND c.status = :filterstatus';
            $params['filterstatus'] = 1;
        } else if ($filter == 1) {
            $where .= ' AND c.status = :filterstatus';
            $params['filterstatus'] = 0;
        }

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($PAGE->url);
    }

    function col_starttime($values) {
        return ($values->starttime) ? userdate($values->starttime, get_string('strftimerecentfull', 'langconfig')) : '-';
    }

    function col_endtime($values) {
        return ($values->endtime) ? userdate($values->endtime, get_string('strftimerecentfull', 'langconfig')) : '-';
    }

    function col_timecreated($values) {
        return (!empty($values->timecreated)) ? userdate($values->timecreated, get_string('strftimedatefullshort', 'langconfig')) : '-';
    }

    function col_discount($values) {
        return (!empty($values->discount)) ? get_string('discountpercents', 'local_ecommerce', $values->discount) : '';
    }

    function col_type($values) {
        switch ($values->type) {
            case \local_ecommerce\discount::$SHOPPINGCART_DISCOUNT_ALL:
                return get_string('allproducts', 'local_ecommerce');
                break;
            case \local_ecommerce\discount::$SHOPPINGCART_DISCOUNT_ANY_SELECTED:
                return get_string('anyproduct', 'local_ecommerce');
                break;
            default:
                return '-';
                break;
        }
    }

    function col_used($values) {
        return ($values->used) ? $values->used : '-';
    }

    function col_status($values)
    {
        $status = get_string('active', 'local_ecommerce');
        if (!$values->status) {
            $status = get_string('inactive', 'local_ecommerce');
        }
        return '<div class = "store-table-' . strtolower($status) . '"><i class = "fa fa-circle"></i>&nbsp;' . $status . '</div>';
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $PAGE;

        $systemcontext   = context_system::instance();
        if (!has_capability('local/ecommerce:editcoupons', $systemcontext)){
            return '';
        }

        $urlparams = array('id' => $values->id, 'sesskey' => sesskey());
        $editURL = new moodle_url('/local/ecommerce/coupons/edit.php', $urlparams);
        $deleteURL = new moodle_url('/local/ecommerce/coupons/edit.php', $urlparams + array('action' => 'delete'));
        if ($values->status) {
            $toggleURL = new moodle_url('/local/ecommerce/coupons/edit.php', $urlparams + array('action' => 'hide'));
            $toggleName = get_string('hide');
            $toggleIcon = 'fa fa-eye';
        } else {
            $toggleURL = new moodle_url('/local/ecommerce/coupons/edit.php', $urlparams + array('action' => 'show'));
            $toggleName = get_string('show');
            $toggleIcon = 'fa fa-eye-slash';
        }

        $renderer = $PAGE->get_renderer('local_ecommerce');
        $params = array(
            'id' => $values->id,
            'buttons' => array(
                array(
                    'name' => get_string('edit'),
                    'icon' => 'fa fa-edit',
                    'url' => $editURL
                ),
                array(
                    'name' => get_string('delete'),
                    'icon' => 'fa fa-trash',
                    'url' => $deleteURL
                ),
                array(
                    'name' => $toggleName,
                    'icon' => $toggleIcon,
                    'url' => $toggleURL
                )
            )
        );

        if (has_capability('local/ecommerce:assign', $systemcontext)) {
            $assignURL = new moodle_url('/local/ecommerce/coupons/assignproducts.php', $urlparams);
            $params['buttons'][] = array(
                'name' => get_string('assignproducts', 'local_ecommerce'),
                'icon' => 'fa fa-plus',
                'url' => $assignURL
            );
        }

        return $renderer->store_print_action_icons($params);
    }
}

