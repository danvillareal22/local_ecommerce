<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class reviews_table extends custom_table {

    function __construct($uniqueid, $productID, $search) {
        global $PAGE, $DB;

        parent::__construct($uniqueid);

        $columns = array('id', 'rating', 'review', 'name', 'username', 'actions');
        $headers = array(
            get_string('review_id', 'local_ecommerce'),
            get_string('rating', 'local_ecommerce'),
            get_string('review', 'local_ecommerce'),
            get_string('product', 'local_ecommerce'),
            get_string('username'),
            get_string('actions', 'local_ecommerce')
        );

        $this->sortable(true, 'id');
        $this->no_sorting('actions');
        $this->is_collapsible = false;

        $this->define_columns($columns);
        $this->define_headers($headers);

        $fields = "r.id, r.rating, r.review, p.name, u.username";
        $from = "{local_ecommerce_ratings} r
                    LEFT JOIN {local_ecommerce_products} p ON p.id = r.productid
                    JOIN {user} u ON u.id = r.userid";

        $where = array('p.visible' => 1);
        $params = array();

        if ($productID) {
            $where[] = "r.productid = :productid";
            $params = array('productid' => $productID);
        }

        // search
        if (!empty($search)) {
            $where[] = "(" . $DB->sql_like('p.name', ':searchname', false, false, false). "  
                         OR " . $DB->sql_like('u.username', ':searchusername', false, false, false). "  
                         OR " . $DB->sql_like('r.rating', ':searchrating', false, false, false).")";
            $params['searchname'] = '%' . $search . '%';
            $params['searchusername'] = '%' . $search . '%';
            $params['searchrating'] = '%' . $search . '%';
        }

        $wheres = implode(' AND ', $where);

        $this->set_sql($fields, $from, $wheres, $params);
        $this->define_baseurl($PAGE->url);
    }

    function col_id($values) {
        return html_writer::link(new moodle_url('/local/ecommerce/reviews/edit.php', array('id'=>$values->id)), $values->id);
    }

    function col_actions($values) {
        global $PAGE;

        $systemcontext   = context_system::instance();
        if (!has_capability('local/ecommerce:manage_reviews', $systemcontext)){
            return '';
        }

        $urlparams = array('id' => $values->id, 'sesskey' => sesskey());
        $deleteURL = new moodle_url('/local/ecommerce/reviews/edit.php', $urlparams + array('action' => 'delete'));

        $renderer = $PAGE->get_renderer('local_ecommerce');
        $params = array(
            'id' => $values->id,
            'buttons' => array(
                array(
                    'name' => get_string('delete'),
                    'icon' => 'fa fa-trash',
                    'url' => $deleteURL
                )
            )
        );

        return $renderer->store_print_action_icons($params);
    }
}

