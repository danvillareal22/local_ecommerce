<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class latest_orders_table extends custom_table {

    public $currency = '';

    function __construct($uniqueid, $daterange) {
        global $CFG, $PAGE;

        parent::__construct($uniqueid);
        $this->currency = \local_ecommerce\payment::get_currency();

        $columns = array('orderid', 'firstname', 'lastname', 'registered', 'products', 'timeupdated', 'amount', 'subtotal', 'discount');
        $headers = array(
                get_string('orderid', 'local_ecommerce'),
                get_string('firstname', 'local_ecommerce'),
                get_string('lastname', 'local_ecommerce'),
                get_string('registeredon', 'local_ecommerce'),
                get_string('products', 'local_ecommerce'),
                get_string('paidon', 'local_ecommerce'),
                get_string('amountpaid', 'local_ecommerce'),
                get_string('total', 'local_ecommerce'),
                get_string('discount', 'local_ecommerce')
        );

        $this->define_headers($headers);
        $this->define_columns($columns);

        $this->sortable(true, 'orderid');
        $this->is_collapsible = false;

        $params = array('status' => \local_ecommerce\payment::$STATUS_COMPLETED);
        $where = '';

        if (!empty($daterange['timefinish']) and !empty($daterange['timestart'])) {
            $where .= ' AND ch.timeupdated BETWEEN :timestart AND :timefinish';
            $params += array('timestart' => $daterange['timestart'], 'timefinish' => $daterange['timefinish']);
        }

        $fields = "ch.id as orderid, ch.item_name as products, ch.timeupdated, ch.amount, ch.subtotal, ch.discount, u.firstname, u.lastname, u.timecreated as registered";
        $from = "{local_ecommerce_checkout} ch JOIN {user} u ON u.id = ch.userid";
        $where = "ch.payment_status = :status AND u.deleted = 0 AND u.suspended = 0 AND u.confirmed = 1 $where";

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($PAGE->url);
    }

    function col_registered($values) {
        return ($values->registered) ? userdate($values->registered, get_string('strftimedate', 'langconfig')) : '-';
    }

    function col_timeupdated($values) {
        return ($values->timeupdated) ? userdate($values->timeupdated, get_string('strftimedate', 'langconfig')) : '-';
    }

    function col_amount($values) {
        return ($values->amount) ? $this->currency.$values->amount : '-';
    }

    function col_subtotal($values) {
        return ($values->subtotal) ? $this->currency.$values->subtotal : '-';
    }

    function col_discount($values) {
        return ($values->discount) ? $this->currency.$values->discount : '-';
    }

}