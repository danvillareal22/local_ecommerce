<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class purchases_table extends custom_table {

    public $currency = '';

    function __construct($uniqueid, $params) {
        global $CFG, $PAGE, $DB, $USER;

        parent::__construct($uniqueid);
        $this->currency = ($params['download']) ? \local_ecommerce\payment::get_currency('code').' ' : \local_ecommerce\payment::get_currency('symbol');

        $columns = array('orderid', 'products', 'timeupdated', 'amount', 'subtotal', 'discount');
        $headers = array(
                get_string('orderid', 'local_ecommerce'),
                get_string('products', 'local_ecommerce'),
                get_string('date_paid', 'local_ecommerce'),
                get_string('amountpaid', 'local_ecommerce'),
                get_string('total', 'local_ecommerce'),
                get_string('discount', 'local_ecommerce')
        );

        $this->define_headers($headers);
        $this->define_columns($columns);

        $this->sortable(true, 'orderid', SORT_DESC);
        $this->is_collapsible = false;
        $this->downloadable = true;

        $sql_params = array('uid' => $USER->id);

        $fields = "ch.id as orderid, ch.item_name as products, ch.timeupdated, ch.amount, ch.subtotal, ch.discount, ch.items";
        $from = "{local_ecommerce_checkout} ch";
        $where = "ch.payment_status = 'completed' AND ch.userid = :uid";

        // search
        if (!empty($params['search'])) {
            $search = $params['search'];
            $where .= " AND (" . $DB->sql_like('ch.id', ':searchorderid', false, false, false). "
                         OR " . $DB->sql_like('ch.item_name', ':searchitemname', false, false, false).")";
            $sql_params['searchorderid'] = '%' . $search . '%';
            $sql_params['searchitemname'] = '%' . $search . '%';
        }
        
        // date range
        if (!empty($params['daterange']['timestart']) and !empty($params['daterange']['timefinish'])) {
            $where .= " AND ch.timeupdated BETWEEN :timestart AND :timefinish";
            $sql_params['timestart'] = $params['daterange']['timestart'];
            $sql_params['timefinish'] = $params['daterange']['timefinish'];
        }

        $this->set_sql($fields, $from, $where, $sql_params);
        $urlParams = $params;
        if (!empty($urlParams['daterange'])) {
            $urlParams['daterange'] = $urlParams['daterange']['daterange'];
        }
        $this->define_baseurl($PAGE->url);
    }

    function col_orderid($values) {
        if (!$this->is_downloading()) {
            $itemsArray = explode(',', $values->items);
            $values->id = $itemsArray[0];
            $imageURL = \local_ecommerce\product::get_product_image($values);
            return $values->id . ' <img src = "' . $imageURL . '" alt = "' . $values->products . '" class = "ecommerce-store-table-thumbnail" />';
        }
        return $values->orderid;
    }

    function col_timeupdated($values) {
        return ($values->timeupdated) ? userdate($values->timeupdated, get_string('strftimedate', 'langconfig')) : '-';
    }

    function col_amount($values) {
        return ($values->amount) ? $this->currency.$values->amount : '-';
    }

    function col_subtotal($values) {
        return ($values->subtotal) ? $this->currency.$values->subtotal : '-';
    }

    function col_status($values) {
        return ($values->status) ? get_string('status_'.$values->status, 'local_ecommerce') : '';
    }

    function col_discount($values) {
        return ($values->discount) ? $this->currency.$values->discount : '-';
    }
}