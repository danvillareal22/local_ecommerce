<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * local_ecommerce
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once('custom_table.php');

class products_table extends custom_table {

    public $search = '';
    public $categoryid = '';
    public $currency = '';

    function __construct($uniqueid, $categoryid, $search, $filter) {
        global $CFG, $USER, $PAGE, $DB;

        parent::__construct($uniqueid);
        $this->currency = \local_ecommerce\payment::get_currency('symbol');

        $systemcontext   = context_system::instance();
        $this->search   = $search;
        $this->categoryid = $categoryid;

        $columns = array('name', 'idnumber', 'category', 'courses', 'price', 'status', 'actions');
        $headers = array(
            get_string('name', 'local_ecommerce'),
            get_string('productcode', 'local_ecommerce'),
            get_string('category', 'local_ecommerce'),
            get_string('courses', 'local_ecommerce'),
            get_string('price', 'local_ecommerce'),
            get_string('status', 'local_ecommerce'),
            get_string('actions', 'local_ecommerce')
        );

        $this->sortable(true, 'name');
        $this->no_sorting('actions');
        $this->is_collapsible = false;

        $this->define_columns($columns);
        $this->define_headers($headers);

        $fields = "p.id, p.name, p.idnumber, p.price, p.visible, p.visible as status, cat.name as category, pc.courses as courses";
        $from = "{local_ecommerce_products} p
                    LEFT JOIN {local_ecommerce_cat} cat ON cat.id = p.categoryid
                    LEFT JOIN (
                            SELECT COUNT(id) as courses, productid 
                                      FROM {local_ecommerce_relations} 
                                      WHERE type = 'course'
                                  GROUP BY productid
                                  ) pc ON pc.productid = p.id  ";

        $where = 'p.id > 0';
        $params = array();

        // category filter
        if ($categoryid) {
            $where .= " AND p.categoryid = :categoryid";
            $params = array('categoryid' => $categoryid);
        }

        // search
        if (!empty($search)) {
            $where .= " AND (" . $DB->sql_like('p.name', ':searchname', false, false, false). "  
                         OR " . $DB->sql_like('p.idnumber', ':searchidnumber', false, false, false).")";
            $params['searchname'] = '%' . $search . '%';
            $params['searchidnumber'] = '%' . $search . '%';
        }

        // filter visibility
        if ($filter == 2) {
            $where .= ' AND p.visible = :visible';
            $params['visible'] = 1;
        } else if ($filter == 1) {
            $where .= ' AND p.visible = :visible';
            $params['visible'] = 0;
        }

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($PAGE->url);
    }

    function col_name($values) {
        return html_writer::link(new moodle_url('/local/ecommerce/view.php', array('id'=>$values->id)), $values->name);
    }

    function col_courses($values) {
        return ($values->courses) ? $values->courses : '-';
    }

    function col_price($values) {
        return ($values->price) ? $this->currency.$values->price : $this->currency.'0';
    }

    function col_status($values)
    {
        $status = get_string('active', 'local_ecommerce');
        if (!$values->status) {
            $status = get_string('inactive', 'local_ecommerce');
        }
        return '<div class = "store-table-' . strtolower($status) . '"><i class = "fa fa-circle"></i>&nbsp;' . $status . '</div>';
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $PAGE;

        $systemcontext   = context_system::instance();
        if (!has_capability('local/ecommerce:editproduct', $systemcontext)){
            return '';
        }

        $urlparams = array('id' => $values->id, 'sesskey' => sesskey());
        $editURL = new moodle_url('/local/ecommerce/products/edit.php', $urlparams);
        $deleteURL = new moodle_url('/local/ecommerce/products/edit.php', $urlparams + array('action' => 'delete'));
        if ($values->visible) {
            $toggleURL = new moodle_url('/local/ecommerce/products/edit.php', $urlparams + array('action' => 'hide'));
            $toggleName = get_string('hide');
            $toggleIcon = 'fa fa-eye';
        } else {
            $toggleURL = new moodle_url('/local/ecommerce/products/edit.php', $urlparams + array('action' => 'show'));
            $toggleName = get_string('show');
            $toggleIcon = 'fa fa-eye-slash';
        }

        $renderer = $PAGE->get_renderer('local_ecommerce');
        $params = array(
            'id' => $values->id,
            'buttons' => array(
                array(
                    'name' => get_string('edit'),
                    'icon' => 'fa fa-edit',
                    'url' => $editURL
                ),
                array(
                    'name' => get_string('delete'),
                    'icon' => 'fa fa-trash',
                    'url' => $deleteURL
                ),
                array(
                    'name' => $toggleName,
                    'icon' => $toggleIcon,
                    'url' => $toggleURL
                )
            )
        );

        if (has_capability('local/ecommerce:assign', $systemcontext)) {
            $assignURL = new moodle_url('/local/ecommerce/products/assigncourses.php', $urlparams);
            $params['buttons'][] = array(
                'name' => get_string('assigncourses', 'local_ecommerce'),
                'icon' => 'fa fa-plus',
                'url' => $assignURL
            );
        }

        return $renderer->store_print_action_icons($params);
    }
}

