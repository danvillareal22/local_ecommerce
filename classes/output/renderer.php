<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_ecommerce\output;
defined('MOODLE_INTERNAL') || die;

use plugin_renderer_base;
use renderable;
use context_system;
use moodle_url;
use tabobject;
use html_writer;

/**
 * Renderer file.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

/**
 * Standard HTML output renderer for badges
 */
class renderer extends plugin_renderer_base {

    public $couponprices = array();

    // Prints tabs
    public function print_manage_tabs($activePage = 'dashboard') {

        global $CFG;
        $dashboardActive = false;
        $categoriesActive = false;
        $productsActive = false;
        $couponsActive = false;
        $discountsActive = false;
        $paymentTypesActive = false;
        $waitlistActive = false;
        $reportsActive = false;
        $salesActive = false;
        $reviewsActive = false;
        $sliderActive = false;

        switch ($activePage) {
            case 'dashboard':
                $dashboardActive = true;
                break;

            case 'categories':
                $categoriesActive = true;
                break;

            case 'products':
                $productsActive = true;
                break;

            case 'coupons':
                $couponsActive = true;
                break;

            case 'discounts':
                $discountsActive = true;
                break;

            case 'payments':
                $paymentTypesActive = true;
                break;

            case 'waitlist':
                $waitlistActive = true;
                break;

            case 'reports':
                $reportsActive = true;
                break;

            case 'sales':
            case 'invoices':
            case 'customers':
                $salesActive = true;
                break;

            case 'reviews':
                $reviewsActive = true;
                break;

            case 'slider':
                $sliderActive = true;
                break;

            default:
                $dashboardActive = true;
                break;
        }
        $context = [
            'dashboardurl' => $CFG->wwwroot . '/local/ecommerce/dashboard',
            'categoriesurl' => $CFG->wwwroot . '/local/ecommerce/categories',
            'productsurl' => $CFG->wwwroot . '/local/ecommerce/products',
            'couponssurl' => $CFG->wwwroot . '/local/ecommerce/coupons',
            'discountsurl' => $CFG->wwwroot . '/local/ecommerce/discounts',
            'paymenttypesurl' => $CFG->wwwroot . '/local/ecommerce/payments',
            'waitlisturl' => $CFG->wwwroot . '/local/ecommerce/waitlist',
            'reportsurl' => $CFG->wwwroot . '/local/ecommerce/reports',
            'salesurl' => $CFG->wwwroot . '/local/ecommerce/sales',
            'reviewsurl' => $CFG->wwwroot . '/local/ecommerce/reviews',
            'sliderurl' => $CFG->wwwroot . '/local/ecommerce/slider',
            'settingsurl' => $CFG->wwwroot . '/admin/category.php?category=local_ecommerce',
            'dashboardactive' => $dashboardActive,
            'categoriesactive' => $categoriesActive,
            'productsactive' => $productsActive,
            'couponsactive' => $couponsActive,
            'discountsactive' => $discountsActive,
            'paymenttypesactive' => $paymentTypesActive,
            'waitlistactive' => $waitlistActive,
            'reportsactive' => $reportsActive,
            'salesactive' => $salesActive,
            'reviewsactive' => $reviewsActive,
            'slideractive' => $sliderActive
        ];
        return $this->render_from_template('local_ecommerce/ecommerce-menu-bar', $context);
    }

    // Prints sales tabs
    public function print_sales_tabs($currenttab = 'allsales') {

        $systemcontext = context_system::instance();
        $row = array();

        $row[] = new tabobject('allsales',
                new moodle_url('/local/ecommerce/sales/index.php'),
                get_string('allsales', 'local_ecommerce'),
                get_string('allsales', 'local_ecommerce')
        );

        $row[] = new tabobject('invoices',
                new moodle_url('/local/ecommerce/sales/invoices.php'),
                get_string('invoices', 'local_ecommerce'),
                get_string('invoices', 'local_ecommerce')
        );

        $row[] = new tabobject('pending',
                new moodle_url('/local/ecommerce/sales/pending.php'),
                get_string('pending', 'local_ecommerce'),
                get_string('pending', 'local_ecommerce')
        );

        $row[] = new tabobject('customers',
                new moodle_url('/local/ecommerce/sales/customers.php'),
                get_string('customers', 'local_ecommerce'),
                get_string('customers', 'local_ecommerce')
        );

        return $this->tabtree($row, $currenttab);
    }

    // Prints earch category panel
    public function print_search_category_panel($parentid = 0, $search = '', $filter = 1) {
        $output = '';

        $systemcontext = context_system::instance();

        $context = new \stdClass();
        $context->canedit = (has_capability('local/ecommerce:editcategory', $systemcontext));
        $context->editurl = new moodle_url('/local/ecommerce/categories/edit.php', array('parentid' => $parentid));
        $context->pageurl = $this->page->url;
        $context->filter = $filter;
        $context->search = $search;
        $context->parentid = $parentid;
        $context->options = array();

        $options = array(3 => get_string('showall', 'local_ecommerce'), 2 => get_string('active', 'local_ecommerce'),
                1 => get_string('inactive', 'local_ecommerce'));
        foreach ($options as $value => $title) {
            $params = array('value' => $value, 'title'=>$title);
            if ($value == $filter) {
                $params['selected'] = 'selected';
            }
            $context->options[] = $params;
        }

        return $this->render_from_template('local_ecommerce/search-category-form', $context);
    }

    public function store_print_categories($outputParams) {
        return $this->render_from_template('local_ecommerce/categories-index', $outputParams);
    }

    public function store_print_dashboard($outputParams) {
        return $this->render_from_template('local_ecommerce/dashboard', $outputParams);
    }

    // Prints search product panel
    public function print_search_product_panel($categoryid = 0, $search = '', $filter = 1) {
        $output = '';

        $systemcontext = context_system::instance();

        $context = new \stdClass();
        $context->canedit = (has_capability('local/ecommerce:editproduct', $systemcontext));
        $context->editurl = new moodle_url('/local/ecommerce/products/edit.php');
        $context->pageurl = $this->page->url;
        $context->filter = $filter;
        $context->search = $search;
        $context->categoryid = $categoryid;
        $context->filteroptions = array();
        $context->categoryoptions = array();

        $filteroptions = array(3 => get_string('showall', 'local_ecommerce'), 2 => get_string('active', 'local_ecommerce'),
                1 => get_string('inactive', 'local_ecommerce'));
        foreach ($filteroptions as $value => $title) {
            $params = array('value' => $value, 'title'=>$title);
            if ($value == $filter) {
                $params['selected'] = 'selected';
            }
            $context->filteroptions[] = $params;
        }

        $categoryoptions = array(0 => get_string('showall', 'local_ecommerce')) + \local_ecommerce\category::get_options();
        foreach ($categoryoptions as $value => $title) {
            $params = array('value' => $value, 'title'=>$title);
            if ($value == $categoryid) {
                $params['selected'] = 'selected';
            }
            $context->categoryoptions[] = $params;
        }

        return $this->render_from_template('local_ecommerce/search-product-form', $context);
    }

    public function store_list_products($outputParams) {
        return $this->render_from_template('local_ecommerce/products-index', $outputParams);
    }

    // Prints search coupons panel
    public function print_search_panel($search = '', $filter = 1, $type = 'coupons', $name = 'Coupons') {
        $output = '';

        $systemcontext = context_system::instance();

        $context = new \stdClass();
        $context->canedit = (has_capability('local/ecommerce:editcoupons', $systemcontext));
        $context->editurl = new moodle_url('/local/ecommerce/'.$type.'/edit.php');
        $context->pageurl = $this->page->url;
        $context->filter = $filter;
        $context->search = $search;
        $context->name = $name;
        $context->filteroptions = array();

        $filteroptions = array(3 => get_string('showall', 'local_ecommerce'), 2 => get_string('active', 'local_ecommerce'),
                1 => get_string('inactive', 'local_ecommerce'));
        foreach ($filteroptions as $value => $title) {
            $params = array('value' => $value, 'title'=>$title);
            if ($value == $filter) {
                $params['selected'] = 'selected';
            }
            $context->filteroptions[] = $params;
        }

        return $this->render_from_template('local_ecommerce/search-form', $context);
    }

    public function store_list_generic_table($outputParams) {
        return $this->render_from_template('local_ecommerce/page-index', $outputParams);
    }

    public function store_print_report_index($outputParams) {
        return $this->render_from_template('local_ecommerce/reports-index', $outputParams);
    }

    public function store_print_assign_courses($outputParams) {
        return $this->render_from_template('local_ecommerce/assign-courses', $outputParams);
    }

    public function print_basic_header($title) {
        $context = new \stdClass();
        $context->title = $title;

        return $this->render_from_template('local_ecommerce/basic-page-header', $context);
    }

    // Print sales filter panel
    public function print_sales_filter_panel($status, $daterange, $search, $type = 'sales') {
        global $CFG;
        $output = '';

        $systemcontext = context_system::instance();

        $context = new \stdClass();
        $context->pageurl = $this->page->url;
        $context->daterange = $daterange;
        $context->search = $search;
        if (!empty($status)) {
            $context->{"status_$status"} = $status;
        }

        $context->download_url = new moodle_url($CFG->wwwroot . '/local/ecommerce/sales/index.php', array('download'=> 'csv', 'sesskey'=>sesskey(), 'daterange' => $daterange['daterange'], 'search' => $search));
        if ($type == 'invoices' and has_capability('local/ecommerce:settings', context_system::instance())) {
            $context->settingsurl = new \moodle_url('/admin/settings.php', array('section'=>'local_ecommerce_invoices'));
            $context->download_url = new moodle_url($CFG->wwwroot . '/local/ecommerce/sales/invoices.php', array('download'=> 'csv', 'sesskey'=>sesskey(), 'daterange' => $daterange['daterange'], 'search' => $search));
        } else if ($type == 'pending' and has_capability('local/ecommerce:settings', context_system::instance())) {
            $context->download_url = new moodle_url($CFG->wwwroot . '/local/ecommerce/sales/pending.php', array('download'=> 'csv', 'sesskey'=>sesskey(), 'daterange' => $daterange['daterange'], 'search' => $search));
        } else if ($type == 'customers' and has_capability('local/ecommerce:settings', context_system::instance())) {
            $context->download_url = new moodle_url($CFG->wwwroot . '/local/ecommerce/sales/customers.php', array('download'=> 'csv', 'sesskey'=>sesskey(), 'daterange' => $daterange['daterange'], 'search' => $search));
        }
        $context->isfilter = ($type == 'customers') ? 0 : 1;


        return $this->render_from_template('local_ecommerce/sales-filter-form', $context);
    }

    public function store_list_sales($outputParams) {
        return $this->render_from_template('local_ecommerce/sales-index', $outputParams);
    }

    // Print sales filter panel
    public function print_single_search_panel($search, $name) {
        $output = '';

        $systemcontext = context_system::instance();

        $context = new \stdClass();
        $context->pageurl = $this->page->url;
        $context->search = $search;
        $context->name = $name;

        return $this->render_from_template('local_ecommerce/single-search-form', $context);
    }


    // Print report filter panel
    public function print_report_filter_panel($id, $daterange, $search, $itemid = 0, $pageHeader = null) {
        $output = '';

        $systemcontext = context_system::instance();

        $context = new \stdClass();
        $context->pageurl = $this->page->url;
        $context->daterange = $daterange;
        $context->id = $id;
        $context->itemid = $itemid;
        $context->search = $search;
        $context->page_header = $pageHeader;

        return $this->render_from_template('local_ecommerce/report-filter-form', $context);
    }

    public function store_print_reports($outputParams) {
        return $this->render_from_template('local_ecommerce/report-page', $outputParams);
    }

    // eCreators store functions.
    public function store_print_admin_breadcrumbs($mainURL, $mainTitle, $secondaryTitle)
    {
        global $CFG;
        $context = array(
            'main_url' => $mainURL,
            'main_title' => $mainTitle,
            'secondary_title' => $secondaryTitle
        );
        return $this->render_from_template('local_ecommerce/admin-breadcrumbs', $context);
    }

    public function store_print_breadcrumbs($mainURL, $mainTitle, $secondaryTitle)
    {
        global $CFG;
        $context = array(
            'main_url' => $mainURL,
            'main_title' => $mainTitle,
            'secondary_title' => $secondaryTitle
        );
        return $this->render_from_template('local_ecommerce/store-breadcrumbs', $context);
    }

    // eCreators store functions.
    public function store_print_checkout_footer($checkoutButton = null)
    {
        global $CFG;
        $footerSocials = true;
        if (empty(get_config('local_ecommerce', 'facebook_link')) && empty(get_config('local_ecommerce', 'twitter_link')) && empty(get_config('local_ecommerce', 'linkedin_link'))) {
            $footerSocials = false;
        }

        $itemsInCart = false;
        if ($checkoutButton !== false) {
            $itemsInCart = \local_ecommerce\checkout::get_products_in_cart();
        }

        $params = array(
            'logged_in' => isloggedin(),
            'items_in_cart' => $itemsInCart,
            'f_book' => get_config('local_ecommerce', 'facebook_link'),
            'twit' => get_config('local_ecommerce', 'twitter_link'),
            'linked_in' => get_config('local_ecommerce', 'linkedin_link'),
            'socials' => $footerSocials,
            'checkout_url' => $CFG->wwwroot . '/local/ecommerce/index.php',
            'shipping_page' => get_config('local_ecommerce', 'enable_shipping_policy'),
            'privacy_page' => get_config('local_ecommerce', 'enable_privacy_policy'),
            'contact_page' => get_config('local_ecommerce', 'enable_contact_page'),
            'shipping_url' => $CFG->wwwroot . '/local/ecommerce/shipping-policy.php',
            'privacy_url' => $CFG->wwwroot . '/local/ecommerce/privacy-policy.php',
            'contact_url' => $CFG->wwwroot . '/local/ecommerce/contact.php'
        );
        return $this->render_from_template('local_ecommerce/store-checkout-footer', $params);
    }

    public function store_print_company_header($logoURL, $siteName)
    {
        global $CFG;
        $context = array(
            'logo_url' => $logoURL,
            'site_name' => $siteName
        );
        return $this->render_from_template('local_ecommerce/store-company-header', $context);
    }

    public function store_print_contact_form($submittedData)
    {
        global $CFG;
        $context = array(
            'pageurl' => $CFG->wwwroot . '/local/ecommerce/contact.php',
            'contact_header' => $this->print_basic_header(get_string('contact_us', 'local_ecommerce')),
            'nameValue' => $submittedData->name,
            'emailValue' => $submittedData->email,
            'phoneValue' => $submittedData->phone,
            'subjectValue' => $submittedData->subject,
            'messageValue' => $submittedData->message,
            'openingHours' => get_config('local_ecommerce', 'opening_hours'),
            'phone' => get_config('local_ecommerce', 'store_phone_number'),
            'facebook' => get_config('local_ecommerce', 'facebook_link'),
            'twitter' => get_config('local_ecommerce', 'twitter_link'),
            'linkedIn' => get_config('local_ecommerce', 'linkedin_link')
        );
        return $this->render_from_template('local_ecommerce/contact-form', $context);
    }

    public function store_print_courses($courses = array()) {
        $courseArray = array();
        foreach ($courses as $course) {
            $courseArray[] = $course;
        }
        $context = [
            'courses' => $courseArray,
            'arecourses' => count($courseArray)
        ];
        return $this->render_from_template('local_ecommerce/product-courses-list', $context);
    }

    public function store_print_menu($activePage, $loggedIn = true) {
        global $CFG;
        switch ($activePage) {
            case 'purchases':
                $storeActive = false;
                $purchasesActive = true;
                $contactActive = false;
                break;

            case 'view':
                $storeActive = false;
                $purchasesActive = false;
                $contactActive = false;
                break;

            case 'contact_us':
                $storeActive = false;
                $purchasesActive = false;
                $contactActive = true;
                break;

            case 'waitlist':
                $storeActive = false;
                $purchasesActive = false;
                $contactActive = false;
                break;

            default:
                $storeActive = true;
                $purchasesActive = false;
                $contactActive = false;
                break;
        }
        $context = [
            'storeurl' => $CFG->wwwroot . '/local/ecommerce/store.php',
            'purchasesurl' => $CFG->wwwroot . '/local/ecommerce/purchases.php',
            'contacturl' => $CFG->wwwroot . '/local/ecommerce/contact.php',
            'storeactive' => $storeActive,
            'purchasesactive' => $purchasesActive,
            'contactactive' => $contactActive,
            'enablecontactpage' => get_config('local_ecommerce', 'enable_contact_page'),
            'isloggedin' => $loggedIn
        ];
        return $this->render_from_template('local_ecommerce/store-menu-bar', $context);
    }

    public function store_print_nav($tabs, $search, $current, $orderBy, $slider) {

        $searchContext = new \stdClass();
        $searchContext->pageurl = $this->page->url;
        $searchContext->search = $search;
        $searchContext->orderby = $orderBy;
        switch ($orderBy) {
            case 'recent':
                $searchContext->orderbyrecent = true;
                break;
            case 'rated':
                $searchContext->orderbyrated = true;
                break;
            case 'popular':
                $searchContext->orderbypopular = true;
                break;
            case 'price-lowest':
                $searchContext->orderbylowestprice = true;
                break;
            case 'price-highest':
                $searchContext->orderbyhighestprice = true;
                break;
            default:
                $orderBy = 'featured';
                $searchContext->orderbyfeatured = true;
                break;
        }
        $context = [
            'tabs' => $this->store_print_tabs($tabs, $search, $orderBy, $current),
            'searchform' => $this->render_from_template('local_ecommerce/store-search-form', $searchContext),
            'slider' => $this->render_from_template('local_ecommerce/image-slider', $slider),
        ];

        return $this->render_from_template('local_ecommerce/store-nav-bar', $context);
    }

    // eCreators store functions.
    public function store_print_policy_page($content)
    {
        global $CFG;
        $context = array(
            'policy_header' => $content['policy_header'],
            'content' => $content['content']
        );
        return $this->render_from_template('local_ecommerce/store-policy-page', $context);
    }

    public function store_print_products($itemsperpage, $categoryName, $products = array(), $page = 0, $type = 2) {

        $context = [
            'products' => $products['products'],
            'isproducts' => count($products['products']),
            'showloadmore' => ($products['total'] > $itemsperpage * ($page + 1)),
            'nextpage' => ($page+1),
            'firstpage' => !$page,
            'limit' => $itemsperpage,
            'type' => $type,
            'shownoproducts' => (!count($products['products']) and $page > 0),
            'categoryname' => $categoryName,
            'cancheckout' => has_capability('local/ecommerce:checkout', context_system::instance()),
            'currency' => \local_ecommerce\payment::get_currency(),
            'enablewaitlist' => get_config('local_ecommerce', 'enablewaitlist'),
            'enablefbooksharing' => get_config('local_ecommerce', 'enable_facebook_sharing'),
            'enabletwitsharing' => get_config('local_ecommerce', 'enable_twitter_sharing'),
            'enablelinkedinsharing' => get_config('local_ecommerce', 'enable_linkedin_sharing')
        ];
        return $this->render_from_template('local_ecommerce/store-list-products', $context);
    }

    public function store_print_review($review) {
        global $CFG;
        $context = array(
            'tabs' => $this->print_manage_tabs($activePage = 'reviews'),
            'review' => $review,
            'deleteurl' => $CFG->wwwroot . '/local/ecommerce/reviews/edit.php?id=' . $review->id . '&action=delete&sesskey=' . sesskey(),
            'cancelurl' => $CFG->wwwroot . '/local/ecommerce/reviews/index.php',
            'review_header' => $this->print_basic_header(get_string('review_for', 'local_ecommerce') . $review->name)
        );
        return $this->render_from_template('local_ecommerce/product-review', $context);
    }

    public function store_print_reviews($categoryName, $productID, $reviews = array(), $page) {
        $context = [
            'reviews' => $reviews['reviews'],
            'arereviews' => count($reviews['reviews']),
            'categoryname' => $categoryName,
            'productid' => $productID,
            'showloadmore' => ($reviews['total'] > 3 * ($page + 1)),
            'nextpage' => ($page+1)
        ];
        return $this->render_from_template('local_ecommerce/product-reviews', $context);
    }

    // Prints search reviews panel.
    public function store_print_search_reviews_panel($productID = 0, $search = '')
    {
        $context = new \stdClass();
        $context->pageurl = $this->page->url;
        $context->search = $search;
        $context->productoptions = array();

        $productOptions = array(0 => get_string('showall', 'local_ecommerce')) + \local_ecommerce\review::get_options();
        foreach ($productOptions as $value => $title) {
            $params = array('value' => $value, 'title'=>$title);
            if ($value == $productID) {
                $params['selected'] = 'selected';
            }
            $context->productoptions[] = $params;
        }

        return $this->render_from_template('local_ecommerce/search-reviews-form', $context);
    }

    public function store_print_tabs($categories, $search, $orderBy, $current = null) {
        $row = array();

        $row[] = new tabobject(
            0,
            new moodle_url(
                $this->page->url,
                array('search' => $search, 'category' => 0, 'orderby' => $orderBy)
            ),
            get_string('allcategories', 'local_ecommerce')
        );

        foreach ($categories['categories'] as $category) {
            $row[] = new tabobject(
                $category->id,
                new moodle_url(
                    $this->page->url,
                    array('search' => $search, 'category' => $category->id, 'orderby' => $orderBy)
                ),
                $category->name
            );
        }

        return $this->tabtree($row, $current);
    }

    public function store_print_purchases_filter_panel($dateRange, $search) {
        global $CFG;
        $context = new \stdClass();
        $context->pageurl = $this->page->url;
        $context->daterange = $dateRange;
        $context->search = $search;
        $context->isfilter = 0;
        $context->download_url = new moodle_url($CFG->wwwroot . '/local/ecommerce/purchases.php', array('download'=> 'csv', 'sesskey'=>sesskey(), 'daterange' => $dateRange['daterange'], 'search' => $search));
        return $this->render_from_template('local_ecommerce/purchases-filter-form', $context);
    }

    public function store_print_purchases($outputParams) {
        return $this->render_from_template('local_ecommerce/list-purchases', $outputParams);
    }

    public function store_print_waitlist($outputParams) {
        return $this->render_from_template('local_ecommerce/ecommerce-waitlist', $outputParams);
    }

    public function store_print_action_icons($params) {
        global $CFG;
        $context = new \stdClass();
        $context->id = $params['id'];
        $context->buttons = $params['buttons'];
        return $this->render_from_template('local_ecommerce/action-icon-container', $context);
    }

    /**
     * Return the dashboard content for the ecommerce.
     *
     * @param dashboard $dashboard The dashboard renderable
     * @return string HTML string
     */
    public function render_dashboard(dashboard $dashboard) {
        return $this->render_from_template('local_ecommerce/main', $dashboard->export_for_template($this));
    }

    /**
     * Return the categories index content for the ecommerce.
     *
     * @param categories_index $categories_index The dashboard renderable
     * @return string HTML string
     */
    public function render_categories_index(categories_index $categories_index) {
        return $this->render_from_template('local_ecommerce/main', $categories_index->export_for_template($this));
    }

    /**
     * Return the categories edit content for the ecommerce.
     *
     * @param categories_index $categories_index The dashboard renderable
     * @return string HTML string
     */
    public function render_categories_edit(categories_edit $categories_edit) {
        return $this->render_from_template('local_ecommerce/main', $categories_edit->export_for_template($this));
    }

    public function render_categories_delete(categories_delete $categories_delete) {
        return $this->render_from_template('local_ecommerce/main', $categories_delete->export_for_template($this));
    }

    /**
     * Return the products index content for the ecommerce.
     *
     * @param categories_index $products_index The ecommerce renderable
     * @return string HTML string
     */
    public function render_products_index(products_index $products_index) {
        return $this->render_from_template('local_ecommerce/main', $products_index->export_for_template($this));
    }

    /**
     * Return the categories edit content for the ecommerce.
     *
     * @param categories_index $categories_index The dashboard renderable
     * @return string HTML string
     */
    public function render_products_edit(products_edit $products_edit) {
        return $this->render_from_template('local_ecommerce/main', $products_edit->export_for_template($this));
    }

    public function render_products_delete(products_delete $products_delete) {
        return $this->render_from_template('local_ecommerce/main', $products_delete->export_for_template($this));
    }

    /**
     * Return the assign courses content for the ecommerce.
     *
     * @param assign_courses $categories_index The dashboard renderable
     * @return string HTML string
     */
    public function render_assign_courses(assign_courses $assign_courses) {
        return $this->render_from_template('local_ecommerce/main', $assign_courses->export_for_template($this));
    }


    /**
     * Return the coupons index content for the ecommerce.
     *
     * @param coupons_index $coupons_index The ecommerce renderable
     * @return string HTML string
     */
    public function render_coupons_index(coupons_index $coupons_index) {
        return $this->render_from_template('local_ecommerce/main', $coupons_index->export_for_template($this));
    }

    /**
     * Return the coupons edit content for the ecommerce.
     *
     * @param coupons_edit $coupons_edit The coupons renderable
     * @return string HTML string
     */
    public function render_coupons_edit(coupons_edit $coupons_edit) {
        return $this->render_from_template('local_ecommerce/main', $coupons_edit->export_for_template($this));
    }

    public function render_coupons_delete(coupons_delete $coupons_delete) {
        return $this->render_from_template('local_ecommerce/main', $coupons_delete->export_for_template($this));
    }


    /**
     * Return the assign courses content for the ecommerce.
     *
     * @param assign_products $categories_index The dashboard renderable
     * @return string HTML string
     */
    public function render_assign_products(assign_products $assign_products) {
        return $this->render_from_template('local_ecommerce/main', $assign_products->export_for_template($this));
    }

    /**
     * Return the discounts index content for the ecommerce.
     *
     * @param discounts_index $discounts_index The ecommerce renderable
     * @return string HTML string
     */
    public function render_discounts_index(discounts_index $discounts_index) {
        return $this->render_from_template('local_ecommerce/main', $discounts_index->export_for_template($this));
    }

    /**
     * Return the coupons edit content for the ecommerce.
     *
     * @param discounts_edit $coupons_edit The coupons renderable
     * @return string HTML string
     */
    public function render_discounts_edit(discounts_edit $discounts_edit) {
        return $this->render_from_template('local_ecommerce/main', $discounts_edit->export_for_template($this));
    }

    public function render_discounts_delete(discounts_delete $discounts_delete) {
        return $this->render_from_template('local_ecommerce/main', $discounts_delete->export_for_template($this));
    }

    /**
     * Return the coupons edit content for the ecommerce.
     *
     * @param discounts_edit $coupons_edit The coupons renderable
     * @return string HTML string
     */
    public function render_product_view(product_view $product_view) {
        return $this->render_from_template('local_ecommerce/product-view', $product_view->export_for_template($this));
    }

    /**
     * Return the ecommerce index content
     *
     * @param ecommerce_index $ecommerce_index The ecommerce renderable
     * @return string HTML string
     */
    public function render_ecommerce_index($ecommerce_index) {
        $context = new \stdClass();
        $context->title = $ecommerce_index['title'];
        $context->products = $ecommerce_index['products'];
        $context->isproducts = $ecommerce_index['isproducts'];
        $context->productscount = $ecommerce_index['productscount'];
        $context->subtotal = $ecommerce_index['subtotal'];
        $context->discount  = $ecommerce_index['discount'];
        $context->total = $ecommerce_index['total'];
        $context->salestax  = $ecommerce_index['salestax'];
        $context->pretaxtotal = $ecommerce_index['pretaxtotal'];
        $context->salestaxname = $ecommerce_index['salestaxname'];
        $context->ecommerceurl = $ecommerce_index['ecommerceurl'];
        $context->checkouturl = $ecommerce_index['checkouturl'];
        $context->isdiscounts = $ecommerce_index['isdiscounts'];
        $context->discounts = $ecommerce_index['discounts'];
        $context->iscoupons = $ecommerce_index['iscoupons'];
        $context->enablediscounts = $ecommerce_index['enablediscounts'];
        $context->enablecoupons = $ecommerce_index['enablecoupons'];
        $context->coupons = $ecommerce_index['coupons'];
        $context->currency = $ecommerce_index['currency'];
        $context->checkout_header = $ecommerce_index['checkout_header'];
        $context->display_shipping_policy = $ecommerce_index['display_shipping_policy'];
        $context->shipping_policy_url = $ecommerce_index['shipping_policy_url'];
        return $this->render_from_template('local_ecommerce/ecommerce-index', $context);
    }

    /**
     * Return the ecommerce checkout content
     *
     * @param ecommerce_index $ecommerce_checkout The ecommerce renderable
     * @return string HTML string
     */
    public function render_ecommerce_checkout(ecommerce_checkout $ecommerce_checkout) {
        return $this->render_from_template('local_ecommerce/checkout-index', $ecommerce_checkout->export_for_template($this));
    }

    /**
     * Return the ecommerce checkout content
     *
     * @param ecommerce_index $ecommerce_checkout The ecommerce renderable
     * @return string HTML string
     */
    public function render_payments_index(payments_index $payments_index) {
        return $this->render_from_template('local_ecommerce/main', $payments_index->export_for_template($this));
    }

    /**
     * Return the ecommerce checkout content
     *
     * @param ecommerce_index $ecommerce_checkout The ecommerce renderable
     * @return string HTML string
     */
    public function render_payments_edit(payments_edit $payments_edit) {
        return $this->render_from_template('local_ecommerce/main', $payments_edit->export_for_template($this));
    }

    public function render_payments_delete(payments_delete $payments_delete) {
        return $this->render_from_template('local_ecommerce/main', $payments_delete->export_for_template($this));
    }

    /**
     * Return the ecommerce checkout content
     *
     * @param ecommerce_index $ecommerce_checkout The ecommerce renderable
     * @return string HTML string
     */
    public function render_sales_index(sales_index $sales_index) {
        return $this->render_from_template('local_ecommerce/main', $sales_index->export_for_template($this));
    }

    /**
     * Return the ecommerce checkout content
     *
     * @param ecommerce_index $ecommerce_checkout The ecommerce renderable
     * @return string HTML string
     */
    public function render_reports_index(reports_index $reports_index) {
        return $this->render_from_template('local_ecommerce/main', $reports_index->export_for_template($this));
    }

    public function render_report_page(report_page $report_page) {
        return $this->render_from_template('local_ecommerce/main', $report_page->export_for_template($this));
    }

    /**
     * Return the ecommerce waitlist content
     *
     * @param ecommerce_index $ecommerce_waitlist The ecommerce renderable
     * @return string HTML string
     */
    public function render_ecommerce_waitlist(ecommerce_waitlist $ecommerce_waitlist) {
        return $this->render_from_template('local_ecommerce/ecommerce-waitlist', $ecommerce_waitlist->export_for_template($this));
    }

    public function render_waitlist_index(waitlist_index $waitlist_index) {
        return $this->render_from_template('local_ecommerce/main', $waitlist_index->export_for_template($this));
    }

    public function render_waitlist_delete(waitlist_delete $waitlist_delete) {
        return $this->render_from_template('local_ecommerce/main', $waitlist_delete->export_for_template($this));
    }

    public function render_reviews_index(reviews_index $reviews_index) {
        return $this->render_from_template('local_ecommerce/main', $reviews_index->export_for_template($this));
    }

    public function store_list_reviews($outputParams) {
        return $this->render_from_template('local_ecommerce/reviews-index', $outputParams);
    }

    public function render_slider_index(slider_index $slider_index) {
        return $this->render_from_template('local_ecommerce/main', $slider_index->export_for_template($this));
    }

    public function render_contact_page(contact_page $contact_page) {
        return $this->render_from_template('local_ecommerce/main', $contact_page->export_for_template($this));
    }
}
