<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class payment {

    public static $STATUS_COMPLETED = 'completed';
    public static $STATUS_PENDING   = 'pending';
    public static $STATUS_REJECTED  = 'rejected';

    public static function get_types() {
        return [
            'paypal'        => get_string('paypal', 'local_ecommerce'),
            'stripe'        => get_string('stripe', 'local_ecommerce'),
//            'authorize'     => get_string('authorizenet', 'local_ecommerce'),
            'twocheckout'   => get_string('twocheckout', 'local_ecommerce'),

            // TODO
            //'cashnet'    => get_string('cashnetpayment', 'local_ecommerce'),
            //'quickbooks'    => get_string('quickbookspayment', 'local_ecommerce'),
            //'freshbooks'    => get_string('freshbookspayment', 'local_ecommerce'),
        ];
    }

    public static function save_paymenttype($data = null) {
        global $DB;

        if (isset($data->settings) and is_array($data->settings)) {
            $data->settings = serialize($data->settings);
        }

        if ($data->id) {
            $data->timemodified = time();
            $DB->update_record('local_ecommerce_payments', $data);

            return $data->id;

        } else {
            $data->timemodified = time();
            $data->timecreated = time();

            return $DB->insert_record('local_ecommerce_payments', $data);
        }
    }

    public static function delete_paymenttype($id = 0) {
        global $DB, $USER, $SITE;

        if (has_capability('local/ecommerce:managepaymenttypes', \context_system::instance())) {

            $payment = $DB->get_record('local_ecommerce_payments', array('id' => $id));

            // delete payment
            $DB->delete_records('local_ecommerce_payments', array('id' => $payment->id));
        }
    }

    public static function get_currencies() {

        $codes = array(
                'AUD', 'BRL', 'CAD', 'CHF', 'CZK', 'DKK', 'EUR', 'GBP', 'HKD', 'HUF', 'ILS', 'JPY',
                'MXN', 'MYR', 'NOK', 'NZD', 'PHP', 'PLN', 'RUB', 'SEK', 'SGD', 'THB', 'TRY', 'TWD', 'USD');
        $currencies = array();
        foreach ($codes as $c) {
            $currencies[$c] = new \lang_string($c, 'core_currencies');
        }

        return $currencies;
    }

    public static function get_currency($type = 'symbol') {

        $currency = get_config('local_ecommerce', 'currency');

        if (empty($currency)) {
            $currency = 'USD';
        }

        return ($type == 'code') ? $currency: self::get_current_currency_code($currency);
    }

    public static function get_current_currency_code($currency) {

        $codes = array(
                'AUD'=>'AUD&#36;',
                'BRL'=>'BRL&#82;&#36;',
                'CAD'=>'CAD&#36;',
                'CHF'=>'CHF&#67;&#72;&#70;',
                'CZK'=>'CZK&#75;&#269;',
                'DKK'=>'DKK&#107;&#114;',
                'EUR'=>'EUR&euro;',
                'GBP'=>'GBP&pound;',
                'HKD'=>'HKD&#36;',
                'HUF'=>'HUF&#70;&#116;',
                'ILS'=>'ILS&#8362;',
                'JPY'=>'JPY&#165;',
                'MXN'=>'MXN&#36;',
                'MYR'=>'MYR&#82;&#77;',
                'NOK'=>'NOK&#107;&#114;',
                'NZD'=>'NZD&#36;',
                'PHP'=>'PHP&#8369;',
                'PLN'=>'PLN&#122;&#322;',
                'RUB'=>'RUB&#1088;&#1091;&#1073;',
                'SEK'=>'SEK&#107;&#114;',
                'SGD'=>'SGD&#36;',
                'THB'=>'THB&#3647;',
                'TRY'=>'TRY&#;',
                'TWD'=>'TWD&#78;&#84;&#36;',
                'USD'=>'USD&dollar;'
        );

        return $codes[$currency];
    }

    public static function get_current_currency_symbol() {

        $currency = get_config('local_ecommerce', 'currency');

        if (empty($currency)) {
            $currency = 'USD';
        }

        $codes = array(
                'AUD'=>'&#36;',
                'BRL'=>'&#82;&#36;',
                'CAD'=>'&#36;',
                'CHF'=>'&#67;&#72;&#70;',
                'CZK'=>'&#75;&#269;',
                'DKK'=>'&#107;&#114;',
                'EUR'=>'&euro;',
                'GBP'=>'&pound;',
                'HKD'=>'&#36;',
                'HUF'=>'&#70;&#116;',
                'ILS'=>'&#8362;',
                'JPY'=>'&#165;',
                'MXN'=>'&#36;',
                'MYR'=>'&#82;&#77;',
                'NOK'=>'&#107;&#114;',
                'NZD'=>'&#36;',
                'PHP'=>'&#8369;',
                'PLN'=>'&#122;&#322;',
                'RUB'=>'&#1088;&#1091;&#1073;',
                'SEK'=>'&#107;&#114;',
                'SGD'=>'&#36;',
                'THB'=>'&#3647;',
                'TRY'=>'&#;',
                'TWD'=>'&#78;&#84;&#36;',
                'USD'=>'&dollar;'
        );

        return $codes[$currency];
    }

    public static function get_active_paymenttypes() {
        global $DB;

        return $DB->get_records('local_ecommerce_payments', array('status'=>1));
    }

    public static function print_payment_forms($renderer, $params) {
        global $CFG, $DB, $USER;

        $output = '';

        if (!(int)$params['payment']->amount) {
            return self::print_nopayment_form($renderer, $params['products'], $params['total'], $params['payment']);
        }

        $paymenttypes = self::get_active_paymenttypes();

        if (!count($paymenttypes) and !get_config('local_ecommerce', 'invoicepayment')) {
            return html_writer::div(get_string('paymenttypesnotsetup', 'local_ecommerce'), array('class'=>'alert alert-warning'));
        }

        $context = new \stdClass(); $i = 0;
        foreach ($paymenttypes as $paymenttype) {
            $path = $CFG->dirroot.'/local/ecommerce/payments/'.$paymenttype->type;

            if (file_exists($path . '/locallib.php')) {
                require_once($path . '/locallib.php');

                $pluginclass = 'local_ecommerce_' . $paymenttype->type . '_payment';
                $plugin = new $pluginclass($paymenttype);
                $context->forms[] = $plugin->payment_form($renderer, $params, $i);
            }
            $i++;
        }

        if (get_config('local_ecommerce', 'invoicepayment')) {
            $context->forms[] = self::print_invoices_form($renderer, $params, $i);
        }

        $context->checkout_header = $params['checkout_header'];

        return $renderer->render_from_template('local_ecommerce/payment-forms', $context);
    }

    public static function print_nopayment_form($renderer, $products, $total, $payment) {
        global $CFG, $USER, $OUTPUT, $PAGE;

        $data = new stdClass();
        $data->cost = $payment->amount;
        $data->paymenturl = new \moodle_url('/local/ecommerce/payments/nopayment.php');
        $data->checkout_id = $payment->id;
        $data->currency_code = self::get_currency('code');

        return $renderer->render_from_template('local_ecommerce/nopaymentform', $data);
    }

    public static function print_invoices_form($renderer, $params, $hidden) {
        global $CFG, $USER, $OUTPUT, $PAGE;

        $payment = $params['payment'];
        $invoiceurl = new \moodle_url('/local/ecommerce/sales/actions.php', array('id'=>$payment->id, 'sesskey'=>sesskey(), 'action'=>'print'));

        $data = new stdClass();
        $data->id = 'invoicepayment';
        $data->name = get_string('invoice', 'local_ecommerce');
        $data->cost = $payment->amount;
        $data->invoiceurl = $invoiceurl->out();
        $data->checkout_id = $payment->id;
        $data->currency_code = self::get_currency('code');
        $data->currency = \local_ecommerce\payment::get_currency();
        $data->icon = 'file';
        $data->total = $data->cost;
        $data->subtotal = $params['subtotal'];
        $data->discount = $params['discount'];
        $data->salestax = $params['salestax'];
        $data->pretaxtotal = $params['pretaxtotal'];
        $data->salestaxname = $params['salestaxname'];
        $data->enablediscounts = false;
        $data->enablecoupons = false;
        $a = new \stdClass();
        $a->cost = $data->currency . $data->cost;
        $a->method = 'an invoice';
        $data->payment_rubric = get_string('payment_rubric', 'local_ecommerce', $a);

        return array(
            'instance' => $data,
            'active' => ($hidden) ? 0 : 1,
            'form' => $renderer->render_from_template('local_ecommerce/invoicespaymentform', $data),
            'icon' => 'file'
        );
    }
}
