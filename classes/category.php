<?php

namespace local_ecommerce;

/**
 * Categories functions
 *
 * Hook functions for changes in core code.
 *
 * @package    local_ecommerce
 * @author     2017 SEBALE
 * @copyright  2017 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;

/**
 * Categories functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class category {

    public static function save_category($data = null) {
        global $DB;

        if ($data->id) {
            $data->timemodified = time();
            $DB->update_record('local_ecommerce_cat', $data);

            return $data->id;
        } else {
            $lastrecord = $DB->get_record_sql("SELECT sortorder FROM {local_ecommerce_cat} ORDER BY sortorder DESC LIMIT 1");
            $data->timemodified = time();
            $data->sortorder = (isset($lastrecord->sortorder)) ? $lastrecord->sortorder+1 : 0;

            return $DB->insert_record('local_ecommerce_cat', $data);
        }
    }

    public static function move_category($category, $action) {
        global $DB;

        if ($action == 'moveup') {
            $prev = $DB->get_record_sql("SELECT * FROM {local_ecommerce_cat} WHERE sortorder < :sortorder ORDER BY sortorder DESC LIMIT 1", array('sortorder'=>$category->sortorder));

            if ($prev) {
                $moveto = $prev->sortorder;
                $prev->sortorder = $category->sortorder;
                $DB->update_record('local_ecommerce_cat', $prev);

                $category->sortorder = $moveto;
                $DB->update_record('local_ecommerce_cat', $category);
            }

        } else {

            $next = $DB->get_record_sql("SELECT * FROM {local_ecommerce_cat} WHERE sortorder > :sortorder ORDER BY sortorder ASC LIMIT 1", array('sortorder'=>$category->sortorder));

            if ($next) {
                $moveto = $next->sortorder;
                $next->sortorder = $category->sortorder;
                $DB->update_record('local_ecommerce_cat', $next);

                $category->sortorder = $moveto;
                $DB->update_record('local_ecommerce_cat', $category);
            }
        }
    }


    public static function delete_category($id = 0) {
        global $DB, $USER, $SITE;

        if (has_capability('local/ecommerce:editcategory', context_system::instance())) {

            $category = $DB->get_record('local_ecommerce_cat', array('id' => $id));

            // delete category
            $DB->delete_records('local_ecommerce_cat', array('id' => $category->id));
        }
    }


    public static function get_options($id = 0, $showCurrent = false) {
        global $DB;
        $tree = self::categories_tree();
        $ignore = array();
        $options = array();

        if (count($tree)) {
            foreach ($tree as $parentid => $categories) {
                // TO DO hierarchy
                foreach ($categories as $category) {
                    if ($id > 0 and $category->id == $id  && !$showCurrent) {
                        continue;
                    }
                    $options[$category->id] = $category->name;
                }
            }
        }

        return $options;
    }

    public static function categories_tree() {
        global $DB;
        $tree = array();

        $categories = $DB->get_records('local_ecommerce_cat', array('visible' => 1), 'sortorder');

        if (count($categories)) {
            foreach ($categories as $category) {
                $tree[$category->parentid][$category->id] = $category;
            }
        }

        return $tree;
    }

    public static function get_available_categories($limit = 12, $page = 0) {
        global $DB, $USER, $CFG;

        $categories = array();

        $fields = 'c.*';
        $orderby = "ORDER BY c.sortorder";
        $wheres = array("c.visible > 0");
        $params = array();

        $wheres = implode(" AND ", $wheres);

        //note: we can not use DISTINCT + text fields due to Oracle and MS limitations, that is why we have the subselect there
        $sql = "SELECT $fields 
              FROM {local_ecommerce_cat} c
             WHERE $wheres 
              $orderby";

        $start = $limit * $page;
        $dbcategories = $DB->get_records_sql($sql, $params, $start, $limit);

        if (count($dbcategories)) {
            foreach ($dbcategories as $category) {
                $category->image = self::get_category_image($category);
                $category->description = self::get_category_info($category);
                $categories[] = $category;
            }
        }

        $sql = "SELECT COUNT(c.id) as categories 
              FROM {local_ecommerce_cat} c
             WHERE $wheres 
              $orderby";
        $categories_count = $DB->get_record_sql($sql, $params);
        $total = ($categories_count->categories) ? $categories_count->categories : 0;

        return array('total' => $total, 'categories' => $categories);
    }

    public static function get_category_image($category) {
        global $CFG, $OUTPUT;

        $systemcontext = context_system::instance();

        $image_url = '';
        $fs = get_file_storage();
        $imgfiles = $fs->get_area_files($systemcontext->id, 'local_ecommerce', 'categoryimage', $category->id);
        foreach ($imgfiles as $file) {
            $filename = $file->get_filename();
            $filetype = $file->get_mimetype();
            if ($filename == '.' or !$filetype) {
                continue;
            }
            $url = moodle_url::make_pluginfile_url($systemcontext->id, 'local_ecommerce', 'categoryimage', $category->id, '/',
                    $filename);
            $image_url = $url->out();
        }

        if ($image_url == "") {
            $image_url = $OUTPUT->image_url('productimg', 'local_ecommerce')->out();
        }

        return $image_url;
    }

    public static function get_category_info($category) {
        global $CFG, $USER, $DB;
        require_once($CFG->dirroot.'/local/ecommerce/lib.php');

        $output = '';

        if (is_int($category)) {
            $category = $DB->get_record('local_ecommerce_cat', array('id'=>$category));
        }

        if (isset($category->description) and !empty($category->description)) {
            // product description
            if (!empty($category->description)) {
                $output = local_ecommerce_get_formatted_text($category->description, context_system::instance(), $category->id, 'description');
            }
        }

        return $output;
    }
}
