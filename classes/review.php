<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;
use MoodleQuickForm_autocomplete;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class review {

    public static function get_user_rating($productID) {
        global $DB, $USER;
        return $DB->get_field('local_ecommerce_ratings', 'rating', array('userid' => ((isset($USER->id)) ? $USER->id : 0), 'productid' => $productID));
    }

    public static function upsert_product_rating ($data = null) {
        global $DB;

        if ($data->id) {
            $data->timeupdated = time();
            $DB->update_record('local_ecommerce_ratings', $data);
            return $data->id;
        } else {
            $data->timecreated = time();
            return $DB->insert_record('local_ecommerce_ratings', $data);
        }
    }

    public static function get_rating($productID) {
        global $DB, $USER;
        $returnObject = $DB->get_record_sql('SELECT SUM(rating) AS total, COUNT(id) AS "count" FROM {local_ecommerce_ratings} WHERE productid = :productid', array('productid' => $productID));
        $returnObject->review = $DB->get_field_sql('SELECT review FROM {local_ecommerce_ratings} WHERE productid = :productid AND userid = :userid', array('productid' => $productID, 'userid' => (isset($USER->id)) ? $USER->id : 0));
        return $returnObject;
    }

    public static function get_reviews($productID, $page = 0) {
        global $DB;
        if (!is_int($page + 0)) {
            $page = 0;
        } else {
            $page *= 3;
        }
        $reviews = $DB->get_records_sql('SELECT r.rating, r.review, u.firstname FROM {local_ecommerce_ratings} r JOIN {user} u ON u.id = r.userid WHERE productid = :productid LIMIT ' . $page . ', 3', array('productid' => $productID));
        $reviewArray = array();
        foreach ($reviews as $review) {
            $returnObject = new stdClass();
            $returnObject->rating = $review->rating * 20;
            $returnObject->review = $review->review;
            $returnObject->firstname = $review->firstname;
            $reviewArray[] = $returnObject;
        }
        $count = $DB->get_field_sql('SELECT COUNT(id) FROM  {local_ecommerce_ratings} WHERE productid = :productid', array('productid' => $productID));
        $returnArray = array(
            'total' => $count,
            'reviews' => $reviewArray
        );
        return $returnArray;
    }

    public static function load_more_reviews($productID, $page) {
        global $PAGE;
        $context = context_system::instance();
        $PAGE->set_context($context);
        $renderer = $PAGE->get_renderer('local_ecommerce');
        $reviews = self::get_reviews($productID, $page);
        return $renderer->store_print_reviews('', $productID, $reviews, $page);
    }

    public static function get_product_review($id) {
        global $DB;
        $returnObject = $DB->get_record_sql('SELECT r.id, p.name, r.rating, r.review, u.username, u.firstname, u.lastname, DATE_FORMAT(FROM_UNIXTIME(r.timecreated), "%d %M %Y") as "reviewcreated", (SELECT COUNT(id) FROM {local_ecommerce_ratings} r2 WHERE r2.userid = r.userid) AS "user_review_count"
                                            FROM {local_ecommerce_ratings} r
                                            LEFT JOIN {local_ecommerce_products} p ON p.id = r.productid
                                            JOIN {user} u ON u.id = r.userid
                                            WHERE r.id = :ratingid', array('ratingid' => $id));
        $returnObject->starcount = number_format($returnObject->rating, 1);
        $returnObject->rating = $returnObject->rating * 20;
        return $returnObject;
    }

    public static function delete_review($id) {
        global $DB;
        $DB->delete_records('local_ecommerce_ratings', array('id' => $id));
    }

    public static function get_options() {
        global $DB;
        $tree = $DB->get_records_sql('SELECT DISTINCT(p.id), p.name
                                            FROM {local_ecommerce_ratings} r
                                            LEFT JOIN {local_ecommerce_products} p ON p.id = r.productid
                                            WHERE p.visible = 1');
        $options = array();
        if (count($tree)) {
            foreach ($tree as $product) {
                $options[$product->id] = $product->name;
            }
        }
        return $options;
    }
}
