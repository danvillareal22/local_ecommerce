<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local stuff for category enrolment plugin.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2017 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Event observer for transcripts.
 */
class local_ecommerce_observer {

    /**
     * Triggered when 'user_deleted' event is triggered.
     *
     * @param \core\event\user_created $event
     */
    public static function local_ecommerce_user_deleted(\core\event\user_deleted $event) {
        global $DB, $CFG, $USER;

        if (get_config('local_ecommerce', 'enabled')){
            $userid = $event->objectid;

            $DB->delete_records('local_ecommerce', array('userid' => $userid));
            $DB->delete_records('local_ecommerce_checkout', array('userid' => $userid));
            $DB->delete_records('local_ecommerce_logs', array('userid' => $userid));
            $DB->delete_records('local_ecommerce_ratings', array('userid' => $userid));
            $DB->delete_records('local_ecommerce_waitlist', array('userid' => $userid));
            $DB->delete_records('local_ecommerce_wishlist', array('userid' => $userid));
        }

    }

    /**
     * Triggered when 'course_deleted' event is triggered.
     *
     * @param \core\event\course_deleted $event
     */
    public static function local_ecommerce_course_deleted(\core\event\course_deleted $event) {
        global $DB, $CFG, $USER;

        if (get_config('local_ecommerce', 'enabled')){
            $courseid = $event->objectid;

            $DB->delete_records('local_ecommerce_relations', array('instanceid' => $courseid, 'type' => 'course'));
        }

    }

}
