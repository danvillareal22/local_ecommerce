<?php
/**
 * The local_ecommerce event.
 *
 * @package   local_ecommerce
 * @author    SEBALE
 * @copyright 2017
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v2 or later
 */

namespace local_ecommerce\event;
defined('MOODLE_INTERNAL') || die();

class local_ecommerce_product_created extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'c';
        $this->data['edulevel'] = self::LEVEL_PARTICIPATING;
        $this->data['objecttable'] = 'local_ecommerce';
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_product_created', 'local_ecommerce');
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        $a = (object) array('userid' => $this->userid, 'productid' => $this->objectid);
        return get_string('event_product_created_description', 'local_ecommerce', $a);
    }

    /**
     * Return the legacy event log data.
     *
     * @return array
     */
    protected function get_legacy_logdata() {
        return(array($this->courseid, 'course', 'product created',
                'view.php?pageid=' . $this->objectid, get_string('event_product_created', 'local_ecommerce'), $this->contextinstanceid));
    }

    /**
     * Get URL related to the action.
     *
     * @return \moodle_url
     */
    public function get_url() {
        return new \moodle_url('/local/local_ecommerce/editproducts.php', array('id' => $this->objectid));
    }

    public function get_id() {
        return $this->objectid;
    }

    public function get_userid() {
        return $this->userid;
    }
}
