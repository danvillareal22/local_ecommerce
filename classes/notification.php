<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;

/**
 * notification functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class notification {

    public static function send_checkout_notification($checkout, $manual = false) {
        global $CFG, $DB;

        if (!get_config('local_ecommerce', 'sendcheckoutnotification') and !$manual) {
            return;
        }

        $products = array();
        if (!empty($checkout->items)) {
            $products = $DB->get_records_sql("
                        SELECT p.*, l.price, l.discountprice, l.discount
                          FROM {local_ecommerce_logs} l 
                     LEFT JOIN {local_ecommerce_products} p ON p.id = l.instanceid  
                         WHERE l.checkoutid = :checkoutid AND l.type = 'product' 
                      ORDER BY p.name", array('checkoutid'=>$checkout->id));
        }

        // Holds values for the essayemailsubject string for the email message
        $a = new stdClass;
        $a->products = $products;
        $a->productslist = '';
        $a->currency = get_config('local_ecommerce', 'currency').' ';
        $a->salestaxname = get_config('local_ecommerce', 'sales_tax_name');
        $a->salestaxpercentage = get_config('local_ecommerce', 'sales_tax_percentage');

        if (count($products)) {
            $products_names = array();
            $reviewURLs = array();
            foreach ($products as $product) {
                $products_names[] = $product->name;
                if (get_config('local_ecommerce', 'enable_reviews')) {
                    $reviewURLs[] = $product->name . ': ' . $CFG->wwwroot . '/local/ecommerce/view.php?id=' . $product->id;
                }
            }
            $a->productslist = implode(', ', $products_names);
            $a->reviewurls = implode(', ', $reviewURLs);
        }

        $userto = $DB->get_record('user', array('id'=>$checkout->userid));
        $a->username = fullname($userto);
        $a->amount = $checkout->amount;
        $a->subtotal = $checkout->subtotal;
        $a->discount = $checkout->discount;
        $a->salestax = $checkout->sales_tax;
        $a->orderid = $checkout->id;


        $userfrom = \core_user::get_support_user();
        $userfrom->maildisplay = true;

        // Fetch message HTML and plain text formats
        $config_message = ($manual) ? get_config('local_ecommerce', 'invoice_message') : get_config('local_ecommerce', 'checkout_message');

        if (!empty($config_message)) {
            foreach ($a as $name => $value) {
                $config_message = str_replace("[[$name]]", $value, $config_message);
            }
            $message = $config_message;
        } else {
            $message  = ($manual) ? get_string('invoiceemailbody', 'local_ecommerce', $a) : get_string('checkoutemailbody', 'local_ecommerce', $a);
        }

        $plaintext = format_text_email($message, FORMAT_HTML);

        // Subject
        $subject = ($manual) ? get_config('local_ecommerce', 'invoice_notification_subject') : get_config('local_ecommerce', 'checkout_subject');

        $eventdata = new \core\message\message();
        $eventdata->userfrom         = $userfrom;
        $eventdata->userto           = $userto;
        $eventdata->subject          = $subject;
        $eventdata->fullmessage      = $plaintext;
        $eventdata->fullmessageformat = FORMAT_HTML;
        $eventdata->fullmessagehtml  = $message;
        $eventdata->smallmessage     = '';
        $eventdata->notification     = 1;

        // Required for messaging framework
        $eventdata->component = 'local_ecommerce';
        $eventdata->name = 'checkout_notify';

        if (get_config('local_ecommerce', 'attachreceipt') or $manual) {
            $filename = clean_filename("invoice".$checkout->id."_".date('m_d_Y').'_'.time().".pdf");
            $usercontext = \context_user::instance($userto->id);
            $file = new stdClass;
            $file->contextid = $usercontext->id;
            $file->component = 'user';
            $file->filearea  = 'private';
            $file->itemid    = 0;
            $file->filepath  = '/';
            $file->filename  = $filename;

            $filecontents = \local_ecommerce\invoices::generate_invoice($checkout, $a, $file->filename);

            $fs = get_file_storage();
            $file = $fs->create_file_from_string($file, $filecontents);
            $eventdata->attachment = $file;
            $eventdata->attachname = $filename;
        }

        message_send($eventdata);

        if (!get_config('local_ecommerce', 'enable_thank_you_email')) {
            return;
        }

        $message = get_config('local_ecommerce', 'thank_you_message');

        if (!empty($message)) {
            foreach ($a as $name => $value) {
                $message = str_replace("[[$name]]", $value, $message);
            }
        } else {
            return;
        }

        $plaintext = format_text_email($message, FORMAT_HTML);

        // Subject
        $subject = get_config('local_ecommerce', 'thank_you_subject');

        $eventdata = new \core\message\message();
        $eventdata->userfrom         = $userfrom;
        $eventdata->userto           = $userto;
        $eventdata->subject          = $subject;
        $eventdata->fullmessage      = $plaintext;
        $eventdata->fullmessageformat = FORMAT_HTML;
        $eventdata->fullmessagehtml  = $message;
        $eventdata->smallmessage     = '';
        $eventdata->notification     = 1;

        // Required for messaging framework
        $eventdata->component = 'local_ecommerce';
        $eventdata->name = 'checkout_notify';

        message_send($eventdata);

        return true;
    }

    public static function send_waitlist_notification($item, $manual = false) {
        global $CFG, $DB;

        $product = $DB->get_record('local_ecommerce_products', array('id'=>$item->productid));
        $userto = $DB->get_record('user', array('id'=>$item->userid));

        $checkouturl = new moodle_url('/local/ecommerce/view.php', array('id'=>$product->id, 'key'=>$item->seatkey));

        // Holds values for the email message
        $a = new stdClass;
        $a->product = format_string($product->name);
        $a->username = fullname($userto);
        $a->checkouturl = $checkouturl->out();

        $userfrom = \core_user::get_support_user();
        $userfrom->maildisplay = true;

        // Fetch message HTML and plain text formats
        $waitlist_message = get_config('local_ecommerce', 'waitlist_message');

        if (!empty($waitlist_message)) {
            foreach ($a as $name => $value) {
                $waitlist_message = str_replace("[[$name]]", $value, $waitlist_message);
            }
            $message = $waitlist_message;
        } else {
            $message  = get_string('waitlistmailbody', 'local_ecommerce', $a);
        }

        $plaintext = format_text_email($message, FORMAT_HTML);

        // Subject
        $subject = get_string('waitlistemailsubject', 'local_ecommerce', $a);

        $eventdata = new \core\message\message();
        $eventdata->userfrom         = $userfrom;
        $eventdata->userto           = $userto;
        $eventdata->subject          = $subject;
        $eventdata->fullmessage      = $plaintext;
        $eventdata->fullmessageformat = FORMAT_HTML;
        $eventdata->fullmessagehtml  = $message;
        $eventdata->smallmessage     = '';
        $eventdata->notification     = 1;

        // Required for messaging framework
        $eventdata->component = 'local_ecommerce';
        $eventdata->name = 'waitlist_notify';

        message_send($eventdata);

        return true;
    }

    public static function send_invoicepayment_notification($checkout) {
        global $CFG, $DB;

        $recipients = get_users_by_capability(\context_system::instance(), 'local/ecommerce:approveinvoices');
        if (!count($recipients)) {
            return;
        }

        $products = array();
        if (!empty($checkout->items)) {
            $products = $DB->get_records_sql("
                        SELECT p.*, l.price, l.discountprice, l.discount
                          FROM {local_ecommerce_logs} l 
                     LEFT JOIN {local_ecommerce_products} p ON p.id = l.instanceid  
                         WHERE l.checkoutid = :checkoutid AND l.type = 'product' 
                      ORDER BY p.name", array('checkoutid'=>$checkout->id));
        }

        // Holds values for the essayemailsubject string for the email message
        $a = new stdClass;
        $a->products = $products;
        $a->productslist = '';
        $a->currency = get_config('local_ecommerce', 'currency').' ';
        $a->salestaxname = get_config('local_ecommerce', 'sales_tax_name');
        $a->salestaxpercentage = get_config('local_ecommerce', 'sales_tax_percentage');

        if (count($products)) {
            $products_names = array();
            $reviewURLs = array();
            foreach ($products as $product) {
                $products_names[] = $product->name;
                if (get_config('local_ecommerce', 'enable_reviews')) {
                    $reviewURLs[] = $product->name . ': ' . $CFG->wwwroot . '/local/ecommerce/view.php?id=' . $product->id;
                }
            }
            $a->productslist = implode(', ', $products_names);
            $a->reviewurls = implode(', ', $reviewURLs);
        }

        $user = $DB->get_record('user', array('id'=>$checkout->userid));
        $a->username = fullname($user);
        $a->amount = $checkout->amount;
        $a->subtotal = $checkout->subtotal;
        $a->discount = $checkout->discount;
        $a->salestax = $checkout->sales_tax;
        $a->orderid = $checkout->id;

        $userfrom = \core_user::get_support_user();
        $userfrom->maildisplay = true;

        // Fetch message HTML and plain text formats
        $config_message = get_config('local_ecommerce', 'invoicepayment_message');

        if (!empty($config_message)) {
            foreach ($a as $name => $value) {
                $config_message = str_replace("[[$name]]", $value, $config_message);
            }
            $message = $config_message;
        } else {
            $message  = get_string('invoicepaymentmailbody', 'local_ecommerce', $a);
        }

        $plaintext = format_text_email($message, FORMAT_HTML);

        // Subject
        $subject = get_string('invoicepaymentmailsubject', 'local_ecommerce', $a);

        $eventdata = new \core\message\message();
        $eventdata->userfrom         = $userfrom;
        $eventdata->subject          = $subject;
        $eventdata->fullmessage      = $plaintext;
        $eventdata->fullmessageformat = FORMAT_HTML;
        $eventdata->fullmessagehtml  = $message;
        $eventdata->smallmessage     = '';
        $eventdata->notification     = 1;

        // Required for messaging framework
        $eventdata->component = 'local_ecommerce';
        $eventdata->name = 'invoicerequest_notify';

        $filename = clean_filename("invoice".$checkout->id."_".date('m_d_Y').'_'.time().".pdf");
        $usercontext = \context_user::instance($user->id);
        $file = new stdClass;
        $file->contextid = $usercontext->id;
        $file->component = 'user';
        $file->filearea  = 'private';
        $file->itemid    = 0;
        $file->filepath  = '/';
        $file->filename  = $filename;

        $filecontents = \local_ecommerce\invoices::generate_invoice($checkout, $a, $file->filename);

        $fs = get_file_storage();
        $file = $fs->create_file_from_string($file, $filecontents);
        $eventdata->attachment = $file;
        $eventdata->attachname = $filename;

        if (count($recipients)) {
            foreach ($recipients as $recipient) {
                $eventdata->userto = $recipient;
                message_send($eventdata);
            }
        }

        return true;
    }

    public static function send_contact_email($data) {
        global $CFG, $DB;

        $userTo = $DB->get_record('user', array('id'=>2));

        // Holds values for the email message
        $a = new stdClass;
        $a->name = format_string($data->name);
        $a->email = $data->email;
        $a->phone = $data->phone;
        $a->message = $data->message;

        $userFrom = \core_user::get_support_user();
        $userFrom->maildisplay = true;
        // Fetch message HTML and plain text formats
        $contactMessage = get_config('local_ecommerce', 'contact_us_email_text');

        if (!empty($contactMessage)) {
            foreach ($a as $name => $value) {
                $contactMessage = str_replace("[[$name]]", $value, $contactMessage);
            }
            $message = $contactMessage;
        } else {
            return false;
        }

        $plaintext = format_text_email($message, FORMAT_HTML);

        // Subject
        $subject = $data->subject;

        $eventdata = new \core\message\message();
        $eventdata->userfrom         = $userFrom;
        $eventdata->userto           = $userTo;
        $eventdata->subject          = $subject;
        $eventdata->fullmessage      = $plaintext;
        $eventdata->fullmessageformat = FORMAT_HTML;
        $eventdata->fullmessagehtml  = $message;
        $eventdata->smallmessage     = '';
        $eventdata->notification     = 1;

        // Required for messaging framework
        $eventdata->component = 'local_ecommerce';
        $eventdata->name = 'checkout_notify';

        if (message_send($eventdata) !== false) {
            return true;
        }
        return false;
    }
}
