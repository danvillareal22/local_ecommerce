<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class settings {

    public static $CORPORATE_HOST = 'https://app.intelliboard.net/CartApi/?';

    public static function check_license() {
        return true;
        global $DB, $USER, $SITE, $CFG;

        $return = false;

        require_once($CFG->libdir . '/filelib.php');
        $license_email = get_config('local_ecommerce', 'license_email');
        $license_apikey = get_config('local_ecommerce', 'license_apikey');

        if (empty($license_email) or empty($license_apikey)) {
            return false;
        }

        $c = new \curl();

        $params = ['email' => $license_email, 'url' => $CFG->wwwroot, 'apikey' => $license_apikey];
        $result = $c->post(self::$CORPORATE_HOST, http_build_query($params, null, '&'));
        $result = json_decode($result);

        if (!isset($result->shoppingcartkey) or !$result->shoppingcartkey) {
            return false;
        } else {
            return true;
        }
    }

}
