<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;
use MoodleQuickForm_autocomplete;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class wishlist {

    public static function save_wishlist_item($data) {
        global $DB;

        if ($data->id) {
            $DB->update_record('local_ecommerce_wishlist', $data);
            return $data->id;
        } else {
            $data->timecreated = time();
            return $DB->insert_record('local_ecommerce_wishlist', $data);
        }
    }

    public static function get_wishlist() {
        global $DB, $USER;
        $productArray = array();
        $params = array('userid' => ((isset($USER->id)) ? $USER->id : 0), 'userid2' => ((isset($USER->id)) ? $USER->id : 0), 'userid3' => ((isset($USER->id)) ? $USER->id : 0));
        $wheres = array("p.visible > 0", 'wish.userid = :userid3');
        $wheres = implode(" AND ", $wheres);
        $wishlistProducts = $DB->get_records_sql('SELECT p.id, p.`name`, p.price, p.description, p.seats, crt.id AS incart, s.sold, wl.wlsize, w.id AS inwaitlist
                                  FROM {local_ecommerce_wishlist} wish
                                  LEFT JOIN {local_ecommerce_products} p ON p.id = wish.productid
                                  LEFT JOIN {local_ecommerce} crt ON crt.productid = p.id AND crt.userid = :userid
                                  LEFT JOIN (SELECT instanceid, COUNT(id) as sold FROM {local_ecommerce_logs} WHERE `type` = "product" AND `status` = "completed" GROUP BY instanceid) s ON s.instanceid = p.id
                                  LEFT JOIN (SELECT productid, COUNT(id) as wlsize FROM {local_ecommerce_waitlist} GROUP BY productid) wl ON wl.productid = p.id 
                                  LEFT JOIN {local_ecommerce_waitlist} w ON w.productid = p.id AND w.userid = :userid2
                                  WHERE ' . $wheres, $params);
        if (is_array($wishlistProducts) && count($wishlistProducts) > 0) {
            foreach ($wishlistProducts as $product) {
                $product->price = format_float($product->price, 2, false);
                $product->image = product::get_product_image($product);
                $product->description = product::get_product_info($product);
                $product->currency = payment::get_currency();

                $products_in_logs = (get_config('local_ecommerce', 'enablewaitlist')) ? (int)$product->sold + (int)$product->wlsize : $product->sold;
                $product->seatsallow = (
                    ($product->seats == 0 or
                        ($product->seats and $product->seats > $products_in_logs)
                    ) and !$product->inwaitlist);

                $producturl = new moodle_url('/local/ecommerce/view.php', array('id'=>$product->id));
                $product->producturl = $producturl->out();
                $productArray[] = $product;
            }
        }

        return $productArray;
    }

    public static function delete_wishlist_item($productID) {
        global $DB, $USER;
        $DB->delete_records('local_ecommerce_wishlist', array('userid' => $USER->id, 'productid' => $productID));
    }
}
