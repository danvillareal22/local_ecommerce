<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package   local_ecommerce
 * @category  task
 * @copyright 2018 SEBALE
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_ecommerce\task;

/**
 * Task to process ecommerce waitlist.
 *
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class waitlist_task extends \core\task\scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('waitlist_task', 'local_ecommerce');
    }

    /**
     * Do the job.
     * Throw exceptions on errors (the job will be retried).
     */
    public function execute() {
        global $DB, $USER, $SITE, $CFG;

        if (!get_config('local_ecommerce', 'enablewaitlist')) {
            return;
        }

        mtrace("eCommerce Waitlist CRON started!");

        $waitlist_duration = get_config('local_ecommerce', 'waitlist_duration');
        $expirationdate = time() - $waitlist_duration;

        $items = $DB->get_records_sql("
              SELECT * 
                FROM {local_ecommerce_waitlist} 
               WHERE sent = :sent AND timemodified < :expirationdate
            ORDER BY timemodified ASC",
                ['sent' => 1, 'expirationdate' => $expirationdate]);

        $products = [];
        if (count($items)) {
            foreach ($items as $item) {
                $products[$item->productid] = (isset($products[$item->productid])) ? $products[$item->productid]+1 : 1;
                $DB->delete_records('local_ecommerce_waitlist', ['id' => $item->id]);
            }
        }

        mtrace("eCommerce Waitlist [".count($items)."] items removed!");

        $i = 0;
        if (count($products)) {
            foreach ($products as $productid=>$seats) {
                $items = $DB->get_records_sql("
              SELECT * 
                FROM {local_ecommerce_waitlist} 
               WHERE productid = :productid AND sent = :sent
            ORDER BY timemodified ASC LIMIT 0, $seats",
                        ['productid' => $productid, 'sent' => 0]);

                if (count($items)) {
                    foreach ($items as $item) {
                        \local_ecommerce\notification::send_waitlist_notification($item);

                        $item->sent = 1;
                        \local_ecommerce\waitlist::save_list_item($item);
                        $i++;
                    }
                }
            }
        }

        mtrace("eCommerce Waitlist [".$i."] items sent!");

        mtrace("eCommerce Waitlist CRON completed!");
    }

}
