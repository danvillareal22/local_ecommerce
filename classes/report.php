<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;

/**
 * notification functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class report {

    public static function get_dashboard_data($daterange) {
        global $CFG, $DB;

        $statistic = self::get_dashboard_statistic($daterange);
        $widgets = self::get_dashboard_widgets($daterange);
        return [
                'graph' => self::get_dashboard_maingraph($daterange),
                'statistic' => $statistic,
                'statisticblocks' => count($statistic),
                'widgets' => $widgets,
                'widgetsblocks' => count($widgets),
        ];
    }

    public static function get_dashboard_maingraph($daterange) {
        global $CFG, $DB;

        // main graph
        $timefinish = (!empty($daterange['timefinish'])) ? $daterange['timefinish'] : time();
        $timestart = (!empty($daterange['timestart'])) ? $daterange['timestart'] : strtotime('last month');
        $ext = 86400;
        $format = 'dd MMM';

        $params = array('timestart' => $timestart, 'timefinish' => $timefinish);
        $data_points = array();

        $paidusers = $DB->get_records_sql("
			SELECT floor(timeupdated / $ext) * $ext AS timepointval, COUNT(DISTINCT (userid)) AS users
			FROM {local_ecommerce_checkout}
			WHERE timeupdated BETWEEN :timestart AND :timefinish AND payment_status = :payment_status 
			GROUP BY timepointval",
                $params + array('payment_status' => \local_ecommerce\payment::$STATUS_COMPLETED)
        );

        if (count($paidusers)) {
            foreach ($paidusers as $item) {
                $data_points[$item->timepointval]['users'] = ($item->users) ? $item->users : 0;
            }
        }

        if (get_config('local_ecommerce', 'enablecoupons')) {
            $couponsused = $DB->get_records_sql("
			SELECT floor(timemodified / $ext) * $ext AS timepointval, COUNT(id) AS coupons
			FROM {local_ecommerce_logs}
			WHERE timemodified BETWEEN :timestart AND :timefinish AND status = :status AND type = :type
			GROUP BY timepointval",
                    $params + array('status' => \local_ecommerce\payment::$STATUS_COMPLETED, 'type' => 'coupon')
            );

            if (count($couponsused)) {
                foreach ($couponsused as $item) {
                    $data_points[$item->timepointval]['couponsused'] = ($item->coupons) ? $item->coupons : 0;
                }
            }
        }

        if (get_config('local_ecommerce', 'enablediscounts')) {
            $discountsapplied = $DB->get_records_sql("
			SELECT floor(timemodified / $ext) * $ext AS timepointval, COUNT(id) AS discounts
			FROM {local_ecommerce_logs}
			WHERE timemodified BETWEEN :timestart AND :timefinish AND status = :status AND type = :type
			GROUP BY timepointval",
                    $params + array('status' => \local_ecommerce\payment::$STATUS_COMPLETED, 'type' => 'discount')
            );
            if (count($discountsapplied)) {
                $json_data = array();
                foreach ($discountsapplied as $item) {
                    $data_points[$item->timepointval]['discounts'] = ($item->discounts) ? $item->discounts : 0;
                }
            }
        }

        if (get_config('local_ecommerce', 'enablewaitlist')) {
            $waitlistseats = $DB->get_records_sql("
			SELECT floor(timemodified / $ext) * $ext AS timepointval, COUNT(id) AS seats
			FROM {local_ecommerce_logs}
			WHERE timemodified BETWEEN :timestart AND :timefinish AND type = :type
			GROUP BY timepointval",
                    $params + array('type' => 'seatkey')
            );
            if (count($waitlistseats)) {
                foreach ($waitlistseats as $item) {
                    $data_points[$item->timepointval]['seats'] = ($item->seats) ? $item->seats : 0;
                }
            }
        }

        if (count($data_points)) {
            $json_data = array();
            foreach($data_points as $timepointval=>$notneeded){
                $day = date("j", $timepointval);
                $month = date("n", $timepointval) - 1;
                $year = date("Y", $timepointval);

                $u = (isset($data_points[$timepointval]['users'])) ? $data_points[$timepointval]['users'] : 0;
                if (get_config('local_ecommerce', 'enablecoupons')) {
                    $c = (isset($data_points[$timepointval]['couponsused'])) ? $data_points[$timepointval]['couponsused'] : 0;
                }
                if (get_config('local_ecommerce', 'enablediscounts')) {
                    $d = (isset($data_points[$timepointval]['discounts'])) ? $data_points[$timepointval]['discounts'] : 0;
                }
                if (get_config('local_ecommerce', 'enablewaitlist')) {
                    $w = (isset($data_points[$timepointval]['seats'])) ? $data_points[$timepointval]['seats'] : 0;
                }

                $json_data[] = "[new Date($year, $month, $day),
                    ".$u."
                    ".((get_config('local_ecommerce', 'enablecoupons')) ? ','.$c : '')."
                    ".((get_config('local_ecommerce', 'enablediscounts')) ? ','.$d : '')."
                    ".((get_config('local_ecommerce', 'enablewaitlist')) ? ','.$w : '')."
                ]";
            }

            return ($json_data) ? implode(",", $json_data) : "";
        } else {
            return "";
        }
    }

    public static function get_dashboard_statistic($daterange) {
        global $CFG, $DB, $OUTPUT;

        // statistics blocks
        $statistic = array();
        $params = array();
        $where = '';

        if (!empty($daterange['timefinish']) and !empty($daterange['timestart'])) {
            $where = ' AND timecreated BETWEEN :timestart AND :timefinish';
            $params += array('timestart' => $daterange['timestart'], 'timefinish' => $daterange['timefinish']);
        }
        $registeredusers = $DB->count_records_sql("SELECT COUNT(id) as stats FROM {user} WHERE deleted = 0 AND id > 1 $where", $params);
        $statistic[] = array('name' => get_string('registeredusers', 'local_ecommerce'), 'data' => $registeredusers, 'icon' => 'sh-icon-users');

        $confirmedusers = $DB->count_records_sql("SELECT COUNT(id) as stats FROM {user} WHERE confirmed = 1 AND deleted = 0 AND id > 1 $where", $params);
        $statistic[] = array('name' => get_string('confirmedusers', 'local_ecommerce'), 'data' => $confirmedusers, 'icon' => 'sh-icon-users_many');

        if (get_config('local_ecommerce', 'enablecoupons')) {
            if (!empty($daterange['timefinish']) and !empty($daterange['timestart'])) {
                $where = ' AND timemodified BETWEEN :timestart AND :timefinish';
            }
            $usedcoupons =
                    $DB->count_records_sql("SELECT COUNT(id) as stats FROM {local_ecommerce_logs} WHERE type = :type AND status = :status $where",
                            $params + array('type' => 'coupon', 'status' => \local_ecommerce\payment::$STATUS_COMPLETED));
            $statistic[] = array('name' => get_string('usedcoupons', 'local_ecommerce'), 'data' => $usedcoupons,
                    'icon' => 'sh-icon-coupons_used');
        }

        if (get_config('local_ecommerce', 'enablediscounts')) {
            if (!empty($daterange['timefinish']) and !empty($daterange['timestart'])) {
                $where = ' AND timemodified BETWEEN :timestart AND :timefinish';
            }
            $discountsapplied =
                    $DB->count_records_sql("SELECT COUNT(id) as stats FROM {local_ecommerce_logs} WHERE type = :type AND status = :status $where",
                            $params + array('type' => 'discount', 'status' => \local_ecommerce\payment::$STATUS_COMPLETED));
            $statistic[] = array('name' => get_string('discountsapplied', 'local_ecommerce'), 'data' => $discountsapplied,
                    'icon' => 'sh-icon-discounts');
        }

        if (!empty($daterange['timefinish']) and !empty($daterange['timestart'])) {
            $where = ' AND timemodified BETWEEN :timestart AND :timefinish';
        }
        $productscreated = $DB->count_records_sql("SELECT COUNT(id) as stats FROM {local_ecommerce_products} WHERE visible = 1 $where", $params);
        $statistic[] = array('name' => get_string('productscreated', 'local_ecommerce'), 'data' => $productscreated, 'icon' => 'sh-icon-products_created');

        if (!empty($daterange['timefinish']) and !empty($daterange['timestart'])) {
            $where = ' AND timeupdated BETWEEN :timestart AND :timefinish';
        }
        $ordersprocessed = $DB->count_records_sql("SELECT COUNT(id) as stats FROM {local_ecommerce_checkout} WHERE payment_status = :status $where", $params + array('status' => \local_ecommerce\payment::$STATUS_COMPLETED));
        $statistic[] = array('name' => get_string('ordersprocessed', 'local_ecommerce'), 'data' => $ordersprocessed, 'icon' => 'sh-icon-enrollments_course');

        return $statistic;
    }

    public static function get_dashboard_widgets($daterange) {
        global $CFG, $DB;

        // widgets blocks
        $widgets = array();
        $params = array();
        $where = '';

        if (!empty($daterange['timefinish']) and !empty($daterange['timestart'])) {
            $where .= ' AND l.timemodified BETWEEN :timestart AND :timefinish';
            $params += array('timestart' => $daterange['timestart'], 'timefinish' => $daterange['timefinish']);
        }

        // discount types used
        if (get_config('local_ecommerce', 'enablediscounts')) {
            $discounttypesused = $DB->get_records_sql("
                SELECT d.name as discount, COUNT(l.id) AS discounts FROM {local_ecommerce_logs} l 
             LEFT JOIN {local_ecommerce_discounts} d ON d.id = l.instanceid AND l.type = :type 
             WHERE l.status = :status AND d.status = :discountstatus $where GROUP BY d.id ORDER BY discounts LIMIT 10",
                    $params + array('type' => 'discount', 'discountstatus' => 1, 'status' => \local_ecommerce\payment::$STATUS_COMPLETED));

            $json_data = array();
            if (count($discounttypesused)) {
                foreach ($discounttypesused as $item) {
                    $json_data[] = "['".$item->discount."', ".(($item->discounts) ? $item->discounts : 0)."]";
                }
            }
            $widgets[] = array('name' => get_string('discounttypesused', 'local_ecommerce'), 'container' => 'discounttypesused',
                    'graphdata' => "[['".get_string('discounts', 'local_ecommerce')."', '".get_string('discounttypesused', 'local_ecommerce')."'], ".implode(',', $json_data)."]");
        }


        // coupons applied
        if (get_config('local_ecommerce', 'enablecoupons')) {
            $couponsapplied = $DB->get_records_sql("
                SELECT c.code as coupon, COUNT(l.id) AS coupons FROM {local_ecommerce_logs} l 
             LEFT JOIN {local_ecommerce_coupons} c ON c.id = l.instanceid AND l.type = :type 
             WHERE l.status = :status AND c.status = :couponstatus $where GROUP BY c.id ORDER BY coupons LIMIT 10",
                    $params + array('type' => 'coupon', 'couponstatus' => 1, 'status' => \local_ecommerce\payment::$STATUS_COMPLETED));

            $json_data = array();
            if (count($couponsapplied)) {
                foreach ($couponsapplied as $item) {
                    $json_data[] = "['".$item->coupon."', ".(($item->coupons) ? $item->coupons : 0)."]";
                }
            }
            $widgets[] = array('name' => get_string('couponsapplied', 'local_ecommerce'), 'container' => 'couponsapplied',
                    'graphdata' => "[['".get_string('coupons', 'local_ecommerce')."', '".get_string('couponsapplied', 'local_ecommerce')."'], ".implode(',', $json_data)."]");
        }

        // payment types
        if (!empty($daterange['timefinish']) and !empty($daterange['timestart'])) {
            $where = ' AND ch.timeupdated BETWEEN :timestart AND :timefinish';
        }
        $paymenttypes = $DB->get_records_sql("
                SELECT pt.name, COUNT(ch.id) AS types 
                  FROM {local_ecommerce_checkout} ch 
             LEFT JOIN {local_ecommerce_payments} pt ON pt.id = ch.paymentid  
             WHERE ch.payment_status = :payment_status $where GROUP BY pt.id ORDER BY types LIMIT 10",
                $params + array('payment_status'=>\local_ecommerce\payment::$STATUS_COMPLETED));

        $json_data = array();
        if (count($paymenttypes)) {
            foreach ($paymenttypes as $type) {
                $name = ($type->name) ? $type->name : get_string('manual', 'local_ecommerce');
                $json_data[] = "['".$name."', ".(($type->types) ? $type->types : 0)."]";
            }
        }

        $widgets[] = array('name' => get_string('paymenttypes', 'local_ecommerce'), 'container' => 'paymenttypes',
                'graphdata' => "[['".get_string('paymenttypes', 'local_ecommerce')."', '".get_string('paymenttypes', 'local_ecommerce')."'], ".implode(',', $json_data)."]");

        return $widgets;
    }

    public static function get_reports_list() {

        $reports = array(
            array(
                'id'          => 1,
                'title'       => get_string('report1title', 'local_ecommerce'),
                'description' => get_string('report1description', 'local_ecommerce'),
                'url'         => new moodle_url('/local/ecommerce/reports/report.php', array('id'=>1)),
            ),
            array(
                'id'          => 2,
                'title'       => get_string('report2title', 'local_ecommerce'),
                'description' => get_string('report2description', 'local_ecommerce'),
                'url'         => new moodle_url('/local/ecommerce/reports/report.php', array('id'=>2)),
            ),
            array(
                'id'          => 3,
                'title'       => get_string('report3title', 'local_ecommerce'),
                'description' => get_string('report3description', 'local_ecommerce'),
                'url'         => new moodle_url('/local/ecommerce/reports/report.php', array('id'=>3)),
            ),
        );

        if (get_config('local_ecommerce', 'enablecoupons')) {
            $reports[] = array(
                    'id' => 4,
                    'title' => get_string('report4title', 'local_ecommerce'),
                    'description' => get_string('report4description', 'local_ecommerce'),
                    'url' => new moodle_url('/local/ecommerce/reports/report.php', array('id' => 4)),
            );
        }

        if (get_config('local_ecommerce', 'enablediscounts')) {
            $reports[] = array(
                    'id' => 5,
                    'title' => get_string('report5title', 'local_ecommerce'),
                    'description' => get_string('report5description', 'local_ecommerce'),
                    'url' => new moodle_url('/local/ecommerce/reports/report.php', array('id' => 5)),
            );
        }

        return $reports;
    }

    public static function get_report($id = 0) {

        $reports = self::get_reports_list();
        $report = null;

        foreach ($reports as $r) {
            if ($r['id'] == $id) {
                $report = (object)$r;
            }
        }

        if (!$report) {
            print_error(get_string('wrongreportid', 'local_ecommerce'));
        }

        return $report;
    }

    public static function get_income() {
        global $DB;

        $data = $DB->get_record_sql("
            SELECT SUM(amount) as income 
              FROM {local_ecommerce_checkout}
             WHERE payment_status = :payment_status
        ", array('payment_status'=>\local_ecommerce\payment::$STATUS_COMPLETED));

        return \local_ecommerce\payment::get_current_currency_symbol() . (($data->income) ? number_format(format_float($data->income, 2, true), 2) : 0);
    }

    public static function get_income_chart_data() {
        global $DB;

        $income = $products = array();
        $json_data = array();

        for ($i = 1; $i <= 12; $i++) {
            $income[$i] = 0;
            $products[$i] = 0;
        }

        $income_data = $DB->get_records_sql("
            SELECT SUM(amount) as income, FROM_UNIXTIME(timeupdated, '%c') as fdate
              FROM {local_ecommerce_checkout}
             WHERE payment_status = :payment_status AND timeupdated >= :startdate
    GROUP BY MONTH(FROM_UNIXTIME(timeupdated,'%Y-%c-%d %H.%i.%s')), 
              YEAR(FROM_UNIXTIME(timeupdated,'%Y-%c-%d %H.%i.%s'))
          ORDER BY timeupdated ASC
        ", array('payment_status'=>\local_ecommerce\payment::$STATUS_COMPLETED, 'startdate' => time()-31536000 ));

        if (count($income_data)) {
            foreach ($income_data as $data) {
                $income[$data->fdate] = $data->income;
            }
        }


        $products_data = $DB->get_records_sql("
            SELECT COUNT(id) as products, FROM_UNIXTIME(timemodified, '%c') as fdate
              FROM {local_ecommerce_logs}
             WHERE status = :status AND type = :type AND timemodified >= :startdate
    GROUP BY MONTH(FROM_UNIXTIME(timemodified,'%Y-%c-%d %H.%i.%s')), 
              YEAR(FROM_UNIXTIME(timemodified,'%Y-%c-%d %H.%i.%s'))
          ORDER BY timemodified ASC
        ", array('type'=>'product', 'status'=>\local_ecommerce\payment::$STATUS_COMPLETED, 'startdate' => time()-31536000 ));

        if (count($products_data)) {
            foreach ($products_data as $data) {
                $products[$data->fdate] = $data->products;
            }
        }

        for ($i = 1; $i <= 12; $i++) {
            $json_data[] = "['".get_string('graphmon'.$i, 'local_ecommerce')."', $income[$i], $products[$i]]";
        }

        return (count($json_data)) ? implode(', ', $json_data) : "";
    }
}
