<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class waitlist {


    public static function save_list_item($data = null) {
        global $DB;

        if (!isset($data->seatkey) or empty($data->seatkey)) {
            $data->seatkey = self::generate_key();
        }
        $data->timemodified = time();

        if ($data->id) {
            $DB->update_record('local_ecommerce_waitlist', $data);
            return $data->id;

        } else {

            return $DB->insert_record('local_ecommerce_waitlist', $data);
        }
    }

    public static function generate_key() {
        global $DB;

        $key = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < 30; $i++) {
            $key .= $codeAlphabet[random_int(0, $max-1)];
        }

        if ($DB->record_exists('local_ecommerce_waitlist', array('seatkey' => $key))) {
            $key = self::generate_key();
        }

        return $key;
    }

    public static function delete_item($item = null) {
        global $DB, $USER, $SITE;

        if (has_capability('local/ecommerce:editwaitlist', \context_system::instance()) or $item->userid = $USER->id) {
            $DB->delete_records('local_ecommerce_waitlist', array('id' => $item->id));

            // delete logs
            $DB->delete_records('local_ecommerce_logs', array('type'=>'seatkey', 'instanceid'=>$item->productid, 'status'=>\local_ecommerce\payment::$STATUS_PENDING, 'userid'=>$item->userid));

        }
    }

    public static function check_allowcheckout($key = '', $product) {
        global $DB, $USER, $SITE;

        $wl_allowcheckout = false;

        if (!empty($key) and get_config('local_ecommerce', 'enablewaitlist')) {
            $wlitem = $DB->get_record('local_ecommerce_waitlist', array('seatkey'=>$key));
            if ($wlitem and $wlitem->userid == $USER->id and $wlitem->productid == $product->id and $wlitem->sent) {
                $wl_allowcheckout = true;
            }
        }

        return $wl_allowcheckout;
    }

    public static function get_product_count_waitlist($product) {
        global $DB, $USER, $SITE;

        $itemsinlist = 0;

        if (get_config('local_ecommerce', 'enablewaitlist')) {
            $itemsinlist = $DB->count_records('local_ecommerce_waitlist', array('productid'=>$product->id));
        }

        return $itemsinlist;
    }

    public static function process_product_waitlist($product, $data) {
        global $DB, $USER, $SITE;

        if (!get_config('local_ecommerce', 'enablewaitlist')) {
            return;
        }

        $items = [];
        if ($product->seats > 0 and $data->seats == 0) {

            $items = $DB->get_records_sql("
              SELECT * 
                FROM {local_ecommerce_waitlist} 
               WHERE productid = :productid AND sent = :sent
            ORDER BY timemodified ASC",
                    ['productid' => $product->id, 'sent' => 0]);

        } else if ($data->seats > $product->seats) {

            $limit = $data->seats-$product->seats;
            $items = $DB->get_records_sql("
              SELECT * 
                FROM {local_ecommerce_waitlist} 
               WHERE productid = :productid AND sent = :sent
            ORDER BY timemodified ASC LIMIT 0, $limit",
                    ['productid' => $product->id, 'sent' => 0]);
        }

        if (count($items)) {
            foreach ($items as $item) {
                \local_ecommerce\notification::send_waitlist_notification($item);

                $item->sent = 1;
                self::save_list_item($item);
            }
        }
    }

    public static function process_checkout($product, $checkout) {
        global $DB, $USER, $SITE;

        if (!get_config('local_ecommerce', 'enablewaitlist')) {
            return;
        }

        $waitlistitems = $DB->get_records('local_ecommerce_waitlist', array('productid' => $product->id, 'userid' => $checkout->userid, 'sent' => 1));
        if (count($waitlistitems)) {
            foreach ($waitlistitems as $item) {

                // insert log
                $record = new stdClass();
                $record->userid = $checkout->userid;
                $record->instanceid = $product->id;
                $record->type = 'seatkey';
                $record->status = \local_ecommerce\payment::$STATUS_COMPLETED;
                $record->timecreated = time();
                $record->timemodified = time();
                $record->checkoutid = $checkout->id;
                $record->details = $item->seatkey;

                if ($exist = $DB->get_record('local_ecommerce_logs', array('type'=>$record->type, 'instanceid'=>$record->instanceid, 'userid'=>$record->userid, 'status'=>\local_ecommerce\payment::$STATUS_PENDING))) {
                    $record->id = $exist->id;
                    $DB->update_record('local_ecommerce_logs', $record);
                } else {
                    $DB->insert_record('local_ecommerce_logs', $record);
                }

                $DB->delete_records('local_ecommerce_waitlist', array('id'=>$item->id));
            }
        }
    }

}
