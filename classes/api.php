<?php

namespace local_ecommerce;

/**
 * Product functions
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use html_writer;
use moodle_url;
use context_system;
use stdClass;
use core_tag_tag;

/**
 * Product functions.
 *
 * @package    local_ecommerce
 * @author     SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class api {

    public static function get_assignable_products($query = '', $instanceid = 0, $type = '') {
        global $DB;
        $products = array();

        if (!$instanceid or !$type) {
            return $products;
        }

        $params = array('instanceid' => $instanceid, 'type' => $type);
        $where = "visible = 1";

        if ($query) {
            $where .= " AND (" . $DB->sql_like('name', ':searchname', false, false, false). "  
                         OR " . $DB->sql_like('idnumber', ':searchidnumber', false, false, false).")";
            $params['searchname'] = '%' . $query . '%';
            $params['searchidnumber'] = '%' . $query . '%';
        }

        $products = $DB->get_records_sql("
            SELECT id, name 
              FROM {local_ecommerce_products} 
              WHERE $where AND id NOT IN (
                          SELECT DISTINCT p.id FROM {local_ecommerce_products} p, 
                          {local_ecommerce_relations} pr WHERE p.id = pr.productid 
                          AND p.id > 0 AND pr.type = :type AND pr.instanceid = :instanceid) 
                ORDER BY name ASC", $params);

        return json_encode(array($products));
    }

    public static function count_ecommerce_products() {
        global $DB, $USER;

        return $DB->count_records('local_ecommerce', array('userid'=>$USER->id));
    }

    public static function delete_ecommerce_product() {
        global $DB, $USER;

        $DB->delete_records('local_ecommerce_logs', array('userid'=>$USER->id, 'type'=>'product', 'status'=>payment::$STATUS_PENDING));
        $DB->delete_records('local_ecommerce', array('userid'=>$USER->id));

        return $DB->count_records('local_ecommerce', array('userid'=>$USER->id));
    }

    public static function add_to_cart($productid) {
        global $DB, $USER;

        $response = array('message' => '', 'status'=>'success', 'productscount' => 0);

        $enablewaitlist = get_config('local_ecommerce', 'enablewaitlist');

        if ($DB->get_record('local_ecommerce', array('userid'=>$USER->id, 'productid'=>$productid))) {
            $response['message'] = get_string('alreadyincart', 'local_ecommerce');
            $response['status'] = 'incart';
        } else if ($enablewaitlist and $DB->get_record('local_ecommerce_waitlist', array('userid' => $USER->id, 'productid' => $productid))) {
            $response['message'] = get_string('alreadyinwaitlist', 'local_ecommerce');
            $response['status'] = 'inwaitlist';
        } else if ($product = $DB->get_record('local_ecommerce_products', array('visible'=>1, 'id'=>$productid))) {

            $sold = $DB->count_records('local_ecommerce_logs', array('instanceid'=>$product->id, 'type'=>'product'));
            $seatsallow = ($product->seats == 0 or
                            ($product->seats and $product->seats > $sold + waitlist::get_product_count_waitlist($product))
                        );

            if (!$seatsallow and $enablewaitlist) {

                $response['message'] = get_string('noseatsforproductusewaitlist', 'local_ecommerce');
                $response['status'] = 'addtowaitlist';

            } elseif (!$seatsallow and !$enablewaitlist) {

                $response['message'] = get_string('noseatsforproduct', 'local_ecommerce');
                $response['status'] = 'error';

            } else {

                $record = new stdClass();
                $record->userid = $USER->id;
                $record->productid = $product->id;
                $record->timecreated = time();

                if ($id = $DB->insert_record('local_ecommerce', $record)) {
                    $response['message'] = get_string('addedtocart', 'local_ecommerce');
                    $response['status'] = 'incart';

                    // create/update checkout
                    if ($products = checkout::get_products_in_cart()) {
                        $coupons = coupon::get_applied_coupons();
                        $discounts = checkout::get_products_discounts($products);
                        $discountprices =
                                checkout::get_checkout_products_prices($products, $coupons, $discounts);
                        $total = checkout::get_cart_total($products, $discountprices);
                    }
                    $payment = checkout::get_payment_details($products, $total);
                    wishlist::delete_wishlist_item($productid);
                }
            }
        }

        $response['productscount'] = $DB->count_records('local_ecommerce', array('userid'=>$USER->id));
        $response['wishlistcount'] = self::count_wishlist_products();

        return $response;
    }

    public static function add_to_waitlist($productid) {
        global $DB, $USER;

        $response = array('message' => '', 'productscount' => 0);

        $exists = $DB->get_record('local_ecommerce_waitlist', array('userid'=>$USER->id, 'productid'=>$productid));

        if (!$exists) {

            if ($product = $DB->get_record('local_ecommerce_products', array('visible'=>1, 'id'=>$productid))) {
                $record = new stdClass();
                $record->userid = $USER->id;
                $record->productid = $product->id;

                $record->id = waitlist::save_list_item($record);
                $item = $DB->get_record('local_ecommerce_waitlist', ['id'=>$record->id]);

                // insert log
                $log = new stdClass();
                $log->userid = $item->userid;
                $log->instanceid = $product->id;
                $log->type = 'seatkey';
                $log->status = payment::$STATUS_PENDING;
                $log->timecreated = time();
                $log->timemodified = time();
                $log->details = $item->seatkey;

                if ($exist = $DB->get_record('local_ecommerce_logs', array('type'=>$log->type, 'instanceid'=>$log->instanceid, 'userid'=>$record->userid, 'status'=>payment::$STATUS_PENDING))) {
                    $log->id = $exist->id;
                    $DB->update_record('local_ecommerce_logs', $log);
                } else {
                    $DB->insert_record('local_ecommerce_logs', $log);
                }

                $response['message'] = get_string('addedtowaitlist', 'local_ecommerce');
            }

        } else {
            $response['message'] = get_string('alreadyinwaitlist', 'local_ecommerce');
        }
        wishlist::delete_wishlist_item($productid);
        $response['productscount'] = $DB->count_records('local_ecommerce_waitlist', array('userid'=>$USER->id));
        $response['wishlistcount'] = self::count_wishlist_products();
        return $response;
    }

    public static function get_daterange($daterange = '') {

        $timestart = 0;
        $timefinish = 0;
        $timestart_date = '';
        $timefinish_date = '';

        if (!empty($daterange)) {
            $range = explode(" to ", $daterange);

            $timestart = ($range[0]) ? strtotime(trim($range[0])) : strtotime('-7 days');
            if ($range[1] && $range[0] == $range[1]) {
                $timefinish = strtotime(trim($range[1])) + 86399;
            } else {
                $timefinish = ($range[1]) ? strtotime(trim($range[1])) : time();
            }

            $timestart_date = date("Y-m-d", $timestart);
            $timefinish_date = date("Y-m-d", $timefinish);
        }

        return array('timestart'=>$timestart, 'timefinish'=>$timefinish, 'timestart_date'=>$timestart_date, 'timefinish_date'=>$timefinish_date, 'daterange'=>$daterange);
    }

    public static function get_purchases_autocomplete($query = '', $dateRange = null) {
        global $DB, $USER;
        $products = array();

        if (!$query) {
            return $products;
        }

        $params = array('uid' => $USER->id);
        $where = array('ch.userid = :uid');

        if ($query) {
            $where[] = "(" . $DB->sql_like('ch.id', ':searchorderid', false, false, false). "
                         OR " . $DB->sql_like('ch.item_name', ':searchitemname', false, false, false).")";
            $params['searchorderid'] = '%' . $query . '%';
            $params['searchitemname'] = '%' . $query . '%';
        }

        if ($dateRange) {
            $dateRange = urldecode($dateRange);
            $dateArray = explode(' ', $dateRange);
            $startDate = strtotime($dateArray[0]);
            $endDate = strtotime($dateArray[2]);

            $where[] = "ch.timeupdated BETWEEN :timestart AND :timefinish";
            $params['timestart'] = $startDate;
            $params['timefinish'] = $endDate;
        }
        $wheres = implode(' AND ', $where);

        $products = $DB->get_records_sql("
            SELECT id, item_name as name
            FROM {local_ecommerce_checkout} ch
            WHERE $wheres
            ORDER BY name ASC"
            , $params
        );

        return json_encode(array($products));
    }

    public static function get_store_autocomplete($query = '', $category = '', $ignoreID = null) {
        global $DB;
        $products = array();

        if (!$query) {
            return $products;
        }

        $params = array();
        $where = array();

        if ($query) {
            $where[] = "(" . $DB->sql_like('name', ':searchname', false, false, false). "  
                         OR " . $DB->sql_like('idnumber', ':searchidnumber', false, false, false).")";
            $params['searchname'] = '%' . $query . '%';
            $params['searchidnumber'] = '%' . $query . '%';
        }

        if ($category) {
            $where[] = "categoryid = :categoryid";
            $params['categoryid'] = $category;
        }

        if (!empty($ignoreID)) {
            $where[] = "id != :ignoreid";
            $params['ignoreid'] = $ignoreID;
        }

        $wheres = implode(' AND ', $where);

        $products = $DB->get_records_sql("
            SELECT id, name 
            FROM {local_ecommerce_products} 
            WHERE $wheres
            ORDER BY name ASC"
            , $params
        );

        return json_encode(array($products));
    }

    public static function add_to_wishlist($productid) {
        global $DB, $USER;

        $response = array('message' => '', 'productscount' => 0);

        $exists = $DB->get_record('local_ecommerce_wishlist', array('userid'=>$USER->id, 'productid'=>$productid));

        if (!$exists) {
            if ($product = $DB->get_record('local_ecommerce_products', array('visible'=>1, 'id'=>$productid))) {
                $record = new stdClass();
                $record->userid = $USER->id;
                $record->productid = $product->id;
                $record->id = wishlist::save_wishlist_item($record);
                $response['message'] = get_string('added_to_wishlist', 'local_ecommerce');
            }
        } else {
            $response['message'] = get_string('already_in_wishlist', 'local_ecommerce');
        }
        $response['productscount'] = $DB->count_records('local_ecommerce_wishlist', array('userid'=>$USER->id));

        return $response;
    }

    public static function count_wishlist_products() {
        global $DB, $USER;
        $count = $DB->count_records('local_ecommerce_wishlist', array('userid' => $USER->id));
        return $count;
    }

    public static function rate_product($productid, $rating, $review) {
        global $DB, $USER;

        $response = array('message' => '');

        if ($product = $DB->get_record('local_ecommerce_products', array('visible'=>1, 'id'=>$productid))) {
            $record = $DB->get_record('local_ecommerce_ratings', array('userid'=>$USER->id, 'productid'=>$productid));
            if (!$record) {
                $record = new stdClass();
                $record->userid = $USER->id;
                $record->productid = $product->id;
            }
            $record->rating = $rating;
            $record->review = $review;
            $record->id = review::upsert_product_rating($record);
            $newRating = review::get_rating($product->id);
            $response['message'] = get_string('product_rated_successfully', 'local_ecommerce');
            $response['newRating'] = round($newRating->total / $newRating->count * 20, 0);
            $response['newRatingUsers'] = $newRating->count;
        }

        return $response;
    }
}
