<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/lib.php');
require($CFG->dirroot . '/enrol/ecommerce/locallib.php');

$id     = optional_param('id', 0, PARAM_INT);
$invoicePaid = optional_param('invoicepaid', 0, PARAM_INT);

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:checkout', $context);

if ($invoicePaid) {
    redirect(new moodle_url('/my/'), get_string('invoice_paid', 'local_ecommerce'));
}

$title = get_string('ecommercecheckout', 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/checkout.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

if ($products = \local_ecommerce\checkout::get_products_in_cart()) {
    $coupons = \local_ecommerce\coupon::get_applied_coupons();
    $discounts = \local_ecommerce\checkout::get_products_discounts($products);
    $discountprices = \local_ecommerce\checkout::get_checkout_products_prices($products, $coupons, $discounts);
    $total = \local_ecommerce\checkout::get_cart_total($products, $discountprices);
}

$renderer = $PAGE->get_renderer('local_ecommerce');
$payment = \local_ecommerce\checkout::get_payment_details($products, $total, $discountprices);

$params = [
    'title' => $title,
    'products' => $products,
    'total' => (isset($total)) ? $total : null,
    'subtotal' => (isset($total->subtotal)) ? $total->subtotal : 0,
    'discount' => (isset($total->discount)) ? $total->discount : 0,
    'salestax' => (isset($total->salesTax)) ? $total->salesTax : null,
    'pretaxtotal' => (isset($total->preTaxTotal)) ? $total->preTaxTotal : null,
    'salestaxname' => (isset($total->salesTax)) ? get_config('local_ecommerce', 'sales_tax_name') . ' @ ' . $total->salesTaxPercentage . '%' : null,
    'payment' => $payment,
    'checkout_header' => $renderer->print_basic_header(get_string('checkout', 'local_ecommerce'))
];
//$renderable = new \local_ecommerce\output\ecommerce_checkout($params);

echo $OUTPUT->header();

echo $renderer->store_print_menu('view');
echo \local_ecommerce\payment::print_payment_forms($renderer, $params);
echo $renderer->store_print_checkout_footer(false);

$PAGE->requires->js_call_amd('local_ecommerce/store', 'init');

echo $OUTPUT->footer();
