<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/tables/waitlist_table.php');

$search     = optional_param('search', '', PARAM_TEXT);
$id         = optional_param('id', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_TEXT);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);
$pageNo     = optional_param('page', 0, PARAM_INT);

require_login();
local_ecommerce_enable('enablewaitlist');

$context = context_system::instance();
require_capability('local/ecommerce:managewaitlist', $context);

$title = get_string('waitlist', 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/waitlist/index.php', array('search' => $search));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$item = $DB->get_record('local_ecommerce_waitlist', array('id'=>$id));
$returnurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/waitlist/index.php');

if ($action == 'delete' and $item = $DB->get_record('local_ecommerce_waitlist', array('id'=>$id))) {
    $product = $DB->get_record('local_ecommerce_products', array('id'=>$item->productid));
    $user = $DB->get_record('user', array('id'=>$item->userid));

    $PAGE->url->param('action', 'delete');
    if ($confirm and confirm_sesskey()) {
        \local_ecommerce\waitlist::delete_item($item);
        redirect($returnurl, get_string('removedfromlist', 'local_ecommerce'));
    }
    $strheading = get_string('removefromlist', 'local_ecommerce');
    $PAGE->navbar->add(get_string('waitlist', 'local_ecommerce'), new moodle_url('/local/ecommerce/products/waitlist.php'));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    $renderer = $PAGE->get_renderer('local_ecommerce');
    $params = [
        'title' => $strheading
    ];

    $renderable = new \local_ecommerce\output\waitlist_delete($params);

    echo $renderer->render($renderable);

    $yesurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/waitlist/index.php',
            array('id' => $item->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey()));
    $msg_params = new stdClass();
    $msg_params->product = format_string($product->name);
    $msg_params->user = fullname($user);

    $message = get_string('deleteuserfromlisttmsg', 'local_ecommerce', $msg_params);
    echo $OUTPUT->confirm($message, $yesurl, $returnurl);
    echo $OUTPUT->footer();
    die;
} elseif ($action == 'mail' and $item) {
    \local_ecommerce\notification::send_waitlist_notification($item, true);
    \local_ecommerce\waitlist::save_list_item($item);

    redirect($returnurl, get_string('messagesent', 'local_ecommerce'));
    exit;
}

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/dashboard/index.php'));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new waitlist_table('waitlist_table', $search);
$renderer = $PAGE->get_renderer('local_ecommerce');

$params = [
        'title' => $title,
        'search' => $search,
        'search_panel' => $renderer->print_single_search_panel($search, $title),
        'tablehtml' => $table->export_for_template($renderer)
];

$urlParams = array(
    'search' => $search
);

//$renderable = new \local_ecommerce\output\waitlist_index($params);

echo $OUTPUT->header();

echo $renderer->print_manage_tabs('waitlist');
echo $renderer->store_list_generic_table($params);

//echo $OUTPUT->paging_bar($table->totalrows, $pageNo, $table->pagesize, new moodle_url('/local/ecommerce/waitlist/index.php', $urlParams));

$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();
