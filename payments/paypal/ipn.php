<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Listens for Instant Payment Notification from PayPal
 *
 * This script waits for Payment notification from PayPal,
 * then double checks that data by sending it back to PayPal.
 * If PayPal verifies this then it sets up the enrolment for that
 * user.
 *
 * @package    local_ecommerce
 * @copyright  2017 SEBALE
 * @author     SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Disable moodle specific debug messages and any errors in output,
// comment out when debugging or better look into error log!
define('NO_DEBUG_DISPLAY', true);

require("../../../../config.php");
require_once($CFG->dirroot."/local/ecommerce/locallib.php");
require_once($CFG->dirroot."/enrol/ecommerce/locallib.php");
require_once($CFG->libdir.'/enrollib.php');
require_once($CFG->libdir . '/filelib.php');

//error_reporting(E_ALL);ini_set('display_errors', '1');$CFG->debug = 32767;  $CFG->debugdisplay = true;

// PayPal does not like when we return error messages here,
// the custom handler just logs exceptions and stops.
set_exception_handler('local_ecommerce_ipn_exception_handler');
$plugin_type = 'PAYPAL';

/// Keep out casual intruders
if (empty($_POST) or !empty($_GET)) {
    print_error("Sorry, you can not use the script that way.");
}

/// Read all the data from PayPal and get it ready for later;
/// we expect only valid UTF-8 encoding, it is the responsibility
/// of user to set it up properly in PayPal business account,
/// it is documented in docs wiki.

$req = 'cmd=_notify-validate';

$data = new stdClass();

foreach ($_POST as $key => $value) {
    $req .= "&$key=".urlencode($value);
    $data->$key = fix_utf8($value);
}

$custom = explode('-', $data->custom);
$data->checkout_id      = (int)$custom[0];
$data->userid           = (int)$custom[1];
$data->paymentid        = (int)$custom[2];
$data->payment_gross    = $data->mc_gross;
$data->payment_currency = $data->mc_currency;
$data->timeupdated      = time();

if (! $checkout = $DB->get_record("local_ecommerce_checkout", array("id"=>$data->checkout_id, "payment_status"=>\local_ecommerce\payment::$STATUS_PENDING))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid checkout id", $data);
    die;
}

// Get the payment records.
if (! $payment = $DB->get_record("local_ecommerce_payments", array("id"=>$data->paymentid, "status"=>1))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid payment id", $data);
    die;
}
$config = (object)unserialize($payment->settings);

/// Open a connection back to PayPal to validate the data
$paypaladdr = $config->paypalsandbox != 1 ? 'www.paypal.com' : 'www.sandbox.paypal.com';
//message_paypal_error_to_admin($paypaladdr, $options);
$c = new curl();
$options = array(
        'returntransfer' => true,
        'httpheader' => array('application/x-www-form-urlencoded', "Host: $paypaladdr"),
        'timeout' => 30,
        'CURLOPT_HTTP_VERSION' => CURL_HTTP_VERSION_1_1,
);
$location = "https://$paypaladdr/cgi-bin/webscr";
$result = $c->post($location, $req, $options);

if (!$result) {  /// Could not connect to PayPal - FAIL
    echo "<p>Error: could not access paypal.com</p>";
    local_ecommerce_log_payment_error($plugin_type, "Could not access paypal.com to verify payment", $data);
    die;
}
/// Connection is OK, so now we post the data to validate it

/// Now read the response and check if everything is OK.
if (strlen($result) > 0) {
    if (strcmp($result, "VERIFIED") == 0) {          // VALID PAYMENT!

        // check the payment_status and payment_reason

        // If status is not completed or pending then unenrol the student if already enrolled
        // and notify admin

        if ($data->payment_status != "Completed" and $data->payment_status != "Pending") {
            local_ecommerce_log_payment_error($plugin_type, "Status not completed or pending. User not enrolled to products", $data);
            die;
        }

        // If currency is incorrectly set then someone maybe trying to cheat the system

        if ($data->mc_currency != \local_ecommerce\payment::get_currency('code')) {
            local_ecommerce_log_payment_error($plugin_type, "Currency does not match course settings, received: ".$data->mc_currency, $data);
            die;
        }

        // If our status is not completed or not pending on an echeck clearance then ignore and die
        // This check is redundant at present but may be useful if paypal extend the return codes in the future

        if (! ( $data->payment_status == "Completed" or
                ($data->payment_status == "Pending" and $data->pending_reason == "echeck") ) ) {
            die;
        }

        // At this point we only proceed with a status of completed or pending with a reason of echeck
        if ($existing = $DB->get_record("local_ecommerce_checkout", array("txn_id"=>$data->txn_id))) {   // Make sure this transaction doesn't exist already
            local_ecommerce_log_payment_error($plugin_type, "Transaction $data->txn_id is being repeated!", $data);
            die;
        }

        if (core_text::strtolower($data->business) !== core_text::strtolower($config->paypalbusiness)) {   // Check that the email is the one we want it to be
            local_ecommerce_log_payment_error($plugin_type, "Business email is {$data->business} (not ".
                    $config->paypalbusiness.")", $data);
            die;

        }

        if (!$user = $DB->get_record('user', array('id'=>$data->userid))) {   // Check that user exists
            local_ecommerce_log_payment_error($plugin_type, "User $data->userid doesn't exist", $data);
            die;
        }

        // Check that amount paid is the correct amount
        $cost = (float) $checkout->amount;

        // Use the same rounding of floats as on the enrol form.
        $cost = format_float($cost, 2, false);

        if ($data->payment_gross < $cost) {
            local_ecommerce_log_payment_error($plugin_type, "Amount paid is not enough ($data->payment_gross < $cost))", $data);
            die;
        }

        // ALL CLEAR !
        $checkout->payment_status = \local_ecommerce\payment::$STATUS_COMPLETED;
        $checkout->pending_reason = ($data->pending_reason) ? $data->pending_reason : '';
        $checkout->reason_code = ($data->reason_code) ? $data->reason_code : '';
        $checkout->txn_id = $data->txn_id;
        $checkout->payment_type = ($data->payment_type) ? $data->payment_type : '';
        $checkout->timeupdated = $data->timeupdated;
        $checkout->paymentid = $data->paymentid;
        $DB->update_record("local_ecommerce_checkout", $checkout);

        // Enrol user
        \local_ecommerce\checkout::enrol_user($checkout);

        // send checkout notification
        \local_ecommerce\notification::send_checkout_notification($checkout);

    } else if (strcmp ($result, "INVALID") == 0) { // ERROR

        $checkout->payment_status = \local_ecommerce\payment::$STATUS_REJECTED;
        $checkout->pending_reason = ($data->pending_reason) ? $data->pending_reason : '';
        $checkout->reason_code = ($data->reason_code) ? $data->reason_code : '';
        $checkout->txn_id = $data->txn_id;
        $checkout->payment_type = ($data->payment_type) ? $data->payment_type : '';
        $checkout->timeupdated = $data->timeupdated;

        $DB->update_record("local_ecommerce_checkout", $checkout);
    }
}

exit;
