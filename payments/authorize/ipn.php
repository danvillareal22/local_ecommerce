<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Listens for Instant Payment Notification from Autorize.net
 *
 * This script waits for Payment notification from PayPal,
 * then double checks that data by sending it back to PayPal.
 * If PayPal verifies this then it sets up the enrolment for that
 * user.
 *
 * @package    local_ecommerce
 * @copyright  2017 SEBALE
 * @author     SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Disable moodle specific debug messages and any errors in output,
// comment out when debugging or better look into error log!
define('NO_DEBUG_DISPLAY', true);

require("../../../../config.php");
require_once($CFG->dirroot."/local/ecommerce/locallib.php");
require_once($CFG->dirroot."/enrol/ecommerce/locallib.php");
require_once($CFG->libdir.'/enrollib.php');
require_once($CFG->libdir .'/filelib.php');

// authorize does not like when we return error messages here,
// the custom handler just logs exceptions and stops.
set_exception_handler('local_ecommerce_ipn_exception_handler');
$plugin_type = 'authorize';

if (empty($_POST) or !empty($_GET)) {
    print_error("Sorry, you can not use the script that way."); die;
}

$data = (object)array_map('utf8_encode', $_POST);

$custom = explode('-', $data->x_cust_id);
$data->checkout_id      = (int)$custom[0];
$data->userid           = (int)$custom[1];
$data->paymentid        = (int)$custom[2];
$data->payment_gross    = $data->x_amount;
$data->timeupdated      = time();

if (! $checkout = $DB->get_record("local_ecommerce_checkout", array("id"=>$data->checkout_id, "payment_status"=>\local_ecommerce\payment::$STATUS_PENDING))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid checkout id", $data);
    die("Not a valid checkout id");
}

// Get the payment records.
if (! $payment = $DB->get_record("local_ecommerce_payments", array("id"=>$data->paymentid, "status"=>1))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid payment id", $data);
    die("Not a valid payment id");
}
$config = (object)unserialize($payment->settings);

// Check if the response is from authorize.net.
$merchantmd5hash = $config->merchantmd5hash;
$loginid = $config->loginid;
$transactionid = $data->x_trans_id;
$amount = $data->x_amount;
$generatemd5hash = strtoupper(md5($merchantmd5hash.$loginid.$transactionid.$amount));

// Required for message_send.
$PAGE->set_context(context_system::instance());

if ($generatemd5hash != $data->x_MD5_Hash) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid merchant hash id", $data);
    die("We can't validate your transaction. Please try again!!"); die;
}

if (!$user = $DB->get_record('user', array('id'=>$data->userid))) {   // Check that user exists
    local_ecommerce_log_payment_error($plugin_type, "User $data->userid doesn't exist", $data);
    die("User $data->userid doesn't exist");
}

// Check that amount paid is the correct amount
$cost = (float) $checkout->amount;

// Use the same rounding of floats as on the enrol form.
$cost = format_float($cost, 2, false);

if ($data->payment_gross < $cost) {
    local_ecommerce_log_payment_error($plugin_type, "Amount paid is not enough ($data->payment_gross < $cost))", $data);
    die("Amount paid is not enough ($data->payment_gross < $cost))");
}

// Send the file, this line will be reached if no error was thrown above.
$checkout->txn_id = $data->x_trans_id;
$checkout->pending_reason = ($data->x_response_reason_text) ? $data->x_response_reason_text : '';
$checkout->reason_code = ($data->x_response_reason_code) ? $data->x_response_reason_code : '';
$checkout->timeupdated = $data->timeupdated;
$checkout->paymentid = $data->paymentid;

if ($data->x_response_code == 1) {
    $checkout->payment_status = \local_ecommerce\payment::$STATUS_COMPLETED;
}
if ($data->x_response_code == 2) {
    $checkout->payment_status = \local_ecommerce\payment::$STATUS_REJECTED;
}

// ALL CLEAR !
$DB->update_record("local_ecommerce_checkout", $checkout);

// Enrol user
\local_ecommerce\checkout::enrol_user($checkout);

// send checkout notification
\local_ecommerce\notification::send_checkout_notification($checkout);

echo '<script type="text/javascript">
     window.location.href="'.$CFG->wwwroot.'/local/ecommerce/payments/return.php?id='.$checkout->id.'";
     </script>';
die();
