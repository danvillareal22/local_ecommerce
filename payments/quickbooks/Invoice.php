<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Subscription
 *
 */
class Invoice extends Application
{

    public function __construct()
    {
        parent::__construct();
        if ($this->user->cohortid) {
            redirect('/profile');
        }
    }

    public function view($id)
    {
        if ($this->user->owner) {
            redirect('/');
        }
        $this->load->model('Quickbooks_invoice_model', 'invoices');
        $invoice = $this->invoices->findById($id);
        $this->load->library('Quickbooks/Online', null, 'quickbooks');
        if ($invoice->type == $this->invoices::TYPE_INVOICE) {
            $path = $this->quickbooks->downloadInvoiceTo($invoice->quickbooks_id);
        } else {
            $path = $this->quickbooks->downloadsalesReceiptTo($invoice->quickbooks_id);
        }
        $newPath = dirname(dirname(BASEPATH)) . '/public_html/tmp/' . $invoice->id . '.pdf';
        rename($path, $newPath);
        redirect('/tmp/' . $invoice->id . '.pdf');
    }

    public function send($id)
    {
        $this->load->model('Quickbooks_invoice_model', 'invoices');
        $invoice = $this->invoices->findById($id);
        if ($invoice && !$this->user->owner) {
            $this->load->library('Quickbooks/Online', null, 'quickbooks');
            if ($invoice->type == $this->invoices::TYPE_INVOICE) {
                $this->quickbooks->sendInvoiceViaEmail($invoice->quickbooks_id);
            } else {
                $this->quickbooks->sendSalesReceiptViaEmail($invoice->quickbooks_id);
            }
            $this->session->set_flashdata('success', clang('app_message_invoice_sent'));
        }
        redirect('/subscription/billing');
    }

    public function create($id)
    {
        $this->load->model('Quickbooks_invoice_model', 'invoices');
        $invoice = $this->invoices->findById($id);
        if ($invoice && !$invoice->type && !$this->user->owner) {
            $this->load->library('Quickbooks/Online', null, 'quickbooks');
            try {
                $quickbooksId = $this->quickbooks->sendInvoice([
                    'amount' => $invoice->total,
                    'description' => $invoice->description,
                    'serviceId' => $invoice->service_id,
                    'userId' => $invoice->user_id,
                    'email' => $this->user->email,
                    'from' => $invoice->period_from,
                    'to' => $invoice->period_to,
                    'dueDate' => date("Y-m-d", $invoice->due_date),
                ]);
                $updatedInvoice = new StdClass();
                $updatedInvoice->quickbooks_id = $quickbooksId;
                $updatedInvoice->id = $invoice->id;
                $this->invoices->update_invoice($updatedInvoice);
                $this->load->model('Deal_model', 'deals');
                $this->load->model('Plan_model', 'plans');
                $this->deals->create_deal((object)[
                    'invoice_id' => $invoice->id,
                    'user_id' => $this->user->id,
                    'plan_id' => $this->plans->getIdByServiceId($invoice->service_id),
                    'updated_at' => time(),
                ]);
                $this->session->set_flashdata('success', clang('app_message_invoice_sent'));
            } catch (Exception $e) {
                $this->session->set_flashdata('error', clang('app_message_status_500') . ' ' . $this->config->item('email_support'));
                log_message('error', $e->getMessage());
            }
        }
        redirect('/subscription/refresh');
    }

    public function authorizePayment($id)
    {
        $this->load->model('Quickbooks_invoice_model', 'invoices');
        $invoice = $this->invoices->findById($id);
        if ($this->user->owner || !$invoice || $invoice->type == $this->invoices::TYPE_INVOICE) {
            redirect('/subscription/billing');
        }

        if (!$invoice->type) {
            $updateInvoice = new StdClass();
            $updateInvoice->id = $id;
            $this->invoices->update_invoice($updateInvoice);
        }

        $this->render('invoice/payment', [
            'title' => clang('app_title_make_payment'),
            'invoice' => $invoice,
        ]);
    }

    public function charge()
    {
        if ($this->user->owner) {
            redirect('/');
        }

        $this->load->library('form_validation');
        $this->load->model('Quickbooks_invoice_model', 'invoice_model');

        $rules = [
            [
                'field' => 'token',
                'label' => clang('app_form_label_token'),
                'rules' => 'required',
            ],
            [
                'field' => 'invoice_id',
                'label' => clang('app_form_label_invoice'),
                'rules' => [
                    'required',
                    function ($value) {
                        return $this->invoice_model->exists([
                            'id' => $value,
                            'user_id' => $this->user->quickbooksId,
                            'status' => $this->invoice_model::STATUS_PENDING,
                        ]);
                    },
                ],
            ],
        ];

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run()) {
            $invoice = $this->invoice_model->findOne(['id' => $this->input->post('invoice_id')]);
            $this->load->library('Quickbooks/Payment', null, 'payments');

            // Send payment to quickbooks

            $updatedInvoice = new StdCLass();
            $updatedInvoice->type = $this->invoice_model::TYPE_SALES_RECEIPT;
            $updatedInvoice->id = $invoice->id;

            if (!$transactionId = $this->payments->charge($invoice->total, $this->input->post('token'), $invoice->id)) {
                $this->session->set_flashdata('error', clang('app_message_transaction_failed'));
                redirect('/subscription/billing');
            }

            // Update local payment

            $updatedInvoice->transaction_id = $transactionId;
            $updatedInvoice->status = $this->invoice_model::STATUS_PAID;

            // create sales receipt

            $this->load->library('Quickbooks/Online', null, 'invoices');

            $salesReceiptId = $this->invoices->createSalesReceipt([
                'amount' => $invoice->total,
                'description' => $invoice->description,
                'service_id' => $invoice->service_id,
                'user_id' => $invoice->user_id,
                'from' => $invoice->period_from,
                'to' => $invoice->period_to,
                'transaction_id' => $updatedInvoice->transaction_id,
                'email' => $this->user->email,
            ]);

            $updatedInvoice->quickbooks_id = $salesReceiptId;
            $this->invoice_model->update_invoice($updatedInvoice);
            $this->load->model('Deal_model', 'deals');
            $this->load->model('Plan_model', 'plans');
            $this->deals->create_deal((object)[
                'invoice_id' => $invoice->id,
                'user_id' => $this->user->id,
                'plan_id' => $this->plans->getIdByServiceId($invoice->service_id),
                'updated_at' => time(),
            ]);
            // Refresh page
            $this->session->set_flashdata('info', clang('app_message_payment_approved'));
            redirect('/subscription/refresh');
        } else {
            $this->session->set_flashdata('error', clang('app_message_status_500') . ' ' . $this->config->item('email_support'));
        }
        redirect('/subscription/billing');
    }
}
