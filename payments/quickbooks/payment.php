<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="billing">
    <div class="head clearfix">
        <h2><?php echo $title; ?></h2>
    </div>
    <div class="inner">
        <form id="payment-form" action="/accept-payment" method="POST">
            <div class="alert alert-danger" style="display:none;" id="error-box"></div>
            <div class="row">
                <div class="col-md-6">
                    <h4>Card Details</h4>
                    <div class="alert alert-info">This data is used by QuickBooks. Intelliboard doesn't store any of your card details at any format.</div>
                    <div class="form-group">
                        <label for="card-number">Card Number</label>
                        <input type="text" name="number" value="" class="form-control" id="card-number" placeholder="xxxxxxxxxxxxxxxx" required>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Expiration Date</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <select id="card-month" name="expMonth" class="form-control" required>
                                            <option value="">Month</option>
                                            <?php for($i = 1; $i <=12; $i++) : ?>
                                                <option value="<?php echo sprintf('%02d', $i) ?>"><?php echo sprintf('%02d', $i) ?></option>
                                            <?php endfor; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select id="card-year" name="expYear" class="form-control" required>
                                            <option value="">Year</option>
                                            <?php for($y = date("Y"); $y <= date("Y") + 10; $y++) : ?>
                                                <option value="<?php echo $y ?>"><?php echo $y ?></option>
                                            <?php endfor; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="card-cvv">Card CVV</label>
                                <input type="text" name="cvc" class="form-control" id="card-cvv" placeholder="xxx" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" id="submit-button" class="btn btn-success btn-fill">Pay Now! ($<?php echo $invoice->total ?>)</button>
            </div>
        </form>
    </div>
</div>
<script src="https://js.appcenter.intuit.com/Content/IA/intuit.ipp.payments<?php echo config('services.quickbooks.baseUrl') == 'development' ? '.sandbox' : '' ?>-0.0.3.js"></script>

<script>
    var errorBox = $("#error-box");
    var submitButton = $("#submit-button");
    $("#payment-form").find("input").keydown(function () {
        errorBox.hide();
    });
    $("#payment-form").submit(function(e) {
        $("body").trigger('start-loader');
        var oldValue = submitButton.html();
        submitButton.html("Sending request...");
        var result = {
            address: {
                postalCode: ""
            }
        };
        var elements = $(this).serializeArray();
        for (var i in elements) {
            result[elements[i].name] = elements[i].value;
        }
        intuit.ipp.payments.tokenize(
            "intelliboard", {
                card: result
            },
            function(token) {
                if (token != null) {
                    var form = $('<form action="/invoice/charge" method="POST" style="display: none;">'+
                        '<input type="hidden" name="token" value="' + token + '">' +
                        '<input type="hidden" name="invoice_id" value="<?php echo $invoice->id ?>">' +
                        '</form>');
                    $("body").append(form);
                    form.submit();
                } else {
                    submitButton.html(oldValue);
                    errorBox.html("Whoops! Something goes wrong. Please check the credentials and try again").show();
                }
            }
        );
        return false;
    });
</script>
