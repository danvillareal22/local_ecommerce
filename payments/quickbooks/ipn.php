<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Listens for Instant Payment Notification from Stripe
 *
 * This script waits for Payment notification from Stripe,
 * then double checks that data by sending it back to Stripe.
 * If Stripe verifies this then it sets up the enrolment for that
 * user.
 *
 * @package    enrol_stripepayment
 * @copyright  2015 Dualcube, Arkaprava Midya, Parthajeet Chakraborty
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Disable moodle specific debug messages and any errors in output,
// comment out when debugging or better look into error log!

require("../../../../config.php");
require($CFG->dirroot."/local/ecommerce/payments/quickbooks/lib/config.php");
require_once($CFG->dirroot."/local/ecommerce/payments/quickbooks/lib/Payment.php");

// Get the checkout records.
if (! $checkout = $DB->get_record("local_ecommerce_checkout", array("id"=>17, "payment_status"=>\local_ecommerce\payment::$STATUS_PENDING))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid checkout id", $data);
    die("Not a valid checkout id");
}

// Get the payment records.
if (! $payment = $DB->get_record("local_ecommerce_payments", array("id"=>9, "status"=>1))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid payment id", $data);
    die("Not a valid payment id");
}
$config = (object)unserialize($payment->settings);
$config->payment_id = $payment->id;
$payments = new Payment($config);
$payments->charge(150, '12345', 123);

die('exit');

define('NO_DEBUG_DISPLAY', true);

require("../../../../config.php");
require_once($CFG->dirroot."/local/ecommerce/locallib.php");
require_once($CFG->dirroot."/enrol/ecommerce/locallib.php");
require_once($CFG->libdir.'/enrollib.php');
require_once($CFG->libdir . '/filelib.php');

require_login();
// Stripe does not like when we return error messages here,
// the custom handler just logs exceptions and stops.
set_exception_handler('local_ecommerce_ipn_exception_handler');
$plugin_type = 'stripe';

// Keep out casual intruders.
if (empty(required_param('stripeToken', PARAM_RAW))) {
    print_error(get_string('stripe_sorry', 'local_ecommerce'));
}

$data = new stdClass();

$data->cmd = required_param('cmd', PARAM_RAW);
$data->charset = required_param('charset', PARAM_RAW);
$data->item_name = required_param('item_name', PARAM_TEXT);
$data->item_name = required_param('item_number', PARAM_TEXT);
$data->item_name = required_param('quantity', PARAM_INT);
$data->on0 = optional_param('on0', array(), PARAM_TEXT);
$data->os0 = optional_param('os0', array(), PARAM_TEXT);
$data->custom = optional_param('custom', array(), PARAM_RAW);
$data->currency_code = required_param('currency_code', PARAM_RAW);
$data->amount = required_param('amount', PARAM_RAW);
$data->for_auction = required_param('for_auction', PARAM_BOOL);
$data->no_note = required_param('no_note', PARAM_INT);
$data->no_shipping = required_param('no_shipping', PARAM_INT);
$data->rm = required_param('rm', PARAM_RAW);
$data->cbt = optional_param('cbt', array(), PARAM_TEXT);
$data->first_name = required_param('first_name', PARAM_TEXT);
$data->last_name = required_param('last_name', PARAM_TEXT);
$data->address = optional_param('address', array(), PARAM_TEXT);
$data->city = optional_param('city', array(), PARAM_TEXT);
$data->email = required_param('email', PARAM_EMAIL);
$data->country = optional_param('country', array(), PARAM_TEXT);
$data->stripeToken = required_param('stripeToken', PARAM_RAW);
$data->stripeTokenType = required_param('stripeTokenType', PARAM_RAW);
$data->stripeEmail = required_param('stripeEmail', PARAM_EMAIL);

$custom = explode('-', $data->custom);
$data->checkout_id      = (int)$custom[0];
$data->userid           = (int)$custom[1];
$data->paymentid        = (int)$custom[2];
$data->payment_gross    = $data->amount;
$data->payment_currency = $data->currency_code;
$data->timeupdated      = time();

local_ecommerce_log_payment_error($plugin_type, "Testing", $data);

// Get the checkout records.
if (! $checkout = $DB->get_record("local_ecommerce_checkout", array("id"=>$data->checkout_id, "payment_status"=>\local_ecommerce\payment::$STATUS_PENDING))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid checkout id", $data);
    die("Not a valid checkout id");
}

// Get the payment records.
if (! $payment = $DB->get_record("local_ecommerce_payments", array("id"=>$data->paymentid, "status"=>1))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid payment id", $data);
    die("Not a valid payment id");
}
$config = (object)unserialize($payment->settings);

// Get the user records.
if (! $user = $DB->get_record("user", array("id" => $data->userid))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid user id", $data);
    redirect($CFG->wwwroot);
}

$PAGE->set_context(context_system::instance());

// If currency is incorrectly set then someone maybe trying to cheat the system.
if ($data->currency_code != \local_ecommerce\payment::get_currency('code')) {
    local_ecommerce_log_payment_error($plugin_type, "Currency does not match settings, received: ".$data->mc_currency, $data);
    die("Currency does not match settings, received: ".$data->mc_currency);
}

// Check that amount paid is the correct amount
$cost = (float) $checkout->amount;

// Use the same rounding of floats as on the enrol form.
$cost = format_float($cost, 2, false);

if ($data->payment_gross < $cost) {
    local_ecommerce_log_payment_error($plugin_type, "Amount paid is not enough ($data->payment_gross < $cost))", $data);
    die("Amount paid is not enough ($data->payment_gross < $cost))");
}

// Let's say each article costs 15.00 bucks.

try {

    require_once($CFG->dirroot.'/local/ecommerce/payments/stripe/lib/Stripe.php');

    Stripe::setApiKey($config->secretkey);
    $charge1 = Stripe_Customer::create(array(
        "email" => required_param('stripeEmail', PARAM_EMAIL),
        "description" => get_string('charge_description1', 'local_ecommerce')
    ));

    $charge = Stripe_Charge::create(array(
      "amount" => $cost * 100,
      "currency" => \local_ecommerce\payment::get_currency('code'),
      "card" => required_param('stripeToken', PARAM_RAW),
      "description" => get_string('charge_description2', 'local_ecommerce'),
      "receipt_email" => required_param('stripeEmail', PARAM_EMAIL)
    ));

    // Send the file, this line will be reached if no error was thrown above.
    $checkout->txn_id = ($charge->balance_transaction) ? $charge->balance_transaction : '';
    $checkout->payment_status = ($charge->status) ? $charge->status : '';
    $checkout->pending_reason = ($charge->failure_message) ? $charge->failure_message : '';
    $checkout->reason_code = ($charge->failure_code) ? $charge->failure_code : '';
    $checkout->timeupdated = $data->timeupdated;
    $checkout->paymentid = $data->paymentid;

    if ($charge->status and $charge->status == 'succeeded') {
        $checkout->payment_status = \local_ecommerce\payment::$STATUS_COMPLETED;
    }

    // ALL CLEAR !
    $DB->update_record("local_ecommerce_checkout", $checkout);

    // Enrol user
    \local_ecommerce\checkout::enrol_user($checkout);

    // send checkout notification
    \local_ecommerce\notification::send_checkout_notification($checkout);

    redirect(new moodle_url($CFG->wwwroot . '/local/ecommerce/payments/return.php', array('id'=>$checkout->id)));

} catch (Stripe_CardError $e) {
    // Catch the errors in any way you like.
    echo 'Error';
}

// Catch the errors in any way you like.

catch (Stripe_InvalidRequestError $e) {
    // Invalid parameters were supplied to Stripe's API.
    echo 'Invalid parameters were supplied to Stripe\'s API';

} catch (Stripe_AuthenticationError $e) {
    // Authentication with Stripe's API failed
    // (maybe you changed API keys recently).
    echo 'Authentication with Stripe\'s API failed';

} catch (Stripe_ApiConnectionError $e) {
    // Network communication with Stripe failed.
    echo 'Network communication with Stripe failed';
} catch (Stripe_Error $e) {

    // Display a very generic error to the user, and maybe send
    // yourself an email.
    echo 'Stripe Error';
} catch (Exception $e) {
    // Something else happened, completely unrelated to Stripe.
    echo 'Something else happened, completely unrelated to Stripe';
}
