<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Listens for Instant Payment Notification from Stripe
 *
 * This script waits for Payment notification from Stripe,
 * then double checks that data by sending it back to Stripe.
 * If Stripe verifies this then it sets up the enrolment for that
 * user.
 *
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require("../../../../config.php");
require($CFG->dirroot."/local/ecommerce/payments/quickbooks/lib/config.php");
require_once($CFG->dirroot."/local/ecommerce/payments/quickbooks/lib/Authorization.php");

$id = required_param('id', PARAM_INT);
$code = required_param('code', PARAM_RAW);
$realmId = required_param('realmId', PARAM_RAW);

// Get the payment records.
if (! $payment = $DB->get_record("local_ecommerce_payments", array("id"=>$id))) {
    die("Not a valid payment id");
}

$config = (object)unserialize($payment->settings);
$config->payment_id = $payment->id;

$authorization = new Authorization($config);
$authorization->setAccessTokenByAuthorizationCode($code, $realmId);

redirect(new moodle_url($CFG->wwwroot . '/local/ecommerce/payments/details.php', array('id'=>$payment->id)));