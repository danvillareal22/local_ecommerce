<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * payment utility script
 *
 * @package    local_ecommerce
 * @copyright  2017 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require("../../../config.php");
require_once($CFG->dirroot."/local/ecommerce/locallib.php");

$id = required_param('id', PARAM_INT);

if (!$checkout = $DB->get_record("local_ecommerce_checkout", array("id"=>$id))) {
    redirect($CFG->wwwroot);
}

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:checkout', $context);

$PAGE->set_url('/local/ecommerce/return.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);
sleep(5);
if ($checkout->payment_status == \local_ecommerce\payment::$STATUS_COMPLETED) {

    redirect($CFG->wwwroot . '/my/', get_string('paymentthanks', 'local_ecommerce'));

} else {

    redirect($CFG->wwwroot . '/my/', get_string('paymentsorry', 'local_ecommerce'));

}
