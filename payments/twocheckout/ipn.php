<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Listens for Instant Payment Notification from Stripe
 *
 * This script waits for Payment notification from Stripe,
 * then double checks that data by sending it back to Stripe.
 * If Stripe verifies this then it sets up the enrolment for that
 * user.
 *
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Disable moodle specific debug messages and any errors in output,
// comment out when debugging or better look into error log!
define('NO_DEBUG_DISPLAY', true);

require("../../../../config.php");
require_once($CFG->dirroot."/local/ecommerce/locallib.php");
require_once($CFG->dirroot."/enrol/ecommerce/locallib.php");
require_once($CFG->libdir.'/enrollib.php');
require_once($CFG->libdir . '/filelib.php');

$data = new stdClass();
foreach ($_REQUEST as $key => $value) {
    $data->{$key} = urlencode($value);
}

local_ecommerce_log_payment_error($plugin_type, "Testing", $data);

// Stripe does not like when we return error messages here,
// the custom handler just logs exceptions and stops.
set_exception_handler('local_ecommerce_ipn_exception_handler');
$plugin_type = 'twocheckout';

$custom = explode('-', $data->custom);
$data->checkout_id      = (int)$custom[0];
$data->userid           = (int)$custom[1];
$data->paymentid        = (int)$custom[2];
$data->payment_gross    = $data->amount;
$data->payment_currency = $data->currency_code;
$data->timeupdated      = time();

// Get the checkout records.
if (! $checkout = $DB->get_record("local_ecommerce_checkout", array("id"=>$data->checkout_id, "payment_status"=>\local_ecommerce\payment::$STATUS_PENDING))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid checkout id", $data);
    die("Not a valid checkout id");
}

// Get the payment records.
if (! $payment = $DB->get_record("local_ecommerce_payments", array("id"=>$data->paymentid, "status"=>1))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid payment id", $data);
    die("Not a valid payment id");
}
$config = (object)unserialize($payment->settings);

// Get the user records.
if (! $user = $DB->get_record("user", array("id" => $data->userid))) {
    local_ecommerce_log_payment_error($plugin_type, "Not a valid user id", $data);
    redirect($CFG->wwwroot);
}

$PAGE->set_context(context_system::instance());

// If currency is incorrectly set then someone maybe trying to cheat the system.
if ($data->currency_code != \local_ecommerce\payment::get_currency('code')) {
    local_ecommerce_log_payment_error($plugin_type, "Currency does not match settings, received: ".$data->mc_currency, $data);
    die("Currency does not match settings, received: ".$data->mc_currency);
}

// Check that amount paid is the correct amount
$cost = (float) $checkout->amount;

// Use the same rounding of floats as on the enrol form.
$cost = format_float($cost, 2, false);

if ($data->payment_gross < $cost) {
    local_ecommerce_log_payment_error($plugin_type, "Amount paid is not enough ($data->payment_gross < $cost))", $data);
    die("Amount paid is not enough ($data->payment_gross < $cost))");
}


// Validate reponse from 2Checkout
if (isset($data->demo) and $data->demo == 'Y') {
    $data->order_number = 1;
}

if ($data->key != strtoupper(md5($config->secret_word . $config->seller_id . $data->order_number . sprintf("%01.2f",$cost)))) {
    local_ecommerce_log_payment_error($plugin_type, 'Wrong secret key: order_number: ' . $data->order_number . 'total: ' . $cost . ' key: ' . $key, $data);

} else {

    // Send the file, this line will be reached if no error was thrown above.
    $checkout->txn_id = ($data->order_number) ? $data->order_number : '';
    $checkout->payment_status = \local_ecommerce\payment::$STATUS_COMPLETED;
    $checkout->pending_reason = '';
    $checkout->reason_code = '';
    $checkout->timeupdated = $data->timeupdated;
    $checkout->paymentid = $data->paymentid;

    // ALL CLEAR !
    $DB->update_record("local_ecommerce_checkout", $checkout);

    // Enrol user
    \local_ecommerce\checkout::enrol_user($checkout);

    // send checkout notification
    \local_ecommerce\notification::send_checkout_notification($checkout);
}

redirect(new moodle_url($CFG->wwwroot . '/local/ecommerce/payments/return.php', array('id'=>$checkout->id)));