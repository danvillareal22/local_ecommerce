<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the definition for the library class for file ecommerce payment
 *
 * This class provides all the functionality for the ecommerce plugin.
 *
 * @package local_ecommerce
 * @copyright 2018 SEBALE
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

class local_ecommerce_twocheckout_payment {

    private $instance = null;

    public function __construct($instance = null) {
        $this->instance = $instance;
    }

    /**
     * Get the name of the file submission plugin
     * @return string
     */
    public function get_name() {
        return get_string('twocheckout', 'local_ecommerce');
    }

    /**
     * Add elements to paymenttype form
     *
     * @param stdClass $data
     * @param MoodleQuickForm $mform
     * @return bool
     */
    public function get_form_elements(stdClass $data, MoodleQuickForm $mform) {
        global $OUTPUT;

        $mform->addElement('text', 'name', get_string('name', 'local_ecommerce'), 'size="50"');
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', get_string('required'), 'required', null, 'client');

        $mform->addElement('text', 'settings[seller_id]', get_string('seller_id', 'local_ecommerce'), 'size="50"');
        $mform->setType('settings[seller_id]', PARAM_RAW);
        $mform->addRule('settings[seller_id]', get_string('required'), 'required', null, 'client');

        $mform->addElement('text', 'settings[secret_word]', get_string('secret_word', 'local_ecommerce'), 'size="50"');
        $mform->setType('settings[secret_word]', PARAM_RAW);
        $mform->addRule('settings[secret_word]', get_string('required'), 'required', null, 'client');

        $mform->addElement('advcheckbox', 'settings[sandbox]', get_string('sandbox', 'local_ecommerce'), '&nbsp;', array('group' => 1), array(0, 1));
        $mform->setType('settings[sandbox]', PARAM_INT);

        return true;
    }

    public function payment_form($output, $params, $hidden) {
        global $CFG, $USER;

        $payment = $params['payment'];
        $config = (object)unserialize($this->instance->settings);

        $data = new \stdClass();
        $data->cost = $payment->amount;
        $data->checkouturl = ($config->sandbox) ? 'https://sandbox.2checkout.com/checkout/purchase' : 'https://www.2checkout.com/checkout/purchase';
        $data->config = $config;
        $data->user_name = fullname($USER);
        $data->user_id = $USER->id;
        $data->item_name = $payment->item_name;
        $data->item_number = $payment->id;
        $data->checkout_id = $payment->id;
        $data->currency_code = \local_ecommerce\payment::get_currency('code');
        $data->currency = \local_ecommerce\payment::get_currency();
        $data->ipnurl = new \moodle_url('/local/ecommerce/payments/twocheckout/ipn.php');
        $data->return = new \moodle_url('/local/ecommerce/payments/return.php', array('id'=>$payment->id));
        $data->first_name = $USER->firstname;
        $data->last_name = $USER->lastname;
        $data->address = $USER->address;
        $data->city = $USER->city;
        $data->email = $USER->email;
        $data->country = $USER->country;
        $data->instance = $this->instance;
        $data->logo = new \moodle_url('/local/ecommerce/payments/twocheckout/pix/logo.png');
        $a = new \stdClass();
        $a->cost = $data->currency . $data->cost;
        $a->method = '2Checkout';
        $data->payment_rubric = get_string('payment_rubric', 'local_ecommerce', $a);
        $data->cards_img =  new \moodle_url('/local/ecommerce/pix/store/credit-cards.png');
        $data->total = $data->cost;
        $data->subtotal = $params['subtotal'];
        $data->discount = $params['discount'];
        $data->salestax = $params['salestax'];
        $data->pretaxtotal = $params['pretaxtotal'];
        $data->salestaxname = $params['salestaxname'];
        $data->enablediscounts = false;
        $data->enablecoupons = false;

        return array(
            'instance' => $this->instance,
            'active' => ($hidden) ? 0 : 1,
            'form' => $output->render_from_template('local_ecommerce/twocheckoutform', $data),
            'icon' => 'credit-card'
        );
    }
}
