<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/forms/edit_paymenttype_form.php');

$id      = optional_param('id', 0, PARAM_INT);
$action  = optional_param('action', '', PARAM_TEXT);
$confirm = optional_param('confirm', 0, PARAM_BOOL);
$type    = optional_param('type', '', PARAM_TEXT);

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:managepaymenttypes', $context);

$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->set_url('/local/ecommerce/payments/edit.php', array('id' => $id));

if ($id) {
    $paymenttype = $DB->get_record('local_ecommerce_payments', array('id' => $id), '*', MUST_EXIST);
    $paymenttype->settings = unserialize($paymenttype->settings);
    $strheading = get_string('editpaymenttype', 'local_ecommerce');
} else {
    $paymenttype = new stdClass();
    $paymenttype->id = 0;
    $paymenttype->type = $type;
    $strheading = get_string('createpaymenttype', 'local_ecommerce');
}

$returnurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/payments/index.php');

if ($action == 'delete' and $paymenttype->id) {
    $PAGE->url->param('action', 'delete');
    if ($confirm and confirm_sesskey()) {
        \local_ecommerce\payment::delete_paymenttype($paymenttype->id);
        redirect($returnurl);
    }
    $strheading = get_string('deletepaymenttype', 'local_ecommerce');
    $PAGE->navbar->add(get_string('paymenttypes', 'local_ecommerce'), new moodle_url('/local/ecommerce/payments/index.php'));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    $renderer = $PAGE->get_renderer('local_ecommerce');
    $params = [
        'title' => $strheading
    ];

    $renderable = new \local_ecommerce\output\payments_delete($params);

    echo $renderer->render($renderable);

    $yesurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/payments/edit.php',
            array('id' => $paymenttype->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey()));
    $message = get_string('deletepaymenttypemsg', 'local_ecommerce', format_string($paymenttype->name));
    echo $OUTPUT->confirm($message, $yesurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}

if ($action == 'show' && $paymenttype->id && confirm_sesskey()) {
    if (!$paymenttype->status) {
        $record = (object) array('id' => $paymenttype->id, 'status' => 1);
        \local_ecommerce\payment::save_paymenttype($record);
    }
    redirect($returnurl);

} else if ($action == 'hide' && $paymenttype->id && confirm_sesskey()) {

    if ($paymenttype->status) {
        $record = (object) array('id' => $paymenttype->id, 'status' => 0);
        \local_ecommerce\payment::save_paymenttype($record);
    }
    redirect($returnurl);
}

$editform = new edit_paymenttype_form(null, array('data' => $paymenttype));

if ($editform->is_cancelled()) {

    redirect($returnurl);

} else if ($data = $editform->get_data() and optional_param('name', '', PARAM_TEXT)) {
    $data->id = \local_ecommerce\payment::save_paymenttype($data);
    redirect($returnurl);
}

$renderer = $PAGE->get_renderer('local_ecommerce');
$params = [
        'title' => $strheading,
        'formhtml' => $editform->export_for_template($renderer)
];
$renderable = new \local_ecommerce\output\payments_edit($params);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/index.php'));
$PAGE->navbar->add(get_string('paymenttypes', 'local_ecommerce'), new moodle_url('/local/ecommerce/payments/index.php'));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);

echo $OUTPUT->header();

echo $renderer->render($renderable);
$PAGE->requires->js_call_amd('local_ecommerce/admin', 'init');

echo $OUTPUT->footer();

