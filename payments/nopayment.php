<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * payment utility script
 *
 * @package    local_ecommerce
 * @copyright  2017 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require("../../../config.php");
require_once($CFG->dirroot."/local/ecommerce/locallib.php");
require_once($CFG->dirroot."/enrol/ecommerce/locallib.php");
require_once($CFG->libdir.'/enrollib.php');

$id = required_param('id', PARAM_INT);

if (!$checkout = $DB->get_record("local_ecommerce_checkout", array("id"=>$id))) {
    redirect($CFG->wwwroot);
}

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:checkout', $context);

$PAGE->set_url('/local/ecommerce/return.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

if ($checkout->payment_status == \local_ecommerce\payment::$STATUS_COMPLETED) {

    redirect($CFG->wwwroot, get_string('paymentthanks', 'local_ecommerce'));

} else {

    if ($products = \local_ecommerce\checkout::get_products_in_cart()) {
        $coupons = \local_ecommerce\coupon::get_applied_coupons();
        $discounts = \local_ecommerce\checkout::get_products_discounts($products);
        $discountprices = \local_ecommerce\checkout::get_checkout_products_prices($products, $coupons, $discounts);
        $total = \local_ecommerce\checkout::get_cart_total($products, $discountprices);
    }

    if (isset($total->total) and $total->total == 0) {

        $checkout->payment_status = \local_ecommerce\payment::$STATUS_COMPLETED;
        $checkout->txn_id = time();
        $checkout->timeupdated = time();
        $DB->update_record("local_ecommerce_checkout", $checkout);

        // Enrol user
        \local_ecommerce\checkout::enrol_user($checkout);

        redirect($CFG->wwwroot, get_string('paymentthanks', 'local_ecommerce'));
    }

    redirect($CFG->wwwroot, get_string('paymenterror', 'local_ecommerce'));
}
