<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @package    local_ecommerce
 * @copyright  2017 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/forms/contact_form.php');

if (!local_ecommerce_guestaccess()) {
    require_login();
}
local_ecommerce_enable();

$name = optional_param('name', '', PARAM_TEXT);
$email = optional_param('email', '', PARAM_EMAIL);
$phone = optional_param('phone', '', PARAM_TEXT);
$subject = optional_param('subject', '', PARAM_TEXT);
$message = optional_param('message', '', PARAM_TEXT);

$context = context_system::instance();

$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->set_url('/local/ecommerce/contact.php');
$returnURL = new moodle_url($CFG->wwwroot . '/local/ecommerce/store.php');
$submittedData = new stdClass();
$submittedData->name = $name;
$submittedData->email = $email;
$submittedData->phone = $phone;
$submittedData->subject = $subject;
$submittedData->message = $message;

$submittedDataArray = array (
    'name' => $name,
    'email' => $email,
    'phone' => $phone,
    'subject' => $subject,
    'message' => $message,
);

if (!empty($submittedData->name) && !empty($submittedData->email) && !empty($submittedData->phone) && !empty($submittedData->subject) && !empty($submittedData->message)) {
    $sendSuccess = \local_ecommerce\notification::send_contact_email($submittedData);
    if ($sendSuccess) {
        $returnMessage = get_string('message_send_success', 'local_ecommerce');
    } else {
        $returnURL = new moodle_url($CFG->wwwroot . '/local/ecommerce/contact.php');
        $returnMessage = get_string('message_send_failure', 'local_ecommerce');
    }
    redirect($returnURL, $returnMessage);
}

$strheading = get_string('contact_us', 'local_ecommerce');
$renderer = $PAGE->get_renderer('local_ecommerce');

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/index.php'));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);

echo $OUTPUT->header();
echo $renderer->store_print_menu('contact_us', isloggedin());
echo $renderer->store_print_contact_form($submittedData);
echo $renderer->store_print_checkout_footer();

$PAGE->requires->js_call_amd('local_ecommerce/store', 'initContact');

echo $OUTPUT->footer();

