<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require($CFG->dirroot . '/local/ecommerce/lib.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');

$id              = required_param('id', PARAM_INT);
$key             = optional_param('key', '', PARAM_TEXT);
$requireLogin    = optional_param('require-login', false, PARAM_BOOL);
if (!local_ecommerce_guestaccess() || !empty($requireLogin)) {
    require_login();
}
local_ecommerce_enable();

if (!empty($requireLogin)) {
    \local_ecommerce\checkout::add_to_cart_from_login($id);
}

$context = context_system::instance();

$product = $DB->get_record('local_ecommerce_products', array('id' => $id), '*', MUST_EXIST);

$title = $product->name;
$PAGE->set_url('/local/ecommerce/view.php', array('id' => $id));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->navbar->add(get_string('store', 'local_ecommerce'), new moodle_url('/local/ecommerce/store.php'));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$renderer = $PAGE->get_renderer('local_ecommerce');
$course_renderer = $PAGE->get_renderer('course');

$allowkeycheckout = \local_ecommerce\waitlist::check_allowcheckout($key, $product);
$products = array($product);
$discounts = \local_ecommerce\checkout::get_products_discounts($products);
$discountPrice = \local_ecommerce\checkout::get_product_price($product, null, $discounts);
$product->discountprice = $discountPrice['price'];
$product->imageurl = \local_ecommerce\product::get_product_image($product);
$product->description = local_ecommerce_get_formatted_text($product->description, context_system::instance(), $product->id, 'description');
$product->courses = \local_ecommerce\product::get_product_courses($product);
if (!empty($product->courses)) {
    require_once($CFG->libdir. '/coursecatlib.php');
    foreach ($product->courses as $key => $course) {
        $courseItem = new course_in_list($course);
        $product->courses[$key]->image_url = \local_ecommerce\product::get_course_images($courseItem);
        $product->courses[$key]->display_image = true;
        if ($CFG->forcelogin && !isloggedin()) {
            $product->courses[$key]->display_image = false;
        }
        $courseUrl = new moodle_url('/course/view.php', array('id' => $course->id));
        $product->courses[$key]->course_url = $courseUrl->out();
    }
}
$product->cancheckout = has_capability('local/ecommerce:checkout', $context);
$product->checkouturl = new \moodle_url('/local/ecommerce/checkout.php');
$product->currency = \local_ecommerce\payment::get_currency();
$product->incart = $DB->record_exists('local_ecommerce', array('productid'=>$product->id, 'userid'=>(isset($USER->id)) ? $USER->id : 0));
$product->inwaitlist = (!$allowkeycheckout and $DB->record_exists('local_ecommerce_waitlist', array('productid'=>$product->id, 'userid'=>(isset($USER->id)) ? $USER->id : 0)));
$sold = $DB->count_records('local_ecommerce_logs', array('instanceid'=>$product->id, 'type'=>'product'));
$product->enablewaitlist = get_config('local_ecommerce', 'enablewaitlist');
$product->seatsallow = (
        ($product->seats == 0 or
                ($product->seats and $product->seats > $sold + \local_ecommerce\waitlist::get_product_count_waitlist($product)) or
                $allowkeycheckout
        ) and !$product->inwaitlist);
$product->inwishlist = (!$product->incart && !$product->inwaitlist && $DB->record_exists('local_ecommerce_wishlist', array('productid'=>$product->id, 'userid'=>(isset($USER->id)) ? $USER->id : 0)));
$product->displaysold = get_config('local_ecommerce', 'display_sales');
$product->sold = $DB->count_records('local_ecommerce_logs', array('type' => 'product', 'status' => 'completed', 'instanceid' => $product->id));
$product->purchased = $DB->record_exists('local_ecommerce_logs', array('instanceid'=>$product->id, 'userid'=>(isset($USER->id)) ? $USER->id : 0, 'type' => 'product', 'status' => 'completed'));
$product->enable_reviews = get_config('local_ecommerce', 'enable_reviews');
$product->enable_review_changes = get_config('local_ecommerce', 'enable_review_changes');
$userRating = \local_ecommerce\review::get_user_rating($product->id);
$product->userrating = !empty($userRating) ? $userRating : 1;
$product->alreadyrated = empty($userRating) ? false : true;
$ratingObject = \local_ecommerce\review::get_rating($product->id);
if (!empty($ratingObject->total) && !empty($ratingObject->count)) {
    $product->rating = round($ratingObject->total / $ratingObject->count * 20, 0);
    $product->ratingnumber = round($ratingObject->total / $ratingObject->count, 1);
    $product->ratingusers = $ratingObject->count;
    $product->review = $ratingObject->review;
    $product->ratingplural = '';
    if ($ratingObject->count > 1) {
        $product->ratingplural = 's';
    }
}
$reviews = \local_ecommerce\review::get_reviews($product->id);
$product->socialmediasharelink = $CFG->wwwroot . '/local/ecommerce/view.php?id=' . $product->id;
$product->enablefbooksharing = get_config('local_ecommerce', 'enable_facebook_sharing');
$product->enabletwitsharing = get_config('local_ecommerce', 'enable_twitter_sharing');
$product->enablelinkedinsharing = get_config('local_ecommerce', 'enable_linkedin_sharing');
$product->producturl = $CFG->wwwroot . '/local/ecommerce/view.php?id=' . $product->id . '&require-login=1';
$carturl = new moodle_url('/local/ecommerce');
$product->carturl = $carturl->out();
$linkedProducts = \local_ecommerce\store::get_store_linked_products($product->id);

$params = [
    'page-type' => 'product-view',
    'product' => $product,
    'reviews' => $renderer->store_print_reviews(get_string('reviews', 'local_ecommerce'), $product->id, $reviews, 0),
    'courses' => $renderer->store_print_courses($product->courses)
];

$renderable = new \local_ecommerce\output\product_view($params);

echo $OUTPUT->header();
echo $renderer->store_print_menu('view', isloggedin());

$mainURL = new moodle_url('/local/ecommerce/store.php');
echo $renderer->store_print_breadcrumbs($mainURL, get_string('store', 'local_ecommerce'), get_string('item_details', 'local_ecommerce'));
echo $renderer->render($renderable);

if ($linkedProducts['total'] > 0) {
    $itemPerPage = 12;
    $page = 0;
    echo $renderer->store_print_products($itemPerPage, get_string('linked_products_teaser', 'local_ecommerce'), $linkedProducts, $page, 2);
}

echo $renderer->store_print_checkout_footer();

if ($product->cancheckout) {
    $PAGE->requires->js_call_amd('local_ecommerce/ecommerce', 'productpage', array('productid' => $product->id));
}

$PAGE->requires->js_call_amd('local_ecommerce/ecommerce', 'purchased');
$PAGE->requires->js(new moodle_url("https://platform.linkedin.com/in.js"));
$PAGE->requires->js_call_amd('local_ecommerce/store', 'init');

echo $OUTPUT->footer();
