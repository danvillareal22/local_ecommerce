<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require($CFG->dirroot . '/local/ecommerce/lib.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');

if (!local_ecommerce_guestaccess()) {
    require_login();
}

local_ecommerce_enable();
$context = context_system::instance();

if (!get_config('local_ecommerce', 'enable_shipping_policy')) {
    redirect($CFG->wwwroot . '/local/ecommerce/store.php');
}

$title = get_string('shipping_policy', 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/shipping-policy.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$renderer = $PAGE->get_renderer('local_ecommerce');

$outputParams = [
    'content' => get_config('local_ecommerce', 'shipping_policy'),
    'policy_header' => $renderer->print_basic_header($title)
];

echo $OUTPUT->header();
echo $renderer->store_print_menu('shipping_policy', isloggedin());
echo $renderer->store_print_policy_page($outputParams);
echo $renderer->store_print_checkout_footer();

$PAGE->requires->js_call_amd('local_ecommerce/store', 'init');

echo $OUTPUT->footer();
