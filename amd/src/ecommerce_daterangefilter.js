// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    local_ecommerce
 * @copyright  2017 SEBALE (http://sebale.net)
 */

define(['jquery', 'core/ajax', 'core/str', 'core/config', 'core/log', 'local_ecommerce/flatpickr'], function($, ajax, str, mdlcfg, log, flatpickr) {

    var SalesFilter = {

        init: function(timestart_date, timefinish_date) {

            var dateRange = $("#daterange").flatpickr({
                mode: "range",
                dateFormat: "Y-m-d",
                defaultDate: [timestart_date, timefinish_date],
                onReady: function(selectedDates, dateStr, instance){
                    jQuery('<div/>', {
                        class: 'flatpickr-calendar-title',
                        text: $("#daterange").attr('title')
                    }).appendTo('.flatpickr-calendar');
                },
                onChange: function(dateObj, dateStr) {
                    if (dateObj.length == 2) {
                        SalesFilter.formsubmit();
                    }
                },
            });

            $('.daterange-clear').click(function(e){
                $("#daterange").val('');
                SalesFilter.formsubmit();
            });

            $('.search-clear').click(function(e){
                $("input[name=\"search\"]").val('');
                SalesFilter.formsubmit();
            });

            $('.ecommerce-icon-calendar').click(function() {
                dateRange.toggle();
            });

        },

        formsubmit: function(timestart_date, timefinish_date) {
            $('#filter_form').submit();
        }

    };

    /**
     * @alias module:local_ecommerce/ecommerce_salesfilter
     */
    return SalesFilter;

});