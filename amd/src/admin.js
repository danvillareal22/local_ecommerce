// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This module is compatible with core/form-autocomplete.
 *
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/notification'], function($, Ajax, Notification) {

    $('.store-dropdown-link').click(function() {
        var id = $(this).data('id');
        admin.openActionDropdown(id);
    });

    $(document).click(function(e) {
        admin.closeActionDropdown(e);
    });

    $('#store-menu-toggle').click(function() {
        var menuBar = $('.ecommerce-menu-bar');
        if (menuBar.is(':visible')) {
            menuBar.hide(400);
        } else {
            menuBar.show(400);
        }
    });

    $('.search-clear').click(function() {
        window.location.href = admin.updateURL('search', '');
    });

    var admin = {

        init: function() {

        },

        closeActionDropdown: function(e) {
            var container = $('.store-dropdown-link');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                $('.store-action-button-dropdown').hide();
            }
        },

        openActionDropdown: function(id) {
            $('.store-action-button-dropdown').hide();
            $('.store-action-button-dropdown[data-id="' + id + '"]').show();
        },

        updateURL: function(key, value) {
            var baseURL = window.location.origin + window.location.pathname + '?' + key + '=' + encodeURI(value);
            var urlParamObject = new URLSearchParams(window.location.search);
            var urlParams = urlParamObject.entries();
            var param = urlParams.next();
            while (!param.done) {
                if (param.value[0] !== key) {
                    baseURL += '&' + param.value[0] + '=' + param.value[1];
                }
                param = urlParams.next();
            }
            return baseURL;
        }
    };

    return admin;

});