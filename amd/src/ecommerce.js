// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    local_ecommerce
 * @copyright  2017 SEBALE (http://sebale.net)
 */

define(['jquery', 'core/ajax', 'core/str', 'core/config', 'core/log'], function($, ajax, str, mdlcfg, log) {

    window.add_to_cart = function(productid, btn) {
        eCommerce.addtocart(productid, btn);
    };

    window.add_to_waitlist = function(productid, btn) {
        eCommerce.addtowaitlist(productid, btn);
    };

    window.add_to_wishlist = function(productid, btn) {
        eCommerce.addtowishlist(productid, btn);
    };

    var eCommerce = {

        init: function() {

            $('.add-coupon').on('click', function (event) {
                $(".product-coupons-form").toggleClass('hidden');
            });

        },

        productpage: function(productid) {

        },

        switchbutton: function(btn, msg, type, switchtype) {

            var btnclick = $(btn).attr('onclick');
            var newbtnclick = btnclick.replace(type, switchtype);
            $(btn).attr('onclick', newbtnclick);

            eCommerce.enablebutton(btn, msg, true);
        },

        disablebutton: function(btn, addLoader) {
            if (addLoader) {
                $(btn).html("<i class='fa fa-spin fa-spinner'></i>");
            }
            $(btn).prop('disabled', true);
        },

        removebutton: function(btn) {
            $(btn).remove();
        },

        enablebutton: function(btn, msg, enable) {
            $(btn).html(msg);
            if (enable) {
                $(btn).prop('disabled', false);
            }
        },

        addtocart: function(productid, btn) {

            str.get_strings([
                { key: 'addtocart', component: 'local_ecommerce' },
                { key: 'incart', component: 'local_ecommerce' },
                { key: 'addtowaitlist', component: 'local_ecommerce' },
                { key: 'inwaitlist', component: 'local_ecommerce' },
                { key: 'in_wishlist', component: 'local_ecommerce' },
                { key: 'checkout', component: 'local_ecommerce' },
            ]).then(function(strs) {

                eCommerce.disablebutton(btn);

                ajax.call([{
                    methodname: 'local_ecommerce_add_to_cart', args: {
                        productid: productid,
                    }
                }])[0]
                    .done(function (response) {
                        try {
                            if (response.status == 'incart') {
                                var checkoutURL = window.location.origin + '/local/ecommerce';
                                eCommerce.enablebutton(btn, strs[1], true);
                                var productButton = $('.btn-add-to-cart[data-product-id="' + productid + '"]');
                                $(productButton).each(function() {
                                    $(this).after('<a class="btn btn-primary active btn-add-to-cart" href="' + checkoutURL + '" data-product-id="' + productid + '">' + strs[1] + '</a>');
                                    $(this).remove();
                                });
                                var footerCheckoutArea = $('.ecommerce-checkout-button-footer');
                                if (!$('.ecommerce-checkout-button-footer .btn-checkout').length) {
                                    footerCheckoutArea.append(
                                        '<button class = "btn btn-primary btn-checkout">' +
                                            '<a href="' + checkoutURL + '"><i class="ecommerce-icon-store-active"></i> ' + strs[5] + '</a>' +
                                        '</button>'
                                    );
                                }
                            } else if (response.status == 'addtowaitlist') {
                                eCommerce.switchbutton(btn, strs[2], 'add_to_cart', 'add_to_waitlist');
                            } else if (response.status == 'inwaitlist') {
                                eCommerce.enablebutton(btn, strs[3], false);
                                var productButton = $('.btn-add-to-cart[data-product-id="' + productid + '"]')[0];
                                $(productButton).html(strs[3]).prop('disabled', true);
                            } else {
                                eCommerce.removebutton(btn);
                            }

                            eCommerce.notification(response.message);
                            if (response.productscount > 0) {
                                $('#nav-ecommerce-popover-container').find('.count-container').html(response.productscount).removeClass('hidden');
                                $('.btn-wishlist[data-product-id="' + productid + '"]').remove();
                                $('div[data-region="wishlist-content-item-container"][data-id="' + productid + '"]').remove();
                            }

                            if (response.wishlistcount > 0) {
                                $('#nav-ecommerce-wishlist-container').find('.count-container').html(response.wishlistcount).removeClass('hidden');
                            } else {
                                $('#nav-ecommerce-wishlist-container').find('.count-container').html('').addClass('hidden');
                                $('div[data-region="empty-wishlist-message"]').show();
                            }
                        } catch (Error) {
                            log.debug(Error.message);
                            eCommerce.enablebutton(btn, strs[0], true);
                        }
                    }).fail(function (ex) {
                    log.debug(ex.message);
                    eCommerce.enablebutton(btn, strs[0], true);
                });

            });

        },

        addtowaitlist: function(productid, btn) {

            str.get_strings([
                { key: 'addtowaitlist', component: 'local_ecommerce' },
                { key: 'inwaitlist', component: 'local_ecommerce' },
            ]).then(function(strs) {

                eCommerce.disablebutton(btn);

                ajax.call([{
                    methodname: 'local_ecommerce_add_to_waitlist', args: {
                        productid: productid,
                    }
                }])[0]
                    .done(function (response) {
                        try {
                            eCommerce.enablebutton(btn, strs[1], false);
                            var productButton = $('.btn-waitlist[data-product-id="' + productid + '"]')[0];
                            $(productButton).html(strs[1]).prop('disabled', true);
                            eCommerce.notification(response.message);
                            $('.btn-wishlist[data-product-id="' + productid + '"]').remove();
                            $('div[data-region="wishlist-content-item-container"][data-id="' + productid + '"]').remove();
                            if (response.wishlistcount > 0) {
                                $('#nav-ecommerce-wishlist-container').find('.count-container').html(response.wishlistcount).removeClass('hidden');
                            } else {
                                $('#nav-ecommerce-wishlist-container').find('.count-container').html('').addClass('hidden');
                                $('div[data-region="empty-wishlist-message"]').show();
                            }
                        } catch (Error) {
                            log.debug(Error.message);
                            eCommerce.enablebutton(btn, strs[0], true);
                        }
                    }).fail(function (ex) {
                        log.debug(ex.message);
                        eCommerce.enablebutton(btn, strs[0], true);
                });

            });

        },

        addtowishlist: function(productid, btn) {
            str.get_strings([
                { key: 'add_to_wishlist', component: 'local_ecommerce' },
                { key: 'in_wishlist', component: 'local_ecommerce' },
            ]).then(function(strs) {

                eCommerce.disablebutton(btn);

                ajax.call([{
                    methodname: 'local_ecommerce_add_to_wishlist', args: {
                        productid: productid,
                    }
                }])[0]
                    .done(function (response) {
                        try {
                            eCommerce.enablebutton(btn, strs[1], false);
                            $(btn).removeClass('btn-primary');
                            $(btn).addClass('btn-secondary');
                            $('.ecommerce-product-view .btn-wishlist').each(function() {
                                eCommerce.disablebutton(this);
                                $(this).removeClass('btn-primary');
                                $(this).addClass('btn-secondary');
                            });
                            eCommerce.notification(response.message);
                            $('#nav-ecommerce-wishlist-container').find('.count-container').html(response.productscount).removeClass('hidden');
                        } catch (Error) {
                            log.debug(Error.message);
                            eCommerce.enablebutton(btn, strs[0], true);
                        }
                    }).fail(function (ex) {
                    log.debug(ex.message);
                    eCommerce.enablebutton(btn, strs[0], true);
                });

            });

        },

        notification: function(msg) {
            $('#user-notifications').html('<div class="alert alert-info alert-block fade in" role="alert">' +
                '    <button type="button" class="close" data-dismiss="alert">×</button>' + msg + '</div>');
            setTimeout(function() { $('.shoping-message').remove(); }, 5000);
        },

        invoicestable: function() {

            $('a.invoice-approve').on('click', function (event) {
                var invoiceid = $(this).attr('data-type-id');
                var element = $(this).parent().parent();

                ajax.call([{
                    methodname: 'local_ecommerce_approve_invoice', args: {
                        id: invoiceid,
                    }
                }])[0]
                    .done(function (response) {
                        try {
                            element.remove();
                            location.reload();
                        } catch (Error) {
                            log.debug(Error.message);
                        }
                    }).fail(function (ex) {
                    log.debug(ex.message);
                });
            });

        },

        purchased: function() {
            str.get_strings([
                { key: 'change_rating', component: 'local_ecommerce' }
            ]).then(function(strs) {

                $('.btn-rating').click(function() {
                    var productID = $(this).data('id');
                    $('#submit-rating').attr('data-id', productID);
                    $('.ratings-star').each(function (index, thisCheckbox) {
                        $(thisCheckbox).prop('checked', false);
                    });
                    $('#st' + $(this).attr('data-current-rating')).prop('checked', true);
                    $('#review').val($('.hidden-review[data-id="' + productID + '"]').html());
                });

                $('#load-more-reviews').click(function() {
                    var productID = $(this).data('product-id');
                    var page = $(this).data('page');
                    ajax.call([{
                        methodname: 'local_ecommerce_load_more_reviews', args: {
                            productid: productID,
                            page: page
                        }
                    }])[0].done(function (response) {
                        try {
                            $('.load-more-box').remove();
                            $('#product-review-container').append(response.reviews);
                        } catch (Error) {
                            log.debug(Error.message);
                        }
                    }).fail(function (ex) {
                        log.debug(ex.message);
                    });
                });

                $('.ratings-star').click(function () {
                    $('.ratings-star').each(function (index, thisCheckbox) {
                        $(thisCheckbox).prop('checked', false);
                    });
                    $(this).prop('checked', true);
                });

                $('#submit-rating').click(function () {
                    var productID = $(this).attr('data-id');
                    var rating = $('.ratings-star:checked').val();
                    var review = $('#review').val();
                    var ratingButton = $('.btn-rating[data-id="' + productID + '"]');
                    ratingButton.html(strs[0]);
                    ratingButton.attr('data-current-rating', rating);
                    $('.hidden-review[data-id="' + productID + '"]').html(review);
                    $('#rating-modal').modal('hide');
                    ajax.call([{
                        methodname: 'local_ecommerce_rate_product', args: {
                            productid: productID,
                            rating: rating,
                            review: review
                        }
                    }])[0]
                        .done(function (response) {
                            try {
                                eCommerce.notification(response.message);
                                if ($('.product-rating[data-id="' + productID + '"]').length) {
                                    $('.product-rating[data-id="' + productID + '"] .full-stars').css({width: response.newRating + '%'});
                                    $('.rating-users[data-id="' + productID + '"]').html('(' + response.newRatingUsers + ')');
                                } else {
                                    $('.ratings-area[data-id="' + productID + '"]').html(
                                        '<div class="product-rating" data-id = "' + productID + '">' +
                                            '<div class="empty-stars"></div>' +
                                            '<div class="full-stars" style="width:' + response.newRating + '%"></div>' +
                                        '</div>' +
                                        '<div class = "rating-users" data-id = "' + productID + '">(' + response.newRatingUsers + ')</div>');
                                }
                            } catch (Error) {
                                log.debug(Error.message);
                            }
                        }).fail(function (ex) {
                            log.debug(ex.message);
                    });
                });
                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
                window.twttr = (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0],
                        t = window.twttr || {};
                    if (d.getElementById(id)) return t;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "https://platform.twitter.com/widgets.js";
                    fjs.parentNode.insertBefore(js, fjs);

                    t._e = [];
                    t.ready = function(f) {
                        t._e.push(f);
                    };

                    return t;
                }(document, "script", "twitter-wjs"));
            });
        }
    };

    /**
     * @alias module:local_ecommerce/ecommerce
     */
    return eCommerce;

});