// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This module is compatible with core/form-autocomplete.
 *
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/notification', 'core/str'], function($, Ajax, Notification, str) {

    var searchBox = $('#search-box');
    searchBox.keyup(function() {
        var urlParamObject = new URLSearchParams(window.location.search);
        var data = {
            query: $(this).val(),
            category: null
        };
        if (urlParamObject.has('category')) {
            data.category = urlParamObject.get('category');
        }
        var list = store.getAutocomplete(data);
        list.done(function(json) {
            var results = store.processResults(this, json);
            $('#search-box-suggestions').remove();
            if (results.length) {
                $('.search-form-input').append('<ul class="form-autocomplete-suggestions" id="search-box-suggestions" role="listbox" aria-hidden="false"></ul>');
                for (var i in results) {
                    $('#search-box-suggestions').append('<li role="option" aria-hidden="false" aria-selected="true" class="search-box-suggestion">' + results[i].label + '</li>');
                }
                $('.search-box-suggestion').mousedown(function() {
                    window.location.href = store.updateURL('search', $(this).html());
                });
            }
        });
    });

    searchBox.blur(function() {
        $('#search-box-suggestions').remove();
    });

    $('.search-clear').click(function() {
        window.location.href = store.updateURL('search', '');
    });

    $('#order-by').change(function() {
        if ($(this).val().length) {
            window.location.href = store.updateURL('orderby', $(this).val());
        }
    });

    var purchasesSearchBox = $('#purchases-search-box');
    purchasesSearchBox.keyup(function() {
        var urlParamObject = new URLSearchParams(window.location.search);
        var data = {
            query: $(this).val()
        };
        if (urlParamObject.has('daterange')) {
            data.daterange = urlParamObject.get('daterange');
        }
        var list = store.getPurchasesAutocomplete(data);
        list.done(function(json) {
            var results = store.processResults(this, json);
            $('#search-box-suggestions').remove();
            if (results.length) {
                $('.search-form-input').append('<ul class="form-autocomplete-suggestions" id="search-box-suggestions" role="listbox" aria-hidden="false"></ul>');
                for (var i in results) {
                    $('#search-box-suggestions').append('<li role="option" aria-hidden="false" aria-selected="true" class="search-box-suggestion">' + results[i].label + '</li>');
                }
                $('.search-box-suggestion').mousedown(function() {
                    window.location.href = store.updateURL('search', $(this).html());
                });
            }
        });
    });

    purchasesSearchBox.blur(function() {
        $('#search-box-suggestions').remove();
    });

    $('#load-more-products-btn').click(function() {
        loadMoreProducts(this)
    });

    function loadMoreProducts(element) {
        var type = $(element).data('type');
        var page = $(element).data('page');
        var limit = $(element).data('limit');
        var urlParamObject = new URLSearchParams(window.location.search);
        var search = urlParamObject.get('search');
        var category = urlParamObject.get('category');
        var orderBy = urlParamObject.get('orderby');
        Ajax.call([{
            methodname: 'local_ecommerce_load_more_products', args: {
                type: type,
                page: page,
                limit: limit,
                search: search,
                category: category,
                orderBy: orderBy
            }
        }])[0].done(function (response) {
            try {
                $('.load-more-products-box').remove();
                $('.frontpage-products-list[data-type=' + type + ']').append(response.products);
                $('#load-more-products-btn').click(function() {
                    loadMoreProducts(this)
                });
                triggerSocialMediaShares();
            } catch (Error) {
                log.debug(Error.message);
            }
        }).fail(function (ex) {
            log.debug(ex.message);
        });
    }

    function triggerSocialMediaShares() {
        var d = document;
        var s = 'script';
        var id = 'facebook-jssdk';
        var js, fjs = d.getElementsByTagName(s)[0];
        FB.XFBML.parse(document.getElementById('shareButton'));

        id = "twitter-wjs";
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        IN.parse();
    }

    var slideIndex = 1;

// Next/previous controls
    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

// Thumbnail image controls
    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = $(".store-slides");
        // var dots = $(".store-slides-dot");
        if (n > slides.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = slides.length
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        /*for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }*/
        slides[slideIndex-1].style.display = "block";
        // dots[slideIndex-1].className += " active";
    }

    $('#next-slide').click(function() {
        plusSlides(1);
    });

    $('#previous-slide').click(function() {
        plusSlides(-1);
    });

    /*$(".store-slides-dot").click(function() {
        currentSlide($(this).data('id'));
    });*/

    $('#invoice-payment-button').click(function() {
        window.location.href = store.updateURL('invoicepaid', 1);
    });

    $('#submit-contact-form').click(function(e) {
        e.preventDefault();
        store.validateContactForm();
    });

    $('#store-menu-toggle').click(function() {
        var menuBar = $('.ecommerce-menu-bar');
        if (menuBar.is(':visible')) {
            menuBar.hide(400);
        } else {
            menuBar.show(400);
        }
    });

    var store = {

        init: function(linkedProducts) {
            if (typeof linkedProducts !== undefined && linkedProducts !== undefined) {
                $('#id_linked_products > option').each(function() {
                    var index = $(this).val();
                    $(this).html(linkedProducts[index]);
                });
            }
        },

        initSlider: function(goSlider) {
            // $( document ).ready(function() {

            // });
            if (typeof goSlider != undefined && goSlider != undefined && goSlider.length && goSlider !== false && goSlider !== 0) {
                showSlides(1);
            }
            if ($('#page-my-index').length) {
                var urlSearchParamObject = new URLSearchParams(window.location.search);
                var urlParams = urlSearchParamObject.entries();
                var paramSet = false;
                var param = urlParams.next();
                while (!param.done) {
                    paramSet = true;
                    param = urlParams.next();
                }
                if (paramSet) {
                    $('html,body').animate({
                        scrollTop: $(".block_products_catalogue").offset().top
                    })
                }
            }
        },

        initContact: function() {
            // $( document ).ready(function() {

            // });
        },

        /**
         * List of products.
         *
         * @param {Object} options Additional parameters to pass to the external function.
         * @return {Promise}
         */
        getAutocomplete: function(args) {
            var promise = Ajax.call([{
                    methodname: 'local_ecommerce_get_store_autocomplete', args: args
                }])[0];
            return promise;
        },

        getPurchasesAutocomplete: function(args) {
            var promise = Ajax.call([{
                    methodname: 'local_ecommerce_get_purchases_autocomplete', args: args
                }])[0];
            return promise;
        },

        /**
         * Process the results for auto complete elements.
         *
         * @param {String} selector The selector of the auto complete element.
         * @param {Array} results An array or results.
         * @return {Array} New array of results.
         */
        processResults: function(selector, results) {
            var options = [];
            data = JSON.parse(results.resp)[0];

            $.each(data, function(index, value) {
                options.push({
                    label: value.name
                });
            });

            return options;
        },

        updateURL: function(key, value) {
            var baseURL = window.location.origin + window.location.pathname + '?' + key + '=' + encodeURI(value);
            var urlParamObject = new URLSearchParams(window.location.search);
            var urlParams = urlParamObject.entries();
            var param = urlParams.next();
            while (!param.done) {
                if (param.value[0] !== key) {
                    baseURL += '&' + param.value[0] + '=' + param.value[1];
                }
                param = urlParams.next();
            }
            return baseURL;
        },

        validateContactForm: function() {
            var errorArea = $('#error-area');
            var name = $('#contact-name');
            var email = $('#contact-email');
            var phone = $('#contact-phone');
            var subject = $('#contact-subject');
            var message = $('#contact-message');
            errorArea.html('').hide();
            $('.contact-form-field').each(function() {
                $(this).css({'border-color': '#C5CED8'});
            });
            str.get_strings([
                { key: 'name_required', component: 'local_ecommerce' },
                { key: 'email_required', component: 'local_ecommerce' },
                { key: 'subject_required', component: 'local_ecommerce' },
                { key: 'message_required', component: 'local_ecommerce' },
                { key: 'email_invalid', component: 'local_ecommerce' },
                { key: 'phone_required', component: 'local_ecommerce' }
            ]).then(function(strs) {
                var valid = true;
                if (!name.val().length) {
                    console.log(strs[0]);
                    errorArea.html(strs[0]).show();
                    name.css({'border-color': '#a94442'});
                    valid = false;
                } else if (!email.val().length) {
                    errorArea.html(strs[1]).show();
                    email.css({'border-color': '#a94442'});
                    valid = false;
                } else if (!phone.val().length) {
                    errorArea.html(strs[5]).show();
                    phone.css({'border-color': '#a94442'});
                    valid = false;
                } else if (!subject.val().length) {
                    errorArea.html(strs[2]).show();
                    subject.css({'border-color': '#a94442'});
                    valid = false;
                } else if (!message.val().length) {
                    errorArea.html(strs[3]).show();
                    message.css({'border-color': '#a94442'});
                    valid = false;
                } else {
                    var validEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email.val());
                    if (!validEmail) {
                        errorArea.html(strs[4]).show();
                        email.css({'border-color': '#a94442'});
                        valid = false;
                    }
                }
                if (valid) {
                    $('#ecommerce-contact-form').submit();
                }
            });
        }
    };

    return store;

});