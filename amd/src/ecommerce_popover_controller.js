// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Controls the eCommerce popover in the nav bar.
 *
 * See template: local_ecommerce/ecommerce_popover_controller
 *
 * @module     local_ecommerce/ecommerce_popover_controller
 * @class      ecommerce_popover_controller
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['jquery', 'core/ajax', 'core/templates', 'core/str', 'core/url',
            'core/notification', 'core/custom_interaction_events', 'core/popover_region_controller',
            'local_ecommerce/ecommerce_repository', 'local_ecommerce/ecommerce_area_events'],
        function($, Ajax, Templates, Str, URL, DebugNotification, CustomEvents,
            PopoverController, eCommerceRepo, eCommerceAreaEvents) {

    var SELECTORS = {
        ALL_PRODUCTS_CONTAINER: '[data-region="all-products"]',
        PRODUCT: '[data-region="product-content-item-container"]',
        EMPTY_MESSAGE: '[data-region="empty-message"]',
        COUNT_CONTAINER: '[data-region="count-container"]',
        PRODUCT_DELETE_LINK: '[data-action="delete-item"]',
        CONTENT_CONTAINER: '[data-region="popover-region-content-container"]',
        CONTENT_FOOTER: '[data-region="popover-region-footer-container"]',
    };

    /**
     * Constructor for the eCommercePopoverController.
     * Extends PopoverRegionController.
     *
     * @param {object} element jQuery object root element of the popover
     */
    var eCommercePopoverController = function(element) {
        // Initialise base class.
        PopoverController.call(this, element);

        this.productsCount = 0;
        this.userId = this.root.attr('data-userid');
        this.container = this.root.find(SELECTORS.ALL_PRODUCTS_CONTAINER);
        this.content_container = this.root.find(SELECTORS.CONTENT_CONTAINER);
        this.footer = this.root.find(SELECTORS.CONTENT_FOOTER);
        this.initialLoad = false;

        // Let's find out how many products items in ecommerce.
        this.loadProductsCount();
    };

    /**
     * Clone the parent prototype.
     */
    eCommercePopoverController.prototype = Object.create(PopoverController.prototype);

    /**
     * Make sure the constructor is set correctly.
     */
    eCommercePopoverController.prototype.constructor = eCommercePopoverController;


    /**
     * Return the jQuery element with the content. This will return either
     * the unread eCommerce container or the all products container
     * depending on which is currently visible.
     *
     * @method getContent
     * @return {object} jQuery object currently visible content contianer
     */
    eCommercePopoverController.prototype.getContent = function() {
        return this.container;
    };

    /**
     * Return the jQuery element with the content. This will return either
     * the unread eCommerce container or the all products container
     * depending on which is currently visible.
     *
     * @method getContent
     * @return {object} jQuery object currently visible content contianer
     */
    eCommercePopoverController.prototype.getRoot = function() {
        return this.root;
    };

    /**
     * Return the jQuery element with the content. This will return either
     * the unread eCommerce container or the all products container
     * depending on which is currently visible.
     *
     * @method getFooter
     * @return {object} jQuery object currently visible content contianer
     */
    eCommercePopoverController.prototype.getFooter = function() {
        return this.footer;
    };

    /**
     * Check if the first load of product has been triggered for the current
     * state of the popover.
     *
     * @method hasDoneInitialLoad
     * @return {bool} true if first product loaded, false otherwise
     */
    eCommercePopoverController.prototype.hasDoneInitialLoad = function() {
        return this.initialLoad;
    };

    /**
     * Show the products count badge on the menu toggle if there
     * are unread products, otherwise hide it.
     *
     * @method renderProductsCount
     */
    eCommercePopoverController.prototype.renderProductsCount = function() {
        var element = this.root.find(SELECTORS.COUNT_CONTAINER);

        if (this.productsCount) {
            element.text(this.productsCount);
            element.removeClass('hidden');
        } else {
            element.addClass('hidden');
        }
    };

    /**
     * Ask the server how many unread products are left, render the value
     * as a badge on the menu toggle and update the aria labels on the menu
     * toggle.
     *
     * @method loadProductsCount
     */
    eCommercePopoverController.prototype.loadProductsCount = function() {
        eCommerceRepo.countProducts({userid: this.userId}).then(function(count) {
            this.productsCount = count;
            this.renderProductsCount();
        }.bind(this));
    };

    /**
     * Find the product element for the given id.
     *
     * @param {int} id
     * @method getProductElement
     * @return {object|null} The product element
     */
    eCommercePopoverController.prototype.getProductElement = function(id) {
        var element = this.root.find(SELECTORS.PRODUCT + '[data-id="' + id + '"]');
        return element.length == 1 ? element : null;
    };

    /**
     * Render the ecommerce data with the appropriate template and add it to the DOM.
     *
     * @method renderProducts
     * @param {array} products data
     * @param {object} container jQuery object the container to append the rendered products
     * @return {object} jQuery promise that is resolved when all products have been
     *                  rendered and added to the DOM
     */
    eCommercePopoverController.prototype.renderProducts = function(products, container) {
        var promises = [];
        var allhtml = [];
        var alljs = [];

        if (products.length) {
            $.each(products, function(index, product) {
                var promise = Templates.render('local_ecommerce/product_content_item', product);
                promises.push(promise);

                promise.then(function(html, js) {
                    allhtml[index] = html;
                    alljs[index] = js;
                })
                .fail(DebugNotification.exception);
            }.bind(this));
        }

        return $.when.apply($.when, promises).then(function() {
            container.html('');
            if (products.length) {
                $.each(products, function(index) {
                    container.append(allhtml[index]);
                    Templates.runTemplateJS(alljs[index]);
                });
            }

        });
    };

    /**
     * Render the ecommerce data with the appropriate template and add it to the DOM.
     *
     * @method renderProducts
     * @param {array} products data
     * @return {object} jQuery promise that is resolved when all products have been
     *                  rendered and added to the DOM
     */
    eCommercePopoverController.prototype.renderTotals = function(result) {

        var promise = Templates.render('local_ecommerce/ecommerce_popover_footer', result);
        var root = this.getRoot();

        promise.then(function(html, js) {
            root.find(SELECTORS.CONTENT_FOOTER).html(html);
            Templates.runTemplateJS(js);
        })
        .fail(DebugNotification.exception);
    };


    /**
     * Send a request to the server to remove a single product and update
     * the unread count and product elements appropriately.
     *
     * @param {jQuery} element
     * @return {Promise|boolean}
     * @method deleteProduct
     */
    eCommercePopoverController.prototype.deleteProduct = function(element) {

        return eCommerceRepo.deleteItem(element.attr('data-id'))
            .then(function() {
                this.productsCount--;
                element.remove();
                location.reload();
            }.bind(this));
    };


    /**
     * Send a request for loading products from the server, if we aren't already
     * loading some and haven't already loaded all of them.
     *
     *
     * @method loadProducts
     * @return {object} jQuery promise that is resolved when eCommerce have been
     *                        retrieved and added to the DOM
     */
    eCommercePopoverController.prototype.loadProducts = function() {
        if (this.isLoading) {
            return $.Deferred().resolve();
        }

        this.startLoading();
        var request = {userid: this.userId};

        var container = this.getContent();
        return eCommerceRepo.query(request).then(function(result) {
            var products = result.products;
            this.productsCount = result.productscount;
            this.initialLoad = true;

            if (products.length) {
                this.root.find(SELECTORS.EMPTY_MESSAGE).hide();
                this.renderProducts(products, container);
                return this.renderTotals(result);
            } else {
                this.footer.empty();
                this.renderTotals(result);
                this.root.find(SELECTORS.EMPTY_MESSAGE).show();
            }

            return false;
        }.bind(this))
            .always(function() {
                this.stopLoading();
            }.bind(this));
    };


    /**
     * Add all of the required event listeners for this eCommerce popover.
     *
     * @method registerEventListeners
     */
    eCommercePopoverController.prototype.registerEventListeners = function() {
        CustomEvents.define(this.root, [
            CustomEvents.events.activate,
        ]);

        // Remove product
        this.root.on(CustomEvents.events.activate, SELECTORS.PRODUCT_DELETE_LINK, function(e) {
            var element = $(e.target).closest(SELECTORS.PRODUCT);
            this.deleteProduct(element);
            e.stopPropagation();
        }.bind(this));

        // Update the product information when the menu is opened.
        this.root.on(this.events().menuOpened, function() {
            this.loadProducts();
        }.bind(this));

        // Set aria attributes when popover is loading.
        this.root.on(this.events().startLoading, function() {
            this.getContent().attr('aria-busy', 'true');
        }.bind(this));

        // Set aria attributes when popover is finished loading.
        this.root.on(this.events().stopLoading, function() {
            this.getContent().attr('aria-busy', 'false');
        }.bind(this));

        // Set aria attributes when popover is finished loading.
        this.content_container.after('<div data-region="popover-region-footer-container" class="popover-region-footer-container"></div>');

    };

    return eCommercePopoverController;
});
