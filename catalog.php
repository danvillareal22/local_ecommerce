<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce products
 *
 * @package    local_ecommerce
 * @copyright  2018
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');

$edit   = optional_param('edit', null, PARAM_BOOL);    // Turn editing on and off

if (!local_ecommerce_guestaccess()) {
    require_login();
}
local_ecommerce_enable();
$context = context_system::instance();

$title = get_string('productscatalogue', 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/catalogue.php');
$PAGE->set_pagelayout('mydashboard');
$PAGE->set_pagelayout('standard');
$PAGE->blocks->add_region('content');
$PAGE->set_context($context);

$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

// Toggle the editing state and switches
if (isloggedin() && $PAGE->user_allowed_editing()) {

    if ($edit !== null) {             // Editing state was specified
        $USER->editing = $edit;       // Change editing state
    } else {                          // Editing state is in session
        if (!empty($USER->editing)) {
            $edit = 1;
        } else {
            $edit = 0;
        }
    }

    // Add button for editing page
    $params = array('edit' => !$edit);
    $editstring = ($edit) ? get_string('updatemymoodleoff') : get_string('updatemymoodleon');

    $url = new moodle_url("$CFG->wwwroot/local/ecommerce/catalogue.php", $params);
    $button = $OUTPUT->single_button($url, $editstring);
    $PAGE->set_button($button);
}

echo $OUTPUT->header();

echo $OUTPUT->custom_block_region('content');

echo $OUTPUT->footer();
