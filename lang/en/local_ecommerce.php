<?php
// This file is part of the Local plans plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin sends users a plans message after logging in
 * and notify a moderator a new user has been added
 * it has a settings page that allow you to configure the messages
 * send.
 *
 * @package    local
 * @subpackage ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'eCommerce';

$string['ecommerce:dashboardview'] = 'eCommerce Dashboard';
$string['ecommerce:managecategories'] = 'eCommerce manage categories';
$string['ecommerce:editcategory'] = 'eCommerce edit category';
$string['ecommerce:manageproducts'] = 'eCommerce manage products';
$string['ecommerce:editproduct'] = 'eCommerce edit product';
$string['ecommerce:managecoupons'] = 'eCommerce manage coupons';
$string['ecommerce:editcoupons'] = 'eCommerce edit coupons';
$string['ecommerce:managediscounts'] = 'eCommerce manage discounts';
$string['ecommerce:editdiscount'] = 'eCommerce edit discount';
$string['ecommerce:managepaymenttypes'] = 'eCommerce manage payment types';
$string['ecommerce:assign'] = 'eCommerce assign';
$string['ecommerce:viewreports'] = 'eCommerce view Reports';
$string['ecommerce:viewsales'] = 'eCommerce view Sales';
$string['ecommerce:settings'] = 'eCommerce Settings';
$string['ecommerce:checkout'] = 'eCommerce checkout';
$string['ecommerce:approveinvoices'] = 'eCommerce Approve Invoices';
$string['ecommerce:editwaitlist'] = 'eCommerce Edit Waitlist';
$string['ecommerce:viewwaitlist'] = 'eCommerce View Waitlist';
$string['ecommerce:managewaitlist'] = 'eCommerce Manage Waitlist';
$string['ecommerce:manageuserfields'] = 'eCommerce Manage User Profile Fields';
$string['ecommerce:viewallproducts'] = 'eCommerce View All Products';
$string['ecommerce:assigncohorts'] = 'eCommerce Assign Cohorts';
$string['enabled'] = 'Enable eCommerce';
$string['enabled_desc'] = '';

$string['managecategories'] = 'eCommerce assign';
$string['eCommerceDisabled'] = 'The eCommerce plugin is disabled';
$string['dashboard'] = 'eCommerce Dashboard';
$string['shopingcartdashboard'] = 'Shopingcart Dashboard';
$string['categories'] = 'Categories';
$string['products'] = 'Products';
$string['coupons'] = 'Coupons';
$string['discounts'] = 'Discounts';
$string['coupons'] = 'Coupons';
$string['paymenttypes'] = 'Payment Types';
$string['payments'] = 'Payments';
$string['sales'] = 'Sales';
$string['reports'] = 'Reports';
$string['settings'] = 'Settings';
$string['active'] = 'Active';
$string['inactive'] = 'Inactive';
$string['showall'] = 'Show All';
$string['searchplaceholder'] = 'Search';
$string['createnew'] = 'Create New';
$string['deletecategory'] = 'Delete Category';
$string['deletecategorymsg'] = 'Are you sure you want to delete category "{$a}"';
$string['editcategory'] = 'Edit category';
$string['createcategory'] = 'Create category';
$string['parentcategory'] = 'Parent category';
$string['name'] = 'Name';
$string['description'] = 'Description';
$string['topcategory'] = 'Top category';
$string['categoryid'] = 'Category ID';
$string['status'] = 'Status';
$string['created'] = 'Created';
$string['actions'] = 'Actions';
$string['createproduct'] = 'Create product';
$string['editproduct'] = 'Edit product';
$string['deleteproduct'] = 'Delete product';
$string['deleteproductmsg'] = 'Are you sure you want to delete product "{$a}"';
$string['productcode'] = 'Product Code';
$string['selectcategory'] = 'Select Category';
$string['price'] = 'Price';
$string['price_excl_gst'] = get_config('local_ecommerce', 'enable_sales_tax') ? '(excl. ' . get_config('local_ecommerce', 'sales_tax_name') . ')' : '';
$string['discountprice'] = 'Discount Price';
$string['categoryidnumbertaken'] = 'Category Id already exists';
$string['productidnumbertaken'] = 'Product Code already exists';
$string['couponcodetaken'] = 'Coupon Code already exists';
$string['image'] = 'Image';
$string['category'] = 'Category';
$string['assigncourses'] = 'Assign courses';
$string['assigncoursesfor'] = 'Assign courses: {$a}';
$string['assignproductsforcoupon'] = 'Assign products for "{$a}" coupon';
$string['assignproductsfordiscount'] = 'Assign products for "{$a}" discount';
$string['courses'] = 'Courses';
$string['assigned'] = 'Assigned';
$string['allcategories'] = 'All Categories';
$string['filter'] = 'Filter';
$string['selectcourses'] = 'Select Courses';
$string['deleterecordconfirmation'] = 'Are you sure want to delete this redord?';
$string['addtocart'] = '<i class="ecommerce-icon-cart"></i> Add to cart';
$string['incart'] = '<i class="ecommerce-icon-cart-active"></i> Go to cart';
$string['viewcourse'] = 'Click to view course';
$string['addedtocart'] = 'Product added to the cart';
$string['alreadyincart'] = 'Product already in cart';
$string['alreadyinwaitlist'] = 'Product already in waitlist';
$string['ecommerce'] = 'eCommerce';
$string['shopingcartisempty'] = 'eCommerce is empty';
$string['remove'] = 'Remove';
$string['subtotal'] = 'Sub-Total';
$string['total'] = 'Total';
$string['discount'] = 'Discount';
$string['checkout'] = 'Checkout';
$string['addcoupon'] = 'Add Coupon';
$string['apply'] = 'Apply';
$string['productremoved'] = 'Product removed';
$string['processpayment'] = 'Process Payment';
$string['continuetocourses'] = 'Back to courses';
$string['user'] = 'User';
$string['cost'] = 'Cost';
$string['ecommercecheckout'] = 'eCommerce Checkout';
$string['assignproducts'] = 'Assign Products';
$string['discountpercents'] = '{$a} %';
$string['code'] = 'Code';
$string['starttime'] = 'Start Time';
$string['endtime'] = 'End Time';
$string['expiration'] = 'Expiration';
$string['usedperuser'] = 'Can be used times per user';
$string['usedcount'] = 'Number of times can be used';
$string['discountinpercentage'] = 'Discount (%)';
$string['discount_type'] = 'Type';
$string['discount_type_help'] = '<strong>All products</strong> applies a discount to all purchases across the board;<br />
<strong>All selected products</strong> applies a discount only to the selected products, and only if all the given products are selected;<br />
<strong>Any selected products</strong> applies a discount to any of the selected products;<br />
<strong>Limited number of selected products</strong> applies a discount to the selected products if the number of those products in the user\'s cart is between the specified minimum and maximum.';
$string['coupon_type'] = 'Type';
$string['coupon_type_help'] = '<strong>All products</strong> applies a discount to all purchases across the board;<br />
<strong>Any selected products</strong> applies a discount to any of the selected products.';
$string['createcoupon'] = 'Create Coupon';
$string['editcoupon'] = 'Edit Coupon';
$string['deletecoupons'] = 'Delete Coupon';
$string['deletecouponmsg'] = 'Are you sure you want to delete this coupon?';
$string['creatediscount'] = 'Create Discount';
$string['editdiscount'] = 'Edit Discount';
$string['deletediscount'] = 'Delete Discount';
$string['deletediscountmsg'] = 'Are you sure you want to delete this discount?';
$string['assignproductshrd'] = 'Assign Products';
$string['selectproducts'] = 'Select Products';
$string['product'] = 'Product';
$string['cannotapplycoupon'] = 'Cannot apply this coupon: <b>{$a}</b>';
$string['invalidcoupon'] = 'Invalid coupon';
$string['couponalreadyapplied'] = 'Coupon already used';
$string['couponinactive'] = 'Coupon is inactive';
$string['couponisover'] = 'Coupon is over';
$string['couponmaxnumber'] = 'used all seats';
$string['couponusermaxnumber'] = 'used all seats';
$string['paymenterror'] = 'Payment error';
$string['anyproduct'] = 'Any selected products';
$string['allselectedproducts'] = 'All selected products';
$string['allproducts'] = 'All products';
$string['condition'] = 'Limited number of selected products';
$string['type'] = 'Type';
$string['currency'] = 'Currency';
$string['minnumber'] = 'Minimum number of products';
$string['maxnumber'] = 'Maximum number of products';
$string['showproductitems'] = 'Show Product items on Product Page';
$string['productscatalogue'] = 'Products Catalogue';
$string['enableguestaccess'] = 'Enable Guest Access';
$string['descriptionhdr'] = 'Product Description';
$string['enableseats'] = 'Enable Seats';
$string['noseatsforproductusewaitlist'] = 'Sorry, there is no available seats for this product. You can add product to waitlist.';
$string['noseatsforproduct'] = 'Sorry, there is no available seats for this product.';
$string['collapsible-action'] = 'COLLAPSE ALL FORMS';
$string['expandable-action'] = 'EXPAND ALL FORMS';
$string['assignnewcourses'] = 'Assign a new course';
$string['currentlyassignedcourses'] = 'Currently assigned courses';
$string['assignnewproducts'] = 'Assign a new product';
$string['currentlyassignedproducts'] = 'Currently assigned products';

/* Store */
$string['accreditation_points'] = 'Accreditation portion <small>e.g. 100 points</small>';
$string['associated_accreditation'] = 'Associated accreditation <small>This will be displayed with the below value e.g. This contributes 20 points towards Optometry 101</small>';
$string['best_rated_items'] = 'Highest rated items';
$string['buy_now'] = 'Buy Now';
$string['payment_method'] = 'Payment Method';
$string['choose_payment_method'] = 'Please choose a payment method below.';
$string['click_process_payment'] = 'Click \'Process Payment\' to continue.';
$string['contact_phone'] = 'Phone';
$string['contact_us'] = 'Contact Us';
$string['download_all'] = 'Download all';
$string['email_required'] = 'Email is a required field';
$string['email_invalid'] = 'Invalid email address';
$string['enter_course_name'] = 'Enter a course name';
$string['enter_product_name'] = 'Enter a product name';
$string['expand_tab_menu'] = 'Expand tab menu';
$string['featured_item'] = 'Featured Item';
$string['featured_items'] = 'Featured items';
$string['item_details'] = 'Item details';
$string['item_purchased'] = 'Item purchased';
$string['linked_products'] = 'Linked Products';
$string['linked_products_teaser'] = 'Related items';
$string['message'] = 'Message';
$string['message_required'] = 'Message is a required field';
$string['message_send_success'] = 'Your message was sent successfully and you should receive a reply shortly.';
$string['message_send_failure'] = 'Your message could not be sent at this time. Please try again later.';
$string['name_required'] = 'Name is a required field';
$string['order_by'] = 'Order by';
$string['order_summary'] = 'Order Summary';
$string['payment_rubric'] = 'You will be paying <strong>{$a->cost}</strong> with {$a->method}.';
$string['phone_required'] = 'Phone is a required field';
$string['physical_product'] = 'Physical Product';
$string['physical_products'] = 'Physical Products';
$string['popular_items'] = 'Popular items';
$string['pre_tax_total'] = 'Total before tax';
$string['price_highest_lowest'] = 'Price: highest to lowest';
$string['price_lowest_highest'] = 'Price: lowest to highest';
$string['purchases'] = 'Purchases';
$string['rate_this_item'] = 'Review this item';
$string['recent_items'] = 'Recent items';
$string['required_rubric'] = 'Required fields are marked: ';
$string['send_message'] = 'Send';
$string['shipping_tandcs'] = 'Shipping and returns policy';
$string['sort_order'] = 'Sort Order';
$string['store'] = 'Store';
$string['subject'] = 'Subject';
$string['subject_required'] = 'Subject is a required field';
$string['this_contributes'] = 'This contributes';
$string['units_sold'] = 'sold';
$string['view_purchases'] = 'View Purchases';
$string['virtual_products'] = 'Virtual Products';
$string['youtube_link'] = 'YouTube video ID (typically 11 or so characters)';
$string['your_cart'] = 'Your Cart';

/* Reviews */
$string['change_rating'] = 'Change review';
$string['delete_review'] = 'Delete review';
$string['delete_review_msg'] = 'Are you sure you want to delete review ID: "{$a}"';
$string['product_rated_successfully'] = 'Product reviewed successfully';
$string['rating'] = 'Rating';
$string['review_word_limit'] = 'Review (1000 characters maximum)';
$string['review'] = 'Review';
$string['review_deleted'] = 'Review ID: "{$a}" successfully deleted';
$string['review_for'] = 'Review: ';
$string['review_id'] = 'Review ID';
$string['reviews'] = 'Reviews';
$string['reviews_total'] = 'review/s total';
$string['submit_rating'] = 'Submit review';

/* Slider */
$string['create_slider'] = 'Create slider';
$string['display_slider'] = 'Display slider';
$string['edit_slider'] = 'Edit slider';
$string['enable_slider'] = 'Enable Slider';
$string['slider'] = 'Slider';
$string['slider_caption_1'] = 'Slider button 1 text';
$string['slider_caption_2'] = 'Slider button 2 text';
$string['slider_caption_3'] = 'Slider button 3 text';
$string['slider_caption_4'] = 'Slider button 4 text';
$string['slider_caption_5'] = 'Slider button 5 text';
$string['slider_created'] = 'Slider created successfully';
$string['slider_image_1'] = 'Slider image 1';
$string['slider_image_2'] = 'Slider image 2';
$string['slider_image_3'] = 'Slider image 3';
$string['slider_image_4'] = 'Slider image 4';
$string['slider_image_5'] = 'Slider image 5';
$string['slider_image'] = '';
$string['slider_image_help'] = 'Image must be 5:1 width to height ratio.  The wider the better. <strong>Ensure that all images are the same height and width</strong>.';
$string['slider_updated'] = 'Slider updated successfully';
$string['slider_url_1'] = 'Slider button 1 URL';
$string['slider_url_2'] = 'Slider button 2 URL';
$string['slider_url_3'] = 'Slider button 3 URL';
$string['slider_url_4'] = 'Slider button 4 URL';
$string['slider_url_5'] = 'Slider button 5 URL';

/* Wishlist */
$string['add_to_wishlist'] = '<i class="ecommerce-icon-heart"></i>';
$string['added_to_wishlist'] = 'Product added to wishlist.';
$string['already_in_wishlist'] = 'This product is already on your wishlist.';
$string['empty_wishlist'] = 'There are no items on your wishlist.';
$string['in_wishlist'] = '<i class="ecommerce-icon-heart"></i>';
$string['show_wishlist'] = 'Open wishlist';
$string['wishlist'] = 'Wishlist';

$string['event_product_deleted'] = 'eCommerce product deleted';
$string['event_product_deleted_description'] = 'User {$a->userid} deleted eCommerce product {$a->productid}';
$string['event_relation_deleted'] = 'eCommerce product relation deleted';
$string['event_product_deleted_description'] = 'User {$a->userid} deleted eCommerce product {$a->productid} relation';
$string['event_relation_created'] = 'eCommerce product relation created';
$string['event_product_created'] = 'eCommerce product created';
$string['event_product_created_description'] = 'User {$a->userid} created eCommerce product {$a->productid} relation';
$string['event_product_updated'] = 'eCommerce product updated';
$string['event_product_updated_description'] = 'User {$a->userid} updated eCommerce product {$a->productid} relation';
$string['tagarea_local_ecommerce'] = 'eCommerce';
$string['tagcollection_ecommerce'] = 'eCommerce tag collection';

$string['paymentsorry'] = 'Thank you for your payment. Your payment is being processed and you should be able to continue to the <a href = "' . $CFG->wwwroot . '/my/?myoverviewtab=courses">course</a> shortly.  Please alert the administrator if your course is not available soon.';
$string['paymentthanks'] = 'Thank you for your payment.<br /><br />If you have purchased a product related to a course, you will now be enrolled in that course and can access it from the Courses link on your <a href = "' . $CFG->wwwroot . '/my/?myoverviewtab=courses">home page</a>.';

// settings
$string['paypalbusiness'] = 'Business PayPal email';
$string['paypalsandbox'] = 'Sandbox';
$string['paymentoptionstext'] = 'Payment options include PayPal, debit card, or credit card';
$string['sendcheckoutnotification'] = 'Send checkout notification';
$string['enable_sales_tax'] = 'Enable sales tax';
$string['sales_tax_name'] = 'Sales tax name (e.g. GST)';
$string['sales_tax_percentage'] = 'Sales tax amount (%)';
$string['store_phone_number'] = 'Contact phone number';
$string['opening_hours'] = 'Office hours';
$string['opening_hours_desc'] = 'Displayed as listed or not at all if left blank';
$string['attachreceipt'] = 'Attach invoice to checkout notification';
$string['company_info'] = 'Company Info';
$string['company_logo'] = 'Company Logo';
$string['invoice_duration'] = 'Invoice Due';
$string['selecttype'] = 'Select Type';
$string['paymenttype'] = 'Payment Type';
$string['stripesecretkey'] = 'Stripe Secret Key';
$string['stripepublishablekey'] = 'Stripe Publishable Key';
$string['validatezipcode'] = 'Validate the billing postal code';
$string['billingaddress'] = 'Require users to enter their billing address';
$string['authorizeloginid'] = 'Authorize.net login ID';
$string['authorizetransactionkey'] = 'Authorize.net transaction key';
$string['authorizemerchantmd5hash'] = 'Authorize.net merchant MD5 hash key';
$string['authorizecheckproductionmode'] = 'Check for production mode';
$string['paymenttypesnotsetup'] = 'Not setup Payment Types';
$string['stripe_sorry'] = "Sorry, you can not use the script that way.";
$string['charge_description1'] = "create customer for email receipt";
$string['charge_description2'] = 'Charge for Product Enrolment Cost.';
$string['sandbox'] = 'Sandbox';
$string['quickbookspayment'] = 'QuickBooks Payment';
$string['clientid'] = 'Client ID';
$string['clientsecret'] = 'Client Secret';
$string['cardnumber'] = 'Card Number';
$string['expirationdate'] = 'Expiration Date';
$string['month'] = 'Month';
$string['year'] = 'Year';
$string['cardcvv'] = 'Card CVV';
$string['seller_id'] = 'Seller ID';
$string['secret_word'] = 'Secret Word';
$string['paymenttype'] = 'Payment Type';
$string['manual'] = 'Manual';
$string['date'] = 'Date';
$string['enablecoupons'] = 'Enable Coupons';
$string['enablediscounts'] = 'Enable Discounts';
$string['enable_reviews'] = 'Enable reviews';
$string['enable_review_changes'] = 'Enable review changes';
$string['enable_review_changes_desc'] = 'Automatically disabled if reviews are disabled';
$string['display_sales'] = 'Display product sales numbers on store page';
$string['display_company_header'] = 'Display company header on store page when user is not logged in';
$string['display_virtual_products'] = 'Display virtual products on store page';
$string['display_virtual_products_desc'] = 'Virtual products are typically linked to courses';
$string['display_physical_products'] = 'Display physical products on store page';
$string['virtual_products_name'] = 'Virtual products name';
$string['virtual_products_name_desc'] = 'Displayed as header on store page';
$string['physical_products_name'] = 'Physical products name';
$string['physical_products_name_desc'] = 'Displayed as header on store page';
$string['default_number_products_per_page'] = 'Default number of products per page';
$string['enable_facebook_sharing'] = 'Enable Facebook sharing';
$string['enable_twitter_sharing'] = 'Enable Twitter sharing';
$string['enable_linkedin_sharing'] = 'Enable LinkedIn sharing';
$string['facebook_link'] = 'Store Facebook link (if applicable)';
$string['twitter_link'] = 'Store Twitter link (if applicable)';
$string['linkedin_link'] = 'Store LinkedIn link (if applicable)';
$string['enableuserfields'] = 'Enable User Profile Fields';
$string['enableuserinterests'] = 'Enable User Interests';
$string['enablecohorts'] = 'Enable Cohorts';
$string['features'] = 'Features';
$string['license'] = 'License';
$string['license_email'] = 'Email';
$string['license_apikey'] = 'API Key';
$string['site_policies'] = 'Site Policies';
$string['enable_shipping_policy'] = 'Display shipping and returns policy page';
$string['shipping_policy'] = 'Shipping and Returns Policy';
$string['shipping_policy_desc'] = '';
$string['enable_privacy_policy'] = 'Display privacy policy page';
$string['privacy_policy'] = 'Privacy Policy';
$string['privacy_policy_desc'] = '';
$string['internal_and_external'] = 'To logged in users and on external store page';
$string['internal_only'] = 'To logged in users only';
$string['external_only'] = 'On external store page only';
$string['user_key'] = 'User Key';
$string['secret_key'] = 'Secret Key';
$string['featuredisabled'] = 'This feature is disabled in plugin confiruration "{$a}"';
$string['ecommercenotavailable'] = 'eCommerce License is not valid';
$string['waitlist'] = 'Waitlist';
$string['enablewaitlist'] = 'Enable Waitlist';
$string['waitlistnotificationmessage'] = 'Product available based on your waitlist position notification message';
$string['waitlistnotificationmessage_desc'] = 'Supported tags: [[product]], [[username]], [[checkouturl]]';
$string['checkout_subject'] = 'Successful Checkout notification subject';
$string['checkoutnotificationmessage'] = 'Successful Checkout notification message';
$string['checkoutnotificationmessage_desc'] = 'Supported tags: [[productslist]], [[orderid]], [[username]], [[amount]], [[discount]], [[subtotal]], [[salestax]], [[currency]], [[salestaxname]], [[salestaxpercentage]], [[reviewurls]]';
$string['enable_thank_you_email'] = 'Enable additional thank you email';
$string['thank_you_subject'] = 'Thank you email subject';
$string['thank_you_message'] = 'Thank you email message';
$string['thank_you_message_desc'] = 'Supported tags: [[productslist]], [[orderid]], [[username]], [[amount]], [[discount]], [[subtotal]], [[salestax]], [[currency]], [[salestaxname]], [[salestaxpercentage]], [[reviewurls]]';
$string['invoice_notification_subject'] = 'Sent invoice notification subject';
$string['invoicenotificationmessage'] = 'Sent invoice notification message';
$string['invoicenotificationmessage_desc'] = 'Supported tags: [[productslist]], [[orderid]], [[username]], [[amount]], [[discount]], [[subtotal]], [[salestax]], [[currency]], [[salestaxname]], [[salestaxpercentage]], [[reviewurls]]';
$string['invoicepaymentnotificationmessage'] = 'Invoice payment notification message';
$string['invoicepaymentnotificationmessage_desc'] = 'Supported tags: [[productslist]], [[orderid]], [[username]], [[amount]], [[discount]], [[subtotal]], [[salestax]], [[currency]], [[salestaxname]], [[salestaxpercentage]], [[reviewurls]]';
$string['enable_awaiting_checkout_emails'] = 'Enable email reminder when cart not checked out';
$string['awaiting_checkout_duration'] = 'Time between cart item added and email sent';
$string['cart_awaiting_checkout_message'] = 'Cart awaiting checkout message';
$string['cart_awaiting_checkout_message_desc'] = 'Scheduled task must be enabled.  Supported tags: [[productslist]], [[username]], [[checkouturl]]';
$string['cart_awaiting_checkout_subject'] = 'Cart awaiting checkout email subject';
$string['enable_awaiting_checkout_emails2'] = 'Enable email follow up when cart not checked out';
$string['awaiting_checkout_duration2'] = 'Time between cart item added and follow up email sent';
$string['cart_awaiting_checkout_message2'] = 'Cart awaiting checkout follow up message';
$string['cart_awaiting_checkout_message_desc2'] = 'Scheduled task must be enabled.  Supported tags: [[productslist]], [[username]], [[checkouturl]]';
$string['cart_awaiting_checkout_subject2'] = 'Cart awaiting checkout follow up email subject';
$string['enable_awaiting_checkout_emails3'] = 'Enable second email follow up when cart not checked out';
$string['awaiting_checkout_duration3'] = 'Time between cart item added and second follow up email sent';
$string['cart_awaiting_checkout_message3'] = 'Cart awaiting checkout second follow up message';
$string['cart_awaiting_checkout_message_desc3'] = 'Scheduled task must be enabled.  Supported tags: [[productslist]], [[username]], [[checkouturl]]';
$string['cart_awaiting_checkout_subject3'] = 'Cart awaiting checkout second follow up email subject';
$string['enable_contact_page'] = 'Enable contact us page';
$string['contact_us_email_text'] = 'Contact us email text';
$string['contact_us_email_text_desc'] = 'Supported tags: [[name]], [[email]], [[phone]], [[message]]';

$string['waitlist_duration'] = 'Waitlist Duration';
$string['seats'] = 'Seats';
$string['inwaitlist'] = '<i class="ecommerce-icon-menu"></i> Added to waitlist';
$string['addtowaitlist'] = '<i class="ecommerce-icon-menu"></i> Add to waitlist';
$string['addedtowaitlist'] = 'Product added to waitlist';
$string['removedfromlist'] = 'Removed from waitlist';
$string['time'] = 'Time';

// DASHBOARD
$string['registeredusers'] = 'Registered Users';
$string['confirmedusers'] = 'Confirmed Users';
$string['usedcoupons'] = 'Used Coupons';
$string['discountsapplied'] = 'Discounts Applied';
$string['productscreated'] = 'Products Created';
$string['ordersprocessed'] = 'Orders Processed';
$string['paidusers'] = 'Paid Users';
$string['couponsused'] = 'Coupons Used';
$string['discounttypesused'] = 'Discount Types Used';
$string['couponsapplied'] = 'Coupons Applied';
$string['enrollmentmethods'] = 'Enrolment Methods';
$string['orderid'] = 'Order ID';
$string['firstname'] = 'First Name';
$string['lastname'] = 'Last Name';
$string['registeredon'] = 'Registered On';
$string['paidon'] = 'Date Range';
$string['date_paid'] = 'Date Paid';
$string['last_action'] = 'Last Action';
$string['statistic'] = 'Statistic';
$string['statistics'] = 'Statistics';
$string['lastorders'] = 'Last Orders';
$string['amountpaid'] = 'Amount Paid';
$string['amount_due'] = 'Amount Due';
$string['poweredby'] = 'Powered by <a href="https://intelliboard.net/">IntelliBoard</a>';
$string['dashboardcontentbox1'] = 'Trends over time';

// nav shopping cart
$string['emptyecommerce'] = 'Your cart is empty.';
$string['showecommerce'] = 'Open cart';
$string['hideecommercewindow'] = 'Close cart';
$string['ecommercewindow'] = 'Show shopping window';

// notifications
// checkout notification
$string['notifications'] = 'Notifications';
$string['checkoutemailsubject'] = 'You successfully purchased course "{$a->productslist}"';
$string['checkoutemailbody'] = 'Hello,

Thank you for your payment! You successfully purchased products "{$a->productslist}".';
$string['invoiceemailsubject'] = 'eCommerce Invoice "{$a->productslist}"';
$string['invoiceemailbody'] = 'Hello,

This is invoice for course "{$a->productslist}".';
$string['messageprovider:checkout_notify'] = 'eCommerce checkout notification';

// waitlits notification
$string['waitlistemailsubject'] = 'Product {$a->product} is now awailable for you';
$string['waitlistmailbody'] = 'Hello,

Product "{$a->product}" is now available for you. You need to login and buy it with this <a href="{$a->checkouturl}">link</a>.';
$string['messageprovider:waitlist_notify'] = 'eCommerce waitlist notification';

// invoice payment request notification
$string['invoicepaymentmailsubject'] = 'eCommerce Invoice Payment Request';
$string['invoicepaymentmailbody'] = 'Hello,

User {$a->username} send request to pay with invoice.
The Order ID is {$a->orderid}.';
$string['messageprovider:invoicerequest_notify'] = 'eCommerce invoice payment request notification';

// invoice
$string['billto'] = 'Bill To';
$string['invoicenumber'] = 'INVOICE #';
$string['invoicedate'] = 'DATE';
$string['invoiceduedate'] = 'DUE DATE';
$string['purchaseorder'] = 'PURCHASE ORDER';
$string['balancedue'] = 'BALANCE DUE';
$string['period'] = 'Period';
$string['amount'] = 'Amount';

// payments
$string['paypal'] = 'PayPal';
$string['paypalpayment'] = 'PayPal';
$string['stripe'] = 'Stripe';
$string['stripepayment'] = 'Stripe';
$string['authorizenet'] = 'Authorize.net';
$string['authorizenetpayment'] = 'Authorize.net';
$string['currency'] = 'Currency';
$string['editpaymenttype'] = 'Edit Payment Type';
$string['createpaymenttype'] = 'Create Payment Type';
$string['deletepaymenttype'] = 'Delete Payment Type';
$string['deletepaymenttypemsg'] = 'Are you sure you want to delete payment type "{$a}"';
$string['twocheckout'] = '2Checkout';
$string['enableinvoicepayment'] = 'Enable Invoice Payment';
$string['downloadinvoice'] = 'Download Invoice';
$string['approvepayment'] = 'Approve Payment';
$string['paymentinvoiceoptionstext'] = 'If you choose to pay by invoice, download, process, and inform the company representative to expect your payment.';

// sales
$string['allsales'] = 'All Sales';
$string['invoices'] = 'Invoices';
$string['customers'] = 'Customers';
$string['pending'] = 'Pending';
$string['pending_sales'] = 'Pending Sales';
$string['customer'] = 'Customer';
$string['email'] = 'Email';
$string['invoice'] = 'Invoice';
$string['status_completed'] = 'Completed';
$string['status_pending'] = 'Pending';
$string['status_rejected'] = 'Rejected';
$string['daterange'] = 'Date Range';
$string['daterangeclear'] = 'Clear Date Range';
$string['invoicenumber'] = 'Invoice #';
$string['dateissued'] = 'Date Issued';
$string['balance'] = 'Balance';
$string['amount_paid'] = 'Amount Paid';
$string['invoice_paid'] = 'Thank you for your order.<br /><br />If you have ordered a product related to a course, you will be enrolled in that course once payment is complete.  It will eb accessible from the Courses link on your <a href = "' . $CFG->wwwroot . '/my/?myoverviewtab=courses">home page</a>.';
$string['settings'] = 'Settings';
$string['general'] = 'General';
$string['invoices'] = 'Invoices';
$string['print'] = 'Print';
$string['mail'] = 'Mail';
$string['wrong-id'] = 'Wrong ID';
$string['openbalance'] = 'Open Balance';
$string['totalpaid'] = 'Total Paid';


// reports
$string['allreports'] = 'All Reports';
$string['run'] = 'Run';
$string['view'] = 'View';
$string['income'] = 'Income';
$string['report1title'] = 'Customer Contact List';
$string['report1description'] = 'This report lists each customer’s phone number, email and billing address, and other contact information.';
$string['report2title'] = 'Sales by Customer';
$string['report2description'] = 'Lists the individual sales transactions for each customer, including dates, types, amounts, and totals.';
$string['report3title'] = 'Sales by Product';
$string['report3description'] = 'Summarizes sales for each item on your Product/Service List. Includes quantity, amount, balance, and payment type.';
$string['report4title'] = 'Coupons Used';
$string['report4description'] = 'Summary of coupons used for each or bundle of products.';
$string['report5title'] = 'Discounts Used';
$string['report5description'] = 'Summary of discounts used for each or bundle of products.';
$string['wrongreportid'] = 'Wrong Report ID';
$string['phone'] = 'Phone';
$string['mobilephone'] = 'Mobile Phone';
$string['userbillingaddress'] = 'Billing Address';
$string['datecouponused'] = 'Date Coupon Was Used On';
$string['productsusedon'] = 'Products Used On';
$string['discountwithpercent'] = 'Discount %';
$string['coupon'] = 'Coupon';
$string['used'] = 'Used';
$string['applied'] = 'Applied';
$string['datediscountused'] = 'Date Discount Was Used On';
$string['totalsaved'] = 'Total Saved';
$string['salesprice'] = 'Sales Price';
$string['details'] = 'Details';
$string['view_details'] = 'View details';
$string['graphmon1'] = 'Jan';
$string['graphmon2'] = 'Feb';
$string['graphmon3'] = 'Mar';
$string['graphmon4'] = 'Apr';
$string['graphmon5'] = 'May';
$string['graphmon6'] = 'Jun';
$string['graphmon7'] = 'Jul';
$string['graphmon8'] = 'Aug';
$string['graphmon9'] = 'Sep';
$string['graphmon10'] = 'Oct';
$string['graphmon11'] = 'Nov';
$string['graphmon12'] = 'Dec';
$string['totalamountreceived'] = 'Total Amount Received';
$string['numberofproductssold'] = 'Number of Products Sold';
$string['totalincome'] = 'Total Income';
$string['reports-index-box-header1'] = 'Earnings and products sold in the last 12 months';
$string['reports-index-box-header2'] = 'Detailed Reports';

// enrollments
$string['enrollmenthdr'] = 'Enrolment Options';
$string['defaultperiod'] = 'Default enrolment duration';
$string['expirynotify'] = 'Notify before enrolment expires';
$string['expirythreshold'] = 'Notification threshold';
$string['expirythreshold_desc'] = 'How long before enrolment expiry should users be notified?';

// waitlist
$string['deletefromlisttmsg'] = 'Are you sure you want to delete product "{$a}" from waitlist?';
$string['deleteuserfromlisttmsg'] = 'Are you sure you want to delete user "{$a->user}" from product "{$a->product}" waitlist?';
$string['removefromlist'] = 'Delete from waitlist';
$string['ecommercewaitlist'] = 'eCommerce Waitlist';
$string['key'] = 'Key';
$string['messagesent'] = 'Message successfully sent';
$string['seatnumber'] = 'Seat Number';
$string['waitlistseats'] = 'Waitlist Seats';

// user fields
$string['userfields'] = 'Filter by Profile Fields';
$string['userfield'] = 'Profile Field';
$string['assignfieldsfor'] = 'Assign Profile Fields to {$a}';
$string['addfield'] = 'Add Field';
$string['deletefield'] = 'Delete Field';
$string['deletefieldmsg'] = 'Are you sure you want to delete field "{$a}"';
$string['editfieldfor'] = 'Edit Field for {$a}';
$string['createfieldfor'] = 'Create Field for {$a}';
$string['selectfield'] = 'Select Field';
$string['selectvalues'] = 'Select Values';
$string['selectvalue'] = 'Select Value';
$string['fielddisabled'] = 'Disabled';
$string['fieldenabled'] = 'Enabled';
$string['beforedate'] = 'Before Date';
$string['afterdate'] = 'After Date';
$string['equaldate'] = 'Equal Date';
$string['fieldname'] = 'Field Name';
$string['fieldshortname'] = 'Field Short Name';
$string['fieldtype'] = 'Field Type';
$string['fieldvalue'] = 'Field Value';
$string['profilefields'] = 'Profile Fields';

// cohorts
$string['assigncohorts'] = 'Filter by Cohorts';
$string['assigncohortsfor'] = 'Assign cohorts for "{$a}" product';
$string['selectcohort'] = 'Select cohort';
$string['cohort'] = 'Cohort';


// cron tasks
$string['awaiting_checkout_task'] = 'Awaiting Cart Checkout Task';
$string['waitlist_task'] = 'eCommerce Waitlist';
