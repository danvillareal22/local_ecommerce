<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings
 *
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

$temp = new admin_settingpage('local_ecommerce_general', get_string('general', 'local_ecommerce'));
//$temp->add(new admin_setting_heading('general', get_string('general', 'local_ecommerce'), ''));

$name = 'local_ecommerce/enabled';
$title = get_string('enabled', 'local_ecommerce');
$description = get_string('enabled_desc', 'local_ecommerce');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$temp->add($setting);

$currencies = \local_ecommerce\payment::get_currencies();
$temp->add(new admin_setting_configselect('local_ecommerce/currency', get_string('currency', 'local_ecommerce'), '', 'USD', $currencies));

// Sales tax.
$name = 'local_ecommerce/enable_sales_tax';
$title = get_string('enable_sales_tax', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$temp->add($setting);

$name = 'local_ecommerce/sales_tax_name';
$title = get_string('sales_tax_name', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$name = 'local_ecommerce/sales_tax_percentage';
$title = get_string('sales_tax_percentage', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$name = 'local_ecommerce/display_company_header';
$title = get_string('display_company_header', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$temp->add($setting);

// Physical products.
$name = 'local_ecommerce/display_virtual_products';
$title = get_string('display_virtual_products', 'local_ecommerce');
$description = get_string('display_virtual_products_desc', 'local_ecommerce');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$temp->add($setting);

/*$name = 'local_ecommerce/display_physical_products';
$title = get_string('display_physical_products', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$temp->add($setting);*/

$name = 'local_ecommerce/virtual_products_name';
$title = get_string('virtual_products_name', 'local_ecommerce');
$description = get_string('virtual_products_name_desc', 'local_ecommerce');
$default = get_string('virtual_products', 'local_ecommerce');
$setting = new admin_setting_configtext($name, $title, $description, $default);
$temp->add($setting);

/*$name = 'local_ecommerce/physical_products_name';
$title = get_string('physical_products_name', 'local_ecommerce');
$description = get_string('physical_products_name_desc', 'local_ecommerce');
$default = get_string('physical_products', 'local_ecommerce');
$setting = new admin_setting_configtext($name, $title, $description, $default);
$temp->add($setting);*/

$name = 'local_ecommerce/default_number_products_per_page';
$title = get_string('default_number_products_per_page', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, $description, 4);
$temp->add($setting);

$name = 'local_ecommerce/store_phone_number';
$title = get_string('store_phone_number', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, $description, null);
$temp->add($setting);

$name = 'local_ecommerce/opening_hours';
$title = get_string('opening_hours', 'local_ecommerce');
$description = get_string('opening_hours_desc', 'local_ecommerce');;
$setting = new admin_setting_configtextarea($name, $title, $description, null);
$temp->add($setting);


$ADMIN->add('local_ecommerce', $temp);
