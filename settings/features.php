<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings
 *
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

$temp = new admin_settingpage('local_ecommerce_features', get_string('features', 'local_ecommerce'));
//$temp->add(new admin_setting_heading('general', get_string('general', 'local_ecommerce'), ''));

// guestaccess
$name = 'local_ecommerce/guestaccess';
$title = get_string('enableguestaccess', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$temp->add($setting);

// coupons
$name = 'local_ecommerce/enablecoupons';
$title = get_string('enablecoupons', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$temp->add($setting);

// discounts
$name = 'local_ecommerce/enablediscounts';
$title = get_string('enablediscounts', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$temp->add($setting);

// reviews
$name = 'local_ecommerce/enable_reviews';
$title = get_string('enable_reviews', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$temp->add($setting);

$name = 'local_ecommerce/enable_review_changes';
$title = get_string('enable_review_changes', 'local_ecommerce');
$description = get_string('enable_review_changes_desc', 'local_ecommerce');
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$temp->add($setting);

// Units sold.
$name = 'local_ecommerce/display_sales';
$title = get_string('display_sales', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$temp->add($setting);

// Social media.
$name = 'local_ecommerce/enable_facebook_sharing';
$title = get_string('enable_facebook_sharing', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$temp->add($setting);

$name = 'local_ecommerce/enable_twitter_sharing';
$title = get_string('enable_twitter_sharing', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$temp->add($setting);

$name = 'local_ecommerce/enable_linkedin_sharing';
$title = get_string('enable_linkedin_sharing', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$temp->add($setting);

$name = 'local_ecommerce/facebook_link';
$title = get_string('facebook_link', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$name = 'local_ecommerce/twitter_link';
$title = get_string('twitter_link', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$name = 'local_ecommerce/linkedin_link';
$title = get_string('linkedin_link', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$ADMIN->add('local_ecommerce', $temp);
