<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings
 *
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

$temp = new admin_settingpage('local_ecommerce_notifications', get_string('notifications', 'local_ecommerce'));
//$temp->add(new admin_setting_heading('waitlist', get_string('waitlist', 'local_ecommerce'), ''));

// checkout message
$name = 'local_ecommerce/sendcheckoutnotification';
$title = get_string('sendcheckoutnotification', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$temp->add($setting);

$name = 'local_ecommerce/checkout_subject';
$title = get_string('checkout_subject', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$name = 'local_ecommerce/checkout_message';
$title = get_string('checkoutnotificationmessage', 'local_ecommerce');
$description = get_string('checkoutnotificationmessage_desc', 'local_ecommerce');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$temp->add($setting);

$name = 'local_ecommerce/enable_thank_you_email';
$title = get_string('enable_thank_you_email', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$temp->add($setting);

$name = 'local_ecommerce/thank_you_subject';
$title = get_string('thank_you_subject', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$name = 'local_ecommerce/thank_you_message';
$title = get_string('thank_you_message', 'local_ecommerce');
$description = get_string('thank_you_message_desc', 'local_ecommerce');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$temp->add($setting);

// invoice message
$name = 'local_ecommerce/invoice_notification_subject';
$title = get_string('invoice_notification_subject', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$name = 'local_ecommerce/invoice_message';
$title = get_string('invoicenotificationmessage', 'local_ecommerce');
$description = get_string('invoicenotificationmessage_desc', 'local_ecommerce');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$temp->add($setting);

// waitlist message
$name = 'local_ecommerce/waitlist_message';
$title = get_string('waitlistnotificationmessage', 'local_ecommerce');
$description = get_string('waitlistnotificationmessage_desc', 'local_ecommerce');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$temp->add($setting);

// invoice payment message
$name = 'local_ecommerce/invoicepayment_message';
$title = get_string('invoicepaymentnotificationmessage', 'local_ecommerce');
$description = get_string('invoicepaymentnotificationmessage_desc', 'local_ecommerce');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$temp->add($setting);

// Cart awaiting checkout message, because people are scum and they need to be irritated.
$name = 'local_ecommerce/enable_awaiting_checkout_emails';
$title = get_string('enable_awaiting_checkout_emails', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$temp->add($setting);

$name = 'local_ecommerce/awaiting_checkout_duration';
$title = get_string('awaiting_checkout_duration', 'local_ecommerce');
$description = '';
$temp->add(new admin_setting_configduration($name, $title, $description, 86400, 3600));

$name = 'local_ecommerce/cart_awaiting_checkout_subject';
$title = get_string('cart_awaiting_checkout_subject', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$name = 'local_ecommerce/cart_awaiting_checkout_message';
$title = get_string('cart_awaiting_checkout_message', 'local_ecommerce');
$description = get_string('cart_awaiting_checkout_message_desc', 'local_ecommerce');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$temp->add($setting);

$name = 'local_ecommerce/enable_awaiting_checkout_emails2';
$title = get_string('enable_awaiting_checkout_emails2', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$temp->add($setting);

$name = 'local_ecommerce/awaiting_checkout_duration2';
$title = get_string('awaiting_checkout_duration2', 'local_ecommerce');
$description = '';
$temp->add(new admin_setting_configduration($name, $title, $description, 86400, 3600));

$name = 'local_ecommerce/cart_awaiting_checkout_subject2';
$title = get_string('cart_awaiting_checkout_subject2', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$name = 'local_ecommerce/cart_awaiting_checkout_message2';
$title = get_string('cart_awaiting_checkout_message2', 'local_ecommerce');
$description = get_string('cart_awaiting_checkout_message_desc2', 'local_ecommerce');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$temp->add($setting);

$name = 'local_ecommerce/enable_awaiting_checkout_emails3';
$title = get_string('enable_awaiting_checkout_emails3', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, false, true, false);
$temp->add($setting);

$name = 'local_ecommerce/awaiting_checkout_duration3';
$title = get_string('awaiting_checkout_duration3', 'local_ecommerce');
$description = '';
$temp->add(new admin_setting_configduration($name, $title, $description, 86400, 3600));

$name = 'local_ecommerce/cart_awaiting_checkout_subject3';
$title = get_string('cart_awaiting_checkout_subject3', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$name = 'local_ecommerce/cart_awaiting_checkout_message3';
$title = get_string('cart_awaiting_checkout_message3', 'local_ecommerce');
$description = get_string('cart_awaiting_checkout_message_desc3', 'local_ecommerce');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$temp->add($setting);

$name = 'local_ecommerce/enable_contact_page';
$title = get_string('enable_contact_page', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$temp->add($setting);

$name = 'local_ecommerce/contact_us_email_text';
$title = get_string('contact_us_email_text', 'local_ecommerce');
$description = get_string('contact_us_email_text_desc', 'local_ecommerce');
$default = '';
$setting = new admin_setting_confightmleditor($name, $title, $description, $default);
$temp->add($setting);

$ADMIN->add('local_ecommerce', $temp);
