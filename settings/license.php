<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings
 *
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

$temp = new admin_settingpage('local_ecommerce_license', get_string('license', 'local_ecommerce'));
//$temp->add(new admin_setting_heading('waitlist', get_string('waitlist', 'local_ecommerce'), ''));

$name = 'local_ecommerce/license_email';
$title = get_string('license_email', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$name = 'local_ecommerce/license_apikey';
$title = get_string('license_apikey', 'local_ecommerce');
$description = '';
$setting = new admin_setting_configtext($name, $title, '', $description);
$temp->add($setting);

$ADMIN->add('local_ecommerce', $temp);
