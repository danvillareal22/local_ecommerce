<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2017 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/classes/output/forms/edit_product_form.php');

$id = optional_param('id', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_TEXT);
$confirm = optional_param('confirm', 0, PARAM_BOOL);
$move = optional_param('move', '', PARAM_TEXT);

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:editproduct', $context);

$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->set_url('/local/ecommerce/products/edit.php', array('id' => $id));

if ($id) {
    $product = $DB->get_record('local_ecommerce_products', array('id' => $id), '*', MUST_EXIST);
    $product->descriptionformat = 1;
    $linkedProducts = \local_ecommerce\store::get_linked_products($product->id);
    $product->linked_products = array_keys($linkedProducts);
} else {
    $product = new stdClass();
    $product->id = 0;
    $product->name = '';
    $product->description = '';
    $product->descriptionformat = 1;
    $linkedProducts = null;
}

$returnurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/products/index.php');

if ($action == 'delete' and $product->id) {
    $PAGE->url->param('action', 'delete');
    if ($confirm and confirm_sesskey()) {
        \local_ecommerce\product::delete_product($product->id);
        redirect($returnurl);
    }
    $strheading = get_string('deleteproduct', 'local_ecommerce');
    $PAGE->navbar->add(get_string('products', 'local_ecommerce'), new moodle_url('/local/ecommerce/products/index.php'));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    $renderer = $PAGE->get_renderer('local_ecommerce');
    $params = [
        'title' => $strheading
    ];

    $renderable = new \local_ecommerce\output\products_delete($params);

    echo $renderer->render($renderable);

    $yesurl = new moodle_url($CFG->wwwroot . '/local/ecommerce/products/edit.php',
            array('id' => $product->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey()));
    $message = get_string('deleteproductmsg', 'local_ecommerce', format_string($product->name));
    echo $OUTPUT->confirm($message, $yesurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}

if ($action == 'show' && $product->id && confirm_sesskey()) {
    if (!$product->visible) {
        $record = (object) array('id' => $product->id, 'visible' => 1);
        \local_ecommerce\product::save_product($record);
    }
    redirect($returnurl);

} else if ($action == 'hide' && $product->id && confirm_sesskey()) {

    if ($product->visible) {
        $record = (object) array('id' => $product->id, 'visible' => 0);
        \local_ecommerce\product::save_product($record);
    }
    redirect($returnurl);
}

$attachmentoptions = array('subdirs' => false, 'maxfiles' => 1, 'maxbytes' => $CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>context_system::instance(), 'accepted_types' => array('.jpg', '.gif', '.png'));
$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES,
        'maxbytes' => $SITE->maxbytes, 'context' => $context, 'descriptionformat' => 1);

if ($product->id) {
    // Edit existing.
    $product = file_prepare_standard_editor($product, 'description', $editoroptions,
            $context, 'local_ecommerce', 'description', $product->id);
    $product = file_prepare_standard_filemanager($product, 'productimage', $attachmentoptions, $context, 'local_ecommerce', 'productimage', $product->id);
    $strheading = get_string('editproduct', 'local_ecommerce');

    $product->tags = core_tag_tag::get_item_tags_array('local_ecommerce', 'local_ecommerce_products', $product->id);

} else {
    // Add new.
    $product = file_prepare_standard_editor($product, 'description', $editoroptions,
            $context, 'local_ecommerce', 'description', null);
    $product = file_prepare_standard_filemanager($product, 'productimage', $attachmentoptions, $context, 'local_ecommerce', 'productimage', null);
    $strheading = get_string('createproduct', 'local_ecommerce');
}

$editform = new edit_product_form(null, array('editoroptions' => $editoroptions, 'attachmentoptions' => $attachmentoptions, 'data' => $product));

if ($editform->is_cancelled()) {

    redirect($returnurl);

} else if ($data = $editform->get_data()) {

    $data->id = \local_ecommerce\product::save_product($data);

    if ($data->id) {

        // save tags
        if (isset($data->tags)) {
            core_tag_tag::set_item_tags('local_ecommerce', 'local_ecommerce_products', $data->id, $context, $data->tags);
        }

        $data = file_postupdate_standard_editor($data, 'description', $editoroptions,
                $context, 'local_ecommerce', 'description', $data->id);

        $data = file_postupdate_standard_filemanager($data, 'productimage', $attachmentoptions, $context, 'local_ecommerce', 'productimage', $data->id);

        \local_ecommerce\product::save_product($data);
    }

    redirect($returnurl);
}

$renderer = $PAGE->get_renderer('local_ecommerce');
$params = [
        'title' => $strheading,
        'formhtml' => $editform->export_for_template($renderer)
];
$renderable = new \local_ecommerce\output\products_edit($params);

$PAGE->navbar->add(get_string('dashboard', 'local_ecommerce'), new moodle_url('/local/ecommerce/index.php'));
$PAGE->navbar->add(get_string('products', 'local_ecommerce'), new moodle_url('/local/ecommerce/products/index.php'));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);

echo $OUTPUT->header();

echo $renderer->render($renderable);

$PAGE->requires->js_call_amd('local_ecommerce/store', 'init', array('linkedProducts' => $linkedProducts));

echo $OUTPUT->footer();

