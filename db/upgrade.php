<?php
// This file is part of the Local plans plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin sends users a plans message after logging in
 * and notify a moderator a new user has been added
 * it has a settings page that allow you to configure the messages
 * send.
 *
 * @package    local
 * @subpackage ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


function xmldb_local_ecommerce_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2018103001) {
        // Define table local_ecommerce to be created.
        $table = new xmldb_table('local_ecommerce');
        // Adding fields to table local_ecommerce.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, '0');
        $table->add_field('productid', XMLDB_TYPE_INTEGER, '10', null, null, null, '0');
        $table->add_field('timecreated', XMLDB_TYPE_CHAR, '10', null, null, null, 0);

        // Adding keys to table local_ecommerce.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_ecommerce.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2018103001) {
        // Define table local_ecommerce_checkout to be created.
        $table = new xmldb_table('local_ecommerce_checkout');

        // Adding fields to table local_ecommerce_checkout.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('item_name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, '0');
        $table->add_field('items', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('payment_status', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('pending_reason', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('reason_code', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('txn_id', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('amount', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('payment_type', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('timeupdated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table local_ecommerce_checkout.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_ecommerce_checkout.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2018103001) {
        // Define table local_ecommerce_coupons to be created.
        $table = new xmldb_table('local_ecommerce_coupons');

        // Adding fields to table local_ecommerce_coupons.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('code', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('starttime', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('endtime', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('expiration', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('usedperuser', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('usedcount', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('discount', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table local_ecommerce_checkout.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_ecommerce_checkout.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }


        // Define table local_ecommerce_coupons to be created.
        $table = new xmldb_table('local_ecommerce_discounts');

        // Adding fields to table local_ecommerce_discounts.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('starttime', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('endtime', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('expiration', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('discount', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table local_ecommerce_discounts.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_ecommerce_discounts.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2018103001) {
        // Define table local_ecommerce_logs to be created.
        $table = new xmldb_table('local_ecommerce_logs');

        // Adding fields to table local_ecommerce_logs.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('instanceid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('type', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('status', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table local_ecommerce_logs.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_ecommerce_logs.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_discounts');

        // Adding field type to local_ecommerce_discounts
        $field = new xmldb_field('type',
                XMLDB_TYPE_INTEGER,
                '1',
                null,
                null,
                null,
                '0');

        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Adding field type to local_ecommerce_discounts
        $field = new xmldb_field('minnumber',
                XMLDB_TYPE_INTEGER,
                '5',
                null,
                null,
                null,
                null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_discounts');

        // Adding field type to local_ecommerce_discounts
        $field = new xmldb_field('maxnumber',
                XMLDB_TYPE_INTEGER,
                '5',
                null,
                null,
                null,
                null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_products');

        // Adding field type to local_ecommerce_discounts
        $field = new xmldb_field('showitems',
                XMLDB_TYPE_INTEGER,
                '1',
                null,
                null,
                null,
                1);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        // update logs table
        $table = new xmldb_table('local_ecommerce_logs');

        // Adding field checkoutid to local_ecommerce_logs
        $field = new xmldb_field('checkoutid',
                XMLDB_TYPE_INTEGER,
                '10',
                null,
                null,
                null,
                '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Adding field price to local_ecommerce_logs
        $field = new xmldb_field('price',
                XMLDB_TYPE_CHAR,
                '255',
                null,
                null,
                null,
                '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Adding field discountprice to local_ecommerce_logs
        $field = new xmldb_field('discountprice',
                XMLDB_TYPE_CHAR,
                '255',
                null,
                null,
                null,
                '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Adding field discount to local_ecommerce_logs
        $field = new xmldb_field('discount',
                XMLDB_TYPE_CHAR,
                '255',
                null,
                null,
                null,
                '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }


        // update checkout table
        $table = new xmldb_table('local_ecommerce_checkout');

        // Adding field subtotal to local_ecommerce_checkout
        $field = new xmldb_field('subtotal',
                XMLDB_TYPE_CHAR,
                '255',
                null,
                null,
                null,
                '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Adding field discount to local_ecommerce_checkout
        $field = new xmldb_field('discount',
                XMLDB_TYPE_CHAR,
                '255',
                null,
                null,
                null,
                '');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        // Define table local_ecommerce_payments to be created.
        $table = new xmldb_table('local_ecommerce_payments');

        // Adding fields to table local_ecommerce_logs.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('settings', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('status', XMLDB_TYPE_INTEGER, '1', null, null, null, 1);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table local_ecommerce_logs.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_ecommerce_logs.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    // enrollment options
    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_products');

        $field = new xmldb_field('enrolperiod',
                XMLDB_TYPE_INTEGER,
                '10',
                null,
                null,
                null,
                0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('enrolexpirynotify',
                XMLDB_TYPE_INTEGER,
                '10',
                null,
                null,
                null,
                0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('enrolexpirythreshold',
                XMLDB_TYPE_INTEGER,
                '10',
                null,
                null,
                null,
                0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    // enrollment options
    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_checkout');

        $field = new xmldb_field('paymentid',
                XMLDB_TYPE_INTEGER,
                '10',
                null,
                null,
                null,
                0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    // enrollment options
    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_logs');

        $field = new xmldb_field('items',
                XMLDB_TYPE_CHAR,
                '255',
                null,
                null,
                null,
                null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('details',
                XMLDB_TYPE_TEXT,
                null,
                null,
                null,
                null,
                null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    // waitilist options
    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_products');

        $field = new xmldb_field('seats',
                XMLDB_TYPE_INTEGER,
                '10',
                null,
                null,
                null,
                0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        // Define table local_ecommerce_waitlist to be created.
        $table = new xmldb_table('local_ecommerce_waitlist');

        // Adding fields to table local_ecommerce_logs.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('productid', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);
        $table->add_field('seatkey', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '10', null, null, null, 0);

        // Adding keys to table local_ecommerce_logs.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_ecommerce_waitlist.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_products');

        $field = new xmldb_field('enableseats',
                XMLDB_TYPE_INTEGER,
                '1',
                null,
                null,
                null,
                0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_waitlist');

        $field = new xmldb_field('sent',
                XMLDB_TYPE_INTEGER,
                '1',
                null,
                null,
                null,
                0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        // update logs table
        $table = new xmldb_table('local_ecommerce_checkout');

        // Adding field invoicepayment to local_ecommerce_checkout
        $field = new xmldb_field('invoicepayment',
                XMLDB_TYPE_INTEGER,
                '1',
                null,
                null,
                null,
                '0');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_products');

        $field = new xmldb_field('physical',
            XMLDB_TYPE_INTEGER,
            '1',
            null,
            null,
            null,
            0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_products');

        $field = new xmldb_field('featured',
            XMLDB_TYPE_INTEGER,
            '1',
            null,
            null,
            null,
            0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        // Define table local_ecommerce to be created.
        $table = new xmldb_table('local_ecommerce_lkd_prods');
        // Adding fields to table local_ecommerce.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('productid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('linked_productid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);

        // Adding keys to table local_ecommerce.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_ecommerce.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2018103001) {
        // Define table local_ecommerce to be created.
        $table = new xmldb_table('local_ecommerce_wishlist');
        // Adding fields to table local_ecommerce.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('productid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);

        // Adding keys to table local_ecommerce.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_ecommerce.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2018103001) {
        // Define table local_ecommerce to be created.
        $table = new xmldb_table('local_ecommerce_ratings');
        // Adding fields to table local_ecommerce.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('productid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('rating', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);

        // Adding keys to table local_ecommerce.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_ecommerce.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_products');
        $field = new xmldb_field('associated_accreditation',
            XMLDB_TYPE_CHAR,
            '255',
            null,
            null,
            null,
            null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('accreditation_points',
            XMLDB_TYPE_CHAR,
            '255',
            null,
            null,
            null,
            null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_ratings');
        $field = new xmldb_field('review',
            XMLDB_TYPE_CHAR,
            '1023',
            null,
            null,
            null,
            null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_checkout');
        $field = new xmldb_field('sales_tax', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $table = new xmldb_table('local_ecommerce_logs');
        $field = new xmldb_field('sales_tax', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_products');
        $field = new xmldb_field('youtube', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        $table = new xmldb_table('local_ecommerce_checkout');
        $field = new xmldb_field('reminder_email',
            XMLDB_TYPE_INTEGER,
            '1',
            null,
            null,
            null,
            0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018103001) {
        // Define table local_ecommerce to be created.
        $table = new xmldb_table('local_ecommerce_slider');
        // Adding fields to table local_ecommerce.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('display_slider', XMLDB_TYPE_CHAR, '15', null, XMLDB_NOTNULL, null, 'both');
        $table->add_field('enable_slider', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('slider_caption_1', XMLDB_TYPE_CHAR, '127');
        $table->add_field('slider_url_1', XMLDB_TYPE_CHAR, '255');
        $table->add_field('slider_caption_2', XMLDB_TYPE_CHAR, '127');
        $table->add_field('slider_url_2', XMLDB_TYPE_CHAR, '255');
        $table->add_field('slider_caption_3', XMLDB_TYPE_CHAR, '127');
        $table->add_field('slider_url_3', XMLDB_TYPE_CHAR, '255');
        $table->add_field('slider_caption_4', XMLDB_TYPE_CHAR, '127');
        $table->add_field('slider_url_4', XMLDB_TYPE_CHAR, '255');
        $table->add_field('slider_caption_5', XMLDB_TYPE_CHAR, '127');
        $table->add_field('slider_url_5', XMLDB_TYPE_CHAR, '255');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, 0);

        // Adding keys to table local_ecommerce.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for local_ecommerce.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
    }

    if ($oldversion < 2018103100) {
        $table = new xmldb_table('local_ecommerce_ratings');
        $field = new xmldb_field('timeupdated',
            XMLDB_TYPE_INTEGER,
            '10',
            null,
            null,
            null,
            0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $table = new xmldb_table('local_ecommerce_slider');
        $field = new xmldb_field('timeupdated',
            XMLDB_TYPE_INTEGER,
            '10',
            null,
            null,
            null,
            0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018110200) {
        $table = new xmldb_table('local_ecommerce_coupons');
        $field = new xmldb_field('type',
            XMLDB_TYPE_INTEGER,
            '1',
            null,
            XMLDB_NOTNULL,
            null,
            2);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    if ($oldversion < 2018110201) {
        $table = new xmldb_table('local_ecommerce_checkout');
        $field = new xmldb_field('reminder_email2',
            XMLDB_TYPE_INTEGER,
            '1',
            null,
            null,
            null,
            0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $table = new xmldb_table('local_ecommerce_checkout');
        $field = new xmldb_field('reminder_email3',
            XMLDB_TYPE_INTEGER,
            '1',
            null,
            null,
            null,
            0);
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    return true;
}
