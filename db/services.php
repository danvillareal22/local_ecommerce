<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service mod_brprojects external functions and service definitions.
 *
 * @package    local_ecommerce
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// We defined the web service functions to install.

$functions = array(
    'local_ecommerce_get_assignable_products' => array(
            'classname'   => 'local_ecommerce_external',
            'methodname'  => 'get_assignable_products',
            'classpath'   => 'local/ecommerce/externallib.php',
            'description' => 'Get assignable products list',
            'type'        => 'read',
            'ajax'        => true
    ),
    'local_ecommerce_add_to_cart' => array(
            'classname'   => 'local_ecommerce_external',
            'methodname'  => 'add_to_cart',
            'classpath'   => 'local/ecommerce/externallib.php',
            'description' => 'Add product to the cart',
            'type'        => 'write',
            'ajax'        => true
    ),
    'local_ecommerce_add_to_waitlist' => array(
            'classname'   => 'local_ecommerce_external',
            'methodname'  => 'add_to_waitlist',
            'classpath'   => 'local/ecommerce/externallib.php',
            'description' => 'Add product to the wait list',
            'type'        => 'write',
            'ajax'        => true
    ),
    'local_ecommerce_get_ecommerce_products' => array(
            'classname' => 'local_ecommerce_external',
            'methodname' => 'get_ecommerce_products',
            'classpath' => 'local/ecommerce/externallib.php',
            'description' => 'Retrieve a list of products in ecommerce',
            'type' => 'read',
            'ajax' => true,
    ),
    'local_ecommerce_get_ecommerce_products_count' => array(
            'classname' => 'local_ecommerce_external',
            'methodname' => 'get_ecommerce_products_count',
            'classpath' => 'local/ecommerce/externallib.php',
            'description' => 'Retrieve the count of products in ecommerce',
            'type' => 'read',
            'ajax' => true,
    ),
    'local_ecommerce_delete_ecommerce_product' => array(
            'classname' => 'local_ecommerce_external',
            'methodname' => 'delete_ecommerce_product',
            'classpath' => 'local/ecommerce/externallib.php',
            'description' => 'Delete product from ecommerce',
            'type' => 'write',
            'ajax' => true,
    ),
    'local_ecommerce_approve_invoice' => array(
            'classname' => 'local_ecommerce_external',
            'methodname' => 'approve_invoice',
            'classpath' => 'local/ecommerce/externallib.php',
            'description' => 'Approve user invoice and enroll into products',
            'type' => 'write',
            'ajax' => true,
    ),
    'local_ecommerce_get_purchases_autocomplete' => array(
        'classname'   => 'local_ecommerce_external',
        'methodname'  => 'get_purchases_autocomplete',
        'classpath'   => 'local/ecommerce/externallib.php',
        'description' => 'Autocomplete for purchases search',
        'type'        => 'read',
        'ajax'        => true
    ),
    'local_ecommerce_get_store_autocomplete' => array(
        'classname'   => 'local_ecommerce_external',
        'methodname'  => 'get_store_autocomplete',
        'classpath'   => 'local/ecommerce/externallib.php',
        'description' => 'Autocomplete for store search',
        'type'        => 'read',
        'ajax'        => true,
        'loginrequired' => false
    ),
    'local_ecommerce_add_to_wishlist' => array(
        'classname'   => 'local_ecommerce_external',
        'methodname'  => 'add_to_wishlist',
        'classpath'   => 'local/ecommerce/externallib.php',
        'description' => 'Add wishlist item',
        'type'        => 'write',
        'ajax'        => true
    ),
    'local_ecommerce_get_wishlist' => array(
        'classname'   => 'local_ecommerce_external',
        'methodname'  => 'get_wishlist',
        'classpath'   => 'local/ecommerce/externallib.php',
        'description' => 'Wishlist product list',
        'type'        => 'read',
        'ajax'        => true
    ),
    'local_ecommerce_get_wishlist_products_count' => array(
        'classname'   => 'local_ecommerce_external',
        'methodname'  => 'get_wishlist_products_count',
        'classpath'   => 'local/ecommerce/externallib.php',
        'description' => 'Wishlist product count',
        'type'        => 'read',
        'ajax'        => true
    ),
    'local_ecommerce_delete_wishlist_item' => array(
        'classname'   => 'local_ecommerce_external',
        'methodname'  => 'delete_wishlist_item',
        'classpath'   => 'local/ecommerce/externallib.php',
        'description' => 'Delete wishlist item',
        'type'        => 'write',
        'ajax'        => true
    ),
    'local_ecommerce_rate_product' => array(
        'classname'   => 'local_ecommerce_external',
        'methodname'  => 'rate_product',
        'classpath'   => 'local/ecommerce/externallib.php',
        'description' => 'Rate product',
        'type'        => 'read',
        'ajax'        => true
    ),
    'local_ecommerce_load_more_reviews' => array(
        'classname'   => 'local_ecommerce_external',
        'methodname'  => 'load_more_reviews',
        'classpath'   => 'local/ecommerce/externallib.php',
        'description' => 'Load more reviews',
        'type'        => 'read',
        'ajax'        => true
    ),
    'local_ecommerce_load_more_products' => array(
        'classname'   => 'local_ecommerce_external',
        'methodname'  => 'load_more_products',
        'classpath'   => 'local/ecommerce/externallib.php',
        'description' => 'Load more products',
        'type'        => 'read',
        'ajax'        => true,
        'loginrequired' => false
    )
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.

$services = array(
        'local_ecommerce' => array(
                'functions' => array(
                        'local_ecommerce_get_assignable_products',
                        'local_ecommerce_add_to_cart',
                        'local_ecommerce_add_to_waitlist',
                        'local_ecommerce_get_ecommerce_products',
                        'local_ecommerce_get_ecommerce_products_count',
                        'local_ecommerce_delete_ecommerce_product',
                        'local_ecommerce_approve_invoice',
                        'local_ecommerce_get_purchases_autocomplete',
                        'local_ecommerce_get_store_autocomplete',
                        'local_ecommerce_add_to_wishlist',
                        'local_ecommerce_get_wishlist',
                        'local_ecommerce_get_wishlist_products_count',
                        'local_ecommerce_delete_wishlist_item',
                        'local_ecommerce_rate_product',
                        'local_ecommerce_load_more_reviews',
                        'local_ecommerce_load_more_products'
                ),
                'restrictedusers' => 0,
                'enabled'=>1
        )
);