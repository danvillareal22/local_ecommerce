<?php
// This file is part of the Local plans plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local
 * @subpackage ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

$tagareas = array(
    array(
        'itemtype' => 'local_ecommerce_products',
        'component' => 'local_ecommerce',
        'collection' => 'ecommerce',
        'searchable' => true,
        'customurl' => '/local/ecommerce/view.php',
        'callback' => 'local_ecommerce_get_tagged_products',
        'callbackfile' => '/local_ecommerce/lib.php',
        'showstandard' => core_tag_tag::HIDE_STANDARD,
    ),
);