<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eCommerce related management functions, this file needs to be included manually.
 *
 * @package    local_ecommerce
 * @copyright  2018
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require($CFG->dirroot . '/local/ecommerce/locallib.php');
require($CFG->dirroot . '/local/ecommerce/lib.php');

$id         = optional_param('id', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_TEXT);
$coupon     = optional_param('coupon', '', PARAM_TEXT);

require_login();
local_ecommerce_enable();

$context = context_system::instance();
require_capability('local/ecommerce:checkout', $context);

if ($id and $action == 'remove') {
    \local_ecommerce\checkout::delete_from_cart($id);
    redirect(new moodle_url('/local/ecommerce/index.php'), get_string('productremoved', 'local_ecommerce'));
} elseif ($coupon and $action == 'couponapply') {
    $result = \local_ecommerce\coupon::coupon_apply($coupon);
    redirect(new moodle_url('/local/ecommerce/index.php'), (($result) ? get_string('cannotapplycoupon', 'local_ecommerce', $result) : null));
} elseif ($id and $action == 'couponremove') {
    \local_ecommerce\coupon::coupon_remove($id);
    redirect(new moodle_url('/local/ecommerce/index.php'));
}

$title = get_string('ecommerce', 'local_ecommerce');
$PAGE->set_url('/local/ecommerce/index.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_context($context);

$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$renderer = $PAGE->get_renderer('local_ecommerce');

$products = \local_ecommerce\checkout::get_products_in_cart();
$productscontexts = [];
$ecommerceurl = new \moodle_url('/local/ecommerce/index.php');
$checkouturl = new \moodle_url('/local/ecommerce/checkout.php');
$currency = \local_ecommerce\payment::get_currency();

if ($products) {

    $coupons = \local_ecommerce\coupon::get_applied_coupons();
    $discounts = \local_ecommerce\checkout::get_products_discounts($products);

    $discountprices = \local_ecommerce\checkout::get_checkout_products_prices($products, $coupons, $discounts);
    $total = \local_ecommerce\checkout::get_cart_total($products, $discountprices);

    foreach ($products as $product) {

        $product->image = \local_ecommerce\product::get_product_image($product);
        $product->description = local_ecommerce\product::get_product_info($product);

        $producturl = new moodle_url('/local/ecommerce/view.php', array('id'=>$product->id));
        $product->producturl = $producturl->out();

        $removeurl = new moodle_url('/local/ecommerce/index.php?id=', array('id'=>$product->id, 'action'=>'remove'));
        $product->removeurl = $removeurl->out();

        $product->price = format_float($product->price, 2, false);
        $product->discountprice = (isset($discountprices[$product->id]['price']) and $discountprices[$product->id]['price'] !== null and format_float($product->price, 2, false) != format_float($discountprices[$product->id]['price'], 2, false)) ? format_float($discountprices[$product->id]['price'], 2, false) : false;
        $product->currency = $currency;

        $productscontexts[] = $product;
    }
}

$params = array(
    'title' => get_string('your_cart', 'local_ecommerce'),
    'products' => $productscontexts,
    'isproducts' => (count($productscontexts)),
    'productscount' => \local_ecommerce\api::count_ecommerce_products(),
    'subtotal' => (isset($total->subtotal)) ? $total->subtotal : 0,
    'discount' => (isset($total->discount)) ? $total->discount : 0,
    'total' => (isset($total->total)) ? $total->total : 0,
    'salestax' => (isset($total->salesTax)) ? $total->salesTax : null,
    'pretaxtotal' => (isset($total->preTaxTotal)) ? $total->preTaxTotal : null,
    'salestaxname' => (isset($total->salesTax)) ? get_config('local_ecommerce', 'sales_tax_name') . ' @ ' . $total->salesTaxPercentage . '%' : null,
    'ecommerceurl' => $ecommerceurl->out(),
    'checkouturl' => $checkouturl->out(),
    'isdiscounts' => (get_config('local_ecommerce', 'enablediscounts') and isset($discounts['used_discounts']) and count($discounts['used_discounts'])),
    'discounts' => (isset($discounts['used_discounts'])) ? $discounts['used_discounts'] : false,
    'iscoupons' => (get_config('local_ecommerce', 'enablecoupons') and isset($coupons) and count($coupons)),
    'enablediscounts' => (get_config('local_ecommerce', 'enablediscounts')),
    'enablecoupons' => (get_config('local_ecommerce', 'enablecoupons')),
    'coupons' => (isset($coupons)) ? array_values($coupons) : false,
    'currency' => $currency,
    'checkout_header' => $renderer->print_basic_header(get_string('checkout', 'local_ecommerce')),
    'display_shipping_policy' => get_config('local_ecommerce', 'enable_shipping_policy'),
    'shipping_policy_url' => new moodle_url('/local/ecommerce/shipping-policy.php')
);

//$renderable = new \local_ecommerce\output\ecommerce_index($params);

echo $OUTPUT->header();
echo $renderer->store_print_menu('view');
echo $renderer->render_ecommerce_index($params);
echo $renderer->store_print_checkout_footer(false);

$PAGE->requires->js_call_amd('local_ecommerce/ecommerce', 'init');
$PAGE->requires->js_call_amd('local_ecommerce/store', 'init');

echo $OUTPUT->footer();
