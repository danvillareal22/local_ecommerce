<?php
// This file is part of the Local plans plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin sends users a plans message after logging in
 * and notify a moderator a new user has been added
 * it has a settings page that allow you to configure the messages
 * send.
 *
 * @package    local
 * @subpackage ecommerce
 * @copyright  2017
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->locate('localplugins') and $ADMIN->locate('root')){

    $ADMIN->add('localplugins', new admin_category('local_ecommerce', get_string('pluginname', 'local_ecommerce')));

    if (has_capability('local/ecommerce:settings', \context_system::instance())) {
        include(dirname(__FILE__) . '/settings/general.php');
        include(dirname(__FILE__) . '/settings/features.php');
        include(dirname(__FILE__) . '/settings/invoices.php');
        include(dirname(__FILE__) . '/settings/waitlist.php');
        include(dirname(__FILE__) . '/settings/notifications.php');
        include(dirname(__FILE__) . '/settings/license.php');
        include(dirname(__FILE__) . '/settings/policies.php');
    }
}