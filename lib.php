<?php

function local_ecommerce_extend_navigation(global_navigation $nav){
    global $CFG, $DB, $USER, $PAGE;

    try {
        $mynode = $PAGE->navigation->find('myprofile', navigation_node::TYPE_ROOTNODE);
        $mynode->collapse = true;
        $mynode->make_inactive();

        $context = context_system::instance();
        if (isloggedin() and get_config('local_ecommerce', 'enabled') and has_capability('local/ecommerce:dashboardview', $context)) {
            $name = get_string('pluginname', 'local_ecommerce');
            $url = new moodle_url($CFG->wwwroot.'/local/ecommerce/dashboard/index.php');
            $nav->add($name, $url);
            $node = $mynode->add($name, $url, 0, null, 'local/ecommerce');
            $node->showinflatnavigation = true;
        }
    } catch (Exception $e) {}
}

function local_ecommerce_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG;
    require_once($CFG->dirroot . '/repository/lib.php');

    $itemid = array_shift($args);
    $filename = array_pop($args);
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }

    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'local_ecommerce', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }
    send_stored_file($file, 86400, 0, $forcedownload, $options);
}


function local_ecommerce_get_tagged_products($tag, $exclusivemode = false, $fromctx = 0, $ctx = 0, $rec = 1, $page = 0) {
    global $OUTPUT;
    // Find items.
    // Please refer to existing callbacks in core for examples.

    // ...

    // Use core_tag_index_builder to build and filter the list of items.
    // Notice how we search for 6 items when we need to display 5 - this way we will know that we need to display a link to the next page.
    $builder = new core_tag_index_builder('local_ecommerce', 'local_ecommerce_products', $query, $params, $page * $perpage, $perpage + 1);

    // ...

    $items = $builder->get_items();
    if (count($items) > $perpage) {
        $totalpages = $page + 2; // We don't need exact page count, just indicate that the next page exists.
        array_pop($items);
    }

    // Build the display contents.
    if ($items) {
        $tagfeed = new core_tag\output\tagfeed();
        foreach ($items as $item) {
            //$tagfeed->add(...);
        }

        $content = $OUTPUT->render_from_template('core_tag/tagfeed', $tagfeed->export_for_template($OUTPUT));

        return new core_tag\output\tagindex($tag, 'local_ecommerce', 'local_ecommerce_products', $content,
                $exclusivemode, $fromctx, $ctx, $rec, $page, $totalpages);
    }
}

function local_ecommerce_get_formatted_text($text, $context, $itemid, $type = 'description') {
    global $CFG;

    require_once($CFG->libdir. '/filelib.php');
    if (!$text) {
        return '';
    }
    $options = array('overflowdiv' => false, 'noclean' => false, 'para' => false);

    $text = file_rewrite_pluginfile_urls($text, 'pluginfile.php', $context->id, 'local_ecommerce', $type, $itemid);
    $text = format_text($text, 1, $options);

    return $text;
}


/**
 * Renders the popup.
 *
 * @param renderer_base $renderer
 * @return string The HTML
 */
function local_ecommerce_render_navbar_output(\renderer_base $renderer) {
    global $USER, $CFG;

    // Early bail out conditions.
    if (!isloggedin() || isguestuser() || user_not_fully_set_up($USER) ||
            get_user_preferences('auth_forcepasswordchange') ||
            ($CFG->sitepolicy && !$USER->policyagreed && !is_siteadmin())) {
        return '';
    }

    $output = '';

    // Add the notifications popover.
    $enabled = get_config('local_ecommerce', 'enabled');
    if ($enabled and has_capability('local/ecommerce:checkout', context_system::instance())) {
        $context = [
                'userid' => $USER->id,
                'urls' => [
                        'checkout' => (new moodle_url('/local/ecommerce/index.php'))->out(),
                ],
        ];
        $output .= $renderer->render_from_template('local_ecommerce/ecommerce_popover', $context);
        $wishlistContext = [
                'userid' => $USER->id,
                'urls' => [
                    'checkout' => (new moodle_url('/local/ecommerce/index.php'))->out(),
                ],
        ];
        $output .= $renderer->render_from_template('local_ecommerce/ecommerce-wishlist-popover', $wishlistContext);
    }
    return $output;
}